using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Orbb.Api.ApiModels;
using Orbb.Api.Controllers;
using Orbb.BL.Mappings;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using StructureMap;
using System.Collections.Generic;
using System.IO;

namespace Orbb.Api.Test.Controllers
{
    [TestClass]
    public class LensFittingControllerTests
    {
        private static LensFittingController _lensFittingController;
        private static Container _container;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
                cfg.AddProfile<ApiMapperProfile>();
            });

            // add framework services
            ServiceCollection services = new ServiceCollection();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            _container.Configure(c =>
            {
                // Populate the container using the service collection
                c.Populate(services);
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container, false);
            BL.Bootstrapper.RegisterWithContainer(_container);

            _lensFittingController = _container.GetInstance<LensFittingController>();
        }

        [TestMethod]
        public void PostFitting_Returns_OkResult()
        {
            EyeData data = LoadJsonFile("RegularEye360MER.json");

            // Act
            OkObjectResult okResult = _lensFittingController.PostFitting(data) as OkObjectResult;

            // Assert
            Assert.IsInstanceOfType(okResult.Value, typeof(List<LensFittingListResult>));
            var lensFittingResult = okResult.Value as List<LensFittingListResult>;
            Assert.AreEqual(14, lensFittingResult[0].Id);
        }

        [TestMethod]
        public void PostFitting_Without_LensType_Returns_BadResult()
        {
            EyeData data = new EyeData
            {
                EdgeLift = 100,
                ApicalClearance = 250,
                MidPeripheralClearance = 250,
                LenssetIds = new List<int>() { 5, 6 },
                Meridians = new List<Meridian>
                {
                    new Meridian
                    {
                        Angle = 0,
                        Rho = 3,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    },
                    new Meridian
                    {
                        Angle = 90,
                        Rho = 0.02288416796979257,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    }
                }
            };

            // Act
            BadRequestResult badResult = _lensFittingController.PostFitting(data) as BadRequestResult;

            Assert.AreEqual(badResult.StatusCode, 400);
        }

        [TestMethod]
        public void PostFitting_Without_Valid_Meridians_Returns_BadResult()
        {
            EyeData data = SetupEyeData();
            data.Meridians = new List<Meridian>
                {
                    new Meridian
                    {
                        Angle = 0,
                        Rho = 3,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    },
                    new Meridian
                    {
                        Angle = 91,
                        Rho = 0.02288416796979257,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    }
                };

            // Act
            BadRequestObjectResult badResult = _lensFittingController.PostFitting(data) as BadRequestObjectResult;

            Assert.AreEqual(badResult.StatusCode, 400);
        }

        [TestMethod]
        public void PostFitting_Without_ExistingLensId_Returns_ValidResult()
        {
            EyeData data = SetupEyeData();
            data.LenssetIds = new List<int> { 0 };

            // Act
            OkObjectResult okResult = _lensFittingController.PostFitting(data) as OkObjectResult;

            Assert.AreEqual(okResult.StatusCode, 200);
        }

        [TestMethod]
        public void PostFitting_Without_Data_Returns_BadResult()
        {
            // Act
            BadRequestObjectResult badResult = _lensFittingController.PostFitting(null) as BadRequestObjectResult;
            Assert.AreEqual(badResult.StatusCode, 400);
        }

        [TestMethod]
        public void PostFittingString_Returns_OkResult()
        {
            EyeData data = LoadJsonFile("RegularEye360MER.json");

            // Act
            OkObjectResult okResult = _lensFittingController.PostFittingString(data) as OkObjectResult;

            // Assert
            Assert.IsInstanceOfType(okResult.Value, typeof(List<LensFittingStringResult>));
            var lensFittingStringResult = okResult.Value as List<LensFittingStringResult>;
            Assert.AreEqual(14, lensFittingStringResult[0].Id);
        }

        [TestMethod]
        public void PostFittingString_Without_LensType_Returns_BadResult()
        {
            EyeData data = new EyeData
            {
                EdgeLift = 100,
                ApicalClearance = 250,
                MidPeripheralClearance = 250,
                LenssetIds = new List<int> { 5, 6 },
                Meridians = new List<Meridian>
                {
                    new Meridian
                    {
                        Angle = 0,
                        Rho = 3,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    },
                    new Meridian
                    {
                        Angle = 90,
                        Rho = 0.02288416796979257,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    }
                }
            };

            // Act
            BadRequestResult badResult = _lensFittingController.PostFittingString(data) as BadRequestResult;

            Assert.AreEqual(badResult.StatusCode, 400);
        }

        [TestMethod]
        public void PostFittingString_Without_Valid_Meridians_Returns_BadResult()
        {
            EyeData data = SetupEyeData();
            data.Meridians = new List<Meridian>
                {
                    new Meridian
                    {
                        Angle = 0,
                        Rho = 3,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    },
                    new Meridian
                    {
                        Angle = 91,
                        Rho = 0.02288416796979257,
                        Apex = 3,
                        Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                    }
                };

            // Act
            BadRequestObjectResult badResult = _lensFittingController.PostFittingString(data) as BadRequestObjectResult;

            Assert.AreEqual(badResult.StatusCode, 400);
        }

        [TestMethod]
        public void PostFittingString_Without_ExistingLensId_Returns_ValidResult()
        {
            EyeData data = SetupEyeData();
            data.LenssetIds = new List<int> { 0 };

            // Act
            OkObjectResult okResult = _lensFittingController.PostFittingString(data) as OkObjectResult;

            Assert.AreEqual(okResult.StatusCode, 200);
        }

        [TestMethod]
        public void PostFittingString_Without_Data_Returns_BadResult()
        {
            // Act
            BadRequestObjectResult badResult = _lensFittingController.PostFittingString(null) as BadRequestObjectResult;
            Assert.AreEqual(badResult.StatusCode, 400);
        }

        private static EyeData SetupEyeData()
        {
            EyeData result = new EyeData
            {
                LensType = 1,
                EdgeLift = 100,
                ApicalClearance = 250,
                MidPeripheralClearance = 250,
                LenssetIds = new List<int>() { 5, 6 },
                Meridians = new List<Meridian>()
            };

            for (int i = 0; i < 180; i++)
            {
                result.Meridians.Add(new Meridian
                {
                    Angle = i,
                    Rho = 0.02288416796979257,
                    Apex = 5,
                    Points = new List<double> { -8, -4, -2, 0, -1, -3, -7 }
                });
            }

            return result;
        }

        private static EyeData LoadJsonFile(string name)
        {
            string jsonFile = File.ReadAllText(@"Data/" + name);
            return JsonConvert.DeserializeObject<EyeData>(jsonFile);
        }
    }
}

