using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.Api.Controllers;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.BL.Mappings;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.Models.Models.Lenssets;
using StructureMap;
using System.Collections.Generic;

namespace Orbb.Api.Test.Controllers
{
    [TestClass]
    public class LensControllerTests
    {
        private static LensController _lensController;
        private static Container _container;
        private static ILensRepository _lensRepository;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            // add framework services
            ServiceCollection services = new ServiceCollection();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            _container.Configure(c =>
            {
                // Populate the container using the service collection
                c.Populate(services);
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container, false);
            BL.Bootstrapper.RegisterWithContainer(_container);

            _lensController = _container.GetInstance<LensController>();
            _lensRepository = _container.GetInstance<ILensRepository>();
        }


        [TestMethod]
        public void GetLenssets_Returns_OkResult()
        {
            // Arrange
            RequestFilterContent filter = new RequestFilterContent { Filter = "" };
            // Act
            OkObjectResult okResult = _lensController.GetLenssets(filter) as OkObjectResult;

            // Assert
            Assert.AreEqual(okResult.StatusCode, 200);
        }

        [TestMethod]
        public void GetLenssets_Returns_CorrectNumberUnfiltered()
        {
            // Arrange
            RequestFilterContent filter = new RequestFilterContent { Filter = "" };
            List<LensDetail> actual = _lensRepository.GetAll(l => l.IsDraft == false, new string[] { });
            int expectedValue = actual.Count;
            // Act
            OkObjectResult okResult = _lensController.GetLenssets(filter) as OkObjectResult;
            List<LensInfo> lensFittingResult = okResult.Value as List<LensInfo>;

            // Assert
            Assert.AreEqual(expectedValue, lensFittingResult.Count);
        }

        [TestMethod]
        public void GetLenssets_Returns_CorrectNumberFiltered()
        {
            // Arrange
            RequestFilterContent filter = new RequestFilterContent { Filter = "zen" };
            List<LensDetail> actual = _lensRepository.GetAll(x => x.Name.ToLower().Contains(filter.Filter.ToLower()) && x.IsDraft == false, null);
            int expectedValue = actual.Count;
            // Act
            OkObjectResult okResult = _lensController.GetLenssets(filter) as OkObjectResult;
            List<LensInfo> lensFittingResult = okResult.Value as List<LensInfo>;

            // Assert
            Assert.AreEqual(expectedValue, lensFittingResult.Count);
        }

        [TestMethod]
        public void GetLenssets_Returns_FilterDoesNotExists()
        {
            // Arrange
            RequestFilterContent filter = new RequestFilterContent { Filter = "fake" };
            // Act
            OkObjectResult okResult = _lensController.GetLenssets(filter) as OkObjectResult;
            List<LensInfo> lensFittingResult = okResult.Value as List<LensInfo>;

            // Assert
            Assert.AreEqual(0, lensFittingResult.Count);
        }

        [TestMethod]
        public void GetLenssets_Returns_NoFilterInRequest()
        {
            // Arrange
            RequestFilterContent filter = new RequestFilterContent();
            List<LensDetail> actual = _lensRepository.GetAll(l => l.IsDraft == false, new string[] { });
            int expectedValue = actual.Count;
            // Act
            OkObjectResult okResult = _lensController.GetLenssets(filter) as OkObjectResult;
            List<LensInfo> lensFittingResult = okResult.Value as List<LensInfo>;

            // Assert
            Assert.AreEqual(expectedValue, lensFittingResult.Count);
        }
    }
}

