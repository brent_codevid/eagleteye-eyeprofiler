﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.Api.ApiModels;
using System.Collections.Generic;

namespace Orbb.Api.Test.ApiModels
{
    [TestClass]
    public class EyeDataTest
    {
        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenDuplicateAngles()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 90},
                    new Meridian {Angle = 0}
                }
            };

            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 91 },
                new Meridian{ Angle = 3 },
                new Meridian{ Angle = 93 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 92 },
                new Meridian{ Angle = 7 },
                new Meridian{ Angle = 97 },
                new Meridian{ Angle = 5 },
                new Meridian{ Angle = 95 },
                new Meridian{ Angle = 2 }
            };

            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenOppositeAngles()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 180}
                }
            };

            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 183 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 187 },
                new Meridian{ Angle = 5 },
                new Meridian{ Angle = 183 }
            };

            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenMissingPerpendicular()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0}
                }
            };
            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 91 },
                new Meridian{ Angle = 3 },
                new Meridian{ Angle = 93 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 92 },
                new Meridian{ Angle = 7 },
                new Meridian{ Angle = 97 },
                new Meridian{ Angle = 5 }
            };
            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsTrueWithValidMeridians()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 90}
                }
            };
            Assert.IsTrue(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 0 },
                new Meridian{ Angle = 45 },
                new Meridian{ Angle = 315 },
                new Meridian{ Angle = 181 },
                new Meridian{ Angle = 271 },
                new Meridian{ Angle = 90 }
            };
            Assert.IsTrue(ed.ValidMeridians());
        }
    }
}
