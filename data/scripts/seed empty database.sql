SET IDENTITY_INSERT Globalization.Languages ON
go
INSERT INTO Globalization.Languages ( [Id] , [Code] , [ShortCode] , [EnglishDescription]  )
VALUES
( 1, N'en-US', N'EN', N'English' ), 
( 2, N'nl-BE', N'NL', N'Dutch' ), 
( 3, N'fr-FR', N'FR', N'French' ), 
( 4, N'de-DE', N'DE', N'German' )
GO
SET IDENTITY_INSERT Globalization.Languages OFF
GO

SET IDENTITY_INSERT Globalization.LanguageTranslations ON
go
INSERT INTO Globalization.LanguageTranslations
        ( Id, ShortCode ,
          value,
          LanguageId)
VALUES
( 1, N'nl-BE', N'Nederlands', 2 ), 
( 2, N'fr-FR', N'N�erlandais', 2 ), 
( 3, N'en-US', N'Dutch', 2 ), 
( 4, N'de-DE', N'Niederl�ndisch', 2 ), 
( 5, N'nl-BE', N'Engels', 1 ), 
( 6, N'fr-FR', N'Anglais', 1 ), 
( 7, N'en-US', N'English', 1 ), 
( 8, N'de-DE', N'�ngl�sisch', 1 ), 
( 9, N'nl-BE', N'Frans', 3 ), 
( 10, N'fr-FR', N'Fran�ais', 3 ), 
( 11, N'en-US', N'French', 3 ), 
( 12, N'de-DE', N'Franz�sich', 3 ), 
( 13, N'nl-BE', N'Duits', 4 ), 
( 14, N'fr-FR', N'Allemand', 4 ), 
( 15, N'en-US', N'German', 4 ), 
( 16, N'de-DE', N'Deutsch', 4 )
GO
SET IDENTITY_INSERT Globalization.LanguageTranslations OFF
GO

SET IDENTITY_INSERT Security.UserGroups ON
INSERT INTO Security.UserGroups
        (Id, Description)
VALUES  ( 1, N'Admin')
GO

SET IDENTITY_INSERT Security.UserGroups OFF

SET IDENTITY_INSERT Security.UserTypes ON

DECLARE	@return_value_rev int, @return_value_test int

EXEC	@return_value_rev = [dbo].[spAddTranslationEntry]
		@value_EN = N'reviewer',
		@value_NL = N'reviewer',
		@value_FR = N'reviewer',
		@isAddDefaultToLower = 1

SELECT	'Return Value' = @return_value_rev

INSERT INTO Security.UserTypes
        (Id, DescriptionId)
VALUES  (1, @return_value_rev)


EXEC	@return_value_test = [dbo].[spAddTranslationEntry]
		@value_EN = N'tester',
		@value_NL = N'tester',
		@value_FR = N'tester',
		@isAddDefaultToLower = 1

SELECT	'Return Value' = @return_value_test

INSERT INTO Security.UserTypes
        ( Id,DescriptionId)
VALUES  (2, @return_value_test)

SET IDENTITY_INSERT Security.UserTypes OFF

INSERT INTO Security.Users
        ( UserName ,
          FirstName ,
          LanguageId ,
          Password ,
		  Salt,
          UserGroupId,
		  Gender,
		  TypeId
          )
VALUES  ( N'admin' , -- UserName - nvarchar(60)
          N'admin' , -- FirstName - nvarchar(30)
          1 , -- LanguageId - int
          N'A924D94C6F175D75FB0888511EB290D1FC4C101FE44200F0D51CF477F55169C8' , -- Password == 'admin'- nvarchar(max)
		  N'mTSz3/QI5SO5ebi8IkrqpQ==',
          1,
		  1,
		  1
        )
		GO

INSERT INTO System.Divisions
        ( FaxNumber,
          Description ,
          ShortCode , 
		  IsDefault
        )
VALUES  ('','Default', 'DEF', 1 )        
GO

DECLARE	@return_value int

EXEC @return_value = dbo.spAddMenuItem @parentMenuItemId = null, -- int
    @value_EN = N'Security', -- nvarchar(max)
    @value_NL = N'Beveiliging', -- nvarchar(max)
    @value_FR = N'S�curit�', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

--SELECT	'Return Value' = @return_value

EXEC dbo.spAddMenuItem @parentMenuItemId = @return_value, -- int
    @value_EN = N'Users', -- nvarchar(max)
    @value_NL = N'Gebruikers', -- nvarchar(max)
    @value_FR = N'Utilisateurs', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC dbo.spAddMenuItem @parentMenuItemId = @return_value, -- int
    @value_EN = N'Usergroups', -- nvarchar(max)
    @value_NL = N'Gebruikersgroepen', -- nvarchar(max)
    @value_FR = N'Groupes des utilisateurs', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC @return_value = dbo.spAddMenuItem @parentMenuItemId = null, -- int
    @value_EN = N'Settings', -- nvarchar(max)
    @value_NL = N'Instellingen', -- nvarchar(max)
    @value_FR = N'Configuration', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC dbo.spAddMenuItem @parentMenuItemId = @return_value, -- int
    @value_EN = N'Lens types', -- nvarchar(max)
    @value_NL = N'Lenstypes', -- nvarchar(max)
    @value_FR = N'Types de lentilles', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC dbo.spAddMenuItem @parentMenuItemId = @return_value, -- int
    @value_EN = N'Lens Categories', -- nvarchar(max)
    @value_NL = N'Lenscategorie�n', -- nvarchar(max)
    @value_FR = N'Cat�gories de lentilles', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC dbo.spAddMenuItem @parentMenuItemId = null, -- int
    @value_EN = N'Lens dataset', -- nvarchar(max)
    @value_NL = N'Lens dataset', -- nvarchar(max)
    @value_FR = N'Donn�es des lentilles', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int

EXEC dbo.spAddMenuItem @parentMenuItemId = null, -- int
    @value_EN = N'Lens supplier', -- nvarchar(max)
    @value_NL = N'Lens leverancier', -- nvarchar(max)
    @value_FR = N'Fournisseur de lentilles', -- nvarchar(max)
    @isAddPermissions = 1, -- bit
    @userGroupId = 1 -- int
GO

