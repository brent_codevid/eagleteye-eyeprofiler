﻿using System;
using System.Collections.Generic;

namespace Orbb.Data.Common
{
    public class SaveResult
    {
        public SaveResult(bool success)
        {
            Success = success;
        }
        public SaveResult(bool success, int affectedRows, Exception saveException = null, String log = "", IEnumerable<string> entityValidationErrors = null)
        {
            Success = success;
            AffectedRows = affectedRows;
            SaveException = saveException;
            Log = log;
            EntityValidationErrors = entityValidationErrors;
        }

        public bool Success { get; }
        public string ReturnData { get; set; }
        public int EntityId { get; set; }
        public int AffectedRows { get; }
        public Exception SaveException { get; }
        public IEnumerable<string> EntityValidationErrors { get; }
        public String Log { get; }
    }
}
