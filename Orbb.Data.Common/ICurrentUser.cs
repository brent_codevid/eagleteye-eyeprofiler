﻿using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Data.Common
{
    public interface ICurrentUser
    {
        UserVm GetCurrentUser();
        void SetCurrentUser(UserVm user);
        void Remove();
    }
}
