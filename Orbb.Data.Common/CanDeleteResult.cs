﻿namespace Orbb.Data.Common
{
    public class CanDeleteResult
    {
        public CanDeleteResult(bool canDelete, string[] reasons)
        {
            CanDelete = canDelete;
            Reasons = reasons;
        }

        public bool CanDelete { get; set; }
        public string[] Reasons { get; set; }
    }
}
