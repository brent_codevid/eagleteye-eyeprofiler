﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Orbb.Data.Common.Extensions
{
    public static class SaveResultExtensions
    {
        /// <summary>
        /// Log errors from save result and compose combined error string 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="result">The current SaveResult.</param>
        /// <param name="logger">The logger.</param>
        /// <param name="errors">Out param: combined error string .</param>
        /// <returns></returns>
        public static void HandleErrors<T>(this SaveResult result, ILogger<T> logger, out string errors)
        {
            errors = string.Empty;

            if (result == null || result.Success)
                return;

            var errorList = new List<string>();

            if (result.SaveException != null)
            {
                logger.LogError(result.SaveException, result.SaveException.Message);
                errorList.Add(result.SaveException.Message);
            }


            if (result.EntityValidationErrors != null)
            {
                foreach (var error in result.EntityValidationErrors)
                {
                    logger.LogError($"validation error {error}");
                    errorList.Add(error);
                }
            }

            errors = string.Join(", ", errorList);
        }
    }
}

