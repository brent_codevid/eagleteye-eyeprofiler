﻿using StructureMap;

namespace Orbb.Data.Common
{
    public static class Bootstrapper
    {
        private static IContainer container;
        public static void RegisterWithContainer(IContainer container)
        {
            // When you receive the error that IDisposable is not defined => Include reference to %ProgramFiles(x86)%\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\Facades\System.Runtime.dll
            container.Configure(c =>
            {
                c.For<ICurrentUser>().Use<CurrentUser>();
                c.For(typeof(ICurrent<>)).Use(typeof(Current<>));
            });

            Bootstrapper.container = container;

        }
    }
}
