﻿using AutoMapper;

namespace Orbb.Data.Common
{
    public class Current<T> : ICurrent<T> where T : class
    {
        private readonly ICurrentStorage<T> storage;
        private readonly IMapper mapper;

        public Current(ICurrentStorage<T> storage, IMapper mapper)
        {
            this.storage = storage;
            this.mapper = mapper;
        }
        public T GetCurrent()
        {
            return storage.Object;
        }

        public void SetCurrent(T value)
        {
            T u = CleanModel(value);
            storage.Object = u;
        }

        public void Remove()
        {
            storage.Remove();
        }

        protected virtual T CleanModel(T value)
        {
            // var newValue = Activator.CreateInstance<T>();
            var newValue = mapper.Map<T>(value);
            return newValue;
        }
    }
}
