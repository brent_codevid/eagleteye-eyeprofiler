﻿namespace Orbb.Data.Common
{
    public interface ICurrent<T> where T : class
    {
        T GetCurrent();
        void SetCurrent(T data);
        void Remove();
    }
}
