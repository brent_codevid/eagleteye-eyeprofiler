﻿namespace Orbb.Data.Common
{
    public static class Enumerations
    {
        public enum Languages
        {
            English = 1,
            Dutch = 2,
            French = 3
        }

        public enum EntityStatuses
        {
            Active = 1,
            Archived = 2,
            Deleted = 13
        }

        public enum AlertMessageTypes
        {
            Info = 1,
            Warning = 3,
            Error = 2

        }

        public enum ReturnCodes
        {
            OK = 1,
            NOT_FOUND = 4,
            CONFLICT = 9,
            ERROR = 98,
            SYSTEM_ERROR = 99
        }

        public enum FixedLanguages
        {
            EN,
            FR,
            NL
        }
        public enum UserTypes
        {
            Planner = 1,
            Technician = 2
        }

        public enum MediaTypes
        {
            Image = 1,
            Video = 2
        }

        public enum MenuTypes
        {
            Default = 1
        }

        public enum ImageTypes
        {
            HomePage = 1,
            Club = 2,
            Thumb = 3,
            Cover = 4,
            Logo = 5
        }
        public enum EventType
        {
            Standard
        }
        public enum LinkedType
        {
            Event,
            News
        }

        public enum PermissionType
        {
            Read = 1,
            Add = 2,
            Update = 3,
            Delete = 4
        }
    }
}