﻿using AutoMapper;
using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Data.Common
{
    public class CurrentUser : Current<UserVm>, ICurrentUser
    {
        public CurrentUser(ICurrentStorage<UserVm> storage, IMapper mapper) : base(storage, mapper) { }

        public UserVm GetCurrentUser()
        {
            return GetCurrent();
        }

        public void SetCurrentUser(UserVm user)
        {
            SetCurrent(user);
        }

        protected override UserVm CleanModel(UserVm value)
        {
            var res = base.CleanModel(value);
            /*if (res != null)
            {
                res.UserGroup = null;
                res.Divisions = null;
                res.DefaultDivision = null;
            }*/
            return res;
        }
    }
}
