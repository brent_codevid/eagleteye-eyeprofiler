﻿namespace Orbb.Data.Common
{
    public class Parameter
    {
        public enum Types
        {
            String,
            Date,
            Integer,
            Boolean,
            Decimal
        };

        public string Name { get; set; }
        public Types Type { get; set; }
        public object Value { get; set; }
    }
}
