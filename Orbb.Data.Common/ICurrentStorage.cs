﻿namespace Orbb.Data.Common
{
    public interface ICurrentStorage<T> where T : class
    {
        T Object
        {
            get;
            set;
        }

        void Remove();
    }
}