﻿namespace Orbb.Data.Common.Tools
{
    public class SimpleCurrent<T> : ICurrent<T> where T : class
    {
        private static T data;
        public T GetCurrent()
        {
            return data;
        }

        public void SetCurrent(T data)
        {
            SimpleCurrent<T>.data = data;
        }

        public void Remove()
        {

        }
    }
}
