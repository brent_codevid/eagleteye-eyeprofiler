﻿using Orbb.Data.ViewModels.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;

namespace Orbb.Data.Common.Tools
{
    public class ViewModelDifference
    {
        public enum EntityStates
        {
            Added,
            Updated,
            Deleted
        }

        public IViewModel NewVersion { get; }
        public IViewModel OldVersion { get; }
        public string Path { get; }
        public EntityStates EntityState { get; }

        private readonly List<PropertyInfo> properties;
        public ReadOnlyCollection<PropertyInfo> Properties => properties.AsReadOnly();

        private ViewModelDifference(IViewModel newVersion, IViewModel oldVersion, string path, EntityStates entityState)
        {
            NewVersion = newVersion;
            OldVersion = oldVersion;
            Path = path;
            EntityState = entityState;
            properties = new List<PropertyInfo>();
        }

        public void AddProperty(PropertyInfo pi)
        {
            properties.Add(pi);
        }

        public void AddProperties(IEnumerable<PropertyInfo> pi)
        {
            foreach (var propertyInfo in pi)
                properties.Add(propertyInfo);
        }

        public static ViewModelDifference New<TViewModel>(TViewModel newVersion, TViewModel oldVersion, string path, EntityStates entityState)
            where TViewModel : class, IViewModel
        {
            return new ViewModelDifference(newVersion, oldVersion, path, entityState);
        }
    }
}
