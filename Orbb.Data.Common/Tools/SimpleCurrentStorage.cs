﻿namespace Orbb.Data.Common.Tools
{
    public class SimpleCurrentStorage<T> : ICurrentStorage<T> where T : class
    {
        private static T data;

        public T Object
        {
            get => data;
            set => data = value;
        }

        public void Remove()
        {
        }
    }
}
