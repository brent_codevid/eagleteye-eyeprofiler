﻿using Orbb.Data.ViewModels.Attributes;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.System;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Orbb.Data.Common.Tools
{
    public class ViewModelComparer
    {
        private readonly List<ViewModelDifference> _differences = new List<ViewModelDifference>();

        private ICurrent<DivisionVM> CurrentDivision { get; }

        public ViewModelComparer(ICurrent<DivisionVM> currentDivision)
        {
            CurrentDivision = currentDivision;
        }

        public List<ViewModelDifference> GetDifferences<TViewModel>(TViewModel oldVersion, TViewModel newVersion)
            where TViewModel : class, IViewModel
        {
            _differences.Clear();
            var rootDifference = ViewModelDifference.New(newVersion, oldVersion, "", newVersion.Id <= 0 ? ViewModelDifference.EntityStates.Added : ViewModelDifference.EntityStates.Updated);
            if (oldVersion != null)
                GetDifferenceForUpdate(oldVersion, newVersion, rootDifference);
            else
                GetDifferenceForAdd(newVersion, rootDifference);
            return new List<ViewModelDifference>(_differences);
        }

        private void GetDifferenceForAdd<TViewModel>(TViewModel newVersion, ViewModelDifference difference)
            where TViewModel : class, IViewModel
        {
            difference.AddProperties(GetScalarProperties(newVersion));
            var references = GetReferenceProperties(newVersion);
            difference.AddProperties(references);
            foreach (var reference in references)
            {
                var value = reference.GetValue(newVersion, null) as IViewModel;
                if (value == null) continue;
                if (value.Id > 0) continue;
                var path = String.Join(".", new[] { difference.Path, reference.Name }.Where(x => !String.IsNullOrWhiteSpace(x)));
                var addDifference = ViewModelDifference.New(value, null, path, ViewModelDifference.EntityStates.Added);
                GetDifferenceForAdd(value, addDifference);
            }

            var collections = GetCollectionProperties(newVersion);
            difference.AddProperties(collections);
            foreach (var collection in collections)
            {
                var value = ((ICollection)collection.GetValue(newVersion, null))?.Cast<IViewModel>()?.ToList();
                if (value == null)
                    continue;

                foreach (var add in value.Where(x => x.Id <= 0))
                {
                    var addDifference = ViewModelDifference.New(add, null, GetNewCollectionPropertyPath(collection, add.Id, difference.Path), ViewModelDifference.EntityStates.Added);
                    GetDifferenceForAdd(add, addDifference);
                }
            }

            _differences.Add(difference);
        }

        private void GetDifferenceForUpdate<TViewModel>(TViewModel oldVersion, TViewModel newVersion, ViewModelDifference difference)
            where TViewModel : class, IViewModel
        {
            foreach (var scalar in GetScalarProperties(newVersion))
            {
                var oldValue = scalar.GetValue(oldVersion, null);
                var newValue = scalar.GetValue(newVersion, null);
                if (!IsPropertyChanged(oldValue, newValue)) continue;

                difference.AddProperty(scalar);
            }
            foreach (var reference in GetReferenceProperties(newVersion))
            {
                var oldValue = reference.GetValue(oldVersion, null) as IViewModel;
                var newValue = reference.GetValue(newVersion, null) as IViewModel;
                if (oldValue == null && newValue == null) continue;
                if (oldValue == null || newValue == null || oldValue.Id != newValue.Id)
                    difference.AddProperty(reference);
                if (newValue != null && newValue.Id <= 0)
                {
                    var path = String.Join(".", new[] { difference.Path, reference.Name }.Where(x => !String.IsNullOrWhiteSpace(x)));
                    var addDifference = ViewModelDifference.New(newValue, oldValue, path, ViewModelDifference.EntityStates.Added);
                    GetDifferenceForAdd(newValue, addDifference);
                }
                if (newValue != null && oldValue != null && newValue.Id == oldValue.Id)
                {
                    var path = String.Join(".", new[] { difference.Path, reference.Name }.Where(x => !String.IsNullOrWhiteSpace(x)));
                    var updateDifference = ViewModelDifference.New(newValue, oldValue, path,
                        ViewModelDifference.EntityStates.Updated);
                    GetDifferenceForUpdate(oldValue, newValue, updateDifference);
                }
            }
            foreach (var collection in GetCollectionProperties(newVersion))
            {
                var oldValue = ((ICollection)collection.GetValue(oldVersion, null)).Cast<IViewModel>()?.ToList();
                var newValue = ((ICollection)collection.GetValue(newVersion, null))?.Cast<IViewModel>()?.ToList();

                //if (newValue == null)
                //    newValue = new List<IViewModel>();
                if (newValue == null)
                    continue;

                if (oldValue.Count != 0 || newValue.Count != 0)
                {
                    var first = (oldValue.Count > 0) ? oldValue[0] : newValue[0];
                    int currentDivisionId = CurrentDivision.GetCurrent().Id;
                    var attr = first.GetType().GetCustomAttributes<FilterByDivisionOnSave>().FirstOrDefault();

                    if (attr != null)
                    {
                        newValue = newValue.Where(v => ((IHasDivision)v).DivisionId == currentDivisionId || ((IHasDivision)v).DivisionId == 0).ToList();
                        oldValue = oldValue.Where(v => ((IHasDivision)v).DivisionId == currentDivisionId).ToList();
                    }
                }
                var collectionAdds = newValue.Where(x => oldValue.All(y => y.Id != x.Id)).ToList();
                var collectionRemoves = oldValue.Where(x => newValue.All(y => y.Id != x.Id)).ToList();
                var collectionUpdates = newValue.Except(collectionAdds).ToList();

                foreach (var update in collectionUpdates)
                {
                    var oldUpdate = oldValue.Single(x => x.Id == update.Id && x.GetType() == update.GetType());
                    var updateDifference = ViewModelDifference.New(update, oldUpdate, GetNewCollectionPropertyPath(collection, update.Id, difference.Path), ViewModelDifference.EntityStates.Updated);
                    GetDifferenceForUpdate(oldValue.Single(x => x.Id == update.Id), update, updateDifference);
                }

                if (collectionAdds.Any() || collectionRemoves.Any())
                {
                    difference.AddProperty(collection);

                    foreach (var add in collectionAdds.Where(x => x.Id <= 0))
                    {
                        var entityState = ViewModelDifference.EntityStates.Added;
                        var addDifference = ViewModelDifference.New(add, null, GetNewCollectionPropertyPath(collection, add.Id, difference.Path), entityState);
                        GetDifferenceForAdd(add, addDifference);
                    }
                }
            }

            if (difference.Properties.Any())
                _differences.Add(difference);
        }

        private static bool IsPropertyChanged(object oldValue, object newValue)
        {
            return !(ReferenceEquals(oldValue, newValue) || oldValue is ValueType && newValue is ValueType && Equals(oldValue, newValue) || oldValue is string && newValue is string && Equals(oldValue, newValue));
        }

        private static string GetNewPropertyPath(PropertyInfo pi, string currentPath)
        {
            var current = currentPath.Split('.').Where(x => !String.IsNullOrWhiteSpace(x)).ToList();
            current.Add(pi.Name);
            var result = String.Join(".", current);
            return result;
        }

        private static string GetNewCollectionPropertyPath(PropertyInfo pi, int itemId, string currentPath)
        {
            var result = GetNewPropertyPath(pi, currentPath) + "[" + itemId + "]";
            return result;
        }

        private static IEnumerable<PropertyInfo> GetScalarProperties<TViewModel>(TViewModel model)
            where TViewModel : class, IViewModel
        {
            return model.GetType().GetProperties()
                .Where(x => x.GetCustomAttribute<IgnoreSave>() == null)
                    .Where(x => x.CanRead && x.CanWrite && (x.PropertyType.IsPrimitive || x.PropertyType.IsEnum || x.PropertyType == typeof(string) || x.PropertyType == typeof(decimal) || Nullable.GetUnderlyingType(x.PropertyType) != null || x.PropertyType == typeof(DateTime)))
                    .ToList();
        }

        private static IEnumerable<PropertyInfo> GetReferenceProperties<TViewModel>(TViewModel model)
            where TViewModel : class, IViewModel
        {
            return model.GetType().GetProperties()
                .Where(x => x.GetCustomAttribute<IgnoreSave>() == null)
                    .Where(
                        x =>
                            x.CanRead && x.CanWrite && x.PropertyType.IsClass &&
                            x.PropertyType.GetInterfaces().Contains(typeof(IViewModel)))
                    .ToList();
        }

        private static IEnumerable<PropertyInfo> GetCollectionProperties<TViewModel>(TViewModel model)
            where TViewModel : class, IViewModel
        {
            var properties = model.GetType().GetProperties().Where(x => x.GetCustomAttribute<IgnoreSave>() == null);

            var result = properties.Where(
                        x =>
                            x.CanRead && x.CanWrite && x.PropertyType.IsGenericType &&
                            x.PropertyType.GetGenericTypeDefinition() == typeof(List<>) &&
                            x.PropertyType.GetGenericArguments()[0].GetInterfaces().Contains(typeof(IViewModel)))
                    .ToList();

            return result;
        }
    }
}
