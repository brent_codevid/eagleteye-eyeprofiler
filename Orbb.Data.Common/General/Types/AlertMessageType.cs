﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Globalization;
using System;

namespace Orbb.Data.Common.General.Types
{
    public class AlertMessageType : IEntity
    {

        public Translation Description { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion


    }


}
