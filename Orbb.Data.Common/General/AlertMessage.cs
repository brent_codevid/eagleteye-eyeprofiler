﻿namespace Orbb.Data.Common.General
{
    public class AlertMessage
    {
        public string Subject { get; set; }
        public string Message { get; set; }
        public Enumerations.AlertMessageTypes AlertMessageType { get; set; }
        public string Internal { get; set; }

    }

}
