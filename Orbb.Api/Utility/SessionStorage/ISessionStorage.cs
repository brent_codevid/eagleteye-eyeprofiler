﻿namespace Orbb.Api.Utility.SessionStorage
{
    public interface ISessionStorage : IStorage
    {
        object Get(string key);
    }
}
