﻿namespace Orbb.Api.Utility.SessionStorage
{
    public interface IStorage
    {
        void Set<T>(string key, T value);
        T Get<T>(string key);
        void Remove(string key);
    }
}
