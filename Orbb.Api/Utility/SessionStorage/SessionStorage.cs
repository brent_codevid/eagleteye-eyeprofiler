﻿using Microsoft.AspNetCore.Http;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Orbb.Api.Utility.SessionStorage
{
    public class SessionStorage : ISessionStorage
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private ISession Session => httpContextAccessor.HttpContext.Session;

        public SessionStorage(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public void Set<T>(string key, T value)
        {
            byte[] data = new byte[0];
            var binaryFormatter = new BinaryFormatter();

            if (value != null)
            {
                using (var ms = new MemoryStream())
                {
                    binaryFormatter.Serialize(ms, value);
                    data = ms.ToArray();
                }
            }

            Session.Set(key, data);
        }

        public T Get<T>(string key)
        {
            T returnValue = default(T);
            var binaryFormatter = new BinaryFormatter();

            byte[] data = Session.Get(key);
            if (data != null && data.Length > 0)
            {
                using (var ms = new MemoryStream(data))
                {
                    returnValue = (T)binaryFormatter.Deserialize(ms);
                }
            }

            return returnValue;
        }

        public object Get(string key)
        {
            object returnValue = null;

            var binaryFormatter = new BinaryFormatter();

            byte[] data = Session.Get(key);
            if (data != null && data.Length > 0)
            {
                using (var ms = new MemoryStream(data))
                {
                    returnValue = binaryFormatter.Deserialize(ms);
                }
            }

            return returnValue;
        }

        public void Remove(string key)
        {
            Session.Remove(key);
        }
    }
}