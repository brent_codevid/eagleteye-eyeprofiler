﻿using Orbb.Data.Common;

namespace Orbb.Api.Utility.SessionStorage
{
    public class SessionCurrentStorage<T> : ICurrentStorage<T> where T : class
    {
        IStorage sessionStorage;
        string key = "Current" + typeof(T).Name;
        public SessionCurrentStorage(ICookieStorage sessionStorage)
        {
            this.sessionStorage = sessionStorage;
        }
        public T Object
        {
            get => sessionStorage.Get<T>(key);

            set => sessionStorage.Set(key, value);
        }

        public void Remove()
        {
            sessionStorage.Remove(key);
        }
    }
}