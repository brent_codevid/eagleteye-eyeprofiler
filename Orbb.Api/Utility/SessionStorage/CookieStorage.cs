﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Orbb.Common.Environment;
using System.Net;

namespace Orbb.Api.Utility.SessionStorage
{
    public class CookieStorage : ICookieStorage
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public CookieStorage(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Set<T>(string key, T value)
        {
            string s = //JsonConvert.SerializeObject(value);
                        JsonConvert.SerializeObject(value,
                            new JsonSerializerSettings
                            {
                                PreserveReferencesHandling = PreserveReferencesHandling.All
                            });
            string enc = WebUtility.UrlEncode(s);

            _httpContextAccessor.HttpContext.Response.Cookies.Delete(Config.CookiePrefix + key);
            _httpContextAccessor.HttpContext.Response.Cookies.Append(Config.CookiePrefix + key, enc);
        }

        public T Get<T>(string key)
        {
            T returnValue = default(T);

            var s = _httpContextAccessor.HttpContext.Request.Cookies[Config.CookiePrefix + key];

            if (s == null)
                return returnValue;

            string value = WebUtility.UrlDecode(s);
            if (value == "null") return returnValue;
            returnValue = JsonConvert.DeserializeObject<T>(value);

            return returnValue;
        }

        public void Remove(string key)
        {
            _httpContextAccessor.HttpContext.Response.Cookies.Delete(Config.CookiePrefix + key);
        }
    }
}
