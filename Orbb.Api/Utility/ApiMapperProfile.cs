﻿using AutoMapper;
using Orbb.BL.Implementations.LensFitting;

namespace Orbb.BL.Mappings
{
    public class ApiMapperProfile : Profile
    {
        public ApiMapperProfile()
        {
            CreateMap<Api.ApiModels.EyeData, EyeData>();
            CreateMap<Api.ApiModels.Meridian, Meridian>();
            CreateMap<Api.ApiModels.Parameters, Parameters>();
            CreateMap<Api.ApiModels.Hvid, Hvid>();
            CreateMap<Api.ApiModels.SimK, SimK>();
            CreateMap<Api.ApiModels.OrthoK, OrthoK>();
            CreateMap<Api.ApiModels.Limbus, Limbus>();
            CreateMap<Api.ApiModels.Circle, Circle>();
            CreateMap<Api.ApiModels.CarthesianCoordinates, Orbb.BL.Implementations.Base.CarthesianCoordinates>()
                .ForMember(dest => dest.XCoord, opt => opt.MapFrom(src => src.X))
                .ForMember(dest => dest.YCoord, opt => opt.MapFrom(src => src.Y));
        }
    }
}
