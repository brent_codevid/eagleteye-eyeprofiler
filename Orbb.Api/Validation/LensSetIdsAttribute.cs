﻿using Orbb.Api.ApiModels;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Orbb.Api.Validation
{
    public class LensSetIdsAttribute : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            EyeData eyeData = (EyeData)validationContext.ObjectInstance;
            IList<int> lensSetIds = eyeData.LenssetIds;

            return lensSetIds.Any(l => l <= 0) ? new ValidationResult(GetErrorMessage()) : ValidationResult.Success;
        }

        private static string GetErrorMessage()
        {
            return "Lens set ids must be positive integers.";
        }
    }
}
