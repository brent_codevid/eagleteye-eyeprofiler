﻿using Newtonsoft.Json;
using Orbb.BL.Interfaces.LensFitting;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Api.ApiModels
{
    public class Meridian : IMeridian
    {
        [JsonRequired, Range(0, 360)]
        public int Angle { get; set; }

        [JsonRequired, Range(0, double.MaxValue)]
        public double Rho { get; set; }

        [JsonRequired, Range(0, int.MaxValue)]
        public int Apex { get; set; }

        [JsonRequired]
        public IList<double> Points { get; set; }
    }
}
