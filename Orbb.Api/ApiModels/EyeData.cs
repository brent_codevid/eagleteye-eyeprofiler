﻿using Newtonsoft.Json;
using Orbb.Api.Validation;
using Orbb.BL.Interfaces.LensFitting;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Orbb.Api.ApiModels
{
    public class EyeData : IEyeData
    {
        public bool? Unbound { get; set; }
        public bool? UnlistedCP2 { get; set; }

        //No current use
        [JsonRequired, Range(1, int.MaxValue)]
        public int LensType { get; set; }

        public double? Bfs { get; set; }


        public double? EdgeLift { get; set; }

        [JsonRequired, LensSetIds]
        public IList<int> LenssetIds { get; set; }

        [JsonRequired]
        public IList<Meridian> Meridians { get; set; }

        public Parameters Parameters { get; set; }

        IList<IMeridian> IEyeData.Meridians { get => new List<IMeridian>(Meridians); set { } }

        public bool ValidMeridians()
        {
            List<int> angles = Meridians.Select(mer => mer.Angle % 180).ToList();

            //Efficient way to check if all elements in angles are unique
            var uniques = new HashSet<int>();
            if (!angles.All(uniques.Add))
                return false;

            //Checking if all angles have exactly one corresponding perpendicular angle
            var perpendiculars = new Dictionary<int, int>();
            foreach (var angle in angles)
            {
                if (perpendiculars.ContainsKey(angle % 90))
                {
                    perpendiculars[angle % 90] += 1;
                }
                else
                {
                    perpendiculars.Add(angle % 90, 1);
                }
            }
            return perpendiculars.All(item => item.Value == 2);
        }
    }
}
