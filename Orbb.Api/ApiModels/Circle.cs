﻿namespace Orbb.Api.ApiModels
{
    public class Circle
    {
        public CarthesianCoordinates Center { get; set; }
        public CarthesianCoordinates Radius { get; set; }
    }
}