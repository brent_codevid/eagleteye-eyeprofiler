﻿using System.Collections.Generic;

namespace Orbb.Api.ApiModels
{
    public class LensFittingListResult
    {
        public double Diameter { get; set; }
        public IList<MeridianResult> Meridians { get; set; }

        public int Id { get; set; }

        public double FittingAngle { get; set; }

        public string SupplierName { get; set; }
        public int SelectedAngle { get; internal set; }
    }

    public class OldLensFittingListResult
    {
        public double Diameter { get; set; }
        public IList<OldMeridianResult> Meridians { get; set; }

        public int Id { get; set; }

        public double FittingAngle { get; set; }

        public string SupplierName { get; set; }
    }
}
