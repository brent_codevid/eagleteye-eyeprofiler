﻿namespace Orbb.Api.ApiModels
{
    public class OrthoK
    {
            public double Sphere { get; set; }
            public double Cylinder { get; set; }
            public double Axis { get; set; }
    }
}