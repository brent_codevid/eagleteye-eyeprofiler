﻿namespace Orbb.Api.ApiModels
{
    public class LensFittingStringResult
    {
        public string Name { get; set; }

        public int Id { get; set; }

        public double FittingAngle { get; set; }

        public string SupplierName { get; set; }
    }
}
