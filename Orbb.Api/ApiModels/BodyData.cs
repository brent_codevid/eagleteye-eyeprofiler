﻿using Newtonsoft.Json;
using Orbb.Api.Validation;
using Orbb.BL.Interfaces.LensFitting;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Orbb.Api.ApiModels
{
    public class BodyData
    {
        public EyeData EyeData { get; set; }

        public string VersionNumber { get; set; }
    }
}
