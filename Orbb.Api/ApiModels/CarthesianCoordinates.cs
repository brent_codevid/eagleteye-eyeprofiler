﻿namespace Orbb.Api.ApiModels
{
    public class CarthesianCoordinates
    {
        public double X { get; set; }
        public double Y { get; set; }
    }
}
