﻿using Orbb.Common.LensFitting;

namespace Orbb.Api.ApiModels
{
    public class MeridianResult
    {
        public NameValueTuple Bcr { get; set; }
        public NameValueTuple Vault { get; set; }
        public NameValueTuple LandingZone { get; set; }
    }

    public class OldMeridianResult
    {
        public string Bcr { get; set; }
        public string Vault { get; set; }
        public string LandingZone { get; set; }
    }
}