﻿namespace Orbb.Api.ApiModels
{
    public class Parameters
    {
        public Hvid Hvid { get; set; }
        public Limbus Limbus { get; set; }
        public SimK SimK { get; set; }
        public OrthoK OrthoK { get; set; }

        public int? ApicalClearance { get; set; }
        public int? LimbalClearance { get; set; }
        public int? MidPeripheralClearance { get; set; }

    }
}