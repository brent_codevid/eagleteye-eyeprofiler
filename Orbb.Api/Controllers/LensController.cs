﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.Models.Models.Lenssets;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [ApiVersion("1.0")]
    [ApiVersion("2.0")]
    public class LensController : ControllerBase //make api controller
    {
        private readonly ILogger<LensController> _logger;
        private readonly ILensRepository _lensRepository;

        public LensController(ILogger<LensController> logger, ILensRepository lensRepository)
        {
            _logger = logger;
            _lensRepository = lensRepository;
        }

        // POST api/lens
        /// <summary>
        /// Returns lenssets (filtered on name, case insensitive).
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/lens
        ///     {
        ///       "filter": "zen"
        ///     }
        /// </remarks>
        /// <returns>Lenssets</returns>
        /// <response code="200">Returns the lenssets</response>
        /// <response code="400">If error occurred</response>   
        [HttpPost]
        [Route("Lenssets")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult GetLenssets([FromBody] RequestFilterContent filter)
        {
            if (!ModelState.IsValid)
                return BadRequest();
            IList<LensInfo> lensInfo = _lensRepository.GetInfo();

            var filterString = filter.Filter?.ToLower();

            if (filterString != null)
            {
                lensInfo = lensInfo.Where(x => x.Name.ToLower().Contains(filterString)).ToList();
            }

            return Ok(lensInfo);
        }
    }

    public class RequestFilterContent
    {
        public string Filter { get; set; }
    }
}
