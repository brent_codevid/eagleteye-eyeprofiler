﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Orbb.Api.ApiModels;
using Orbb.BL.Interfaces.Datacapture;
using Orbb.BL.Interfaces.LensFitting;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.ViewModels.Models.LensFitter;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.Api.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    public class LensFittingController : ControllerBase //make api controller
    {
        private readonly ILogger<LensFittingController> _logger;
        private readonly IMapper _mapper;
        private readonly ILensFitter lensFitter;
        private readonly ILensRepository _lensRepository;
        private readonly ILensfittingCaptureRepository _lensfittingCapture;

        private const string _eyeDataFormat = "The request should contain LensType(integer), LenssetIds(List of integers) and meridians(List of meridians).";
        private const string _meridianAngles = "All meridians should have a unique angle and each meridian has to have a corresponding perpendicular meridian.";

        public LensFittingController(ILogger<LensFittingController> logger, ILensFitter lensFitter, ILensRepository lensRepository, ILensfittingCaptureRepository lensfittingCapture, IMapper mapper)
        {
            _logger = logger;
            this.lensFitter = lensFitter;
            _lensRepository = lensRepository;
            _mapper = mapper;
            _lensfittingCapture = lensfittingCapture;
        }

        // POST api/lensfitting
        /// <summary>
        /// Returns lensfitting.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/lensfitting
        ///     {
        ///       "lensType": 1,
        ///       "apicalClearance": 250,
        ///       "limbalClearance": 100,
        ///       "midPeripheralClearance": 250,
        ///       "edgeLift": 100,
        ///       "lensSetIds": [
        ///           1, 5
        ///       ],
        ///       "meridians": [
        ///         {
        ///             "angle": 0,
        ///             "rho": 0.02288416796979257,
        ///             "apex": 372,
        ///             "points": [-8, -4, -2, 0, -1, -3, -7]
        ///         }
        ///       ]
        ///     }
        /// </remarks>
        /// <returns>Best lensfitting</returns>
        /// <response code="200">Returns the lensfitting</response>
        /// <response code="400">If error occurred</response>   
        [HttpPost]
        [Route("Fitting")]
        [ApiVersion("1.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult PostFitting([FromBody] EyeData eyeData)
        {
            try
            {
                if (!ModelState.IsValid || eyeData == null)
                    return BadRequest(_eyeDataFormat);

                if (!eyeData.ValidMeridians())
                    return BadRequest(_meridianAngles);

                IList<int> validIds = _lensRepository.GetValidIds();
                eyeData.LenssetIds = eyeData.LenssetIds.Intersect(validIds).ToList();
                if (eyeData.LenssetIds.Count == 0)
                {
                    return Ok("No results");
                }
                IList<LensFittingResultVm> fittings = lensFitter.GetBestFittingLens(_mapper.Map<BL.Implementations.LensFitting.EyeData>(eyeData));
                IList<OldLensFittingListResult> results = new List<OldLensFittingListResult>();

                foreach (LensFittingResultVm fitting in fittings)
                {
                    _lensfittingCapture.CaptureFitting(fitting.Id, fitting.Name, "anonymous api-user");
                    List<OldMeridianResult> mers = new List<OldMeridianResult>();
                    OldLensFittingListResult result = new OldLensFittingListResult
                    {
                        Diameter = fitting.Diameter,
                        FittingAngle = fitting.FittingAngle,
                        SupplierName = fitting.SupplierName,
                        Id = fitting.Id,
                    };

                    mers.Add(new OldMeridianResult());
                    mers.Add(new OldMeridianResult());
                    mers.Add(new OldMeridianResult());
                    mers.Add(new OldMeridianResult());

                    for (int i = 0; i < fitting.LandingZones.Count; i++)
                    {
                        mers[i].LandingZone = fitting.LandingZones[i].Value;
                    }

                    for (int i = 0; i < fitting.Bcrs.Count; i++)
                    {
                        mers[i].Bcr = fitting.Bcrs[i].Value;
                    }

                    for (int i = 0; i < fitting.Vaults.Count; i++)
                    {
                        mers[i].Vault = fitting.Vaults[i].Value;
                    }

                    result.Meridians = mers;
                    results.Add(result);
                }
                return Ok(results);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return BadRequest();
            }
        }

        // POST api/lensfitting
        /// <summary>
        /// Returns lensfitting.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/lensfitting
        ///     {
        ///       "lensType": 1,
        ///       "apicalClearance": 250,
        ///       "limbalClearance": 100,
        ///       "midPeripheralClearance": 250,
        ///       "edgeLift": 100,
        ///       "lensSetIds": [
        ///           1, 5
        ///       ],
        ///       "meridians": [
        ///         {
        ///             "angle": 0,
        ///             "rho": 0.02288416796979257,
        ///             "apex": 372,
        ///             "points": [-8, -4, -2, 0, -1, -3, -7]
        ///         }
        ///       ]
        ///     }
        /// </remarks>
        /// <returns>Best lensfitting</returns>
        /// <response code="200">Returns the lensfitting</response>
        /// <response code="400">If error occurred</response>   
        [HttpPost]
        [Route("Fitting")]
        [ApiVersion("2.0")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult PostFitting2([FromBody] EyeData eyeData)
        {


            try
            {
                if (!ModelState.IsValid || eyeData == null)
                    return BadRequest(_eyeDataFormat);

                if (!eyeData.ValidMeridians())
                    return BadRequest(_meridianAngles);

                IList<int> validIds = _lensRepository.GetValidIds();
                eyeData.LenssetIds = eyeData.LenssetIds.Intersect(validIds).ToList();
                if (eyeData.LenssetIds.Count == 0)
                {
                    return Ok("No results");
                }
                IList<LensFittingResultVm> fittings = lensFitter.GetBestFittingLens(_mapper.Map<BL.Implementations.LensFitting.EyeData>(eyeData));
                IList<LensFittingListResult> results = new List<LensFittingListResult>();

                foreach (LensFittingResultVm fitting in fittings)
                {
                    _lensfittingCapture.CaptureFitting(fitting.Id, fitting.Name, "anonymous api-user");
                    List<MeridianResult> mers = new List<MeridianResult>();
                    LensFittingListResult result = new LensFittingListResult
                    {
                        Diameter = fitting.Diameter,
                        FittingAngle = fitting.FittingAngle,
                        SelectedAngle = fitting.SelectedAngle,
                        SupplierName = fitting.SupplierName,
                        Id = fitting.Id,
                    };

                    mers.Add(new MeridianResult());
                    mers.Add(new MeridianResult());
                    mers.Add(new MeridianResult());
                    mers.Add(new MeridianResult());

                    for (int i = 0; i < fitting.LandingZones.Count; i++)
                    {
                        mers[i].LandingZone = fitting.LandingZones[i];
                    }

                    for (int i = 0; i < fitting.Bcrs.Count; i++)
                    {
                        mers[i].Bcr = fitting.Bcrs[i];
                    }

                    for (int i = 0; i < fitting.Vaults.Count; i++)
                    {
                        mers[i].Vault = fitting.Vaults[i];
                    }

                    result.Meridians = mers;
                    results.Add(result);
                }
                return Ok(results);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return BadRequest();
            }
        }

        // POST api/lensfitting
        /// <summary>
        /// Returns lensfitting.
        /// </summary>
        /// <remarks>
        /// Sample request:
        ///
        ///     POST /api/lensfitting
        ///     {
        ///       "lensType": 1,
        ///       "apicalClearance": 250,
        ///       "limbalClearance": 100,
        ///       "midPeripheralClearance": 250,
        ///       "edgeLift": 100,
        ///       "lensSetIds": [
        ///           1, 5
        ///       ],
        ///       "meridians": [
        ///         {
        ///             "angle": 0,
        ///             "rho": 0.02288416796979257,
        ///             "apex": 372,
        ///             "points": [-8, -4, -2, 0, -1, -3, -7]
        ///         }
        ///       ]
        ///     }
        /// </remarks>
        /// <returns>Best lensfitting</returns>
        /// <response code="200">Returns the lensfitting in text format</response>
        /// <response code="400">If error occurred</response>   
        [HttpPost]
        [Route("FittingString")]
        [ProducesResponseType(200)]
        [ProducesResponseType(400)]
        public ActionResult PostFittingString([FromBody] EyeData eyeData)
        {
            try
            {
                if (!ModelState.IsValid || eyeData == null)
                    return BadRequest(_eyeDataFormat);

                if (!eyeData.ValidMeridians())
                    return BadRequest(_meridianAngles);

                IList<int> validIds = _lensRepository.GetValidIds();
                eyeData.LenssetIds = eyeData.LenssetIds.Intersect(validIds).ToList();
                if (eyeData.LenssetIds.Count == 0)
                {
                    return Ok("No results");
                }
                IList<LensFittingStringResult> results = lensFitter.GetBestFittingLens(_mapper.Map<BL.Implementations.LensFitting.EyeData>(eyeData))
                    .Select(fitting => new LensFittingStringResult
                    {
                        FittingAngle = fitting.FittingAngle,
                        SupplierName = fitting.SupplierName,
                        Id = fitting.Id,
                        Name = fitting.Name
                    }
                    )
                    .ToList();

                return Ok(results);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);

                return BadRequest();
            }
        }
    }
}
