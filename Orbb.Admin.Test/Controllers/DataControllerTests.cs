﻿using AutoMapper;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.Admin.Controllers;
using Orbb.BL.Mappings;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.ViewModels.Models.Other;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.Admin.Test.Controllers
{
    [TestClass]
    public class DataControllerTests
    {
        private static Container _container;
        private static DataController _controller;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            // add framework services
            ServiceCollection services = new ServiceCollection();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            _container.Configure(c =>
            {
                // Populate the container using the service collection
                c.Populate(services);
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(IHostingEnvironment)).Use(typeof(DummyHostingEnvironment));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            BL.Bootstrapper.RegisterWithContainer(_container);

            _controller = _container.GetInstance<DataController>();
        }

        [TestMethod]
        public void CheckIfGetLandingZoneComboboxItemsContainsOnlyTheExpectedValues()
        {
            string[] expectedValues = Enum.GetNames(typeof(LandingZoneBaseType)).Select(x => (string)_controller.stringLocalizer[x]).ToArray();

            JsonResult result = _controller.GetLandingZoneComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetTypeComboboxItemsContainsOnlyTheExpectedValues()
        {
            string[] expectedValues = Enum.GetNames(typeof(LensTypes)).Select(x => (string)_controller.stringLocalizer[x]).ToArray();

            JsonResult result = _controller.GetTypeComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetSectionsComboboxItemsContainsOnlyTheExpectedValues()
        {
            string[] expectedValues = Enum.GetNames(typeof(Sections));

            JsonResult result = _controller.GetSectionsComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetAxisComboboxItemsContainsOnlyTheExpectedValues()
        {
            string[] expectedValues = Enum.GetNames(typeof(ControlPointCategories)).Select(x => (string)_controller.stringLocalizer[x]).ToArray();

            JsonResult result = _controller.GetAxisComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetSupplierComboboxItemsContainsTheExpectedValues()
        {
            var actual = _controller.SupplierRepository.GetAll(null);
            string[] expectedValues = actual.Select(x => x.Name).ToArray();

            JsonResult result = _controller.GetSupplierComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetSag2ItemsReturnsTheCorrectNumberOfItems()
        {
            var lenssetIds = _controller.LensRepository.GetValidIds();
            foreach (var id in lenssetIds)
            {
                var expectedValue = _controller.LensRepository.GetLenssetById(id).CP2ToCP1List.Count;
                JsonResult result = _controller.GetSag2Items(id, new DataSourceRequest());
                Assert.AreEqual(expectedValue, ((DataSourceResult)result.Value).Total);
            }
        }

        [TestMethod]
        public void CheckIfGetSag2ItemsReturnsNoItemsWhenLenssetDoesntExist()
        {
            JsonResult result = _controller.GetSag2Items(0, new DataSourceRequest());
            Assert.IsTrue(((DataSourceResult)result.Value).Total == 0, "The method should return no CP2ToCP1Items if the lensset doesn't exist");
        }

        [TestMethod]
        public void CheckIfGetCustomerGridItemsReturnsAnItemForAllLenssetsWithNewCountry()
        {
            JsonResult result = _controller.GetCustomerGridItems(1, new DataSourceRequest(), 1);
            var expected = _controller.CountryRepository.GetSingle(c => c.Id == 1, null);
            Assert.AreEqual(expected.AvailableLenssets.Count, ((DataSourceResult)result.Value).Total, "The method should return an item for all the lenssets");
        }

        [TestMethod]
        public void CheckIfGetCustomerGridItemsReturnsAnItemForAllLenssets()
        {
            JsonResult result = _controller.GetCustomerGridItems(1, new DataSourceRequest(), 1);
            var expected = _controller.CountryRepository.GetSingle(c => c.Id == 1, null);
            Assert.AreEqual(expected.AvailableLenssets.Count, ((DataSourceResult)result.Value).Total, "The method should return an item for all the lenssets");
        }

        [TestMethod]
        public void CheckIfGetCustomerComboboxItemsContainsTheExpectedValues()
        {
            var actual = _controller.CustomerRepository.GetAll(null);
            string[] expectedValues = actual.Select(x => x.Name).ToArray();

            JsonResult result = _controller.GetCustomerComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetOrderStatusComboboxItemsContainsOnlyTheExpectedValues()
        {
            string[] expectedValues = Enum.GetNames(typeof(OrderStatus)).Select(x => (string)_controller.stringLocalizer[x]).ToArray();

            JsonResult result = _controller.GetOrderStatusComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<string> values = new List<string>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Text), "The value " + value.Text + " is not expected");
                values.Add(value.Text);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetLensComboboxItemsContainsTheExpectedValues()
        {
            var actual = _controller.LensRepository.GetAll(null);
            int[] expectedValues = actual.Select(x => x.Id).ToArray();

            JsonResult result = _controller.GetLensesComboboxItems();
            List<ComboBoxItemVm> results = (List<ComboBoxItemVm>)result.Value;
            List<int> values = new List<int>();

            foreach (ComboBoxItemVm value in results)
            {
                Assert.IsTrue(expectedValues.Contains(value.Id), "The value " + value.Id + " is not expected");
                values.Add(value.Id);
            }
            foreach (var expectedValue in expectedValues)
            {
                Assert.IsTrue(values.Contains(expectedValue), "The value " + expectedValue + " is missing");
            }
        }

        [TestMethod]
        public void CheckIfGetOrdersReturnsTheCorrectAmountOfOrders()
        {
            var customerIds = _controller.CustomerRepository.GetAll(null).Select(x => x.Id);
            foreach (var id in customerIds)
            {
                var expectedValue = _controller.OrderRepository.GetOrdersByCustomerId(id).Count;
                JsonResult result = _controller.GetOrders(id, new DataSourceRequest());
                Assert.AreEqual(expectedValue, ((DataSourceResult)result.Value).Total);
            }
        }

        [TestMethod]
        public void CheckIfGetOrdersReturnsZeroWhenTheCustomerDoesntExist()
        {
            JsonResult result = _controller.GetOrders(0, new DataSourceRequest());
            Assert.IsTrue(((DataSourceResult)result.Value).Total == 0, "The method should return 0 if the customer doesn't exist");
        }
    }
}

public class DummyHostingEnvironment : IHostingEnvironment
{
    public string EnvironmentName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public string ApplicationName { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public string WebRootPath { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public IFileProvider WebRootFileProvider { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public string ContentRootPath { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
    public IFileProvider ContentRootFileProvider { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
}