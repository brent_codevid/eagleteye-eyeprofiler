﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Implementations.Base;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Test.Implementations.Base
{
    [TestClass]
    public class MathHelpersTest
    {
        [TestMethod]
        public void PolToCartTests()
        {
            CarthesianCoordinates result = MathHelpers.PolToCart(0, 1);
            Assert.AreEqual(1, result.XCoord, 5);
            Assert.AreEqual(0, result.YCoord, 5);

            result = MathHelpers.PolToCart(1, 1);
            Assert.AreEqual(0.54030230586814, result.XCoord, 5);
            Assert.AreEqual(0.8414709848079, result.YCoord, 5);

            result = MathHelpers.PolToCart(2, 1);
            Assert.AreEqual(-0.41614683654714, result.XCoord, 5);
            Assert.AreEqual(0.90929742682568, result.YCoord, 5);

            result = MathHelpers.PolToCart(2, 2);
            Assert.AreEqual(-0.83229367309429, result.XCoord, 5);
            Assert.AreEqual(1.8185948536514, result.YCoord, 5);
        }

        [TestMethod]
        public void PolToCartWithDegreesTests()
        {
            CarthesianCoordinates result = MathHelpers.PolToCartWithDegrees(0, 1);
            Assert.AreEqual(1, result.XCoord, 5);
            Assert.AreEqual(0, result.YCoord, 5);

            result = MathHelpers.PolToCartWithDegrees(45, 1);
            Assert.AreEqual(0.70710678118655, result.XCoord, 5);
            Assert.AreEqual(0.70710678118655, result.YCoord, 5);

            result = MathHelpers.PolToCartWithDegrees(90, 1);
            Assert.AreEqual(0, result.XCoord, 5);
            Assert.AreEqual(1, result.YCoord, 5);

            result = MathHelpers.PolToCartWithDegrees(260, 3);
            Assert.AreEqual(-0.5209445330008, result.XCoord, 5);
            Assert.AreEqual(-2.9544232590366, result.YCoord, 5);
        }

        [TestMethod]
        public void CartToPolTests()
        {
            PolarCoordinates result = MathHelpers.CartToPol(1, 0);
            Assert.AreEqual(0, result.DegreeAngle, 5);
            Assert.AreEqual(1, result.Rho, 5);

            result = MathHelpers.CartToPol(0.70710678118655, 0.70710678118655);
            Assert.AreEqual(45, result.DegreeAngle, 5);
            Assert.AreEqual(1, result.Rho, 5);

            result = MathHelpers.CartToPol(0, 1);
            Assert.AreEqual(90, result.DegreeAngle, 5);
            Assert.AreEqual(1, result.Rho, 5);

            result = MathHelpers.CartToPol(-0.5209445330008, -2.9544232590366);
            Assert.AreEqual(260, result.DegreeAngle, 5);
            Assert.AreEqual(3, result.Rho, 5);
        }

        [TestMethod]
        public void AngleBetweenTests()
        {
            CarthesianCoordinates c1 = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            CarthesianCoordinates c2 = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            CarthesianCoordinates c3 = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            var result = MathHelpers.AngleBetween(c1, c2, c3);
            Assert.AreEqual(1.5707963268, result, 5);

            c1 = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            c2 = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            c3 = new CarthesianCoordinates { XCoord = -1, YCoord = 0 };
            result = MathHelpers.AngleBetween(c1, c2, c3);
            Assert.AreEqual(3.1415926536, result, 5);

            c1 = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            c2 = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            c3 = new CarthesianCoordinates { XCoord = 1.4142135623731, YCoord = 1.4142135623731 };
            result = MathHelpers.AngleBetween(c1, c2, c3);
            Assert.AreEqual(0.7853981634, result, 5);

            c1 = new CarthesianCoordinates { XCoord = 2, YCoord = 0 };
            c2 = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            c3 = new CarthesianCoordinates { XCoord = 2.4142135623731, YCoord = 1.4142135623731 };
            result = MathHelpers.AngleBetween(c1, c2, c3);
            Assert.AreEqual(0.7853981634, result, 5);
        }

        [TestMethod]
        public void IsOtherSideWhenSameSideReturnsFalseTests()
        {
            CarthesianCoordinates a = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            CarthesianCoordinates b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            CarthesianCoordinates p = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            CarthesianCoordinates reference = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            var result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            reference = new CarthesianCoordinates { XCoord = 1, YCoord = 2 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            reference = new CarthesianCoordinates { XCoord = 1, YCoord = 2 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsOtherSideWhenBothOnLineReturnsFalseTests()
        {
            CarthesianCoordinates a = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            CarthesianCoordinates b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            CarthesianCoordinates p = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            CarthesianCoordinates reference = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            var result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            reference = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 2, YCoord = 2 };
            reference = new CarthesianCoordinates { XCoord = 3, YCoord = 3 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsOtherSideWhenOneOnLineReturnsFalseTests()
        {
            CarthesianCoordinates a = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            CarthesianCoordinates b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            CarthesianCoordinates p = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            CarthesianCoordinates reference = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            var result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 2, YCoord = 0 };
            reference = new CarthesianCoordinates { XCoord = -1, YCoord = 0 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 2, YCoord = 2 };
            reference = new CarthesianCoordinates { XCoord = 0, YCoord = 3 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void IsOtherSideWhenOnOtherSideReturnsTrueTests()
        {
            CarthesianCoordinates a = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            CarthesianCoordinates b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            CarthesianCoordinates p = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            CarthesianCoordinates reference = new CarthesianCoordinates { XCoord = 1, YCoord = -1 };
            var result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsTrue(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = 1, YCoord = 0 };
            reference = new CarthesianCoordinates { XCoord = 0, YCoord = 1 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsTrue(result);

            a = new CarthesianCoordinates { XCoord = 1, YCoord = 1 };
            b = new CarthesianCoordinates { XCoord = 0, YCoord = 0 };
            p = new CarthesianCoordinates { XCoord = -2, YCoord = 2 };
            reference = new CarthesianCoordinates { XCoord = 3, YCoord = -3 };
            result = MathHelpers.IsOtherSide(a, b, p, reference);
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsArrayLengthZeroWhenNoDataProvided()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(null, 5);
            Assert.AreEqual(0, result.Length);

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double>(), 5);
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsArrayLengthZeroWhenSpanIsZeroOrLess()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5 }, 0);
            Assert.AreEqual(0, result.Length);

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5 }, -2);
            Assert.AreEqual(0, result.Length);
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsOriginalValueWhenOnlyOneValue()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5 }, 3);
            Assert.IsTrue(new double[] { 5 }.SequenceEqual(result));
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsOriginalValuesWhenOnlyTwoValues()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5, 2 }, 3);
            Assert.IsTrue(new double[] { 5, 2 }.SequenceEqual(result));
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsOriginalValuesWhenSpanIsOne()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5, 2, 6, 7 }, 1);
            Assert.IsTrue(new double[] { 5, 2, 6, 7 }.SequenceEqual(result));
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsOriginalValuesWhenSpanIsTwo()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 5, 2, 6, 7 }, 2);
            Assert.IsTrue(new double[] { 5, 2, 6, 7 }.SequenceEqual(result));
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsCorrectValuesWhenSpanSizeReachable()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 2, 3, 4, 5, 6 }, 3);
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(result), "First test where no smoothing was needed failed");

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 2, 3, 4, 5, 6 }, 5);
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(result), "Second test where no smoothing was needed failed");

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 5, 3, 1, 2, 3 }, 3);
            Assert.IsTrue(new double[] { 0, 2, 3, 3, 2, 2, 3 }.SequenceEqual(result), "First test where smoothing was needed failed");

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 5, 5, 4, 5, -9 }, 5);
            Assert.IsTrue(new double[] { 0, 2, 3, 4, 2, 0, -9 }.SequenceEqual(result), "Second test where smoothing was needed failed");
        }

        [TestMethod]
        public void SimpleMovingAverageSmoothingReturnsCorrectValuesWhenSpanSizeNotReachable()
        {
            var result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 2, 3, 4, 5, 6 }, 100);
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(result), "First test where no smoothing was needed failed");

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 5, 5, 4, 146, 0 }, 100);
            Assert.IsTrue(new double[] { 0, 2, 3, 23, 32, 50, 0 }.SequenceEqual(result), "First test where smoothing was needed failed");

            result = MathHelpers.SimpleMovingAverageSmoothing(new List<double> { 0, 1, 5, 5, 4, 66, 80 }, 100);
            Assert.IsTrue(new double[] { 0, 2, 3, 23, 32, 50, 80 }.SequenceEqual(result), "Second test where smoothing was needed failed");
        }
    }
}
