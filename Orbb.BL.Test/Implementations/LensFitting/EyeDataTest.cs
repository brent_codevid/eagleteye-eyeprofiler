﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Implementations.LensFitting;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Orbb.BL.Test.Implementations.LensFitting
{
    [TestClass]
    public class EyeDataTest
    {
        [TestMethod]
        public void ValidateMeridianGetAndSetAngle()
        {
            Meridian m = new Meridian();
            m.Angle = 42;
            Assert.AreEqual(42, m.Angle);
        }

        [TestMethod]
        public void ValidateMeridianGetAndSetRho()
        {
            Meridian m = new Meridian();
            m.Rho = 420.0;
            Assert.AreEqual(420.0, m.Rho, 5);
        }

        [TestMethod]
        public void ValidateMeridianGetAndSetApex()
        {
            Meridian m = new Meridian();
            m.Angle = 42;
            Assert.AreEqual(42, m.Angle);
        }

        [TestMethod]
        public void ValidateMeridianGetAndSetPoints()
        {
            List<double> expected = new List<double>
            {
                1.0,
                2.0/3.0,
                Math.PI,
                5
            };
            Meridian m = new Meridian();
            m.Points = new List<double>(expected);
            CollectionAssert.AreEqual(expected, (ICollection)m.Points, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetMeridians()
        {
            EyeData ed = new EyeData();
            var meridians = new List<Meridian>
            {
                new Meridian {Angle = 0},
                new Meridian {Angle = 90},
                new Meridian {Angle = 0}
            };
            ed.Meridians = new List<Meridian>(meridians);
            CollectionAssert.AreEqual(meridians, (ICollection)ed.Meridians, Comparer<Meridian>.Default);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetLenssetIds()
        {
            EyeData ed = new EyeData();
            var ids = new List<int>
            {
                1,7,42
            };
            ed.LenssetIds = new List<int>(ids);
            CollectionAssert.AreEqual(ids, (ICollection)ed.LenssetIds);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetLensType()
        {
            EyeData ed = new EyeData();
            ed.LensType = 704;
            Assert.AreEqual(704, ed.LensType);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetApicalClearance()
        {
            EyeData ed = new EyeData();
            ed.ApicalClearance = 164;
            Assert.AreEqual(164, ed.ApicalClearance);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetLimbalClearance()
        {
            EyeData ed = new EyeData();
            ed.LimbalClearance = 1704;
            Assert.AreEqual(1704, ed.LimbalClearance);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetMidPeriphClearance()
        {
            EyeData ed = new EyeData();
            ed.MidPeripheralClearance = 7104;
            Assert.AreEqual(7104, ed.MidPeripheralClearance);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetEdgeLift()
        {
            EyeData ed = new EyeData();
            ed.EdgeLift = 704.44;
            Assert.AreEqual(704.44, (double)ed.EdgeLift, 5);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetUnbound()
        {
            EyeData ed = new EyeData();
            ed.Unbound = true;
            Assert.AreEqual(true, ed.Unbound);
        }

        [TestMethod]
        public void ValidateEyeDataGetAndSetUnlistedCP2()
        {
            EyeData ed = new EyeData();
            ed.UnlistedCP2 = true;
            Assert.AreEqual(true, ed.UnlistedCP2);
        }

        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenDuplicateAngles()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 90},
                    new Meridian {Angle = 0}
                }
            };

            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 91 },
                new Meridian{ Angle = 3 },
                new Meridian{ Angle = 93 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 92 },
                new Meridian{ Angle = 7 },
                new Meridian{ Angle = 97 },
                new Meridian{ Angle = 5 },
                new Meridian{ Angle = 95 },
                new Meridian{ Angle = 2 }
            };

            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenOppositeAngles()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian> {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 180}
                }
            };

            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 183 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 187 },
                new Meridian{ Angle = 5 },
                new Meridian{ Angle = 183 }
            };

            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsFalseWhenMissingPerpendicular()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0}
                }
            };
            Assert.IsFalse(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 1 },
                new Meridian{ Angle = 91 },
                new Meridian{ Angle = 3 },
                new Meridian{ Angle = 93 },
                new Meridian{ Angle = 2 },
                new Meridian{ Angle = 92 },
                new Meridian{ Angle = 7 },
                new Meridian{ Angle = 97 },
                new Meridian{ Angle = 5 }
            };
            Assert.IsFalse(ed.ValidMeridians());
        }

        [TestMethod]
        public void ValidMeridiansReturnsTrueWithValidMeridians()
        {
            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>
                {
                    new Meridian {Angle = 0},
                    new Meridian {Angle = 90}
                }
            };
            Assert.IsTrue(ed.ValidMeridians());

            ed.Meridians = new List<Meridian>
            {
                new Meridian{ Angle = 0 },
                new Meridian{ Angle = 45 },
                new Meridian{ Angle = 315 },
                new Meridian{ Angle = 181 },
                new Meridian{ Angle = 271 },
                new Meridian{ Angle = 90 }
            };
            Assert.IsTrue(ed.ValidMeridians());
        }
    }
}
