﻿using AutoMapper;
using MathNet.Spatial.Euclidean;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Orbb.BL.Implementations.LensFitting;
using Orbb.BL.Mappings;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using StructureMap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Orbb.BL.Test.Implementations.LensFitting
{
    [TestClass]
    public class SemiMeridiansTest
    {
        private static Container _container;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            // add framework services
            ServiceCollection services = new ServiceCollection();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            _container.Configure(c =>
            {
                // Populate the container using the service collection
                c.Populate(services);
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container, false);
            Bootstrapper.RegisterWithContainer(_container);
        }

        [TestMethod]
        public void CalculatePlaneVectorWherePlaneEqualsXyPlane()
        {
            SemiMeridians semiMers = new SemiMeridians(360);
            for (int i = 0; i < 360; i++)
            {
                semiMers.Add(new SemiMeridian());
                semiMers[i].Angle = i;
                semiMers[i].DataPoints = new List<(double Rho, double zValues)>
                {
                    (1, 0),
                    (2, 0)
                };
                semiMers[i].RhoDistance = 1;
            }

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 1 };
            object resultObj = methodInfo.Invoke(semiMers, parameters);
            Vector3D result = (Vector3D)resultObj;

            Assert.AreEqual(0, result.X);
            Assert.AreEqual(0, result.Y);
            Assert.AreEqual(1, result.Z);
        }

        [TestMethod]
        public void CalculatePlaneVectorWherePlaneEqualsXyPlaneAndPointDistanceTooHigh()
        {
            SemiMeridians semiMers = new SemiMeridians(360);
            for (int i = 0; i < 360; i++)
            {
                semiMers.Add(new SemiMeridian());
                semiMers[i].Angle = i;
                semiMers[i].DataPoints = new List<(double Rho, double zValues)>
                {
                    (1, 0),
                    (2, 0)
                };
                semiMers[i].RhoDistance = 1;
            }

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 10 };
            object resultObj = methodInfo.Invoke(semiMers, parameters);
            Vector3D result = (Vector3D)resultObj;

            Assert.AreEqual(0, result.X);
            Assert.AreEqual(0, result.Y);
            Assert.AreEqual(1, result.Z);
        }

        [TestMethod]
        public void CalculatePlaneVectorHasAcceptableValues()
        {
            SemiMeridians semiMers = LoadJsonFile("SemiMeridians.json");

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 1 };
            object resultObj = methodInfo.Invoke(semiMers, parameters);
            Vector3D result = (Vector3D)resultObj;

            Assert.IsTrue(Math.Abs(result.X) < 1);
            Assert.IsTrue(Math.Abs(result.Y) < 1);
            Assert.IsTrue(Math.Abs(result.Z - 1) < 0.00001);
        }

        [TestMethod]
        public void CalculatePlaneVectorHasAcceptableValuesWithLimitedSemiMeridians()
        {
            for (int filterNumber = 2; filterNumber < 15; filterNumber++)
            {
                SemiMeridians semiMers = LoadJsonFile("SemiMeridians.json");
                int count = 0;
                for (int i = 0; i < 360; i++)
                {
                    if (semiMers[count].Angle % filterNumber != 0)
                    {
                        semiMers.RemoveAt(count);
                    }
                    else
                    {
                        count++;
                    }

                }

                MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
                object[] parameters = { 1 };
                object resultObj = methodInfo.Invoke(semiMers, parameters);
                Vector3D result = (Vector3D)resultObj;

                Assert.IsTrue(Math.Abs(result.X) < 1);
                Assert.IsTrue(Math.Abs(result.Y) < 1);
                Assert.IsTrue(Math.Abs(result.Z - 1) < 0.00001);
            }
        }

        [TestMethod]
        public void CalculatePlaneVectorFailsOnEmptySemiMeridians()
        {
            SemiMeridians semiMers = new SemiMeridians();

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 1 };

            Exception ex = Assert.ThrowsException<TargetInvocationException>(() => methodInfo.Invoke(semiMers, parameters), "A TargetInvocationException should have been thrown");
            Assert.AreEqual("This configuration of SemiMeridians resulted in an invalid Normal vector", ex.InnerException.Message);
        }

        [TestMethod]
        public void CalculatePlaneVectorOwnNormalDoesNotTransformData()
        {
            SemiMeridians semiMersOriginal = LoadJsonFile("SemiMeridians.json");
            SemiMeridians semiMers = LoadJsonFile("SemiMeridians.json");

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("CalculatePlaneVector", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 1 };
            object resultObj = methodInfo.Invoke(semiMers, parameters);
            Vector3D result = (Vector3D)resultObj;


            //semiMers.RotateData(result);

            SemiMeridian semOriginal = semiMersOriginal[0];
            SemiMeridian sem = semiMers[0];

            var rho = sem.DataPoints[sem.DataPoints.Count - 1].Item1;
            var zValue = sem.DataPoints[sem.DataPoints.Count - 1].Item2;

            var closestTuple = semOriginal.DataPoints.Aggregate((x, y) => Math.Abs(x.Item1 - rho) < Math.Abs(y.Item1 - rho) ? x : y);
            Assert.AreEqual(closestTuple.Item2, -zValue, 2);

        }

        [TestMethod]
        public void DelaunayRepopulationTest()
        {
            //Create semiMeridians 45 degrees from one another, all with max Rho 3
            SemiMeridians sms = new SemiMeridians(8);
            for (int i = 0; i < 8; i++)
            {
                sms.Add(new SemiMeridian
                {
                    Angle = i * 45,
                    RhoDistance = 1,
                    DataPoints = new List<(double Rho, double zValues)> {
                        (0,0),
                        (0.1,0.1),
                        (0.2,0.2),
                        (0.3,0.3),
                        (0.4,0.4),
                        (0.5,0.5),
                        (0.6,0.6),
                        (0.7,0.7),
                        (0.8,0.8),
                        (0.9,0.9),
                        (1,1),
                        (2,2),
                        (3,3)
                    }
                });
            }

            //Create square grid of vertexes from -3,-3 to 3,3
            List<Vertex> vertexes = new List<Vertex>();
            for (int i = -3; i <= 3; i++)
            {
                for (int j = -3; j <= 3; j++)
                {
                    vertexes.Add(new Vertex(i, j, Math.Sqrt(Math.Pow(i, 2) + Math.Pow(j, 2))));
                }
            }

            MethodInfo methodInfo = typeof(SemiMeridians).GetMethod("DelaunayRepopulation", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { vertexes, vertexes[31] };
            methodInfo.Invoke(sms, parameters);
            foreach (SemiMeridian semiMeridian in sms)
            {
                foreach (var (rho, zValue) in semiMeridian.DataPoints)
                {
                    Assert.AreEqual(rho, zValue, 5);
                }
            }
        }

        private static SemiMeridians LoadJsonFile(string name)
        {
            string jsonFile = File.ReadAllText(@"Data/" + name);
            return JsonConvert.DeserializeObject<SemiMeridians>(jsonFile);
        }
    }
}