﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Implementations.LensFitting;
using System;
using System.Collections.Generic;

namespace Orbb.BL.Test.Implementations.LensFitting
{
    [TestClass]
    public class EyeAnalysisResultsTest
    {
        [TestMethod]
        public void ValidateGetAndSetAngle()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.Angle = 1698;
            Assert.AreEqual(1698, ear.Angle);
        }

        [TestMethod]
        public void ValidateGetAndSetCP1Val()
        {
            double[] expected = {
                1.0,
                2.0/3.0,
                Math.PI,
                5
            };
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.CP1Val = new double[expected.Length];
            expected.CopyTo(ear.CP1Val, 0);
            CollectionAssert.AreEqual(expected, ear.CP1Val, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateGetAndSetCP2Val()
        {
            double[] expected = {
                1.0,
                2.0/3.0,
                Math.PI,
                5,
                7
            };
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.CP2Val = new double[expected.Length];
            expected.CopyTo(ear.CP2Val, 0);
            CollectionAssert.AreEqual(expected, ear.CP2Val, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateGetAndSetCP4Val()
        {
            double[] expected = {
                2.0/3.0,
                Math.PI,
                5,
                7
            };
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.CP4Val = new double[expected.Length];
            expected.CopyTo(ear.CP4Val, 0);
            CollectionAssert.AreEqual(expected, ear.CP4Val, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateGetAndSetSettlingMerCP2()
        {
            double[] expected = {
                1.0,
                2.0/3.0,
                Math.PI,
                5,
                7,
                9
            };
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.SettlingMerCP2 = new double[expected.Length];
            expected.CopyTo(ear.SettlingMerCP2, 0);
            CollectionAssert.AreEqual(expected, ear.SettlingMerCP2, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateGetAndSetSettlingMerCP4()
        {
            double[] expected = {
                1.0,
                2.0/3.0,
                Math.PI,
                5,
                7,
                1
            };
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.SettlingMerCP4 = new double[expected.Length];
            expected.CopyTo(ear.SettlingMerCP4, 0);
            CollectionAssert.AreEqual(expected, ear.SettlingMerCP4, Comparer<double>.Default);
        }

        [TestMethod]
        public void ValidateGetAndSetApicalClearance()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.ApicalClearance = 12.44;
            Assert.AreEqual(12.44, (double)ear.ApicalClearance, 5);
        }

        [TestMethod]
        public void ValidateGetAndSetLimbalClearance()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.LimbalClearance = 0.53;
            Assert.AreEqual(0.53, (double)ear.LimbalClearance, 5);
        }

        [TestMethod]
        public void ValidateGetAndSetMidPeripheralClearance()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.MidPeripheralClearance = 1256.1;
            Assert.AreEqual(1256.1, (double)ear.MidPeripheralClearance, 5);
        }

        [TestMethod]
        public void ValidateGetAndSetEdgeLift()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.EdgeLift = 704.44;
            Assert.AreEqual(704.44, (double)ear.EdgeLift, 5);
        }

        [TestMethod]
        public void ValidateGetAndSetCP1Corr()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults();
            ear.CP1Corr = 999.999;
            Assert.AreEqual(999.999, ear.CP1Corr, 5);
        }

        [TestMethod]
        public void GetCP1MeanTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP1Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP1Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP1Mean is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(3, result[1]);

            ear.CP1Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP1Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP1Mean is not 2 long");
            Assert.AreEqual(3, result[0]);
            Assert.AreEqual(1, result[1]);
        }

        [TestMethod]
        public void GetCP1DiffTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP1Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP1Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP1Diff is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(2, result[1]);

            ear.CP1Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP1Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP1Diff is not 2 long");
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }

        [TestMethod]
        public void GetCP2MeanTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP2Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP2Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP2Mean is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(3, result[1]);

            ear.CP2Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP2Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP2Mean is not 2 long");
            Assert.AreEqual(3, result[0]);
            Assert.AreEqual(1, result[1]);
        }

        [TestMethod]
        public void GetCP2DiffTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP2Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP2Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP2Diff is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(2, result[1]);

            ear.CP2Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP2Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP2Diff is not 2 long");
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }

        [TestMethod]
        public void GetCP4MeanTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP4Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP4Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP4Mean is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(3, result[1]);

            ear.CP4Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP4Mean();

            Assert.AreEqual(2, result.Length, "The result of GetCP4Mean is not 2 long");
            Assert.AreEqual(3, result[0]);
            Assert.AreEqual(1, result[1]);
        }

        [TestMethod]
        public void GetCP4DiffTest()
        {
            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP4Val = new double[] { 1, 2, 3, 4 }
            };
            double[] result = ear.GetCP4Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP4Diff is not 2 long");
            Assert.AreEqual(2, result[0]);
            Assert.AreEqual(2, result[1]);

            ear.CP4Val = new double[] { 3, 2, 3, 0 };
            result = ear.GetCP4Diff();

            Assert.AreEqual(2, result.Length, "The result of GetCP4Diff is not 2 long");
            Assert.AreEqual(0, result[0]);
            Assert.AreEqual(2, result[1]);
        }
    }
}
