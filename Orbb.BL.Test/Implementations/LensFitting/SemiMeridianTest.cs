﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Implementations.LensFitting;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Test.Implementations.LensFitting
{
    [TestClass]
    public class SemiMeridianTest
    {
        private static SemiMeridian SemiMeridianDummyGenerator()
        {
            SemiMeridian dummy = new SemiMeridian
            {
                RhoDistance = 1,
                Angle = 0,
                DataPoints = new List<(double Rho, double zValues)> {
                    (0,0),
                    (1,0.5),
                    (2,1),
                    (3,1.5),
                    (4,2),
                    (5,2.5),
                    (6,3)
                }
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetAndSetRhoDistance()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.RhoDistance = 1532;
            Assert.AreEqual(1532, sm.RhoDistance);
        }

        [TestMethod]
        public void ValidateGetAndSetAngle()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.Angle = 361;
            Assert.AreEqual(361, sm.Angle);
        }

        [TestMethod]
        public void ValidateGetAndSetDataPoints()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            var expected = new List<(double Rho, double zValues)>
            {
                (0, 0),
                (1, 1),
                (2, 5),
                (3, 3),
                (4, 75),
                (5, -3),
                (6, 0)
            };
            sm.DataPoints = new List<(double Rho, double zValues)>(expected);
            CollectionAssert.AreEqual(expected, sm.DataPoints);
        }

        [TestMethod]
        public void ValidateGetAndSetExtrapolateDistance()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.ExtrapolateDistance = 4500;
            Assert.AreEqual(4500, sm.ExtrapolateDistance);
        }

        [TestMethod]
        public void ValidateGetAndSetSmoothDataMinimumRho()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.SmoothDataMinimumRho = -264;
            Assert.AreEqual(-264, sm.SmoothDataMinimumRho);
        }

        [TestMethod]
        public void GetRhosAndValuesShouldReturnCorrectValues()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            (double[] rhos, double[] zValues) = sm.GetRhosAndValues();
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(rhos), "GetRhosAndValues returns the wrong Rho-values");
            Assert.IsTrue(new[] { 0, 0.5, 1, 1.5, 2, 2.5, 3 }.SequenceEqual(zValues), "GetRhosAndValues returns the wrong Z-values");
        }

        [TestMethod]
        public void GetRhosShouldReturnCorrectValues()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            double[] result = sm.GetRhos();
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(result), "GetRhos returns the wrong Rho-values");
        }

        [TestMethod]
        public void GetValuesShouldReturnCorrectValues()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            double[] result = sm.GetZValues();
            Assert.IsTrue(new[] { 0, 0.5, 1, 1.5, 2, 2.5, 3 }.SequenceEqual(result), "GetZValues returns the wrong Z-values");
        }

        [TestMethod]
        public void GetInterpolatedValueShouldReturnCorrectValue()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            double result = sm.GetInterpolatedValue(1);
            Assert.AreEqual(0.5, result);
            result = sm.GetInterpolatedValue(1.5);
            Assert.AreEqual(0.75, result);
            result = sm.GetInterpolatedValue(6);
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void ExtrapolateShouldHaveTheCorrectEffect()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.ExtrapolateDistance = 5;
            sm.Extrapolate(10);
            (double[] rhos, double[] zValues) = sm.GetRhosAndValues();
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 }.SequenceEqual(rhos), "Extrapolate generates wrong Rho-values");
            Assert.IsTrue(new[] { 0, 0.5, 1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5 }.SequenceEqual(zValues), "Extrapolate generates wrong Z-values");
        }

        [TestMethod]
        public void SmoothDataShouldHaveTheCorrectEffect()
        {
            SemiMeridian sm = SemiMeridianDummyGenerator();
            sm.SmoothDataMinimumRho = 1;
            sm.DataPoints[6] = (6, 4.5);
            sm.SmoothData();
            (double[] rhos, double[] zValues) = sm.GetRhosAndValues();
            Assert.IsTrue(new double[] { 0, 1, 2, 3, 4, 5, 6 }.SequenceEqual(rhos), "SmoothData generates wrong Rho-values");
            Assert.IsTrue(new[] { 0, 0.5, 1, 1.5, 2, 3, 4.5 }.SequenceEqual(zValues), "SmoothData generates wrong Z-values");
        }
    }
}
