﻿using AutoMapper;
using MathNet.Numerics.Interpolation;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Implementations.LensFitting;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.BL.Mappings;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.Models.Models.Lenssets;
using StructureMap;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Test.Implementations.LensFitting
{
    [TestClass]
    public class LensFitterTest
    {
        private static Container _container;
        private static ILensRepository _lensRepository;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            // add framework services
            ServiceCollection services = new ServiceCollection();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();

            _container.Configure(c =>
            {
                // Populate the container using the service collection
                c.Populate(services);
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container, false);
            Bootstrapper.RegisterWithContainer(_container);

            _lensRepository = _container.GetInstance<ILensRepository>();
        }

        [TestMethod]
        public void GenerateLensDetailsTest()
        {
            LensFitter lf = new LensFitter(_lensRepository);

            var existingId = _lensRepository.GetValidIds()[0];
            LensDetail existingLens = _lensRepository.GetLenssetById(existingId);

            existingLens.LensDiameter = 20;
            existingLens.CP1.RelativeChordLength = false;
            existingLens.CP1.SemiChordLength = 10;
            existingLens.CP1.DiameterMultiplier = 0.1;
            existingLens.CP2.RelativeChordLength = false;
            existingLens.CP2.SemiChordLength = 30;
            existingLens.CP2.DiameterMultiplier = 0.3;
            existingLens.CP4.RelativeChordLength = false;
            existingLens.CP4.SemiChordLength = 40;
            existingLens.CP4.DiameterMultiplier = 0.4;

            EyeData ed = new EyeData
            {
                ApicalClearance = 7654321,
                MidPeripheralClearance = 1234567,
                EdgeLift = 97531,
                LenssetIds = new List<int> { existingId }
            };

            MethodInfo methodInfo = typeof(LensFitter).GetMethod("GenerateLensDetails", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { ed };
            IList<LensDetail> resultObj = methodInfo.Invoke(lf, parameters) as IList<LensDetail>;

            Assert.AreEqual(1, resultObj.Count, "Expected 1 LensDetail, but got " + resultObj.Count + " LensDetails.");
            LensDetail result = resultObj[0];
            Assert.AreEqual((double)ed.ApicalClearance, result.SuggestedApicalClearance, 5, "SuggestedApicalClearance was wrong.");
            Assert.AreEqual((double)ed.MidPeripheralClearance, result.SuggestedMidPeriphClearance, 5, "SuggestedMidPeriphClearance was wrong.");
            Assert.AreEqual((double)ed.EdgeLift, result.CP4.Lift, 5, "CP4.Lift was wrong.");
            Assert.AreEqual((double)existingLens.CP1.SemiChordLength, (double)result.CP1.SemiChordLength, 5, "CP1.SemiChordLength was wrong.");
            Assert.AreEqual((double)existingLens.CP2.SemiChordLength, (double)result.CP2.SemiChordLength, 5, "CP2.SemiChordLength was wrong.");
            Assert.AreEqual((double)existingLens.CP4.SemiChordLength, (double)result.CP4.SemiChordLength, 5, "CP4.SemiChordLength was wrong.");


            existingLens.CP1.RelativeChordLength = true;
            existingLens.CP2.RelativeChordLength = true;
            existingLens.CP4.RelativeChordLength = true;

            resultObj = methodInfo.Invoke(lf, parameters) as IList<LensDetail>;

            Assert.AreEqual(1, resultObj.Count, "Expected 1 LensDetail, but got " + resultObj.Count + " LensDetails.");
            result = resultObj[0];
            Assert.AreEqual((double)ed.ApicalClearance, result.SuggestedApicalClearance, 5, "SuggestedApicalClearance was wrong.");
            Assert.AreEqual((double)ed.MidPeripheralClearance, result.SuggestedMidPeriphClearance, 5, "SuggestedMidPeriphClearance was wrong.");
            Assert.AreEqual((double)ed.EdgeLift, result.CP4.Lift, 5, "CP4.Lift was wrong.");
            Assert.AreEqual((double)existingLens.LensDiameter * (double)existingLens.CP1.DiameterMultiplier, (double)result.CP1.SemiChordLength, 5, "CP1.SemiChordLength was wrong.");
            Assert.AreEqual((double)existingLens.LensDiameter * (double)existingLens.CP2.DiameterMultiplier, (double)result.CP2.SemiChordLength, 5, "CP2.SemiChordLength was wrong.");
            Assert.AreEqual((double)existingLens.LensDiameter * (double)existingLens.CP4.DiameterMultiplier, (double)result.CP4.SemiChordLength, 5, "CP4.SemiChordLength was wrong.");
        }

        [TestMethod]
        public void GenerateSemiMeridiansTest()
        {
            LensFitter lf = new LensFitter(_lensRepository);

            EyeData ed = new EyeData
            {
                Meridians = new List<Meridian>()
            };
            for (int i = 0; i < 180; i++)
            {
                ed.Meridians.Add(new Meridian
                {
                    Angle = i,
                    Rho = 1,
                    Points = new List<double>
                    {
                        -13.5,5,-12,-11,-10,-9,-8,-7,-6,-5,-4,-3,-2,-1,0,-1,-2,-3,-4,-5,-6,-7,-8,-9,-10,-11,-12,5,-13.5
                    },
                    Apex = 14
                });
            }

            FieldInfo prop = lf.GetType().GetField("diameter", BindingFlags.NonPublic | BindingFlags.Instance);
            prop.SetValue(lf, 38);

            MethodInfo methodInfo = typeof(LensFitter).GetMethod("GenerateSemiMeridians", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { ed, 38 };
            SemiMeridians resultObj = methodInfo.Invoke(lf, parameters) as SemiMeridians;

            Assert.AreEqual(360, resultObj.Count, "GenerateSemiMeridians did not generate all 360 semiMeridians");
            for (int i = 0; i < 359; i++)
            {
                List<SemiMeridian> sms = resultObj.Where(item => item.Angle == i).ToList();
                Assert.AreNotEqual(0, sms.Count, "No meridian with angle " + i + " was generated.");
                Assert.AreEqual(1, sms.Count, "Two or more meridians with angle " + i + " were generated.");
                SemiMeridian sm = sms[0];
                for (int j = 0; j < 21; j++)
                {
                    List<(double Rho, double zValues)> datapoints = sm.DataPoints.Where(item => (int)item.Item1 == j).ToList();
                    Assert.AreNotEqual(0, datapoints.Count, "No datapoint with Rho " + j + " was found on the meridian with angle " + i + ".");
                    Assert.AreEqual(1, sms.Count, "Two or more datapoint with Rho " + j + " were found on the meridian with angle " + i + ".");
                    Assert.AreEqual(-j, datapoints[0].Item2, 5, "Expected a datapoint (" + j + "," + (-j) + ") but got a datapoint (" + (int)datapoints[0].Item1 + "," + (int)datapoints[0].Item2 + ") instead on meridian with angle" + i + ".");
                }
            }
        }

        [TestMethod]
        public void ThresholdTest()
        {
            LensFitter lf = new LensFitter(_lensRepository);

            LensDetail ld = new LensDetail
            {
                CP2 = new ControlPoint2
                {
                    Threshold = 1000
                },
                CP4 = new ControlPoint4
                {
                    Threshold = 1000
                }
            };

            EyeAnalysisResults ear = new EyeAnalysisResults
            {
                CP2Val = new double[]
                {
                    0,0,0,10
                },
                CP4Val = new double[]
                {
                    0,10,0,0
                }
            };
            MethodInfo methodInfo = typeof(LensFitter).GetMethod("Threshold", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { ear, ld };
            (ControlPointCategories vaultDesign, ControlPointCategories lzDesign) = ((ControlPointCategories vaultDesign, ControlPointCategories lzDesign))methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(ControlPointCategories.quad, vaultDesign);
            Assert.AreEqual(ControlPointCategories.quad, lzDesign);

            ear = new EyeAnalysisResults
            {
                CP2Val = new double[]
                {
                    5,10,5,10
                },
                CP4Val = new double[]
                {
                    20,10,20,10
                }
            };
            parameters[0] = ear;
            (vaultDesign, lzDesign) = ((ControlPointCategories vaultDesign, ControlPointCategories lzDesign))methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(ControlPointCategories.toric, vaultDesign);
            Assert.AreEqual(ControlPointCategories.toric, lzDesign);

            ear = new EyeAnalysisResults
            {
                CP2Val = new[]
                {
                    9.5,10,9.75,10.25
                },
                CP4Val = new[]
                {
                    1,1.5,1.75,1.25
                }
            };
            parameters[0] = ear;
            (vaultDesign, lzDesign) = ((ControlPointCategories vaultDesign, ControlPointCategories lzDesign))methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(ControlPointCategories.sferic, vaultDesign);
            Assert.AreEqual(ControlPointCategories.sferic, lzDesign);
        }

        [TestMethod]
        public void CalculateSettlingTest()
        {
            LensFitter lf = new LensFitter(_lensRepository);
            Dictionary<int, double> inputOutput = new Dictionary<int, double>
            {
                {0,3 },
                {1,2 },
                {2,1 },
                {3,0 },
                {4,-1 },
                {5,-2 },
                {6,-3 },
                {7,-4 },
            };
            SemiMeridians sms = new SemiMeridians();
            foreach (var item in inputOutput)
            {
                sms.Add(new SemiMeridian
                {
                    RhoDistance = 1,
                    Angle = item.Key,
                    DataPoints = new List<(double Rho, double zValues)>
                {
                    (2,item.Key),
                    (3,item.Key),
                    (4,item.Key)
                }
                });
            }

            FieldInfo prop = lf.GetType().GetField("_semiMeridians", BindingFlags.NonPublic | BindingFlags.Instance);
            prop.SetValue(lf, sms);

            MethodInfo methodInfo = typeof(LensFitter).GetMethod("CalculateSettling", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 3, 500 };
            object resultObj = methodInfo.Invoke(lf, parameters);
            foreach (var item in inputOutput)
            {
                Assert.AreEqual(item.Value, ((Dictionary<int, double>)resultObj)[item.Key], 2);
            }
        }

        [TestMethod]
        public void LensAngleFinderTests()
        {
            LensFitter lf = new LensFitter(_lensRepository);
            Dictionary<int, int> testData = new Dictionary<int, int>
            {
                {0,3 },
                {180,3 },
                {1,3 },
                {181,2 },
                {90,4 },
                {270,4 },
                {91,5 },
                {271,4 },
            };
            SemiMeridians sms = new SemiMeridians();
            foreach (var item in testData)
            {
                sms.Add(new SemiMeridian
                {
                    RhoDistance = 1,
                    Angle = item.Key,
                    DataPoints = new List<(double Rho, double zValues)>
                {
                    (2,item.Value),
                    (3,item.Value),
                    (4,item.Value)
                }
                });
            }

            FieldInfo prop = lf.GetType().GetField("_semiMeridians", BindingFlags.NonPublic | BindingFlags.Instance);
            prop.SetValue(lf, sms);

            MethodInfo methodInfo = typeof(LensFitter).GetMethod("LensAngleFinder", BindingFlags.NonPublic | BindingFlags.Instance);
            object[] parameters = { 3.5, ControlPointCategories.sferic };
            object resultObj = methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(1, (int)resultObj);

            parameters[1] = ControlPointCategories.toric;
            resultObj = methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(1, (int)resultObj);

            parameters[1] = ControlPointCategories.quad;
            resultObj = methodInfo.Invoke(lf, parameters);
            Assert.AreEqual(181, (int)resultObj);
        }

        [TestMethod]
        public void InterpolateMeridianTests()
        {
            LensFitter lf = new LensFitter(_lensRepository);
            SemiMeridian meridian = new SemiMeridian
            {
                RhoDistance = 1,
                Angle = 0,
                DataPoints = new List<(double Rho, double zValues)>
                {
                    (0,0),
                    (1,1),
                    (2,2),
                    (3,3)
                }
            };

            MethodInfo methodInfo = typeof(LensFitter).GetMethod("InterpolateMeridian", BindingFlags.NonPublic | BindingFlags.Static);
            object[] parameters = { meridian };
            object resultObj = methodInfo.Invoke(lf, parameters);
            IInterpolation interpolator = (IInterpolation)resultObj;
            Assert.AreEqual(1, interpolator.Interpolate(1));
            Assert.AreEqual(1.5, interpolator.Interpolate(1.5));
            Assert.AreEqual(4, interpolator.Interpolate(4));


            meridian = new SemiMeridian
            {
                RhoDistance = 1,
                Angle = 45,
                DataPoints = new List<(double Rho, double zValues)>
                {
                    (0,0),
                    (1,1),
                    (2,4),
                    (3,9)
                }
            };

            parameters[0] = meridian;
            resultObj = methodInfo.Invoke(lf, parameters);
            interpolator = (IInterpolation)resultObj;
            Assert.AreEqual(1, interpolator.Interpolate(1));
            Assert.AreEqual(2.5, interpolator.Interpolate(1.5));
            Assert.AreEqual(6.5, interpolator.Interpolate(2.5));
            Assert.AreEqual(14, interpolator.Interpolate(4));
        }
    }
}
