﻿using System.IO;

namespace Orbb.BL.Test
{
    public static class TestHelper
    {
        public static string GetBinPath()
        {
            return Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        }

        public static string GetProjectPath()
        {
            string appRoot = GetBinPath();
            DirectoryInfo dir = new DirectoryInfo(appRoot).Parent.Parent.Parent;
            var name = dir.Name;
            return dir.FullName + @"\" + name + @"\";
        }

        /*
        public static string GetTestProjectPath()
        {
            string appRoot = GetBinPath();
            DirectoryInfo dir = new DirectoryInfo(appRoot).Parent.Parent;
            return dir.FullName + @"\";
        }

        public static string GetMainProjectPath()
        {
            string testProjectPath = GetTestProjectPath();
            // Just hope it ends in the standard .Test, lop it off, done.
            string path = testProjectPath.Substring(0, testProjectPath.Length - 6) + @"\";
            return path;
        }

        public static string GetMainProjectBinPath()
        {
            return GetMainProjectPath() + @"\" + "bin" + @"\" +"debug" + @"\";
        }
        */

        public static string GetAdminProjectBinPath()
        {
            DirectoryInfo dir = new DirectoryInfo(GetProjectPath()).Parent.Parent;
            var name = dir.FullName;
            name = name + @"\" + "Orbb.Admin" + @"\" + "bin" + @"\" + "net461" + @"\";
            return name;
        }

    }
}
