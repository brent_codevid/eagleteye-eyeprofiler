﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System.Collections.Generic;
using System.Linq;
using Telerik.JustMock;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class ControlPoint1ValidationTest
    {
        private static Container _container;
        private static ControlPoint1Validation _controlPoint1Validation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _controlPoint1Validation = _container.GetInstance<ControlPoint1Validation>();
        }

        private static ControlPoint1 ControlPoint1DummyGenerator()
        {
            ControlPoint1 dummy = new ControlPoint1
            {
                DisplayCpFormat = "",
                RelativeChordLength = false,
                SemiChordLength = 1,
                SupplierSpecificName = "BC",
                MaxValue = 2,
                MinValue = 1,
                StepSize = 1,
                Lift = 0,
                Correction = 0,
                Category = 1,
                RelativeSag = false,
                MeasurementUnit = MeasurementUnit.BaseCurve,
                DisplayValueFormat = "",
                LensDetails = new List<LensDetail> { new LensDetail { UseCP2ToCP1List = false } }
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _controlPoint1Validation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _controlPoint1Validation.OfEntityType;
            Assert.AreEqual(typeof(ControlPoint1), entityType);
        }

        [TestMethod]
        public void ValidateShouldCallValidateControlPoint1Method()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            Mock.Arrange(() => new ControlPoint1Validation().ValidateControlPoint1(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            ControlPoint1Validation cp1V = Mock.Create(() => new ControlPoint1Validation());
            ValidationRuleResult result = cp1V.Validate(cp, null);

            Mock.Assert(() => cp1V.ValidateControlPoint1(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldCallBaseValidateMethodIfUseCP2ToCP1ListIsFalse()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            Mock.Arrange(() => new ControlPointValidation().ValidateControlPoint(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();
            Mock.Arrange(() => new ControlPoint1Validation().ValidateControlPoint1(cp, null))
                .CallOriginal();

            ControlPoint1Validation cp1V = Mock.Create(() => new ControlPoint1Validation());
            ValidationRuleResult result = cp1V.Validate(cp, null);

            Mock.Assert(() => cp1V.ValidateControlPoint(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldNotCallBaseValidateMethodIfUseCP2ToCP1ListIsTrue()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = true;
            Mock.Arrange(() => new ControlPointValidation().ValidateControlPoint(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string>() });
            Mock.Arrange(() => new ControlPoint1Validation().ValidateControlPoint1(cp, null))
                .CallOriginal();

            ControlPoint1Validation cp1V = Mock.Create(() => new ControlPoint1Validation());
            cp1V.Validate(cp, null);

            Mock.Assert(() => cp1V.ValidateControlPoint(cp, null), Occurs.Never());
        }

        [TestMethod]
        public void ValidateShouldCallBaseValidateDisplayCpFormatIfUseCP2ToCP1ListIsTrue()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = true;
            Mock.Arrange(() => new ControlPointValidation().ValidateDisplayCpFormat(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();
            Mock.Arrange(() => new ControlPoint1Validation().ValidateControlPoint1(cp, null))
                .CallOriginal();

            ControlPoint1Validation cp1V = Mock.Create(() => new ControlPoint1Validation());
            ValidationRuleResult result = cp1V.Validate(cp, null);

            Mock.Assert(() => cp1V.ValidateDisplayCpFormat(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        //We know that we call the base validation methods and include the results so we don't need to redo the tests from ControlPointValidationTest

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            ValidationRuleResult result = _controlPoint1Validation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMultipleLensDetailsAreLinked()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.Add(new LensDetail());
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndMaxValueIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.MaxValue = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_MISSINGMAXVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndMinValueIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.MinValue = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_MISSINGMINVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndStepSizeIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.StepSize = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_MISSINGSTEPSIZE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListAndRelativeChordLengthAreFalseAndSemiChordLengthIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeChordLength = false;
            cp.SemiChordLength = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGSEMICHORDLENGTH", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseC2ToCP1ListIsFalseAndRelativeChordLengthIsTrueAndDiameterMultiplierIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeChordLength = true;
            cp.DiameterMultiplier = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGDIAMETERMULTIPLIER", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndMeasurementUnitIsSagAndCP1SagCP2PercentageIsNull()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.MeasurementUnit = MeasurementUnit.SAG;
            cp.CP1SagCP2Percentage = null;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGCP1SAGCP2PERCENTAGE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndRelativeSagIsTrueAndStandardValuesAreMissing()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeSag = true;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 should be present");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 2;
            cp.StandardVal1 = 1;
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 and 2 should be present if Category is toric");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 4;
            cp.StandardVal3 = 1;
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 through 4 should be present if Category is quad-specific");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfUseCP2ToCP1ListIsFalseAndRelativeSagIsTrueAndStandardValuesArePresent()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeSag = true;
            cp.Category = 1;
            cp.StandardVal1 = 1;
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is sferic and StandardVal1 is present");

            cp.Category = 2;
            cp.StandardVal2 = 1;
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is toric and StandardVal1 and 2 are present");

            cp.Category = 4;
            cp.StandardVal3 = 1;
            cp.StandardVal4 = 1;
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is quad-specific and StandardVal1 through 4 are present");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsUnbalancedAmountOfBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{}{";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain an unbalanced amount of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsOnePairOfBracketsButNotOneOfTheRequiredFields()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain a pair of brackets but no {vars}, {vars:abs} or {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsTwoPairsOfBracketsButNotTheRequiredFields()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{}{}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{name}{}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{var:abs}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsThreeOrMorePairsOfBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{}{}{}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{name}{}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var:abs}{name}{}";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP1VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNoBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains no brackets");

            cp.DisplayValueFormat = "test var name";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains no brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameInBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{name}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name} and no other brackets");

            cp.DisplayValueFormat = "test var {name} test";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsVarInBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{var}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var} and no other brackets");

            cp.DisplayValueFormat = "test {var} name test";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsVarAbsInBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{var:abs}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var:abs} and no other brackets");

            cp.DisplayValueFormat = "test {var:abs} name test";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameAndVarInBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{name}{var}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var} and no other brackets");

            cp.DisplayValueFormat = "test {var} {name} test";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameAndVarAbsInBrackets()
        {
            ControlPoint1 cp = ControlPoint1DummyGenerator();
            cp.DisplayValueFormat = "{name}{var:abs}";
            ValidationRuleResult result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var:abs} and no other brackets");

            cp.DisplayValueFormat = "test {var:abs} {name} test";
            result = _controlPoint1Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var:abs} and no other brackets");
        }
    }
}
