﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System.Collections.Generic;
using System.Linq;
using Telerik.JustMock;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class ControlPoint4ValidationTest
    {
        private static Container _container;
        private static ControlPoint4Validation _controlPoint4Validation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _controlPoint4Validation = _container.GetInstance<ControlPoint4Validation>();
        }

        private static ControlPoint4 ControlPoint4DummyGenerator()
        {
            ControlPoint4 dummy = new ControlPoint4
            {
                DisplayCpFormat = "",
                RelativeChordLength = false,
                SemiChordLength = 1,
                SupplierSpecificName = "BC",
                MaxValue = 2,
                MinValue = 1,
                StepSize = 1,
                Lift = 0,
                Correction = 0,
                Category = 1,
                CalculateLz = false,
                Threshold = 0,
                PositiveName = "",
                NegativeName = "",
                StandardVal1 = 1,
                StandardVal2 = 1,
                StandardVal3 = 1,
                StandardVal4 = 1,
                DisplayLandingZoneStepType = DisplayLandingZoneStepType.Microns,
                LensDetails = new List<LensDetail> { new LensDetail { UseCP2ToCP1List = false } }
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _controlPoint4Validation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _controlPoint4Validation.OfEntityType;
            Assert.AreEqual(typeof(ControlPoint4), entityType);
        }

        [TestMethod]
        public void ValidateShouldCallValidateMethod()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            Mock.Arrange(() => new ControlPoint4Validation().ValidateControlPoint4(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            ControlPoint4Validation cp4V = Mock.Create(() => new ControlPoint4Validation());
            ValidationRuleResult result = cp4V.Validate(cp, null);

            Mock.Assert(() => cp4V.ValidateControlPoint4(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldCallBaseValidateMethod()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            Mock.Arrange(() => new ControlPointValidation().ValidateControlPoint(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();
            Mock.Arrange(() => new ControlPoint4Validation().ValidateControlPoint4(cp, null))
                .CallOriginal();

            ControlPoint4Validation cp4V = Mock.Create(() => new ControlPoint4Validation());
            ValidationRuleResult result = cp4V.Validate(cp, null);

            Mock.Assert(() => cp4V.ValidateControlPoint(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        //We know that we call the base validation methods and include the results so we don't need to redo the tests from ControlPointValidationTest

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            ValidationRuleResult result = _controlPoint4Validation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMultipleLensDetailsAreLinked()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.LensDetails.Add(new LensDetail());
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
        }


        [TestMethod]
        public void ValidateShouldReturnFalseIfPositiveNameContainsUnbalancedAmountOfBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{}{";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain an unbalanced amount of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfPositiveNameContainsOnePairOfBracketsButNotOneOfTheRequiredFields()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain a pair of brackets but no {vars}, {vars:abs} or {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfPositiveNameContainsTwoPairsOfBracketsButNotTheRequiredFields()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{}{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{var}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{var}{var:abs}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfPositiveNameContainsThreeOrMorePairsOfBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{}{}{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{var}{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);

            cp.PositiveName = "{var:abs}{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "PositiveName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_POS", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsNoBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains no brackets");

            cp.PositiveName = "test var name";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains no brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsNameInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{name}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name} and no other brackets");

            cp.PositiveName = "test var {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsVarInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{var}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {var} and no other brackets");

            cp.PositiveName = "test {var} name test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsVarAbsInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{var:abs}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {var:abs} and no other brackets");

            cp.PositiveName = "test {var:abs} name test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsNameAndVarInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{name}{var}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name}, {var} and no other brackets");

            cp.PositiveName = "test {var} {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name}, {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfPositiveNameContainsNameAndVarAbsInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.PositiveName = "{name}{var:abs}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name}, {var:abs} and no other brackets");

            cp.PositiveName = "test {var:abs} {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if PositiveName contains {name}, {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfNegativeNameContainsUnbalancedAmountOfBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{}{";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain an unbalanced amount of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfNegativeNameContainsOnePairOfBracketsButNotOneOfTheRequiredFields()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain a pair of brackets but no {vars}, {vars:abs} or {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfNegativeNameContainsTwoPairsOfBracketsButNotTheRequiredFields()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{}{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{var}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{var}{var:abs}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfNegativeNameContainsThreeOrMorePairsOfBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{}{}{}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{var}{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);

            cp.NegativeName = "{var:abs}{name}{}";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "NegativeName can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_INVALID_FORMAT_NEG", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsNoBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains no brackets");

            cp.NegativeName = "test var name";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains no brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsNameInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{name}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name} and no other brackets");

            cp.NegativeName = "test var {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsVarInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{var}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {var} and no other brackets");

            cp.NegativeName = "test {var} name test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsVarAbsInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{var:abs}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {var:abs} and no other brackets");

            cp.NegativeName = "test {var:abs} name test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsNameAndVarInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{name}{var}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name}, {var} and no other brackets");

            cp.NegativeName = "test {var} {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name}, {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfNegativeNameContainsNameAndVarAbsInBrackets()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.NegativeName = "{name}{var:abs}";
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name}, {var:abs} and no other brackets");

            cp.NegativeName = "test {var:abs} {name} test";
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if NegativeName contains {name}, {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfCalculateLzIsTrueAndLzPercentageIsNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.CalculateLz = true;
            cp.LzPercentage = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGLZPERCENTAGE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfCalculateLzIsFalseAndStandardValuesAreMissing()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.CalculateLz = false;
            cp.StandardVal1 = null;
            cp.StandardVal2 = null;
            cp.StandardVal3 = null;
            cp.StandardVal4 = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 should be present");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 2;
            cp.StandardVal1 = 1;
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 and 2 should be present if Category is toric");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 4;
            cp.StandardVal3 = 1;
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 through 4 should be present if Category is quad-specific");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGSTANDARDVAL", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfCalculateLzIsFalseAndStandardValuesArePresent()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.CalculateLz = false;
            cp.Category = 1;
            cp.StandardVal1 = 1;
            cp.StandardVal2 = null;
            cp.StandardVal3 = null;
            cp.StandardVal4 = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is sferic and StandardVal1 is present");

            cp.Category = 2;
            cp.StandardVal2 = 1;
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is toric and StandardVal1 and 2 are present");

            cp.Category = 4;
            cp.StandardVal3 = 1;
            cp.StandardVal4 = 1;
            result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is quad-specific and StandardVal1 through 4 are present");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMaxValueIsNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.MaxValue = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGMAXVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMinValueIsNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.MinValue = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGMINVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfStepSizeIsNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.StepSize = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP4VM_MISSINGSTEPSIZE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfRelativeChordLengthAreFalseAndSemiChordLengthAreNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeChordLength = false;
            cp.SemiChordLength = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGSEMICHORDLENGTH", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfRelativeChordLengthIsTrueAndDiameterMultiplierIsNull()
        {
            ControlPoint4 cp = ControlPoint4DummyGenerator();
            cp.RelativeChordLength = true;
            cp.DiameterMultiplier = null;
            ValidationRuleResult result = _controlPoint4Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGDIAMETERMULTIPLIER", result.Remarks[0]);
        }
    }
}
