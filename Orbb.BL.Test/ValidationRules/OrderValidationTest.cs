﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System;
using System.Collections.Generic;
using Telerik.JustMock;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class OrderValidationTest
    {
        private static Container _container;
        private static OrderValidation _orderValidation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _orderValidation = _container.GetInstance<OrderValidation>();
        }

        private static Order OrderDummyGenerator()
        {
            Customer dummyCustomer = new Customer
            {
                Id = 1
            };

            LensSupplier dummySupplier = new LensSupplier
            {
                Id = 1
            };

            Order dummy = new Order
            {
                Date = DateTime.Now,
                Status = 1,
                CustomerId = 1,
                Customer = dummyCustomer,
                SupplierId = 1,
                Supplier = dummySupplier,
            };

            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _orderValidation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _orderValidation.OfEntityType;
            Assert.AreEqual(typeof(Order), entityType);
        }

        [TestMethod]
        public void ValidateShouldCallValidateMethod()
        {
            Order ld = OrderDummyGenerator();
            Mock.Arrange(() => new OrderValidation().ValidateOrder(ld, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            OrderValidation ldv = Mock.Create(() => new OrderValidation());
            ValidationRuleResult result = ldv.Validate(ld, null);

            Mock.Assert(() => ldv.ValidateOrder(ld, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new ControlPoint1();
            ValidationRuleResult result = _orderValidation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfStatusIsNotIncludedInEnumOrderStatus()
        {
            Order o = OrderDummyGenerator();
            o.Status = 0;
            ValidationRuleResult result = _orderValidation.Validate(o, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_O_INVALIDSTATUS", result.Remarks[0]);
        }
    }
}
