﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System.Collections.Generic;
using Telerik.JustMock;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class ControlPointValidationTest
    {
        private static Container _container;
        private static ControlPointValidation _controlPointValidation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _controlPointValidation = _container.GetInstance<ControlPointValidation>();
        }

        private static ControlPoint ControlPointDummyGenerator()
        {
            ControlPoint dummy = new ControlPoint
            {
                DisplayCpFormat = "",
                RelativeChordLength = false,
                SemiChordLength = 1,
                SupplierSpecificName = "BC",
                MaxValue = 2,
                MinValue = 1,
                StepSize = 1,
                Lift = 0,
                Correction = 0,
                Category = 1
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _controlPointValidation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _controlPointValidation.OfEntityType;
            Assert.AreEqual(typeof(ControlPoint), entityType);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            ValidationRuleResult result = _controlPointValidation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateDisplayCpFormatMethodShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldCallValidateControlPointMethod()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            Mock.Arrange(() => new ControlPoint1Validation().ValidateControlPoint(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            ControlPointValidation cpV = Mock.Create(() => new ControlPointValidation());
            ValidationRuleResult result = cpV.Validate(cp, null);

            Mock.Assert(() => cpV.ValidateControlPoint(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldCallValidateDisplayCpFormatMethod()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            Mock.Arrange(() => new ControlPointValidation().ValidateDisplayCpFormat(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            Mock.Arrange(() => new ControlPointValidation().ValidateControlPoint(cp, null))
                .CallOriginal();

            ControlPointValidation cpV = Mock.Create(() => new ControlPointValidation());
            ValidationRuleResult result = cpV.ValidateControlPoint(cp, null);

            Mock.Assert(() => cpV.ValidateDisplayCpFormat(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnFalseIfDisplayCpFormatContainsUnbalancedAmountOfBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayCpFormat = "}";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayCpFormat = "{}{";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain an unbalanced amount of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnFalseIfDisplayCpFormatContainsOnePairOfBracketsButNotOneOfTheRequiredFields()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain a pair of brackets but no {vars} or {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnFalseIfDisplayCpFormatContainsTwoPairsOfBracketsButNotTheRequiredFields()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{}{}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain two pairs of brackets but not {vars} and {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayCpFormat = "{vars}{}";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain two pairs of brackets but not {vars} and {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayCpFormat = "{name}{}";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain two pairs of brackets but not {vars} and {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnFalseIfDisplayCpFormatContainsThreeOrMorePairsOfBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{}{}{}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayCpFormat = "{vars}{name}{}";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsFalse(result.Valid, "DisplayCpFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnTrueIfDisplayCpFormatContainsNoBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains no brackets");

            cp.DisplayCpFormat = "test vars name";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains no brackets");
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnTrueIfDisplayCpFormatContainsNameInBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{name}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {name} and no other brackets");

            cp.DisplayCpFormat = "test vars {name} test";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {name} and no other brackets");
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnTrueIfDisplayCpFormatContainsVarsInBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{vars}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {vars} and no other brackets");

            cp.DisplayCpFormat = "test {vars} name test";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {vars} and no other brackets");
        }

        [TestMethod]
        public void ValidateDisplayCpFormatShouldReturnTrueIfDisplayCpFormatContainsNameAndVarsInBrackets()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.DisplayCpFormat = "{name}{vars}";
            ValidationRuleResult result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {name}, {vars} and no other brackets");

            cp.DisplayCpFormat = "test {vars} {name} test";
            result = _controlPointValidation.ValidateDisplayCpFormat(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayCpFormat contains {name}, {vars} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfSemiChordLengthIsNegative()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.SemiChordLength = -1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "SemiChordLength can't be negative");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_SEMICHORDLENGTH", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfSemiChordLengthIsPositive()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.SemiChordLength = 1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validation should be true if SemiChordLength is positive");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMinValueIsLargerThanMaxValue()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.MaxValue = 1;
            cp.MinValue = 2;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MINMAX", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterMinIsEqualToLensDiameterMax()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.MaxValue = 1;
            cp.MinValue = 1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if MaxValue is equal to MinValue");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterMinIsSmallerThanLensDiameterMax()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.MaxValue = 2;
            cp.MinValue = 1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if MaxValue is larger than MinValue");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfStepSizeIsNegative()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.StepSize = -1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_STEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateLensDetailShouldReturnFalseIStepSizeIsZero()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.StepSize = 0;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_STEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfStepSizeIsPositive()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.StepSize = 1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfStepSizeDoesNotSplitIntervalInEqualPieces()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.MaxValue = 2;
            cp.MinValue = 1;
            cp.StepSize = 0.3;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MINMAXSTEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfStepSizeSplitsIntervalInEqualPieces()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.MaxValue = 2;
            cp.MinValue = 1;
            cp.StepSize = 0.5;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfCategoryIsNotIncludedInEnumControlPointCategories()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.Category = 0;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP_INVALIDCATEGORY", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfCategoryIsIncludedInEnumControlPointCategories()
        {
            ControlPoint cp = ControlPointDummyGenerator();
            cp.Category = 1;
            ValidationRuleResult result = _controlPointValidation.Validate(cp, null);
            Assert.IsTrue(result.Valid);
        }
    }
}