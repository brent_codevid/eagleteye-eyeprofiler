﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System.Collections.Generic;
using Telerik.JustMock;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class LensDetailValidationTest
    {
        private static Container _container;
        private static LensDetailValidation _lensDetailValidation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _lensDetailValidation = _container.GetInstance<LensDetailValidation>();
        }

        private static LensDetail LensDetailDummyGenerator()
        {
            ControlPoint1 dummyCP1 = new ControlPoint1
            {
                Id = 1,
                MaxValue = 2,
                MinValue = 1,
                StepSize = 1,
                SemiChordLength = 1,
                Category = 1
            };

            ControlPoint2 dummyCP2 = new ControlPoint2
            {
                Id = 1,
                Category = 1
            };

            ControlPoint4 dummyCP4 = new ControlPoint4
            {
                Id = 1,
                Category = 1
            };

            LensDetail dummy = new LensDetail
            {
                LensDiameter = 1,
                LensDiameterMax = 2,
                LensDiameterMin = 1,
                LensDiameterStepSize = 1,
                LensDiameterPerc = 1,
                Type = 1,
                CP1Id = 1,
                CP1 = dummyCP1,
                CP2Id = 1,
                CP2 = dummyCP2,
                CP4Id = 1,
                CP4 = dummyCP4
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _lensDetailValidation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _lensDetailValidation.OfEntityType;
            Assert.AreEqual(typeof(LensDetail), entityType);
        }

        [TestMethod]
        public void ValidateShouldCallValidateMethod()
        {
            LensDetail ld = LensDetailDummyGenerator();
            Mock.Arrange(() => new LensDetailValidation().ValidateLensDetail(ld, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            var ldv = Mock.Create(() => new LensDetailValidation());
            var result = ldv.Validate(ld, null);

            Mock.Assert(() => ldv.ValidateLensDetail(ld, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            var result = _lensDetailValidation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterAndLensDiameterMaxAreNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid, "LensDiameter and LensDiameterMax can't be null at the same time");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LDVM_LENSDIAMETER", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterAndLensDiameterMinAreNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMin = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid, "LensDiameter and LensDiameterMin can't be null at the same time");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LDVM_LENSDIAMETER", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterAndLensDiameterStepSizeAndLensDiameterPercAreNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterStepSize = null;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid, "LensDiameter, LensDiameterStepSize and LensDiameterPerc can't be null at the same time");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LDVM_LENSDIAMETER", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterIsNotNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = 1;
            ld.LensDiameterMax = null;
            ld.LensDiameterMin = null;
            ld.LensDiameterStepSize = null;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid, "Validation should be true if LensDiameter is not null");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterMinAndLensDiameterMaxAndLensDiameterStepSizeAreNotNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = 1;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid, "Validation should be true if LensDiameterMin, LensDiameterMax and LensDiameterStepSize are not null");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterMinAndLensDiameterMaxAndLensDiameterPercAreNotNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = null;
            ld.LensDiameterPerc = 1;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid, "Validation should be true if LensDiameterMin, LensDiameterMax and LensDiameterPerc are not null");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterMinIsLargerThanLensDiameterMax()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 1;
            ld.LensDiameterMin = 2;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LDVM_MINMAX", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterMinIsEqualToLensDiameterMax()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 1;
            ld.LensDiameterMin = 1;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LDVM_MINMAX", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterMinIsSmallerThanLensDiameterMax()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = 1;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterStepSizeIsNegative()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterStepSize = -1;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_STEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterStepSizeIsZero()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterStepSize = 0;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_STEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterStepSizeIsPositive()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = 1;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfLensDiameterStepSizeDoesNotSplitIntervalInEqualPieces()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = 0.3;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MINMAXSTEP", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensDiameterStepSizeSplitsIntervalInEqualPieces()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.LensDiameter = null;
            ld.LensDiameterMax = 2;
            ld.LensDiameterMin = 1;
            ld.LensDiameterStepSize = 0.5;
            ld.LensDiameterPerc = null;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }



        [TestMethod]
        public void ValidateShouldReturnFalseIfLensTypeIsNotIncludedInEnumLensTypes()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.Type = 0;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_LD_INVALIDTYPE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfLensTypeIsIncludedInEnumLensTypes()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.Type = 1;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }


        [TestMethod]
        public void ValidateShouldReturnFalseIfCP2ToCP1ListIsEmptyAndUseCP2ToCP1ListIsTrue()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.UseCP2ToCP1List = true;
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGCP2TOCP1ITEMS", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfCP2ToCP1ListIsNotEmptyAndUseCP2ToCP1ListIsTrue()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.UseCP2ToCP1List = true;
            ld.CP2ToCP1List.Add(new CP2ToCP1Item());
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfCP2ToCP1ListIsEmptyAndUseCP2ToCP1ListIsFalse()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.UseCP2ToCP1List = false;
            ld.CP2ToCP1List = new List<CP2ToCP1Item>();
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        /*
        [TestMethod]
        public void ValidateLensDetailShouldReturnTrueIfCCP2ToCP1ListIsNotEmptyAndControlPoint1MaxValueIsNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.CP1.MaxValue = null;
            ld.CP2ToCP1List.Add(new CP2ToCP1Item());
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateLensDetailShouldReturnTrueIfCCP2ToCP1ListIsNotEmptyAndControlPoint1MinValueIsNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.CP1.MinValue = null;
            ld.CP2ToCP1List.Add(new CP2ToCP1Item());
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateLensDetailShouldReturnTrueIfCCP2ToCP1ListIsNotEmptyAndControlPoint1StepSizeIsNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.CP1.StepSize = null;
            ld.CP2ToCP1List.Add(new CP2ToCP1Item());
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }

        [TestMethod]
        public void ValidateLensDetailShouldReturnTrueIfCCP2ToCP1ListIsNotEmptyAndControlPoint1SemiChordLengthIsNull()
        {
            LensDetail ld = LensDetailDummyGenerator();
            ld.CP1.SemiChordLength = null;
            ld.CP2ToCP1List.Add(new CP2ToCP1Item());
            var result = _lensDetailValidation.Validate(ld, null);
            Assert.IsTrue(result.Valid);
        }
        */
    }
}
