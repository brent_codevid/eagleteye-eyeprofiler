﻿using AutoMapper;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orbb.BL.Mappings;
using Orbb.BL.ValidationRules;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Orders;
using StructureMap;
using System.Collections.Generic;
using System.Linq;
using Telerik.JustMock;

namespace Orbb.BL.Test.ValidationRules
{
    [TestClass]
    public class ControlPoint2ValidationTest
    {
        private static Container _container;
        private static ControlPoint2Validation _controlPoint2Validation;

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            Config.BasePath = TestHelper.GetAdminProjectBinPath();
            // Add StructureMap dependency injection framework
            _container = new Container();

            // Automapper configuration
            MapperConfiguration config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            _container.Configure(c =>
            {
                c.For(typeof(ICurrent<>)).Use(typeof(SimpleCurrent<>));
                c.For(typeof(ICurrentStorage<>)).Use(typeof(SimpleCurrentStorage<>));
                c.For(typeof(ILogger<>)).Use(typeof(NullLogger<>));
                c.For(typeof(IMapper)).Use(config.CreateMapper());
            });

            Common.Bootstrapper.RegisterWithContainer(_container);
            Bootstrapper.RegisterWithContainer(_container);

            _controlPoint2Validation = _container.GetInstance<ControlPoint2Validation>();
        }

        private static ControlPoint2 ControlPoint2DummyGenerator()
        {
            ControlPoint2 dummy = new ControlPoint2
            {
                DisplayCpFormat = "",
                RelativeChordLength = false,
                SemiChordLength = 1,
                SupplierSpecificName = "BC",
                MaxValue = 2,
                MinValue = 1,
                StepSize = 1,
                Lift = 0,
                Correction = 0,
                Category = 1,
                RelativeSag = false,
                Threshold = 0,
                DisplayValueFormat = "",
                LensDetails = new List<LensDetail> { new LensDetail { UseCP2ToCP1List = false } }
            };
            return dummy;
        }

        [TestMethod]
        public void ValidateGetOperations()
        {
            var operations = _controlPoint2Validation.Operations;
            Assert.AreEqual(2, operations.Count);
            Assert.IsTrue(operations.Contains(EntityOperations.Add));
            Assert.IsTrue(operations.Contains(EntityOperations.Update));
        }

        [TestMethod]
        public void ValidateGetOfEntityType()
        {
            var entityType = _controlPoint2Validation.OfEntityType;
            Assert.AreEqual(typeof(ControlPoint2), entityType);
        }

        [TestMethod]
        public void ValidateShouldCallValidateMethod()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            Mock.Arrange(() => new ControlPoint2Validation().ValidateControlPoint2(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();

            ControlPoint2Validation cp2V = Mock.Create(() => new ControlPoint2Validation());
            ValidationRuleResult result = cp2V.Validate(cp, null);

            Mock.Assert(() => cp2V.ValidateControlPoint2(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        [TestMethod]
        public void ValidateShouldCallBaseValidateMethod()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            Mock.Arrange(() => new ControlPointValidation().ValidateControlPoint(cp, null))
                .Returns(new ValidationRuleResult { Valid = true, Remarks = new List<string> { "#CheckIfIncludedInResult" } })
                .MustBeCalled();
            Mock.Arrange(() => new ControlPoint2Validation().ValidateControlPoint2(cp, null))
                .CallOriginal();

            ControlPoint2Validation cp2V = Mock.Create(() => new ControlPoint2Validation());
            ValidationRuleResult result = cp2V.Validate(cp, null);

            Mock.Assert(() => cp2V.ValidateControlPoint(cp, null));
            Assert.IsTrue(result.Remarks.Contains("#CheckIfIncludedInResult"), "The results from the base methods should be included");
        }

        //We know that we call the base validation methods and include the results so we don't need to redo the tests from ControlPointValidationTest

        [TestMethod]
        public void ValidateShouldReturnFalseIfWrongType()
        {
            IEntity e = new Order();
            ValidationRuleResult result = _controlPoint2Validation.Validate(e, null);
            Assert.IsFalse(result.Valid);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMultipleLensDetailsAreLinked()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.Add(new LensDetail());
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
        }


        [TestMethod]
        public void ValidateShouldReturnFalseIfMaxValueIsNull()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.MaxValue = null;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_MISSINGMAXVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfMinValueIsNull()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.MinValue = null;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_MISSINGMINVALUE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfUseCP2ToCP1ListIsFalseAndStepSizeIsNull()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.StepSize = null;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_MISSINGSTEPSIZE", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfRelativeChordLengthAreFalseAndSemiChordLengthAreNull()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeChordLength = false;
            cp.SemiChordLength = null;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGSEMICHORDLENGTH", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfRelativeChordLengthIsTrueAndDiameterMultiplierIsNull()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.RelativeChordLength = true;
            cp.DiameterMultiplier = null;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid);
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_MISSINGDIAMETERMULTIPLIER", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfRelativeSagIsTrueAndStandardValuesAreMissing()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeSag = true;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 should be present");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 2;
            cp.StandardVal1 = 1;
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 and 2 should be present if Category is toric");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);

            cp.Category = 4;
            cp.StandardVal3 = 1;
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "StandardVal1 through 4 should be present if Category is quad-specific");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CPVM_RELATIVESAGSTANDARDVAL", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfRelativeSagIsTrueAndStandardValuesArePresent()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.LensDetails.FirstOrDefault().UseCP2ToCP1List = false;
            cp.RelativeSag = true;
            cp.Category = 1;
            cp.StandardVal1 = 1;
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is sferic and StandardVal1 is present");

            cp.Category = 2;
            cp.StandardVal2 = 1;
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is toric and StandardVal1 and 2 are present");

            cp.Category = 4;
            cp.StandardVal3 = 1;
            cp.StandardVal4 = 1;
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate is true when Category is quad-specific and StandardVal1 through 4 are present");
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsUnbalancedAmountOfBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain only one bracket");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{}{";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain an unbalanced amount of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsOnePairOfBracketsButNotOneOfTheRequiredFields()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain a pair of brackets but no {vars}, {vars:abs} or {name}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsTwoPairsOfBracketsButNotTheRequiredFields()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{}{}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{name}{}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{var:abs}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain two pairs of brackets but not {name} and {var} or {var:abs}");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnFalseIfDisplayValueFormatContainsThreeOrMorePairsOfBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{}{}{}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var}{name}{}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);

            cp.DisplayValueFormat = "{var:abs}{name}{}";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsFalse(result.Valid, "DisplayValueFormat can't contain three pairs of brackets");
            Assert.AreEqual(1, result.Remarks.Count);
            Assert.AreEqual("VAL_CP2VM_INVALID_FORMAT", result.Remarks[0]);
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNoBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains no brackets");

            cp.DisplayValueFormat = "test var name";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains no brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameInBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{name}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name} and no other brackets");

            cp.DisplayValueFormat = "test var {name} test";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsVarInBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{var}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var} and no other brackets");

            cp.DisplayValueFormat = "test {var} name test";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsVarAbsInBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{var:abs}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var:abs} and no other brackets");

            cp.DisplayValueFormat = "test {var:abs} name test";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {var:abs} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameAndVarInBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{name}{var}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var} and no other brackets");

            cp.DisplayValueFormat = "test {var} {name} test";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var} and no other brackets");
        }

        [TestMethod]
        public void ValidateShouldReturnTrueIfDisplayValueFormatContainsNameAndVarAbsInBrackets()
        {
            ControlPoint2 cp = ControlPoint2DummyGenerator();
            cp.DisplayValueFormat = "{name}{var:abs}";
            ValidationRuleResult result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var:abs} and no other brackets");

            cp.DisplayValueFormat = "test {var:abs} {name} test";
            result = _controlPoint2Validation.Validate(cp, null);
            Assert.IsTrue(result.Valid, "Validate should be true if DisplayValueFormat contains {name}, {var:abs} and no other brackets");
        }
    }
}
