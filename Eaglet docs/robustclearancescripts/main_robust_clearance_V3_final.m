clc
clear

%script to run the function robust_clearance

OZ_diam=9;%mm full chord. Value as given at CP1*2
%parameters as already defined in the current contat lens design page
min_clearance=150;%microns NEW parameter
ap_clearance= 250;%excisting parameter
settling=100;%best would be to take the max number of settling as calculated
maxBC=10; %excisting parameter
stepBC=0.1;%excisting parameter
maxSAG=5000; %excisting parameter
stepSAG=50;%excisting parameter
%
ecc=0;%not in use so far. will be 0 by default
BC=7;%just something. This is calculated previously in CP1
SAG=4300;%calculated with the current algorithms
SAGplus_corr=0;%just a correction factor which gives more freedom when not performing as expected

%%load 360 meridians of the eye up to chord length of CP1
[data, pathname] = uigetfile('*.mat', 'Load ESP MAT file');
[X,Y,Z,SimK,DATA_ALL] = ESP_loader_cleaner_CRT(data, pathname);
F=scatteredInterpolant(X,Y,Z,'linear','none');
Z=F(X,Y);
DIST=7.5;
[X,Y,Z] = Fix_rotation_using_geodesic_distance_BOSTON8(X,Y,Z, DIST);
ind=find(X.^2+Y.^2<=(OZ_diam/2).^2);%trim data to CP1 chord length
X = X(ind);
Y = Y(ind);
Z = Z(ind);
%up to here (almost) everything is already available within Codevid

%% calculate SAG of the BC from CP1
[~,rho]=cart2pol(X,Y);
Z_ctl=BC-sqrt(BC^2-(1-ecc^2)*rho.^2)/(1-ecc^2);
Z_ctl=(Z_ctl*1000)-ap_clearance;%mm to microns

%% residuals
residuals=abs(Z*1000)-Z_ctl;% remove - sign
[res_clearance,location_cone]=min(residuals);%find lowest residual
%here stops the script when no residual is lower than min_clearance

if res_clearance<min_clearance
    [~,chord_min]=cart2pol(X(location_cone),Y(location_cone));
    chord_min=chord_min*2;    
    SAGdiff=min_clearance-res_clearance;
    SAGplus=ceil(SAGdiff/stepSAG)*stepSAG;%round up
    SAGplus=SAGplus+SAGplus_corr;%tweak factor
        
    SAG2=(Z_ctl(location_cone)/1000)-((SAGdiff-SAGplus)/1000);        
    flattestBC=(chord_min^2+4*SAG2^2)/(8*SAG2);%this will align BC as good as possible. Same formula as CP1. The current CP1 formula contains a little mistake. Value after 8* is not correct.
    flattestBC=round(flattestBC/stepBC)*stepBC;
end

%% new value for CP1 will be 
BCnew=flattestBC;
%% new value for CP3 will be
SAGnew=SAG+SAGplus;%here a new lookup through the tables might be required when function is set to list