function [X,Y,Z,SimK,DATA_ALL] = ESP_loader_cleaner_CRT(data, pathname)
% load mat file and height data
[X,Y,Z,T,bisphere,SimK,DATA_ALL] = load_ESP_XYZTB_simK(data, pathname);
%filter 1 removing NANS and strange tangent angles
[X,Y,Z,T,bisphere] = filter1 (X,Y,Z,T,bisphere);
%2nd filter removing large tangent changes (more thn 2 degrees)
Tfilter = filter2(X,Y,T);
%3d filter finding the last local peak
[X,Y,Z,~,~,~] = filter3(Tfilter,X,Y,Z,T,bisphere);