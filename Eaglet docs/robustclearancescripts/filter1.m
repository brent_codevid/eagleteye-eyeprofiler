function [X,Y,Z,T,bisphere] = filter1 (X,Y,Z,T,bisphere)
%first simple filter to remove artefacts
T(T >= 58) = NaN;

%remove NANS unmeasured area
nans=isnan(T);
indnans=find(nans==0);
X=X(indnans);
Y=Y(indnans);
Y=-Y;
Z=Z(indnans);
T=T(indnans);
bisphere=bisphere(indnans);