function  [xr, yr, zr] = Fix_rotation_using_geodesic_distance_BOSTON8(x, y, z, DIST)
    % Author: P. Wachel & D. R. Iskander 01.04.2014
    % tweaked: R. Stortelder 2017
    
    NO_OF_MERID = 8;   %old number 360 but 16 gives better results for me
    NO_OF_PTS = 60;     % number of integration points 50
    
    F = scatteredInterpolant(x,y,z,'linear','none');
    xout = NaN*zeros(NO_OF_MERID,1);
    yout = NaN*zeros(NO_OF_MERID,1);
    zout = NaN*zeros(NO_OF_MERID,1);
    
    Rho     = linspace(0, DIST*1.1, NO_OF_PTS)';
    for i = 1:NO_OF_MERID
       Theta   = (i*360/NO_OF_MERID)*pi/180*ones(NO_OF_PTS, 1);
       [xp,yp] = pol2cart(Theta, Rho);
       zp      = F(xp, yp);
       integ  = cumsum((diff(xp).^2 + diff(yp).^2 + diff(zp).^2).^0.5);
       ind     = find(integ>=DIST,1,'first');
       if not(isempty(ind)),
           xout(i) = xp(ind);
           yout(i) = yp(ind);
           zout(i) = zp(ind); 
       end
    end
    
   %figure(19);plot3(x(1:100:end),y(1:100:end),z(1:100:end),'.');grid on;
   %hold on;plot3(xout,yout,zout,'r.');grid on;   
   Cfs         = [xout yout ones(size(xout))]\zout; % z = Cfs(1) * x + Cfs(2) * y + Cfs(3)
   VN          = [Cfs(1), Cfs(2), -1]'; % Normal vector
   VN          = VN/norm(VN);
   V0          = [0 0 1]';
   Rot_mat     = vrrotvec2mat(vrrotvec(-VN,V0));
   xyz         = [x y z];
   xyzr        = (Rot_mat*xyz')';
   xr          = xyzr(:,1);
   yr          = xyzr(:,2);
   zr          = xyzr(:,3);
   
   %use next lines to show 3d plot of the extrapolated eye. Usefull to see
   %unsmoothed areas where real data and extrapolation is merged
   
   %temp        = (Rot_mat*[xout yout zout]')';
   %figure(20);
   %plot3(xr(1:100:end),yr(1:100:end),zr(1:100:end),'.');
   %plot3(xr,yr,zr);
   %grid on;
   %hold on;
   %plot3(temp(:,1),temp(:,2),temp(:,3),'r.');
   %grid on; 
end
