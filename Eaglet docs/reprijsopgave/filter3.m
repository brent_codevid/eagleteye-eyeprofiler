function [X,Y,Z,T,bisphere, last_peak] = filter3(Tfilter,X,Y,Z,T,bisphere)
NO_OF_MER = 360;
%finds the last local peak to cut off if a peak is found
last_peak = 200*ones(NO_OF_MER,1);
for i=1:360
    [pks,locs]     = findpeaks(Tfilter(i,:));
    if not(isempty(pks))
        while not(isempty(pks(end)) <15)
            locs(end) =[];
            pks(end)  =[]; 
        end
        if not(isempty(locs)),
        last_peak(i) = locs(end);
        end
    end
end

step = 5/length(Tfilter);%400 values between 5 and 10mm.
cut_off = (last_peak.*step)+5;%distance from the apex

ind = 1;
degrees = 0.0087;
[theta_meas,rho_meas] = cart2pol(X,Y);
for i=1:180
    Theta     = i*pi/180;
    Rho       = cut_off(i);
    ind_theta = find(abs(theta_meas-Theta) <degrees);
    ind_rho   = find(rho_meas(ind_theta) <= Rho);
    ind_cut   = ind_theta(ind_rho);
    ind       = [ind ind_cut'];
end

cut_off2 = flipud(cut_off(181:360));
for i=1:180
    Theta     = -i*pi/180;
    Rho       = cut_off2(i);
    ind_theta = find(abs(theta_meas-Theta) <degrees);
    ind_rho   = find(rho_meas(ind_theta) <= Rho);
    ind_cut   = ind_theta(ind_rho);
    ind       = [ind ind_cut'];
end
ind = unique(ind);
X = X(ind);
Y = Y(ind);
Z = Z(ind);
T = T(ind);
bisphere=bisphere(ind);