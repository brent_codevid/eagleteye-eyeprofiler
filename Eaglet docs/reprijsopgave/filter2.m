function Tfilter = filter2(X,Y,T)
%loads the 3d cloud of rough;ly cleand Tangent data. Removes large
%differences (2 degrees or more)
NO_VALUES = 400;
NO_OF_MER = 360;

%interpolate tangent in 3d cloud
FT=scatteredInterpolant(X,Y,T,'linear','none');

rho = linspace(5,10,NO_VALUES)';
Tfilter = NaN*zeros(NO_OF_MER,NO_VALUES);
for i=1:NO_OF_MER
    Theta   = i*pi/180;
    [xp,yp] = pol2cart(Theta, rho);
    zp      = FT(xp, yp);
    if not(isempty(zp)),
    Tfilter(i,:) = zp; 
    end
end

Tfilter_diff = diff(Tfilter,1,2);
Tfilter_diff1 = zeros(360,1);
Tfilter_diff = [Tfilter_diff1 Tfilter_diff];
Tfilter(Tfilter_diff >= 2) = NaN;
nans=isnan(Tfilter);
col_nans = NaN*zeros(NO_OF_MER,1);
for i=1:360
    a = find(nans(i,:),1);
    if not(isempty(a))
    col_nans(i) = a;
    end
end
for i=1:360
    if not(isnan(col_nans(i)))
    Tfilter(i,col_nans(i):end) = NaN;
    end
end