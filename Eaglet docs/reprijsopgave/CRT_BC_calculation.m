function BC = CRT_BC_calculation(K,S,corr)
%K=flattest K value (mm)
%S=subjective spherical equivalent
%mm to DPT = mm*337.5
%0.5 is the CRT constante
%corr=correction factor to flatten the BC
K_dpt = 337.5/K;
BC    = K_dpt-abs(S)-corr;%in DPT