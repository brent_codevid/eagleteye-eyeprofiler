%CRT_fitter
clear
clc

%load data and CRT parameters
[corr,OZ,diamRZD,maxPOWER,maxCYLINDER,S_RZD,T1_RZD,T2_RZD, RZDmin, RZDmax, LZAmin,LZAmax,diams,TLT] =CRT_parameters;
MAXlensSIZE=12;
% load ESP datafile
[data, pathname] = uigetfile('*.mat', 'Load ESP MAT file');
[X,Y,Z,SimK,DATA_ALL] = ESP_loader_cleaner_CRT(data, pathname);
ind=find(X.^2+Y.^2<=(MAXlensSIZE/2).^2);
X = X(ind);
Y = Y(ind);
Z = Z(ind);
F=scatteredInterpolant(X,Y,Z,'linear','linear');%interpolate 3d cloud

%ask subjective refraction
prompt = {'Enter sphere','Enter cylinder','axis'};
dlg_title = 'Rx';
num_lines = 1;
defans    = {'0','0','180'};
answer = inputdlg(prompt,dlg_title,num_lines,defans);
sphere = cell2mat(answer(1));
sphere = str2num(sphere);
cilinder = cell2mat(answer(2));
cilinder = str2num(cilinder);
axis = cell2mat(answer(3));
axis = str2num(axis);

sphere=sphere/(1-0.012*sphere);%assume 12mm vertex
sph_equi=sphere+(cilinder/2);%spheric equivalent

%check if parameter range
issue1=[];issue2=[];issue3=[];
if abs(sph_equi)>abs(maxPOWER)
    issue1='Spheric equivalent higher than S-6';
end
if abs(cilinder)>abs(maxCYLINDER)
    issue2='Cylinder is higher than C-1.75';
end
if abs(sphere)<(abs(cilinder)*2)%not specified in the parameter file yet
    issue3='Cylinder larger than 1/2* of the spherical power';
end

%HVID rule for diameter selection
HVID=DATA_ALL.Parameters;
HVID=HVID.Iris;
HVID=HVID.HVID;
if isnan(HVID)
    HVID=12;
end
if  HVID<diams(1)%Leah Johnsons HVID rule
    diam=10;
elseif (HVID >= diams(1)) && (HVID <= diams(2))
    diam=10.5;
elseif (HVID >= diams(2)) && (HVID <= diams(3))
    diam=11;
elseif (HVID >= diams(3)) && (HVID <= diams(4))
    diam=11.5;
elseif HVID>diams(4)
    diam=12;
end

%chord parameters
diamLZA=diam-0.5;%maybe adjust

%find the flat meridian
flat_axis = lens_rotation_finder(2, diamLZA/2, F);

%%--------------------
%use simK or calculate K from elevation map?
BCdpt = CRT_BC_calculation(SimK,sph_equi,corr);
BCmm  = 337.5/BCdpt;
BCmm  = ceil(BCmm*10)/10; %always round up to 0.1mm
%%--------------------

%find SAGs
%get all data points for the n-semi chords
NO_PTS=100;
MER=4;
MER_Z = NaN*zeros(MER,NO_PTS);
Rho = linspace(0, (diam/2), NO_PTS)';
for i = 1:MER
       Theta   = ((i*360/MER)+flat_axis-90)*pi/180*ones(NO_PTS, 1);
       [xp,yp] = pol2cart(Theta, Rho);
       zp      = F(xp, yp);
       if not(isempty(zp))
           MER_Z(i,:) = zp';
       end
end
MER_Z = abs(MER_Z);

flatMER=mean([MER_Z(1,:);MER_Z(3,:)],1);
steepMER=mean([MER_Z(2,:);MER_Z(4,:)],1);

%% find SAG values for selected control points 
CP1 = OZ/2;%use semi-meridian instead of full-meridian
CP3 = diamRZD/2;
CP4 = diamLZA/2;

CP1 = find(abs(Rho-CP1) <0.05,1);
CP3 = find(abs(Rho-CP3) <0.05,1);
CP4 = find(abs(Rho-CP4) <0.05,1);

CP3_Z = [flatMER(CP3),steepMER(CP3)];
CP4_Z = [flatMER(CP4),steepMER(CP4)];

SAG_8mm = CP3_Z*1000;

%calculate BC based on OZdiam/SAG instead of Sim K
OZ_Z=flatMER(CP1);%now looks to the flat meridian only
BC = (OZ^2+4*OZ_Z^2)/(8*OZ_Z);
BCdpt6 = CRT_BC_calculation(BC,sphere,corr);%061720: sphere instead sph_equi
BCmm6= ceil((337.5/BCdpt6)*10)/10;

%BCtoSAG conversion
SAG_OZ=BCmm6-sqrt(BCmm6^2-(OZ/2)^2);

%
CP3diff = abs(diff(CP3_Z)*1000);
CP4diff = abs(diff(CP4_Z)*1000);

%
if CP3diff < S_RZD%S_RZD should be tweakable
    RZDdesign=0;
end
if CP3diff > S_RZD
    RZDdesign=1;
end
if CP3diff > T1_RZD%T_RZD should be tweakable
    RZDdesign=2;
end
if CP3diff > T2_RZD%T_RZD should be tweakable
    RZDdesign=3;
end

%RZD calculations
[SAG2,n] = min(CP3_Z);
RZD = round((SAG2*1000)+TLT);
RZD = RZD-(SAG_OZ*1000);
corrRZD=0;%correction in microns
RZD=RZD+corrRZD;
RZD = ceil(RZD/25)*25;%make this tweakable. Ceil up or round. As well a SAG correction factor

RZD2= (25*RZDdesign)+RZD;

if RZD2==RZD
    RZD2=[];
end

%LZA calculations
corrLZA_SAG=0;%correction factor in SAG
LZA1 = CP4_Z(n)-CP3_Z(n)+corrLZA_SAG;

LZA1 = round(-atand(LZA1/((diamLZA-diamRZD)/2)));
Slope_f=-LZA1;
corrLZA_TANGENT=0;%correction factor in angles
LZA1=LZA1+corrLZA_TANGENT;

if n==1
    LZA2= CP4_Z(2)-CP3_Z(2);
    LZA2 = round(-atand(LZA2/((diamLZA-diamRZD)/2)));
else
    LZA2= CP4_Z(1)-CP3_Z(1);
    LZA2 = round(-atand(LZA2/((diamLZA-diamRZD)/2)));
end
Slope_s=-LZA2;

if LZA2==LZA1
    LZA2=[];
end

%% not required to output spheric/toric. Just return 1 value if spheric and 2 if toric 
if RZDdesign==0
    designRZD='spheric';
else
    designRZD='toric';
end

if isempty(LZA2)
    designLZA='spheric';
else
    designLZA='toric';
end

S_L1=(SAG_OZ*1000)+RZD;
S_L2=(SAG_OZ*1000)+RZD2;

%print fitting advice
msg{1} = sprintf('CRT fit                                                             ' );
msg{2} = sprintf('BC in mm: %4.2f',BCmm6) ;
msg{3} = sprintf('BC inDPT: %4.1f',BCdpt6) ;
msg{4} = sprintf('RZD design is %s',designRZD);
msg{5} = sprintf('RZDflat:  %4.0f  /  SAG@8mm = %4.0f',RZD,SAG_8mm(1)) ;
msg{6} = sprintf('RZDsteep:  %4.0f /  SAG@8mm = %4.0f',RZD2,SAG_8mm(2));
msg{7} = sprintf('LZA design is %s',designLZA);
msg{8} = sprintf('LZAflat:  %4.0f  /  Slope eye flat = %4.0f',LZA1,Slope_f);
msg{9} = sprintf('LZAsteep:  %4.0f /  Slope eye steep = %4.0f',LZA2,Slope_s) ;
msg{10}= sprintf('TD: %4.1f',diam);

msg{11}= sprintf('SAG OZ+RZDf: %4.0f',S_L1);
msg{12}= sprintf('SAG OZ+RZDs: %4.0f',S_L2);
if any([issue1,issue2,issue3])
msg{11}= sprintf('possible issues:');
msg{12}= sprintf('%s',issue1);
msg{13}= sprintf('%s',issue2);
msg{14}= sprintf('%s',issue3);
end

msgbox(msg, 'DRL First Lens Fit')

%plot flat/steep and difference meridians
diff=(flatMER-steepMER)*1000;
flatMER=flatMER*1000;
steepMER=steepMER*1000;
figure
plot(Rho,-flatMER,Rho,-steepMER)
yyaxis right
plot(Rho,diff)
legend('Flat meridian','Steep meridian','SAG difference')
xlabel('semi-chord (mm)')
yyaxis left
ylabel('SAG (microns)')
yyaxis right
ylabel('SAG difference (microns)') 