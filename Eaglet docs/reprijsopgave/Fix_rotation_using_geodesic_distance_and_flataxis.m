function  [xr, yr, zr,Rot_mat] = Fix_rotation_using_geodesic_distance_and_flataxis(F,x,y,z, DIST,flat_axis)
    % Author: P. Wachel & D. R. Iskander 01.04.2014
    % tweaked: R. Stortelder 2017
    
    NO_OF_MERID = 16;
    
    xout = NaN*zeros(NO_OF_MERID,1);
    yout = NaN*zeros(NO_OF_MERID,1);
    zout = NaN*zeros(NO_OF_MERID,1);
    
    Rho     = DIST/2;
    for i = 1:NO_OF_MERID
       Theta   = ((i*360/NO_OF_MERID)+flat_axis-90)*pi/180;
       [xp,yp] = pol2cart(Theta, Rho);
       zp      = F(xp, yp);
       xout(i) = xp;
       yout(i) = yp;
       zout(i) = zp;
    end
    
   %figure(19);plot3(x(1:100:end),y(1:100:end),z(1:100:end),'.');grid on;
   %hold on;plot3(xout,yout,zout,'r.');grid on;   
   Cfs         = [xout yout ones(size(xout))]\zout; % z = Cfs(1) * x + Cfs(2) * y + Cfs(3)
   VN          = [Cfs(1), Cfs(2), -1]'; % Normal vector
   VN          = VN/norm(VN);
   V0          = [0 0 1]';
   Rot_mat     = vrrotvec2mat(vrrotvec(-VN,V0));
   xyz         = [x y z];
   xyzr        = (Rot_mat*xyz')';
   xr          = xyzr(:,1);
   yr          = xyzr(:,2);
   zr          = xyzr(:,3);
   
   %temp        = (Rot_mat*[xout yout zout]')';
   %figure(20);
   %plot3(xr(1:100:end),yr(1:100:end),zr(1:100:end),'.');
   %plot3(xr,yr,zr);
   %grid on;
   %hold on;
   %plot3(temp(:,1),temp(:,2),temp(:,3),'r.');
   %grid on; 
end
