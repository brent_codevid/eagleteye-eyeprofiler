%general ortho-K fitter
%can handle 3 to 5 control points
%control point1=end of OZ. Most often used to calculate a BC
%control point 3 most often points where lens lands on the eye (end of
%reverser curve)
%control point 4 usually at the end of the landing zone. But might be in
%the middle as well (newer designs like DRL
%control point 4 if cp3 is in the middle of the landing zone CP4 is at the
%end
%control point 5 usually at the total lens diameter to control the edge
%lift

%each control point derives a sag at a certain chord length. Each CP can be
%reduced in microns (tear layer)

%% load lens parameters
%required: 
%chord lengths for CP1, CP3, (3.5), CP4, (CP5)
%clearance for each CP
%although the OZ (chord CP1) might be fixed it could be a number of values
%as well which are pre-defined
[corr,OZ,diamRZD,maxPOWER,maxCYLINDER,S_RZD,T1_RZD,T2_RZD, RZDmin, RZDmax, LZAmin,LZAmax,diams,TLT,BCmin,BCmax,BCstep] =CRT_parameters;%example
[corr,OZ,diam

%% I assume this part is already available
%load eye data
MAXlensSIZE=12;
[data, pathname] = uigetfile('*.mat', 'Load ESP MAT file');
[X,Y,Z,SimK,DATA_ALL] = ESP_loader_cleaner_CRT(data, pathname);
ind=find(X.^2+Y.^2<=(MAXlensSIZE/2).^2);
X = X(ind);
Y = Y(ind);
Z = Z(ind);
F=scatteredInterpolant(X,Y,Z,'linear','linear');%interpolate 3d cloud

%% HVID will be included in the .json. I believe most of the framework is already available within Codevid
%HVID rule for diameter selection
HVID=DATA_ALL.Parameters;
HVID=HVID.Iris;
HVID=HVID.HVID;
if isnan(HVID)
    HVID=12;
end
if  HVID<diams(1)
    diam=10;
elseif (HVID >= diams(1)) && (HVID <= diams(2))
    diam=10.5;
elseif (HVID >= diams(2)) && (HVID <= diams(3))
    diam=11;
elseif (HVID >= diams(3)) && (HVID <= diams(4))
    diam=11.5;
elseif HVID>diams(4)
    diam=12;
end

%find the flat meridian
flat_axis = lens_rotation_finder(2, diam/2, F);

%find SAGs
%get all data points for the n-semi chords
NO_PTS=100;
MER=4;
MER_Z = NaN*zeros(MER,NO_PTS);
Rho = linspace(0, (diam/2), NO_PTS)';
for i = 1:MER
       Theta   = ((i*360/MER)+flat_axis-90)*pi/180*ones(NO_PTS, 1);
       [xp,yp] = pol2cart(Theta, Rho);
       zp      = F(xp, yp);
       if not(isempty(zp))
           MER_Z(i,:) = zp';
       end
end
MER_Z = abs(MER_Z);

flatMER=mean([MER_Z(1,:);MER_Z(3,:)],1);
steepMER=mean([MER_Z(2,:);MER_Z(4,:)],1);

%% The entry values will be requested within the local sw.
[sphere,cilinder,~]=subj_refr;%axis so far is not in use. Values will be given in the .json. 
%Only the lens list should give a sign to the local sw to request them.

vertex=0.012;%allow this value to be adjusted from within .json. Default should be 0.012
sphere=sphere/(1-vertex*sphere);
sph_equiv=sphere+(cilinder/2);%spheric equivalent

%% CP1
%OZ=Optical Zone diameter = chordlength CP1
sphere_rule='sphere';%Most orthoK lenses use only the spheric power. But sometimes spherical equivalent ('sphere equiv')
BC_rule='jessen';%most often the Jessen formula is used. The other option is 'SAG'
BC_zone ='chord';%default 'chord' (use the chord length of the OZ) other option is 'simK'

%simple rules to check if the fit is possible
issues=ortho_k_issues(sph_equi,maxPOWER,maxCYLINDER);%ideally also Codevid generates a warning when exceeding lens limits

[BCmm,BCdpt]=BC_orthoK_calculator(OZ,sphere,sph_equiv,SAGeye,SimK,BCstep,BCmin, BCmax);

%% CP3 Does design a SAG or a reverse curve (RC)
%design= RC or RZD or SAG (RZD is brand naming but will work for us)
designCP3 = 'RC';
CP3_cl = 0.005;%mm


BC_sag=BCmm-sqrt(BCmm^2-OZ^2);%OZ has to be semi-chord length
BC_sag=BC_sag+a_clearance;%make sure to work in mm
CP3_sag=CP3_sag-CP3_cl;%SAG of the eye minus CP3 (mid peripheral) clearance (mm)

if designCP3 ='RC'
    CP3_sag_diff=CP3_sag-BC_sag;
    RC=sqrt(OZ^2+((((CP3_chord^2-OZ^2)+CP3_sag_diff^2)/(2*CP3_sag_diff))^2));
elseif designCP3 = 'RZD'
    CP3_sag_diff=CP3_sag-BC_sag;%Apply step size, lens limits and correct naming (mm/microns etc)
elseif designCP3 = 'SAG'
    %exactly the same as the current algorithm for CP1
end    

%% CP3.5 is always calling for a curve 
CP35='yes'% call this CP or not
if CP35=='yes'
	CP35_cl = 0.00;%mm to be set in the lens menu or to be overruled in .json
    CP35_chord = 
    CP3_sag=CP3_sag-CP3_cl;%SAG of the eye minus CP3 (mid peripheral) clearance (mm)
    CP35_sag=CP35_sag-CP35_cl;

    CP35_sag_diff=CP35_sag-CP3_sag;
    RC35=sqrt(OZ^2+((((CP3_chord^2-OZ^2)+CP35_sag_diff^2)/(2*CP35_sag_diff))^2));%RC4=reverse curve CP35
end  

%% CP4
%Does design a tangent angle or an alignment curve(AC)
designCP4 = 'tangent'% or 'AC' 3rd option will be AAC=alignment asphere curve. Will be later
if designCP4 == 'tangent'
    
elseif designCP4 == 'AC'
    %same method as CP3.5
    


%% CP5 often not in use. Does design an edge clearance

        
