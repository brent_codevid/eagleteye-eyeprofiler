function degrees_angle = lens_rotation_finder(design, diam, F)
%finds the largest difference between orthogonal axis of the eye. Returns
%the angle of the flattest angle (lowest SAG).
%if the design is quad-specific it returns the angle of the lowest
%semi-meridian of the flattest angle found it the previous step
%design = 2 (toric)  4 (quad-specific)
%diam = diameter of the scleral lens
%F=point cloud of the eye
%find largest orthogonal difference
mer = 360;
Rho = diam;%-0.5;%-0.5 is arbitrary, better formula possible?
    
for i = 1:mer
    Theta   = (i*360/mer)*pi/180;
    [xp,yp] = pol2cart(Theta, Rho);
    eye_diff(i)     = F(xp, yp);
end

zp2 = mean([eye_diff(1:180);eye_diff(181:mer)]);
zp3 = [zp2, zp2];

for i=1:180
    degrees_angle(i) = diff([zp3(i),zp3(i+90)]);
end

[~,degrees_angle] =min(degrees_angle);

    if design == 4
        temp = [eye_diff(degrees_angle),eye_diff(degrees_angle+180)];
        [~,temp] = max(temp);%correction to return correct axis
        if temp == 2
            degrees_angle = degrees_angle+180;%correction wrong variable
        end
    end