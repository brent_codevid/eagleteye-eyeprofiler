function [X,Y,Z,T,B,DATA_ALL] = load_ESP_XYZTB
%T=tangent
%B=bisphere
% load ESP datafile
[data, pathname] = uigetfile('*.mat', 'Load ESP MAT file');

DATA_ALL = load([pathname data]);

%sim K parameters
Kflat=DATA_ALL.Parameters;
Kflat=Kflat.SimK;
Kflat=Kflat.Flat;
Kflat=Kflat.Radius;

Ksteep=DATA_ALL.Parameters;
Ksteep=Ksteep.SimK;
Ksteep=Ksteep.Flat;
Ksteep=Ksteep.Radius;
%%
%LOAD SAG data
Height=DATA_ALL.Height;
Z=Height.Data;

%read apex
Apex=Height.Apex;
Apex=double(Apex);

%read scale
Scale=DATA_ALL.Scale;
Scale=double(Scale);

%real size height data
[s1,s2]=size(Z);
s1=double(s1);
s2=double(s2);
xx=((1:s2)-Apex(1))*Scale(1);
yy=((1:s1)-Apex(2))*Scale(2);
[X,Y]=meshgrid(xx,yy);

%%
%load tangent
T=DATA_ALL.TangentAngles;
T=T.Data;

%load bisphere map
B=DATA_ALL.Bisphere;
B=B.Data;
B=B.*1000;%transform to microns