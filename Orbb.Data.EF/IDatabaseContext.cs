﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models.System;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Orbb.Data.EF
{
    public interface IDatabaseContext : IDisposable
    {
        IQueryable Query(Type entityType);
        IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity;
        IQueryable<TResult> QuerySql<TResult>(string query, params object[] parameters);
        void Add<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Add<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity;
        void Update<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Update<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity;
        void Delete<TEntity>(TEntity entity) where TEntity : class, IEntity;
        void Delete<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity;

        SaveResult SaveChanges(ICurrentUser currentUser, ICurrent<DivisionVM> division = null, bool deleteInSaveChanges = true);

        DbEntityEntry Entry(object entity);
        void Clear();
        string GetConnectionString { get; }
    }
}