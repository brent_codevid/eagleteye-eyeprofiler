﻿using System.Collections.Generic;

namespace Orbb.Data.EF.EntityValidation
{
    public class ValidationRuleResult
    {
        public bool Valid { get; set; }
        public List<string> Remarks { get; set; }
    }
}