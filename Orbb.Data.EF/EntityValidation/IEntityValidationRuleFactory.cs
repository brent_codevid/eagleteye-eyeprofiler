﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityValidation
{
    public interface IEntityValidationRuleFactory
    {
        void RegisterRule(IEntityValidationRule rule, Type type);
        List<IEntityValidationRule> GetValidationRules<T>() where T : IEntity, new();

        ValidationRuleResult Validate(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext,
            ICurrentUser currentUser);
    }
}