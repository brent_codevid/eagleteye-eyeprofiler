﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityValidation
{
    public class EntityValidationRuleFactory : IEntityValidationRuleFactory
    {
        private readonly Dictionary<string, List<IEntityValidationRule>> validationRulesDictionary = new Dictionary<string, List<IEntityValidationRule>>();

        public void RegisterRule(IEntityValidationRule rule, Type type)
        {
            if (validationRulesDictionary.ContainsKey(type.FullName))
                validationRulesDictionary[type.FullName].Add(rule);
            else
            {
                var list = new List<IEntityValidationRule> { rule };
                validationRulesDictionary.Add(type.FullName, list);
            }
        }

        public List<IEntityValidationRule> GetValidationRules<T>() where T : IEntity, new()
        {
            if (validationRulesDictionary.ContainsKey(typeof(T).FullName))
            {
                var rules = validationRulesDictionary[typeof(T).FullName];
                return rules;
            }

            return null;
        }

        public ValidationRuleResult Validate(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext, ICurrentUser currentUser)
        {
            var result = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
            if (validationRulesDictionary.ContainsKey(type.FullName))
            {
                var rules = validationRulesDictionary[type.FullName];

                var list = rules as IList<IEntityValidationRule>;
                foreach (var rule in list)
                {
                    var res = ValidateSingle(operation, entity, type, dbContext, currentUser, rule);
                    result.Valid = result.Valid && res.Valid;
                    if (res.Remarks?.Count > 0)
                        result.Remarks.AddRange(res.Remarks);
                }
            }

            return result;
        }

        private ValidationRuleResult ValidateSingle(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext, ICurrentUser currentUser,
            IEntityValidationRule rule)
        {
            if (!rule.Operations.Contains(operation))
                return new ValidationRuleResult { Valid = true };

            return rule.Validate(entity, dbContext);
        }
    }
}
