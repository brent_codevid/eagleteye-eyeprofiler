﻿using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityValidation
{
    public enum EntityOperations
    {
        Add, Update, Delete
    }
    public interface IEntityValidationRule
    {
        List<EntityOperations> Operations { get; }
        Type OfEntityType { get; }
        ValidationRuleResult Validate(IEntity entity, IDatabaseContext context);
    }
}
