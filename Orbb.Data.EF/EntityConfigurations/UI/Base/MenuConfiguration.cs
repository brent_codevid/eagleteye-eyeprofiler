﻿using Orbb.Data.Models.Models.UI.Base;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.UI.Base
{
    public class MenuConfiguration : EntityTypeConfiguration<MenuItem>
    {
        public MenuConfiguration()
        {
            ToTable("Base.MenuItems", "UI");

            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
