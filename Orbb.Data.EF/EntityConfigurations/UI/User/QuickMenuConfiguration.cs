﻿using Orbb.Data.Models.Models.UI.User;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.UI.User
{
    public class UserUISetupConfiguration : EntityTypeConfiguration<UserQuickMenu>
    {
        public UserUISetupConfiguration()
        {
            ToTable("User.QuickMenuItems", "UI");

            HasRequired(p => p.MenuItems);

            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
