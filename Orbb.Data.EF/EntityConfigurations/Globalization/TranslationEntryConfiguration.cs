﻿using Orbb.Data.Models.Models.Globalization;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Globalization
{
    class TranslationEntryConfiguration : EntityTypeConfiguration<TranslationEntry>
    {
        public TranslationEntryConfiguration()
        {
            ToTable("TranslationEntries", "Globalization");
            HasRequired(x => x.Language);
            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
