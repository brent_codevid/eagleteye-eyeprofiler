﻿using Orbb.Data.Models.Models.Globalization;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Globalization
{
    class TranslationConfiguration : EntityTypeConfiguration<Translation>
    {
        public TranslationConfiguration()
        {
            ToTable("Translations", "Globalization");
            Property(x => x.DefaultValue).IsRequired();
            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
