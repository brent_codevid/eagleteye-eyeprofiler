﻿using Orbb.Data.Models.Models.Globalization;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Globalization
{
    class LanguageConfiguration : EntityTypeConfiguration<Language>
    {
        public LanguageConfiguration()
        {
            ToTable("Languages", "Globalization");
            Property(x => x.ShortCode).IsRequired().HasMaxLength(2);
            Property(x => x.Code).IsRequired().HasMaxLength(5);
            Property(x => x.EnglishDescription).IsRequired().HasMaxLength(40);
            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);

            // HasRequired(x => x.Description);
        }
    }
}
