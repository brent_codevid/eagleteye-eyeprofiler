﻿using Orbb.Data.Models.Models.Globalization;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Globalization
{
    class LanguageTranslationConfiguration : EntityTypeConfiguration<LanguageTranslation>
    {
        public LanguageTranslationConfiguration()
        {
            ToTable("LanguageTranslations", "Globalization");
            Property(x => x.ShortCode).IsRequired().HasMaxLength(5);
            Property(x => x.Value).IsRequired();
            Property(x => x.LanguageId).IsRequired();

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
