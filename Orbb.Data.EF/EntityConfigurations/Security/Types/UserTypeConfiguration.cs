﻿using Orbb.Data.Models.Models.Security.Types;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Security.Types
{
    public class UserTypeConfiguration : EntityTypeConfiguration<UserType>
    {
        public UserTypeConfiguration()
        {
            ToTable("UserTypes", "Security");
            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
