﻿using Orbb.Data.Models.Models.Security;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Security
{
    public class UserGroupConfiguration : EntityTypeConfiguration<UserGroup>
    {
        public UserGroupConfiguration()
        {
            ToTable("UserGroups", "Security");
            Property(p => p.Description).IsRequired().HasMaxLength(100);

            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
