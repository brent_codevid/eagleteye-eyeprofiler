﻿using Orbb.Data.Models.Models.Security;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Security
{
    public class SecuredObjectConfiguration : EntityTypeConfiguration<SecuredObject>
    {
        public SecuredObjectConfiguration()
        {
            ToTable("SecuredObject", "Security");
            Property(p => p.Object).IsRequired().HasMaxLength(100);
            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
