﻿using Orbb.Data.Models.Models.Security;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Security
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            ToTable("Users", "Security");
            Property(c => c.UserName).IsRequired().HasMaxLength(60);
            Property(c => c.FirstName).HasMaxLength(30);
            Property(c => c.LastName).HasMaxLength(30);
            Property(c => c.EmailAddress).HasMaxLength(200);
            Property(c => c.CreatedBy).HasMaxLength(60);
            Property(c => c.LastModifiedBy).HasMaxLength(60);
            HasOptional(c => c.UserGroup)
                .WithMany(g => g.Users).WillCascadeOnDelete(false);
            HasOptional(x => x.DefaultDivision)
                .WithMany(x => x.DefaultDivisionUsers).HasForeignKey(x => x.DefaultDivisionId);
            HasMany(c => c.Divisions);
        }
    }
}
