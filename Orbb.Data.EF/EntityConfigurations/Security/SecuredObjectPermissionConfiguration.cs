﻿using Orbb.Data.Models.Models.Security;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Security
{
    public class SecuredObjectPermissionConfiguration : EntityTypeConfiguration<SecuredObjectPermission>
    {
        public SecuredObjectPermissionConfiguration()
        {
            ToTable("SecuredObjectPermission", "Security");
            HasRequired(p => p.SecuredObject);
            Property(p => p.CreatedBy).HasMaxLength(30);
            Property(p => p.LastModifiedBy).HasMaxLength(30);
        }
    }
}
