﻿using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.LensSupplier
{
    public class LensSupplierConfiguration : EntityTypeConfiguration<Models.Models.LensSuppliers.LensSupplier>
    {
        public LensSupplierConfiguration()
        {
            ToTable("LensSuppliers", "LensSuppliers");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
