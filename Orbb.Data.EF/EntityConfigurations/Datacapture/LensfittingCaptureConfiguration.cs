﻿using Orbb.Data.Models.Models.Datacapture;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class LensfittingCaptureConfiguration : EntityTypeConfiguration<LensfittingCapture>
    {
        public LensfittingCaptureConfiguration()
        {
            ToTable("LensfittingCapture", "Datacaptures");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);

        }
    }
}
