﻿using Orbb.Data.Models.Models.Customers;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Customers
{
    public class CustomerConfiguration : EntityTypeConfiguration<Customer>
    {
        public CustomerConfiguration()
        {
            ToTable("Customers", "Customers");
            HasMany(c => c.UsedLenssets).WithMany().Map(m =>
            {
                m.MapLeftKey("CustomerId");
                m.MapRightKey("LenssetId");
                m.ToTable("UsedLenses", "Customers");
            });

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);

        }
    }
}
