﻿using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Base
{
    public class CountryConfiguration : EntityTypeConfiguration<Country>
    {
        public CountryConfiguration()
        {
            ToTable("Countries", "Base");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
            HasMany<LensDetail>(x => x.AvailableLenssets).WithMany(c => c.Countries).Map(cs =>
            {
                cs.MapLeftKey("CountryId");
                cs.MapRightKey("LensDetailId");
                cs.ToTable("LensesForCountry");
            });
        }
    }
}
