﻿using Orbb.Data.Models.Models.Base;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Base
{
    public class CurrencyConfiguration : EntityTypeConfiguration<Currency>
    {
        public CurrencyConfiguration()
        {
            ToTable("Currencies", "Base");
            Property(x => x.Code).IsRequired().HasMaxLength(20);
            HasRequired(x => x.Description);
        }
    }
}