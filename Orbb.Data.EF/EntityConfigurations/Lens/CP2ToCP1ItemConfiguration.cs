﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class CP2ToCP1ItemConfiguration : EntityTypeConfiguration<CP2ToCP1Item>
    {
        public CP2ToCP1ItemConfiguration()
        {
            ToTable("CP2ToCP1Items", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
