﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class LensConfiguration : EntityTypeConfiguration<LensDetail>
    {
        public LensConfiguration()
        {
            ToTable("Lenses", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
