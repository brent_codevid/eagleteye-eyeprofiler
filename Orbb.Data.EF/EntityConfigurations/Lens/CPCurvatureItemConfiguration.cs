﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class CPCurvatureItemConfiguration : EntityTypeConfiguration<CPCurvatureItem>
    {
        public CPCurvatureItemConfiguration()
        {
            ToTable("CPCurvatureItems", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
