﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class CPTangentItemConfiguration : EntityTypeConfiguration<CPTangentItem>
    {
        public CPTangentItemConfiguration()
        {
            ToTable("CPTangentItems", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
