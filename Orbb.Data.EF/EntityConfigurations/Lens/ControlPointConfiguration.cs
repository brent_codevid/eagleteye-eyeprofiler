﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class ControlPointConfiguration : EntityTypeConfiguration<ControlPoint>
    {
        public ControlPointConfiguration()
        {
            ToTable("ControlPoints", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);

        }
    }
}
