﻿using Orbb.Data.Models.Models.Lenssets;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.Lens
{
    public class CPListItemConfiguration : EntityTypeConfiguration<CPListItem>
    {
        public CPListItemConfiguration()
        {
            ToTable("CPListItems", "Lenses");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
