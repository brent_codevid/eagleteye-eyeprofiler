﻿using Orbb.Data.Models.Models.System;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System
{
    class LogItemConfiguration : EntityTypeConfiguration<LogItem>
    {
        public LogItemConfiguration()
        {
            ToTable("LogItems", "System");

            Property(x => x.Action).IsRequired().HasMaxLength(30);
            Property(x => x.ByUser).IsRequired().HasMaxLength(60);
            Property(x => x.Description).IsOptional().HasMaxLength(300);

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
