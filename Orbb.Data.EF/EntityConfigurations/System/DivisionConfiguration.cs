﻿using Orbb.Data.Models.Models.System;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System
{
    class DivisionConfiguration : EntityTypeConfiguration<Division>
    {
        public DivisionConfiguration()
        {
            ToTable("Divisions", "System");

            Property(x => x.Description).IsRequired().HasMaxLength(100);
            Property(x => x.ShortCode).IsRequired().HasMaxLength(10);
            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
