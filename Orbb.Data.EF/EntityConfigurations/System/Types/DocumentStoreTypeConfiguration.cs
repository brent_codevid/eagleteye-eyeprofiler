﻿using Orbb.Data.Models.Models.System.Types;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System.Types
{
    class DocumentStoreTypeConfiguration : EntityTypeConfiguration<DocumentStoreType>
    {
        public DocumentStoreTypeConfiguration()
        {
            ToTable("DocumentStoreTypes", "System.Types");

            Property(x => x.Description).IsRequired().HasMaxLength(30);

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
