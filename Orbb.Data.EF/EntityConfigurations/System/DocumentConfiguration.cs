﻿using Orbb.Data.Models.Models.System;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System
{
    class DocumentConfiguration : EntityTypeConfiguration<Document>
    {
        public DocumentConfiguration()
        {
            ToTable("Documents", "System");

            Property(x => x.From).IsOptional().HasMaxLength(100);
            Property(x => x.To).IsOptional().HasMaxLength(100);
            Property(x => x.Description).IsOptional().HasMaxLength(300);
            Property(x => x.FileName).IsRequired().HasMaxLength(100);
            Property(x => x.Extension).IsRequired().HasMaxLength(20);

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
