﻿using Orbb.Data.Models.Models.System;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System
{
    class InstallationSettingsConfiguration : EntityTypeConfiguration<InstallationSettings>
    {
        public InstallationSettingsConfiguration()
        {
            ToTable("InstallationSettings", "System");

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
