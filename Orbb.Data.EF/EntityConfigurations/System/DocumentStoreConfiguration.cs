﻿using Orbb.Data.Models.Models.System;
using System.Data.Entity.ModelConfiguration;

namespace Orbb.Data.EF.EntityConfigurations.System
{
    class DocumentStoreConfiguration : EntityTypeConfiguration<DocumentStore>
    {
        public DocumentStoreConfiguration()
        {
            ToTable("DocumentStores", "System");

            Property(x => x.Url).IsRequired().HasMaxLength(100);

            Property(x => x.CreatedBy).HasMaxLength(60);
            Property(x => x.LastModifiedBy).HasMaxLength(60);
        }
    }
}
