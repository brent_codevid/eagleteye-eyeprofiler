﻿using Microsoft.Extensions.Logging;
using Orbb.Data.Common;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.EF.EntityTriggers;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.EF.Utility;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Linq;
using System.Reflection;

namespace Orbb.Data.EF
{
    [DbConfigurationType(typeof(DbConfig))]
    public class DatabaseContext : DbContext, IDatabaseContext
    {
        [SetterProperty]
        public ILogger<DatabaseContext> Logger { get; set; }
        [SetterProperty]
        public IEntitySequenceGeneratorFactory SequenceGeneratorFactory { get; set; }
        [SetterProperty]
        public IEntityValidationRuleFactory ValidationRuleFactory { get; set; }
        [SetterProperty]
        public IEntityDeleterFactory DeleterFactory { get; set; }
        [SetterProperty]
        public IEntityTriggerFactory TriggerFactory { get; set; }
        public ICurrent<DivisionVM> CurrentDivision { get; set; }

        public DatabaseContext() : base(DbMigrationConfiguration.ConnectionString)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            Configuration.LazyLoadingEnabled = false;

            var typesToRegister = Assembly.GetExecutingAssembly().GetTypes()
                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                          .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                                                         type.BaseType.GetGenericTypeDefinition() ==
                                                         typeof(EntityTypeConfiguration<>));
            foreach (var configurationInstance in typesToRegister.Select(Activator.CreateInstance))
                modelBuilder.Configurations.Add((dynamic)configurationInstance);

            base.OnModelCreating(modelBuilder);
        }

        public IQueryable Query(Type entityType)
        {
            if (!entityType.GetInterfaces().Contains(typeof(IEntity)))
                throw new ArgumentException("Cannot query a non-entity type", "entityType");
            return Set(entityType);
        }
        public IQueryable<TEntity> Query<TEntity>() where TEntity : class, IEntity
        {
            return Set<TEntity>();
        }
        public IQueryable<TResult> QuerySql<TResult>(string query, params object[] parameters)
        {
            return Database.SqlQuery<TResult>(query, parameters).AsQueryable();
        }
        public void Add<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null)
                throw new ArgumentNullException($"adding entity {typeof(TEntity)} null!");

            var set = Set(entity.GetNonProxiedType());
            set.Add(entity);
            TriggerFactory.Trigger(EntityOperations.Add, entity, entity.GetNonProxiedType(), this);
        }

        public void Add<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
                Add(entity);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            if (entity != null && !Entry(entity).IsAdded())
            {
                Entry(entity).CurrentValues.SetValues(entity);
                Entry(entity).State = EntityState.Modified;
                TriggerFactory.Trigger(EntityOperations.Update, entity, entity.GetNonProxiedType(), this);
            }
        }

        //public void Update<TEntity>(TEntity entity) where TEntity : class, IEntity
        //{
        //    TEntity currentEntity = null;
        //    try
        //    {
        //        currentEntity = GetCurrentEntity(entity);
        //    }
        //    catch (Exception e)
        //    {
        //        if (logger != null)
        //            logger.LogMessage(e.Message);
        //        else
        //        {
        //            Console.WriteLine(e.Message);
        //        }
        //    }
        //    if (currentEntity != null && !Entry(currentEntity).IsAdded())
        //    {
        //        Entry(currentEntity).CurrentValues.SetValues(entity);
        //        Entry(currentEntity).State = EntityState.Modified;
        //        triggerFactory.Trigger(EntityOperations.Update, entity, entity.GetNonProxiedType(), this);
        //    }
        //}

        public void Update<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
                Update(entity);
        }

        private TEntity GetCurrentEntity<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var currentEntity = Set<TEntity>().First(c => c.Id == entity.Id);
            return currentEntity;
        }

        SaveResult IDatabaseContext.SaveChanges(ICurrentUser currentUser, ICurrent<DivisionVM> currentDivision, bool deleteInSaveChanges)
        {
            try
            {
                ChangeTracker.DetectChanges();

                VerifyChangedEntities(currentUser, currentDivision);

                foreach (var entry in ChangeTracker.Entries<IEntity>().Where(x => x.State == EntityState.Added))
                {
                    SequenceGeneratorFactory.GenerateSequence(entry.Entity, entry.Entity.GetNonProxiedType(), this);
                }

                var changedEntities = ChangeTracker.Entries<IEntity>().Where(e => e.State != EntityState.Unchanged).ToList();

                // Do validation
                var validationResult = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
                foreach (var entry in changedEntities)
                {
                    EntityOperations? operation = null;
                    switch (entry.State)
                    {
                        case EntityState.Added:
                            operation = EntityOperations.Add;
                            break;
                        case EntityState.Deleted:
                            operation = EntityOperations.Delete;
                            break;
                        case EntityState.Modified:
                            operation = EntityOperations.Update;
                            break;
                    }
                    if (operation != null)
                    {
                        var validateResult = ValidationRuleFactory.Validate(operation.Value, entry.Entity,
                            entry.Entity.GetNonProxiedType(), this,
                            currentUser);
                        validationResult.Valid = validationResult.Valid && validateResult.Valid;
                        if (validateResult.Remarks?.Count > 0)
                            validationResult.Remarks.AddRange(validateResult.Remarks);

                        if (deleteInSaveChanges && operation == EntityOperations.Delete)
                        {
                            IHasCanDelete deleter = (IHasCanDelete)DeleterFactory.GetEntityDeleter(entry.Entity.GetNonProxiedType());
                            if (deleter != null)
                            {
                                var res = deleter.CanDelete(this, entry.Entity.Id, DeleterFactory, out var reasons);
                                if (!res)
                                {
                                    validateResult.Valid = false;
                                    validateResult.Remarks.AddRange(reasons);
                                    return new SaveResult(false, 0, null, "", validateResult.Remarks);
                                }

                                var beforeDeleter = deleter as IBeforeDeleteOnSaveChanges;
                                if (beforeDeleter != null)
                                {
                                    var beforeDeleteResult = beforeDeleter.BeforeDelete(this, entry.Entity.Id, currentUser, DeleterFactory);
                                    if (!beforeDeleteResult)
                                    {
                                        var validationError =
                                            $"{entry.Entity.GetNonProxiedType().Name} with Id {entry.Entity.Id} cannot be removed";
                                        validateResult.Valid = false;
                                        validateResult.Remarks.Add(validationError);

                                        return new SaveResult(false, 0, null, "", validateResult.Remarks);
                                    }
                                }
                            }
                        }
                    }
                }

                // Verify entities, to prevent unneeded database changes (due to changes done in deleter)
                VerifyChangedEntities(currentUser, currentDivision);

                if (!validationResult.Valid)
                {
                    if (validationResult.Remarks == null)
                        validationResult.Remarks = new List<string>();
                    return new SaveResult(false, 0, null, "", validationResult.Remarks);
                }

                bool saveFailed;
                Exception saveException = null;
                var log = "";
                int returnValue = 0;

                try
                {

                    returnValue = base.SaveChanges();
                    saveFailed = false;
                }
                catch (DbUpdateException ex)
                {
                    saveException =
                        new Exception(
                            "SAVE EXCEPTION" + Environment.NewLine + "Entries: " +
                            String.Join(", ",
                                        ex.Entries.Select(
                                            x => (x.Entity as IEntity).GetNonProxiedType().Name + ": " + (x.Entity as IEntity).Id)),
                            ex);
                    if (ex.Entries.Any())
                    {
                        foreach (var entry in ex.Entries.Where(entry => entry.State != EntityState.Added))
                            entry.OriginalValues.SetValues(entry.GetDatabaseValues());
                    }

                    saveFailed = true;
                }
                catch (OptimisticConcurrencyException ex)
                {
                    saveException = ex;
                    saveFailed = true;

                    // update original values from the database
                    var entry = ex.StateEntries.Single();
                    ((IObjectContextAdapter)this).ObjectContext.Refresh(refreshMode: RefreshMode.StoreWins, entity: entry.Entity);
                }
                if (saveFailed)
                {
                    Logger?.LogError(saveException, saveException.Message);
                }
                var result = new SaveResult(!saveFailed, returnValue, saveFailed ? saveException : null, log);

                return result;

            }
            catch (DbEntityValidationException ex)
            {
                Logger.LogError(ex, ex.Message);
                ICollection<string> validationErrors = new List<string>();

                foreach (var error in ex.EntityValidationErrors)
                {
                    foreach (var detail in error.ValidationErrors)
                    {
                        validationErrors.Add(
                            $"{error.Entry.Entity.GetType()}.{detail.PropertyName}::{detail.ErrorMessage}");
                    }
                }

                var result = new SaveResult(false, 0, ex, string.Empty, validationErrors);
                return result;
            }
            catch (Exception ex)
            {
                Logger.LogError(ex, ex.Message);
                var result = new SaveResult(false, 0, ex);
                return result;
            }
        }

        private void VerifyChangedEntities(ICurrentUser currentUser, ICurrent<DivisionVM> currentDivision)
        {
            foreach (var entry in ChangeTracker.Entries<IEntity>().Where(x => x.State != EntityState.Unchanged && x.State != EntityState.Deleted))
                CheckIfModified(entry);

            if (currentUser?.GetCurrentUser() != null)
            {
                var modifiedList = ChangeTracker.Entries<IEntity>().Where(x => x.State == EntityState.Modified).ToList();
                foreach (var entry in modifiedList)
                {
                    entry.Entity.LastModifiedOn = DateTime.Now;
                    entry.Entity.LastModifiedBy = currentUser.GetCurrentUser().UserName;
                }

                var addedList = ChangeTracker.Entries<IEntity>().Where(x => x.State == EntityState.Added).ToList();
                foreach (var entry in addedList)
                {
                    entry.Entity.CreatedOn = DateTime.Now;
                    entry.Entity.CreatedBy = currentUser.GetCurrentUser().UserName;
                    if (entry.Entity is IHasDivision division && currentDivision?.GetCurrent() != null) // CHECK FOR IHASDIVISIONS????
                    {
                        division.DivisionId = currentDivision.GetCurrent().Id;
                    }
                }
            }
        }

        private void CheckIfModified(DbEntityEntry<IEntity> entry)
        {
            //TODO: does this method work for reference properties?
            if (entry.State != EntityState.Modified) return;
            var orig = entry.OriginalValues;
            var curr = entry.CurrentValues;

            var changed = false;
            foreach (var propertyName in orig.PropertyNames)
            {
                var origValue = orig[propertyName];
                var curValue = curr[propertyName];
                var testsNotChanged = new[]
                    {
                        origValue == curValue,
                        (origValue ?? new object()).Equals(curValue ?? new object()),
                        (origValue is byte[] bytes && bytes.SequenceEqual((byte[]) curValue))
                    };
                if (testsNotChanged.Any(x => x)) continue;
                changed = true;
                break;
            }

            if (!changed)
                entry.State = EntityState.Unchanged;
        }

        public void Delete<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            try
            {
                DeleteInternal(entity);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void DeleteInternal<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            var currentEntity = GetCurrentEntity(entity);

            TriggerFactory.Trigger(EntityOperations.Delete, entity, entity.GetNonProxiedType(), this);
            //Entry(currentEntity).State = EntityState.Deleted;
            if (Entry(currentEntity).IsAdded())
            {
                Detach(currentEntity);
            }
            else
            {
                Set<TEntity>().Remove(currentEntity);
            }
        }

        public void Delete<TEntity>(IList<TEntity> entities) where TEntity : class, IEntity
        {
            foreach (var entity in entities)
                Delete(entity);
        }

        public void Detach<TEntity>(TEntity entity) where TEntity : class, IEntity
        {
            Entry(entity).State = EntityState.Detached;
        }

        public void Clear()
        {
            var usedObjects = ((IObjectContextAdapter)this).ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Added |
                                                                                                  EntityState.Deleted |
                                                                                                  EntityState.Modified |
                                                                                                  EntityState.Unchanged)
                .ToList();
            foreach (var o in usedObjects)
            {
                try
                {

                    Detach(o.Entity as IEntity);
                }
                catch (Exception ex)
                {
                    string s = ex.Message;
                }
            }
        }

        public string GetConnectionString => Database.Connection.ConnectionString;

        public void SetConnectionString(string value)
        {
            Database.Connection.ConnectionString = value;
        }
    }
}
