﻿using Orbb.Data.Models.Interfaces;

namespace Orbb.Data.EF.EntityInitializers
{
    public interface IEntityInitializer<TEntityType> where TEntityType : IEntity
    {
        void InitializeForNew(TEntityType entity, IDatabaseContext dbContext);
    }
}