﻿using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;
using System.Linq;

namespace Orbb.Data.EF.EntityInitializers
{
    public class EntityInitializerBase<T> : IEntityInitializer<T> where T : IEntity
    {
        [SetterProperty]
        public ICurrent<DivisionVM> CurrentDivision { get; set; }
        public virtual void InitializeForNew(T entity, IDatabaseContext dbContext)
        {
            if (typeof(IHasDivisions).IsAssignableFrom(typeof(T)))
            {
                if (!Config.UseDivisions)
                {
                    var defaultDivision = dbContext.Query<Division>().FirstOrDefault(x => x.IsDefault);
                    if (defaultDivision != null)
                        (entity as IHasDivisions).Divisions.Add(defaultDivision);
                }
                else
                {
                    if (CurrentDivision?.GetCurrent() != null)
                    {
                        var divisionId = CurrentDivision.GetCurrent().Id;
                        var div = dbContext.Query<Division>().FirstOrDefault(x => x.Id == divisionId);
                        (entity as IHasDivisions).Divisions.Add(div);
                    }
                }
            }

            if (typeof(IHasDivision).IsAssignableFrom(typeof(T)))
            {
                if (!Config.UseDivisions)
                {
                    var defaultDivision = dbContext.Query<Division>().FirstOrDefault(x => x.IsDefault);
                    if (defaultDivision != null)
                        (entity as IHasDivision).DivisionId = defaultDivision.Id;
                }
                else
                {
                    if (CurrentDivision?.GetCurrent() != null)
                    {
                        var divisionId = CurrentDivision.GetCurrent().Id;
                        (entity as IHasDivision).DivisionId = divisionId;
                    }
                }
            }
        }
    }
}
