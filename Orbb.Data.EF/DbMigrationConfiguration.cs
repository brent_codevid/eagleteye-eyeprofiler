﻿using Orbb.Common.Environment;
using System;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;

namespace Orbb.Data.EF
{
    public class DbMigrationConfiguration : DbMigrationsConfiguration<DatabaseContext>
    {
        public static string connectionString;
        public static string ConnectionString
        {
            get
            {

                if (String.IsNullOrEmpty(connectionString))
                    LoadConnectionString();
                Console.WriteLine("Using connectionstring: " + connectionString);
                return connectionString;
            }
        }

        public DbMigrationConfiguration()
        {
            LoadConnectionString();

            //AutomaticMigrationsEnabled = true;
            //AutomaticMigrationDataLossAllowed = true;
            TargetDatabase = new DbConnectionInfo(ConnectionString, "System.Data.SqlClient"); // use connection string

            //TargetDatabase = new DbConnectionInfo("ConnectionStringName"); // use connection string name in app.config
        }

        public static void LoadConnectionString()
        {
            connectionString = Config.ConnectionString;
        }

        protected override void Seed(DatabaseContext context)
        {
        }
    }
}
