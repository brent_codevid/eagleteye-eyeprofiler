﻿using Orbb.Data.EF.EntityDelete;
using Orbb.Data.EF.EntityTriggers;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.EF.Utility;
using StructureMap;

namespace Orbb.Data.EF
{
    public static class Bootstrapper
    {
        public static void RegisterWithContainer(IContainer container)
        {

            //InitializeDb(container);
            Common.Bootstrapper.RegisterWithContainer(container);

            container.Configure(c =>
            {
                c.For<IDatabaseContext>().Use<DatabaseContext>();

                IEntityInitializerFactory initializerFactory = new EntityInitializerFactory(container);
                c.For<IEntityInitializerFactory>().Use(initializerFactory);
                IEntityDeleterFactory deleterFactory = new EntityDeleteFactory();
                c.For<IEntityDeleterFactory>().Use(deleterFactory);
                IEntitySequenceGeneratorFactory sequenceGeneratorFactory = new EntitySequenceGeneratorFactory(container);
                c.For<IEntitySequenceGeneratorFactory>().Use(sequenceGeneratorFactory);
                IEntityValidationRuleFactory validationFactory = new EntityValidationRuleFactory();
                c.For<IEntityValidationRuleFactory>().Use(validationFactory);
                IEntityTriggerFactory triggerFactory = new EntityTriggerFactory();
                c.For<IEntityTriggerFactory>().Use(triggerFactory);
            });
        }

        private static void InitializeDb(IContainer container)
        {
            using (var db = container.GetInstance<DatabaseContext>())
            {
                db.Database.Initialize(false);
                container.Configure(c => c.For<DatabaseContext>().Use(db));
            }
        }
    }
}
