namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddCustomerCountry : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.CountryLensDetail", newName: "LensDetailCountry");
            DropPrimaryKey("dbo.LensDetailCountry");
            AddColumn("Customers.Customers", "CountryId", c => c.Int(nullable: true));
            AddPrimaryKey("dbo.LensDetailCountry", new[] { "LensDetail_Id", "Country_Id" });
            CreateIndex("Customers.Customers", "CountryId");
            AddForeignKey("Customers.Customers", "CountryId", "Base.Countries", "Id");
        }

        public override void Down()
        {
            DropForeignKey("Customers.Customers", "CountryId", "Base.Countries");
            DropIndex("Customers.Customers", new[] { "CountryId" });
            DropPrimaryKey("dbo.LensDetailCountry");
            DropColumn("Customers.Customers", "CountryId");
            AddPrimaryKey("dbo.LensDetailCountry", new[] { "Country_Id", "LensDetail_Id" });
            RenameTable(name: "dbo.LensDetailCountry", newName: "CountryLensDetail");
        }
    }
}
