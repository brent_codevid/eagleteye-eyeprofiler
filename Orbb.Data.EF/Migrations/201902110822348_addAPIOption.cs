namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class addAPIOption : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "IncludeInApiResult", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "IncludeInApiResult");
        }
    }
}
