namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAddCalculatedCP1SagCP3PercentageToOtherCPs : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "AddCalculatedCP1SagCP3PercentageToOtherCPs", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "AddCalculatedCP1SagCP3PercentageToOtherCPs");
        }
    }
}
