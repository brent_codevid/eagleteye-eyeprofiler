namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixDiameterAndSections : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "CurvatureSectionCount", c => c.Int(nullable: false));
            DropColumn("Lenses.CPCurvatureItems", "Diameter");
        }
        
        public override void Down()
        {
            AddColumn("Lenses.CPCurvatureItems", "Diameter", c => c.Double(nullable: false));
            DropColumn("Lenses.ControlPoints", "CurvatureSectionCount");
        }
    }
}
