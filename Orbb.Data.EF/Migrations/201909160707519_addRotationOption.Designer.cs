// <auto-generated />
namespace Orbb.Data.EF
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class addRotationOption : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(addRotationOption));
        
        string IMigrationMetadata.Id
        {
            get { return "201909160707519_addRotationOption"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
