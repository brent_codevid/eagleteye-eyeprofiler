namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMeridianSelectionProps : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseNewMer1Selector", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "Meridian1Position", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Lenses.Lenses", "Meridian1Position");
            DropColumn("Lenses.Lenses", "UseNewMer1Selector");
        }
    }
}
