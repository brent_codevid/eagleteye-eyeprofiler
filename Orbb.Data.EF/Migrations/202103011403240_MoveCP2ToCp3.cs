namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MoveCP2ToCp3 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [Lenses].[ControlPoints] SET [Discriminator] = 'ControlPoint3' WHERE Discriminator = 'ControlPoint2'");

            RenameColumn(table: "Lenses.Lenses", name: "CP3Id", newName: "CP3IdOLD");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3", newName: "UseCP3OLD");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP3Id", newName: "IX_CP3IdOLD");

            RenameColumn(table: "Lenses.Lenses", name: "CP2Id", newName: "CP3Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP2", newName: "UseCP3");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP2Id", newName: "IX_CP3Id");

            RenameColumn(table: "Lenses.Lenses", name: "CP3IdOLD", newName: "CP2Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3OLD", newName: "UseCP2");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP3IdOLD", newName: "IX_CP2Id");
        }
        
        public override void Down()
        {

        }
    }
}
