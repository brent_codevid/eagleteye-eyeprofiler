namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RestructureTangentList1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "TangentPosition", c => c.Int(nullable: false));
            Sql("UPDATE [Lenses].[ControlPoints] SET [TangentPosition] = 1");
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "TangentPosition");
        }
    }
}
