namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveLensCategoryReferenceFromLensset : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Lenses.ControlPoints", "Category_Id", "Settings.LensCategories");
            DropIndex("Lenses.ControlPoints", new[] { "CategoryId" });
            AddColumn("Lenses.ControlPoints", "Category", c => c.Int(nullable: false));
            Sql("UPDATE Lenses.ControlPoints SET Category = 1 WHERE CategoryId = 1;");
            Sql("UPDATE Lenses.ControlPoints SET Category = 2 WHERE CategoryId = 3;");
            Sql("UPDATE Lenses.ControlPoints SET Category = 4 WHERE CategoryId = 5;");
            DropColumn("Lenses.ControlPoints", "CategoryId");
        }

        public override void Down()
        {
            AddColumn("Lenses.ControlPoints", "CategoryId", c => c.Int(nullable: false));
            Sql("UPDATE Lenses.ControlPoints SET CategoryId = 1 WHERE Category = 1;");
            Sql("UPDATE Lenses.ControlPoints SET CategoryId = 3 WHERE Category = 2;");
            Sql("UPDATE Lenses.ControlPoints SET CategoryId = 5 WHERE Category = 4;");
            DropColumn("Lenses.ControlPoints", "Category");
            CreateIndex("Lenses.ControlPoints", "CategoryId");
            AddForeignKey("Lenses.ControlPoints", "Category_Id", "Settings.LensCategories", "Id");
        }
    }
}
