namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class addNonLineairBool : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "CalculateNonLineair", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "CalculateNonLineair");
        }
    }
}
