namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemovedOrderStatusTable : DbMigration
    {
        public override void Up()
        {
            DropTable("Settings.OrderStatus");
        }

        public override void Down()
        {
            CreateTable(
                "Settings.OrderStatus",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);
        }
    }
}
