namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddListsNonLinearOptions : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Lenses.CPCurvatureItems",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    ControlPoint_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Lenses.ControlPoints", t => t.ControlPoint_Id)
                .Index(t => t.ControlPoint_Id);

            CreateTable(
                "Lenses.CPListItems",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CPValue = c.Double(nullable: false),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    ControlPoint_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Lenses.ControlPoints", t => t.ControlPoint_Id)
                .Index(t => t.ControlPoint_Id);

            CreateTable(
                "Lenses.CPTangentItems",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    ControlPoint_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Lenses.ControlPoints", t => t.ControlPoint_Id)
                .Index(t => t.ControlPoint_Id);

        }

        public override void Down()
        {
            DropForeignKey("Lenses.CPTangentItems", "ControlPoint_Id", "Lenses.ControlPoints");
            DropForeignKey("Lenses.CPListItems", "ControlPoint_Id", "Lenses.ControlPoints");
            DropForeignKey("Lenses.CPCurvatureItems", "ControlPoint_Id", "Lenses.ControlPoints");
            DropIndex("Lenses.CPTangentItems", new[] { "ControlPoint_Id" });
            DropIndex("Lenses.CPListItems", new[] { "ControlPoint_Id" });
            DropIndex("Lenses.CPCurvatureItems", new[] { "ControlPoint_Id" });
            DropTable("Lenses.CPTangentItems");
            DropTable("Lenses.CPListItems");
            DropTable("Lenses.CPCurvatureItems");
        }
    }
}
