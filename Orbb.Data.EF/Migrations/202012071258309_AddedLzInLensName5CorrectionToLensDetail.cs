namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLzInLensName5CorrectionToLensDetail : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "LzInLensName5Correction", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.Lenses", "LzInLensName5Correction");
        }
    }
}
