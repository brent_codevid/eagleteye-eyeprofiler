namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddUseCP3ToCP1Field : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseCP3ToCP1List", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("Lenses.Lenses", "UseCP3ToCP1List");
        }
    }
}
