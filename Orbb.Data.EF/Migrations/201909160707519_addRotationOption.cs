namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addRotationOption : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "RotateEyeDataBeforeFitting", c => c.Boolean(nullable: false));
            Sql("UPDATE [Lenses].[Lenses] SET[RotateEyeDataBeforeFitting] = 1 WHERE Id > 0");
        }
        
        public override void Down()
        {
            DropColumn("Lenses.Lenses", "RotateEyeDataBeforeFitting");
        }
    }
}
