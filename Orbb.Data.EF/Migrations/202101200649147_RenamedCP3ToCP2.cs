namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedCP3ToCP2 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Lenses.CP3ToCP1Items", newName: "CP2ToCP1Items");
            RenameColumn(table: "Lenses.CP2ToCP1Items", name: "CP3Value", newName: "CP2Value");
            RenameColumn(table: "Lenses.Lenses", name: "CP3Id", newName: "CP2Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3ToCP1List", newName: "UseCP2ToCP1List");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3", newName: "UseCP2");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP3Id", newName: "IX_CP2Id");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP3Percentage", newName: "CP1SagCP2Percentage");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP3Correction", newName: "CP1SagCP2Correction");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP3MinValue", newName: "CP1SagCP2MinValue");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP3MaxValue", newName: "CP1SagCP2MaxValue");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP3StepSize", newName: "CP1SagCP2StepSize");
            RenameColumn(table: "Lenses.ControlPoints", name: "AddCalculatedCP1SagCP3PercentageToOtherCPs", newName: "AddCalculatedCP1SagCP2PercentageToOtherCPs");
        }
        
        public override void Down()
        {
            RenameColumn(table: "Lenses.ControlPoints", name: "AddCalculatedCP1SagCP2PercentageToOtherCPs", newName: "AddCalculatedCP1SagCP3PercentageToOtherCPs");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP2StepSize", newName: "CP1SagCP3StepSize");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP2MaxValue", newName: "CP1SagCP3MaxValue");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP2MinValue", newName: "CP1SagCP3MinValue");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP2Correction", newName: "CP1SagCP3Correction");
            RenameColumn(table: "Lenses.ControlPoints", name: "CP1SagCP2Percentage", newName: "CP1SagCP3Percentage");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP2Id", newName: "IX_CP3Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP2", newName: "UseCP3");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP2ToCP1List", newName: "UseCP3ToCP1List");
            RenameColumn(table: "Lenses.Lenses", name: "CP2Id", newName: "CP3Id");
            RenameColumn(table: "Lenses.CP2ToCP1Items", name: "CP2Value", newName: "CP3Value");
            RenameTable(name: "Lenses.CP2ToCP1Items", newName: "CP3ToCP1Items");
        }
    }
}
