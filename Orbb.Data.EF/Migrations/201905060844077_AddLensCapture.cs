namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddLensCapture : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Datacaptures.LensfittingCapture",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    LensDetailId = c.Int(nullable: false),
                    FittedLensName = c.String(),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Lenses.Lenses", t => t.LensDetailId)
                .Index(t => t.LensDetailId);

        }

        public override void Down()
        {
            DropForeignKey("Datacaptures.LensfittingCapture", "LensDetailId", "Lenses.Lenses");
            DropIndex("Datacaptures.LensfittingCapture", new[] { "LensDetailId" });
            DropTable("Datacaptures.LensfittingCapture");
        }
    }
}
