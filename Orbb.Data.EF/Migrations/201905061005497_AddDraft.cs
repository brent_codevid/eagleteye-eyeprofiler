namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddDraft : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "IsDraft", c => c.Boolean(nullable: false));
        }

        public override void Down()
        {
            DropColumn("Lenses.Lenses", "IsDraft");
        }
    }
}
