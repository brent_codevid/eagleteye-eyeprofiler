namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class FixContext : DbMigration
    {
        public override void Up()
        {
            DropIndex("Customers.Customers", new[] { "CountryId" });
            AlterColumn("Customers.Customers", "CountryId", c => c.Int());
            CreateIndex("Customers.Customers", "CountryId");
        }

        public override void Down()
        {
            DropIndex("Customers.Customers", new[] { "CountryId" });
            AlterColumn("Customers.Customers", "CountryId", c => c.Int(nullable: false));
            CreateIndex("Customers.Customers", "CountryId");
        }
    }
}
