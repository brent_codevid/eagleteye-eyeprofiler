namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class addRoundingStrategy : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "RoundingStrategy", c => c.Int(nullable: false));
            Sql("UPDATE [Lenses].[ControlPoints] SET [RoundingStrategy] = 1");
        }

        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "RoundingStrategy");
        }
    }
}
