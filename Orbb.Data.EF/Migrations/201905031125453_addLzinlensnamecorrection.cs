namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class addLzinlensnamecorrection : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "LzInLensName1Correction", c => c.Double());
            AddColumn("Lenses.Lenses", "LzInLensName2Correction", c => c.Double());
            AddColumn("Lenses.Lenses", "LzInLensName3Correction", c => c.Double());
            AddColumn("Lenses.Lenses", "LzInLensName4Correction", c => c.Double());
        }

        public override void Down()
        {
            DropColumn("Lenses.Lenses", "LzInLensName4Correction");
            DropColumn("Lenses.Lenses", "LzInLensName3Correction");
            DropColumn("Lenses.Lenses", "LzInLensName2Correction");
            DropColumn("Lenses.Lenses", "LzInLensName1Correction");
        }
    }
}
