namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixCurvatureItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.CPCurvatureItems", "ExcentricitiesString", c => c.String());
            AddColumn("Lenses.CPCurvatureItems", "RadiiString", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.CPCurvatureItems", "ExcentricitiesString");
            DropColumn("Lenses.CPCurvatureItems", "RadiiString");
        }
    }
}
