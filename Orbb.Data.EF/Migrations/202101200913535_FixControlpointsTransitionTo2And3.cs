namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixControlpointsTransitionTo2And3 : DbMigration
    {
        public override void Up()
        {
            Sql("UPDATE [Lenses].[ControlPoints] SET [Discriminator] = 'ControlPoint2' WHERE Discriminator = 'ControlPoint3'");
            Sql("UPDATE [Lenses].[ControlPoints] SET [Discriminator] = 'ControlPoint3' WHERE Discriminator = 'ControlPoint3_5'");
        }
        
        public override void Down()
        {
            Sql("UPDATE [Lenses].[ControlPoints] SET [Discriminator] = 'ControlPoint3_5' WHERE Discriminator = 'ControlPoint3'");
            Sql("UPDATE [Lenses].[ControlPoints] SET [Discriminator] = 'ControlPoint3' WHERE Discriminator = 'ControlPoint2'");
        }
    }
}
