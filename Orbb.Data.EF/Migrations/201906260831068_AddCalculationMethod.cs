namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddCalculationMethod : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "CalculationMethod", c => c.Int(nullable: false));
            Sql("UPDATE [Lenses].[ControlPoints] SET [CalculationMethod] = 1");
        }

        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "CalculationMethod");
        }
    }
}
