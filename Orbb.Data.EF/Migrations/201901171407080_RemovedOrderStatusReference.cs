namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemovedOrderStatusReference : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Orders.Orders", "StatusId", "Settings.OrderStatus");
            DropIndex("Orders.Orders", new[] { "StatusId" });
            AddColumn("Orders.Orders", "Status", c => c.Int(nullable: false));
            Sql("update Orders.Orders set Status = StatusId;");
            DropColumn("Orders.Orders", "StatusId");
        }

        public override void Down()
        {
            AddColumn("Orders.Orders", "StatusId", c => c.Int(nullable: false));
            Sql("update Orders.Orders set StatusId = Status;");
            DropColumn("Orders.Orders", "Status");
            CreateIndex("Orders.Orders", "StatusId");
            AddForeignKey("Orders.Orders", "StatusId", "Settings.OrderStatus", "Id");
        }
    }
}
