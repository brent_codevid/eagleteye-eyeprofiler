namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RestructureTangentList : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.CPTangentItems", "CPTangentValue", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Lenses.CPTangentItems", "CPTangentValue");
        }
    }
}
