namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class NAME : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.LensDetailCountry", newName: "LensesForCountry");
            RenameColumn(table: "dbo.LensesForCountry", name: "LensDetail_Id", newName: "CountryId");
            RenameColumn(table: "dbo.LensesForCountry", name: "Country_Id", newName: "LensDetailId");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_Country_Id", newName: "IX_LensDetailId");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_LensDetail_Id", newName: "IX_CountryId");
            DropPrimaryKey("dbo.LensesForCountry");
            AddPrimaryKey("dbo.LensesForCountry", new[] { "LensDetailId", "CountryId" });
        }

        public override void Down()
        {
            DropPrimaryKey("dbo.LensesForCountry");
            AddPrimaryKey("dbo.LensesForCountry", new[] { "LensDetail_Id", "Country_Id" });
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_CountryId", newName: "IX_LensDetail_Id");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_LensDetailId", newName: "IX_Country_Id");
            RenameColumn(table: "dbo.LensesForCountry", name: "LensDetailId", newName: "Country_Id");
            RenameColumn(table: "dbo.LensesForCountry", name: "CountryId", newName: "LensDetail_Id");
            RenameTable(name: "dbo.LensesForCountry", newName: "LensDetailCountry");
        }
    }
}
