namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNameToCurvature : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.CPCurvatureItems", "SupplierSpecificName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.CPCurvatureItems", "SupplierSpecificName");
        }
    }
}
