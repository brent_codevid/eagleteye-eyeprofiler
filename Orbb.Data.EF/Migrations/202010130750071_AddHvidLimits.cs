namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHvidLimits : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseHvidForDiameters", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "HvidVal10", c => c.Double(nullable: false));
            AddColumn("Lenses.Lenses", "HvidVal105", c => c.Double(nullable: false));
            AddColumn("Lenses.Lenses", "HvidVal11", c => c.Double(nullable: false));
            AddColumn("Lenses.Lenses", "HvidVal115", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Lenses.Lenses", "HvidVal115");
            DropColumn("Lenses.Lenses", "HvidVal11");
            DropColumn("Lenses.Lenses", "HvidVal105");
            DropColumn("Lenses.Lenses", "HvidVal10");
            DropColumn("Lenses.Lenses", "UseHvidForDiameters");
        }
    }
}
