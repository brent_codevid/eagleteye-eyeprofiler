namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddOptionalControlPointsToModel : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseCP1", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "UseCP3", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "UseCP4", c => c.Boolean(nullable: false));
            Sql("UPDATE [Lenses].[Lenses] SET [UseCP1] = 1 ,[UseCP3] = 1 ,[UseCP4] = 1");
        }

        public override void Down()
        {
            DropColumn("Lenses.Lenses", "UseCP4");
            DropColumn("Lenses.Lenses", "UseCP3");
            DropColumn("Lenses.Lenses", "UseCP1");
        }
    }
}
