namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addCp1CorrectionAttributes : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "CP1SagCP3Correction", c => c.Double());
            AddColumn("Lenses.ControlPoints", "CP1SagCP3MinValue", c => c.Double());
            AddColumn("Lenses.ControlPoints", "CP1SagCP3MaxValue", c => c.Double());
            AddColumn("Lenses.ControlPoints", "CP1SagCP3StepSize", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "CP1SagCP3StepSize");
            DropColumn("Lenses.ControlPoints", "CP1SagCP3MaxValue");
            DropColumn("Lenses.ControlPoints", "CP1SagCP3MinValue");
            DropColumn("Lenses.ControlPoints", "CP1SagCP3Correction");
        }
    }
}
