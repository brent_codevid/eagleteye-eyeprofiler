namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddedDisplayLandingZoneFields : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "DisplayLandingZoneStepType", c => c.Int());
            AddColumn("Lenses.ControlPoints", "DisplayLandingZoneCenter", c => c.Int());
            AddColumn("Lenses.ControlPoints", "DisplayLandingZonePlusExtrema", c => c.Int());
            AddColumn("Lenses.ControlPoints", "DisplayLandingZoneMinExtrema", c => c.Int());
        }

        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "DisplayLandingZoneMinExtrema");
            DropColumn("Lenses.ControlPoints", "DisplayLandingZonePlusExtrema");
            DropColumn("Lenses.ControlPoints", "DisplayLandingZoneCenter");
            DropColumn("Lenses.ControlPoints", "DisplayLandingZoneStepType");
        }
    }
}
