// <auto-generated />
namespace Orbb.Data.EF
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddLensCapture : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddLensCapture));
        
        string IMigrationMetadata.Id
        {
            get { return "201905060844077_AddLensCapture"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
