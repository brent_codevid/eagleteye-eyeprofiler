namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCP35 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseCP3_5", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "CP3_5Id", c => c.Int());
            AddColumn("Lenses.ControlPoints", "IncreaseClearance1", c => c.Boolean());
            AddColumn("Lenses.ControlPoints", "MinimalClearance1", c => c.Double());
            AddColumn("Lenses.ControlPoints", "SagCorrectionClearance1", c => c.Double());
            CreateIndex("Lenses.Lenses", "CP3_5Id");
            AddForeignKey("Lenses.Lenses", "CP3_5Id", "Lenses.ControlPoints", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Lenses.Lenses", "CP3_5Id", "Lenses.ControlPoints");
            DropIndex("Lenses.Lenses", new[] { "CP3_5Id" });
            DropColumn("Lenses.ControlPoints", "SagCorrectionClearance1");
            DropColumn("Lenses.ControlPoints", "MinimalClearance1");
            DropColumn("Lenses.ControlPoints", "IncreaseClearance1");
            DropColumn("Lenses.Lenses", "CP3_5Id");
            DropColumn("Lenses.Lenses", "UseCP3_5");
        }
    }
}
