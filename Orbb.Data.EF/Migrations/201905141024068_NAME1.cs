namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class NAME1 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.LensesForCountry", name: "LensDetailId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.LensesForCountry", name: "CountryId", newName: "LensDetailId");
            RenameColumn(table: "dbo.LensesForCountry", name: "__mig_tmp__0", newName: "CountryId");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_LensDetailId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_CountryId", newName: "IX_LensDetailId");
            RenameIndex(table: "dbo.LensesForCountry", name: "__mig_tmp__0", newName: "IX_CountryId");
        }

        public override void Down()
        {
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_CountryId", newName: "__mig_tmp__0");
            RenameIndex(table: "dbo.LensesForCountry", name: "IX_LensDetailId", newName: "IX_CountryId");
            RenameIndex(table: "dbo.LensesForCountry", name: "__mig_tmp__0", newName: "IX_LensDetailId");
            RenameColumn(table: "dbo.LensesForCountry", name: "CountryId", newName: "__mig_tmp__0");
            RenameColumn(table: "dbo.LensesForCountry", name: "LensDetailId", newName: "CountryId");
            RenameColumn(table: "dbo.LensesForCountry", name: "__mig_tmp__0", newName: "LensDetailId");
        }
    }
}
