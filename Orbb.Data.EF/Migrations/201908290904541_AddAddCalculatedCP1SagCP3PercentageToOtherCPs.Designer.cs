// <auto-generated />
namespace Orbb.Data.EF
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddAddCalculatedCP1SagCP3PercentageToOtherCPs : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddAddCalculatedCP1SagCP3PercentageToOtherCPs));
        
        string IMigrationMetadata.Id
        {
            get { return "201908290904541_AddAddCalculatedCP1SagCP3PercentageToOtherCPs"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
