namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenamedCP3_5ToCP3 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "Lenses.Lenses", name: "CP3_5Id", newName: "CP3Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3_5", newName: "UseCP3");
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP3_5Id", newName: "IX_CP3Id");
        }
        
        public override void Down()
        {
            RenameIndex(table: "Lenses.Lenses", name: "IX_CP3Id", newName: "IX_CP3_5Id");
            RenameColumn(table: "Lenses.Lenses", name: "UseCP3", newName: "UseCP3_5");
            RenameColumn(table: "Lenses.Lenses", name: "CP3Id", newName: "CP3_5Id");
        }
    }
}
