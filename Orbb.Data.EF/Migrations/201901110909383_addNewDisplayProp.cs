namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class addNewDisplayProp : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "DisplayValueFormat", c => c.String());
            AddColumn("Lenses.ControlPoints", "DisplayCpFormat", c => c.String());
            DropColumn("Lenses.ControlPoints", "Name");
        }

        public override void Down()
        {
            AddColumn("Lenses.ControlPoints", "Name", c => c.String());
            DropColumn("Lenses.ControlPoints", "DisplayCpFormat");
            DropColumn("Lenses.ControlPoints", "DisplayValueFormat");
        }
    }
}
