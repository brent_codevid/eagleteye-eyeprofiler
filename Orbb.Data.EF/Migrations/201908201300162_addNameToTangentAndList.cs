namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addNameToTangentAndList : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.CPListItems", "SupplierSpecificName", c => c.String());
            AddColumn("Lenses.CPTangentItems", "SupplierSpecificName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.CPTangentItems", "SupplierSpecificName");
            DropColumn("Lenses.CPListItems", "SupplierSpecificName");
        }
    }
}
