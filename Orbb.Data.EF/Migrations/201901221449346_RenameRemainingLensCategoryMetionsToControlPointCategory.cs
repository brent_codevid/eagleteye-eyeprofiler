namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RenameRemainingLensCategoryMetionsToControlPointCategory : DbMigration
    {
        public override void Up()
        {
            Sql("update Globalization.Translations set DefaultValue = 'controlpoint categories' where DefaultValue like 'lens categories';");
            Sql("update Globalization.TranslationEntries set Value = 'Controlpoint Categories' where Value like 'Lens Categories';");
            Sql("update Globalization.TranslationEntries set Value = 'Controlepunt Categorie�n' where Value like 'Lenscategorie�n';");
            Sql("update Globalization.TranslationEntries set Value = 'Cat�gories de points de contr�le' where Value like 'Cat�gories de lentilles';");
            Sql("update Security.SecuredObject set Object = 'CONTROLPOINT CATEGORIES' where Object like 'LENS CATEGORIES';");
        }

        public override void Down()
        {
            Sql("update Globalization.Translations set DefaultValue = 'lens categories' where DefaultValue like 'controlpoint categories';");
            Sql("update Globalization.TranslationEntries set Value = 'Lens Categories' where Value like 'Controlpoint Categories';");
            Sql("update Globalization.TranslationEntries set Value = 'Lenscategorie�n' where Value like 'Controlepunt Categorie�n';");
            Sql("update Globalization.TranslationEntries set Value = 'Cat�gories de lentilles' where Value like 'Cat�gories de points de contr�le';");
            Sql("update Security.SecuredObject set Object = 'LENS CATEGORIES' where Object like 'CONTROLPOINT CATEGORIES';");
        }
    }
}
