namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveLensTypeTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Lenses.Lenses", "Type_Id", "Settings.LensTypes");
            DropIndex("Lenses.Lenses", new[] { "TypeId" });
            AddColumn("Lenses.Lenses", "Type", c => c.Int(nullable: false));
            Sql("update Lenses.Lenses set Type = 1;");
            DropColumn("Lenses.Lenses", "TypeId");
        }

        public override void Down()
        {
            AddColumn("Lenses.Lenses", "TypeId", c => c.Int());
            Sql("update Lenses.Lenses set TypeId = 3;");
            DropColumn("Lenses.Lenses", "Type");
            CreateIndex("Lenses.Lenses", "TypeId");
            AddForeignKey("Lenses.Lenses", "Type_Id", "Settings.LensTypes", "Id");
        }
    }
}
