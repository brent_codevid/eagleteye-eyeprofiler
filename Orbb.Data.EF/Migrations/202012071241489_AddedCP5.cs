namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCP5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.Lenses", "UseCP5", c => c.Boolean(nullable: false));
            AddColumn("Lenses.Lenses", "CP5Id", c => c.Int());
            AddColumn("Lenses.ControlPoints", "IncreaseClearance2", c => c.Boolean());
            AddColumn("Lenses.ControlPoints", "MinimalClearance2", c => c.Double());
            AddColumn("Lenses.ControlPoints", "SagCorrectionClearance2", c => c.Double());
            AddColumn("Lenses.ControlPoints", "EdgeLift", c => c.Double());
            CreateIndex("Lenses.Lenses", "CP5Id");
            AddForeignKey("Lenses.Lenses", "CP5Id", "Lenses.ControlPoints", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("Lenses.Lenses", "CP5Id", "Lenses.ControlPoints");
            DropIndex("Lenses.Lenses", new[] { "CP5Id" });
            DropColumn("Lenses.ControlPoints", "EdgeLift");
            DropColumn("Lenses.ControlPoints", "SagCorrectionClearance2");
            DropColumn("Lenses.ControlPoints", "MinimalClearance2");
            DropColumn("Lenses.ControlPoints", "IncreaseClearance2");
            DropColumn("Lenses.Lenses", "CP5Id");
            DropColumn("Lenses.Lenses", "UseCP5");
        }
    }
}
