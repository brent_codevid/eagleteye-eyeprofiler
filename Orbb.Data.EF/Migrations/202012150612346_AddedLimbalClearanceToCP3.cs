namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedLimbalClearanceToCP3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "LimbalClearance", c => c.Double());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "LimbalClearance");
        }
    }
}
