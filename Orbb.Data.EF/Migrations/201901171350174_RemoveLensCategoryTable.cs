namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveLensCategoryTable : DbMigration
    {
        public override void Up()
        {
            DropTable("Settings.LensCategories");
        }

        public override void Down()
        {
            CreateTable(
                "Settings.LensCategories",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Name = c.String(),
                    NumberOfAxes = c.Int(nullable: false),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                })
                .PrimaryKey(t => t.Id);

        }
    }
}
