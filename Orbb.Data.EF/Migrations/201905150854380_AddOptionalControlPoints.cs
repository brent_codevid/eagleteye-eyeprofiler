namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddOptionalControlPoints : DbMigration
    {
        public override void Up()
        {
            DropIndex("Lenses.Lenses", new[] { "CP1Id" });
            DropIndex("Lenses.Lenses", new[] { "CP3Id" });
            DropIndex("Lenses.Lenses", new[] { "CP4Id" });
            AlterColumn("Lenses.Lenses", "CP1Id", c => c.Int());
            AlterColumn("Lenses.Lenses", "CP3Id", c => c.Int());
            AlterColumn("Lenses.Lenses", "CP4Id", c => c.Int());
            CreateIndex("Lenses.Lenses", "CP1Id");
            CreateIndex("Lenses.Lenses", "CP3Id");
            CreateIndex("Lenses.Lenses", "CP4Id");
        }

        public override void Down()
        {
            DropIndex("Lenses.Lenses", new[] { "CP4Id" });
            DropIndex("Lenses.Lenses", new[] { "CP3Id" });
            DropIndex("Lenses.Lenses", new[] { "CP1Id" });
            AlterColumn("Lenses.Lenses", "CP4Id", c => c.Int(nullable: false));
            AlterColumn("Lenses.Lenses", "CP3Id", c => c.Int(nullable: false));
            AlterColumn("Lenses.Lenses", "CP1Id", c => c.Int(nullable: false));
            CreateIndex("Lenses.Lenses", "CP4Id");
            CreateIndex("Lenses.Lenses", "CP3Id");
            CreateIndex("Lenses.Lenses", "CP1Id");
        }
    }
}
