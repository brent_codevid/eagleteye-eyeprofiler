namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveNews : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Media", "NewsDetail_Id", "Base.News");
            DropForeignKey("Base.News", "NewsText_Id", "Globalization.Translations");
            DropForeignKey("Base.News", "NewsTitle_Id", "Globalization.Translations");
            DropIndex("dbo.Media", new[] { "NewsDetail_Id" });
            DropIndex("Base.News", new[] { "NewsText_Id" });
            DropIndex("Base.News", new[] { "NewsTitle_Id" });
            DropColumn("dbo.Media", "NewsDetail_Id");
            DropTable("Base.News");
        }

        public override void Down()
        {
            CreateTable(
                "Base.News",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Author = c.String(),
                    AuthorCredentials = c.String(),
                    DisplayDate = c.DateTime(nullable: false),
                    Location = c.String(),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    DisplayOrder = c.Int(nullable: false),
                    NewsText_Id = c.Int(),
                    NewsTitle_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            AddColumn("dbo.Media", "NewsDetail_Id", c => c.Int());
            CreateIndex("Base.News", "NewsTitle_Id");
            CreateIndex("Base.News", "NewsText_Id");
            CreateIndex("dbo.Media", "NewsDetail_Id");
            AddForeignKey("Base.News", "NewsTitle_Id", "Globalization.Translations", "Id");
            AddForeignKey("Base.News", "NewsText_Id", "Globalization.Translations", "Id");
            AddForeignKey("dbo.Media", "NewsDetail_Id", "Base.News", "Id");
        }
    }
}
