namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveEvents : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Base.Events", "EventInfo_Id", "Globalization.Translations");
            DropForeignKey("Base.Events", "EventTitle_Id", "Globalization.Translations");
            DropForeignKey("dbo.Media", "EventDetail_Id", "Base.Events");
            DropIndex("Base.Events", new[] { "EventInfo_Id" });
            DropIndex("Base.Events", new[] { "EventTitle_Id" });
            DropIndex("dbo.Media", new[] { "EventDetail_Id" });
            DropTable("Base.Events");
            DropTable("dbo.Media");
        }

        public override void Down()
        {
            CreateTable(
                "dbo.Media",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Filename = c.String(),
                    DisplayOrder = c.Int(nullable: false),
                    Type = c.Int(nullable: false),
                    MimeType = c.String(),
                    CreatedBy = c.String(),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(),
                    LastModifiedOn = c.DateTime(),
                    EventDetail_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                "Base.Events",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Location = c.String(),
                    DisplayOrder = c.Int(nullable: false),
                    RegistrationMandatory = c.Boolean(nullable: false),
                    FinalRegistrationDate = c.DateTime(),
                    RegistrationUrl = c.String(),
                    MoreInfoUrl = c.String(),
                    StartDate = c.DateTime(),
                    EndDate = c.DateTime(),
                    Price = c.String(),
                    Type = c.Int(nullable: false),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    EventInfo_Id = c.Int(),
                    EventTitle_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            CreateIndex("dbo.Media", "EventDetail_Id");
            CreateIndex("Base.Events", "EventTitle_Id");
            CreateIndex("Base.Events", "EventInfo_Id");
            AddForeignKey("dbo.Media", "EventDetail_Id", "Base.Events", "Id");
            AddForeignKey("Base.Events", "EventTitle_Id", "Globalization.Translations", "Id");
            AddForeignKey("Base.Events", "EventInfo_Id", "Globalization.Translations", "Id");
        }
    }
}
