namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class AddCountries : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Base.Countries", "Description_Id", "Globalization.Translations");
            DropIndex("Base.Countries", new[] { "Description_Id" });
            CreateTable(
                "dbo.CountryLensDetail",
                c => new
                {
                    Country_Id = c.Int(nullable: false),
                    LensDetail_Id = c.Int(nullable: false),
                })
                .PrimaryKey(t => new { t.Country_Id, t.LensDetail_Id })
                .ForeignKey("Base.Countries", t => t.Country_Id, cascadeDelete: true)
                .ForeignKey("Lenses.Lenses", t => t.LensDetail_Id, cascadeDelete: true)
                .Index(t => t.Country_Id)
                .Index(t => t.LensDetail_Id);

            AddColumn("Base.Countries", "Description", c => c.String());
            AlterColumn("Base.Countries", "Code", c => c.String());
            DropColumn("Base.Countries", "Description_Id");
        }

        public override void Down()
        {
            AddColumn("Base.Countries", "Description_Id", c => c.Int(nullable: false));
            DropForeignKey("dbo.CountryLensDetail", "LensDetail_Id", "Lenses.Lenses");
            DropForeignKey("dbo.CountryLensDetail", "Country_Id", "Base.Countries");
            DropIndex("dbo.CountryLensDetail", new[] { "LensDetail_Id" });
            DropIndex("dbo.CountryLensDetail", new[] { "Country_Id" });
            AlterColumn("Base.Countries", "Code", c => c.String(nullable: false));
            DropColumn("Base.Countries", "Description");
            DropTable("dbo.CountryLensDetail");
            CreateIndex("Base.Countries", "Description_Id");
            AddForeignKey("Base.Countries", "Description_Id", "Globalization.Translations", "Id");
        }
    }
}
