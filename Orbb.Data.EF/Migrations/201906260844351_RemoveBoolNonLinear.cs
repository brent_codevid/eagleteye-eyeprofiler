namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RemoveBoolNonLinear : DbMigration
    {
        public override void Up()
        {
            DropColumn("Lenses.ControlPoints", "CalculateNonLineair");
        }

        public override void Down()
        {
            AddColumn("Lenses.ControlPoints", "CalculateNonLineair", c => c.Boolean(nullable: false));
        }
    }
}
