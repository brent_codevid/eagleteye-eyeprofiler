// <auto-generated />
namespace Orbb.Data.EF
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class AddCustomerCountry : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddCustomerCountry));
        
        string IMigrationMetadata.Id
        {
            get { return "201905081022405_AddCustomerCountry"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
