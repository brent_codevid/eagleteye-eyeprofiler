namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddClearanceParameters : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "IncreaseClearance", c => c.Boolean());
            AddColumn("Lenses.ControlPoints", "MinimalClearance", c => c.Double());
            AddColumn("Lenses.ControlPoints", "SagCorrectionClearance", c => c.Double());
            Sql("update [Lenses].[ControlPoints] set IncreaseClearance = 0");
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "SagCorrectionClearance");
            DropColumn("Lenses.ControlPoints", "MinimalClearance");
            DropColumn("Lenses.ControlPoints", "IncreaseClearance");
        }
    }
}
