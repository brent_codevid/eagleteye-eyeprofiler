namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCurvatureItem : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.CPCurvatureItems", "Diameter", c => c.Double(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("Lenses.CPCurvatureItems", "Diameter");
        }
    }
}
