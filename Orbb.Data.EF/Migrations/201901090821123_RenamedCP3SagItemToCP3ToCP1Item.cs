namespace Orbb.Data.EF
{
    using System.Data.Entity.Migrations;

    public partial class RenamedCP3SagItemToCP3ToCP1Item : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("Lenses.CP3SagItems", "LensDetail_Id", "Lenses.Lenses");
            DropIndex("Lenses.CP3SagItems", new[] { "LensDetail_Id" });
            CreateTable(
                "Lenses.CP3ToCP1Items",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    CP3Value = c.Double(nullable: false),
                    CP1Value = c.Double(nullable: false),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    LensDetail_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Lenses.Lenses", t => t.LensDetail_Id)
                .Index(t => t.LensDetail_Id);

            DropTable("Lenses.CP3SagItems");
        }

        public override void Down()
        {
            CreateTable(
                "Lenses.CP3SagItems",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    SagValue = c.Double(nullable: false),
                    ResultValue = c.Double(nullable: false),
                    CreatedBy = c.String(maxLength: 60),
                    CreatedOn = c.DateTime(),
                    LastModifiedBy = c.String(maxLength: 60),
                    LastModifiedOn = c.DateTime(),
                    LensDetail_Id = c.Int(),
                })
                .PrimaryKey(t => t.Id);

            DropForeignKey("Lenses.CP3ToCP1Items", "LensDetail_Id", "Lenses.Lenses");
            DropIndex("Lenses.CP3ToCP1Items", new[] { "LensDetail_Id" });
            DropTable("Lenses.CP3ToCP1Items");
            CreateIndex("Lenses.CP3SagItems", "LensDetail_Id");
            AddForeignKey("Lenses.CP3SagItems", "LensDetail_Id", "Lenses.Lenses", "Id");
        }
    }
}
