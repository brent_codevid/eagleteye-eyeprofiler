namespace Orbb.Data.EF
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixDiameter : DbMigration
    {
        public override void Up()
        {
            AddColumn("Lenses.ControlPoints", "CurvatureDiametersString", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("Lenses.ControlPoints", "CurvatureDiametersString");
        }
    }
}
