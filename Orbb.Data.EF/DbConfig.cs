﻿using System.Data.Entity;

namespace Orbb.Data.EF
{
    public class DbConfig : DbConfiguration
    {
        public DbConfig()
        {
            SetDefaultConnectionFactory(new System.Data.Entity.Infrastructure.SqlConnectionFactory());
            SetProviderServices("System.Data.SqlClient", System.Data.Entity.SqlServer.SqlProviderServices.Instance);
        }
    }
}
