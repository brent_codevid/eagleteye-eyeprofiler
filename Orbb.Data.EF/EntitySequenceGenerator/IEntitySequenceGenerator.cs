﻿using Orbb.Data.Models.Interfaces;

namespace Orbb.Data.EF.EntitySequenceGenerator
{
    public interface IEntitySequenceGenerator<in TEntityType> where TEntityType : IEntity
    {
        void SetSequence(TEntityType entity, IDatabaseContext dbContext);
    }
}