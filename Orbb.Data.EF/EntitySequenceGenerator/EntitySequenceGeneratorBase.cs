﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;

namespace Orbb.Data.EF.EntitySequenceGenerator
{
    public abstract class EntitySequenceGeneratorBase<T> : IEntitySequenceGenerator<T> where T : IEntity
    {
        [SetterProperty]
        public ICurrent<DivisionVM> CurrentDivision { get; set; }
        public abstract void SetSequence(T entity, IDatabaseContext dbContext);
    }
}
