﻿using Orbb.Data.EF.EntitySequenceGenerator;
using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.EF.Utility
{
    public interface IEntitySequenceGeneratorFactory
    {
        IEntitySequenceGenerator<T> GetEntitySequenceGenerator<T>() where T : IEntity, new();
        void GenerateSequence(IEntity entity, Type type, IDatabaseContext dbContext);
    }
}