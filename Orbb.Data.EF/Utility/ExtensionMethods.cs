﻿using Orbb.Data.Common.Tools;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;

namespace Orbb.Data.EF.Utility
{
    public static class ExtensionMethods
    {
        public static Type GetNonProxiedType<TEntity>(this TEntity entity) where TEntity : class, IEntity
        {
            if (entity == null)
                return null;
            return System.Data.Entity.Core.Objects.ObjectContext.GetObjectType(entity.GetType());
        }

        internal static bool IsModified<TEntity>(this DbEntityEntry<TEntity> entry)
            where TEntity : class, IEntity
        {
            return entry.State == EntityState.Modified;
        }

        internal static bool IsAdded<TEntity>(this DbEntityEntry<TEntity> entry)
            where TEntity : class, IEntity
        {
            return entry.State == EntityState.Added;
        }

        internal static bool IsDeleted<TEntity>(this DbEntityEntry<TEntity> entry)
            where TEntity : class, IEntity
        {
            return entry.State == EntityState.Deleted;
        }

        internal static bool IsUnchanged<TEntity>(this DbEntityEntry<TEntity> entry)
            where TEntity : class, IEntity
        {
            return entry.State == EntityState.Unchanged;
        }
        public static bool WriteOverlapDifferences(this IEnumerable<ViewModelDifference> dbChanges, IEnumerable<ViewModelDifference> clientChanges, StringBuilder sb)
        {
            var dbChangesOverlap = new List<ViewModelDifference>();
            var commonModels = dbChanges.Where(x => clientChanges.Any(y => y.NewVersion.Id == x.NewVersion.Id && y.NewVersion.GetType() == x.NewVersion.GetType())).ToList();
            foreach (var dbModel in commonModels)
            {
                var difference = ViewModelDifference.New(dbModel.NewVersion, dbModel.OldVersion, dbModel.Path, ViewModelDifference.EntityStates.Updated);
                var clientModel = clientChanges.Single(x => x.NewVersion.Id == dbModel.NewVersion.Id && x.NewVersion.GetType() == dbModel.NewVersion.GetType());
                var commonProperties = dbModel.Properties.Where(x => clientModel.Properties.Any(y => y.Name == x.Name)).ToList();
                difference.AddProperties(commonProperties);
                if (difference.Properties.Any())
                    dbChangesOverlap.Add(difference);
            }

            foreach (var difference in dbChangesOverlap)
            {
                var original = difference.OldVersion;
                var database = difference.NewVersion;
                var current = clientChanges.Single(x => x.OldVersion?.Id == original.Id && x.OldVersion?.GetType() == original.GetType());
                foreach (var property in difference.Properties)
                {
                    if (property.PropertyType.GetInterfaces().Contains(typeof(ICollection)))
                    {
                        sb.AppendFormat("\t- Name: {0}.{1}, the collection contents have changed",
                            difference.Path,
                            property.Name);
                    }
                    else
                    {
                        var currentValue = property.GetValue(current.NewVersion, null); //Changed current to current.NewVersion cause of bug when address changed
                        var originalValue = property.GetValue(original, null);
                        var databaseValue = property.GetValue(database, null);
                        if (property.PropertyType.GetInterfaces().Contains(typeof(IViewModel)))
                        {
                            currentValue = currentValue != null ? $"{property.PropertyType.Name.Replace("Lookup", "")} {((IViewModel)currentValue).Id}"
                                : "N/A";
                            originalValue = originalValue != null ? $"{property.PropertyType.Name.Replace("Lookup", "")} {((IViewModel)originalValue).Id}"
                                : "N/A";
                            databaseValue = databaseValue != null ? $"{property.PropertyType.Name.Replace("Lookup", "")} {((IViewModel)databaseValue).Id}"
                                : "N/A";
                        }
                        else if (property.PropertyType == typeof(DateTime) || property.PropertyType == typeof(DateTime?))
                        {
                            currentValue = currentValue != null ? $"{currentValue:g}" : "N/A";
                            originalValue = originalValue != null ? $"{originalValue:g}" : "N/A";
                            databaseValue = databaseValue != null ? $"{databaseValue:g}" : "N/A";
                        }

                        sb.AppendFormat("\t- Name: {0}.{1}, your value: {2}, your original value: {3}, current database value: {4}",
                            difference.Path,
                            property.Name,
                            currentValue,
                            originalValue,
                            databaseValue);
                    }
                    sb.AppendLine();
                }
            }
            return dbChangesOverlap.Any();
        }
    }
}
