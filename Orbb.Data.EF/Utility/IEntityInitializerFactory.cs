﻿using Orbb.Data.EF.EntityInitializers;
using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.EF.Utility
{
    public interface IEntityInitializerFactory
    {
        IEntityInitializer<T> GetEntityInitializer<T>() where T : IEntity, new();
        void Initialize(IEntity entity, Type type, IDatabaseContext dbContext);
        void RegisterInitializer(Type forType, Type initializer);
    }
}