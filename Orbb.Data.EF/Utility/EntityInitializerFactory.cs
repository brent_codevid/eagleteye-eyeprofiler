﻿using Orbb.Data.EF.EntityInitializers;
using Orbb.Data.Models.Interfaces;
using StructureMap;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.Utility
{
    public class EntityInitializerFactory : IEntityInitializerFactory
    {
        private readonly Dictionary<string, Type> initializers = new Dictionary<string, Type>();
        private readonly IContainer container;

        public EntityInitializerFactory(IContainer container)
        {
            this.container = container;
        }

        public void RegisterInitializer(Type forType, Type initializer)
        {
            initializers.Add(forType.FullName, initializer);
        }

        public IEntityInitializer<T> GetEntityInitializer<T>() where T : IEntity, new()
        {
            if (initializers.ContainsKey(typeof(T).FullName))
            {
                var initializer = container.GetInstance(initializers[typeof(T).FullName]);
                return (IEntityInitializer<T>)initializer;
            }

            return null;
        }

        public void Initialize(IEntity entity, Type type, IDatabaseContext dbContext)
        {
            if (initializers.ContainsKey(type.FullName))
            {
                var initializer = container.GetInstance(initializers[type.FullName]);
                var t = typeof(EntityInitializerBase<>).MakeGenericType(type);
                t.GetMethod("InitializeForNew").Invoke(initializer, new object[] { entity, dbContext });
            }
        }
    }
}
