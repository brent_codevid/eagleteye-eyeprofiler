﻿using Orbb.Data.EF.EntitySequenceGenerator;
using Orbb.Data.Models.Interfaces;
using StructureMap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Orbb.Data.EF.Utility
{
    public class EntitySequenceGeneratorFactory : IEntitySequenceGeneratorFactory
    {
        private readonly Dictionary<string, Type> generators = new Dictionary<string, Type>();
        private readonly IContainer container;

        public EntitySequenceGeneratorFactory(IContainer container)
        {
            this.container = container;
            BuildList();
        }

        private void BuildList()
        {
            var types = Assembly.GetExecutingAssembly().GetTypes();
            var typesToRegister = types
                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                           .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                                               type.BaseType.GetGenericTypeDefinition() ==
                                               typeof(EntitySequenceGeneratorBase<>));
            foreach (var generatorType in typesToRegister)
            {
                Type generatorForType = generatorType.GetMethods()[0].GetParameters()[0].ParameterType;
                generators.Add(generatorForType.FullName, generatorType);
            }
        }

        public IEntitySequenceGenerator<T> GetEntitySequenceGenerator<T>() where T : IEntity, new()
        {
            if (generators.ContainsKey(typeof(T).FullName))
            {
                var generator = container.GetInstance(generators[typeof(T).FullName]);
                return (IEntitySequenceGenerator<T>)generator;
            }

            return null;
        }

        public void GenerateSequence(IEntity entity, Type type, IDatabaseContext dbContext)
        {
            if (generators.ContainsKey(type.FullName))
            {
                var generator = container.GetInstance(generators[type.FullName]);
                var t = typeof(EntitySequenceGeneratorBase<>).MakeGenericType(type);
                t.GetMethod("SetSequence").Invoke(generator, new object[] { entity, dbContext });
            }
        }
    }
}
