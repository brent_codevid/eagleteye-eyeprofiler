﻿using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace Orbb.Data.EF.Utility
{

    public class ViewModelSaver<TViewModel, TEntity>
        where TViewModel : class, IViewModel
        where TEntity : class, IEntity, new()
    {
        private readonly IDatabaseContext databaseContext;
        private readonly ICurrentUser currentUser;
        private readonly ICurrent<DivisionVM> currentDivision;
        private readonly IEntityInitializerFactory entityInitializerFactory;
        private readonly IEntityDeleterFactory entityDeleterFactory;

        public ViewModelSaver(IDatabaseContext context,
            ICurrentUser currentUser,
            ICurrent<DivisionVM> currentDivision,
            IEntityInitializerFactory entityInitializerFactory,
            IEntityDeleterFactory entityDeleterFactory)
        {
            databaseContext = context;
            this.currentUser = currentUser;
            this.currentDivision = currentDivision;
            this.entityInitializerFactory = entityInitializerFactory;
            this.entityDeleterFactory = entityDeleterFactory;
        }


        public SaveResult Save(TViewModel current, TViewModel original, TViewModel databaseVersion, params string[] modelIncludes)
        {
            Func<TEntity, IEnumerable<ViewModelDifference>, IDatabaseContext, IEntityInitializerFactory, IEntityDeleterFactory, SaveResult> saveFunc =
                (entity, changes, db, eif, edf) =>
                {
                    var mapper = new ViewModelToEntityMapper(db, eif, edf, currentUser);
                    mapper.Map(entity, changes);

                    var result = db.SaveChanges(currentUser, currentDivision);
                    result.EntityId = entity.Id;

                    return result;
                };

            if (current.Id <= 0)
            {
                //save new item
                var comparer = new ViewModelComparer(currentDivision);
                var clientChanges = comparer.GetDifferences(null, current);

                //save
                var entity = new TEntity();
                return saveFunc(entity, clientChanges, databaseContext, entityInitializerFactory, entityDeleterFactory);
            }
            else
            {
                var comparer = new ViewModelComparer(currentDivision);
                var clientChanges = comparer.GetDifferences(original, current);
                if (!clientChanges.Any()) // No changes
                {
                    var res = new SaveResult(true, 0) { EntityId = current.Id };
                    return res;
                }

                var dbChanges = comparer.GetDifferences(original, databaseVersion);
                if (dbChanges.Any())
                {
                    // There are changes between the original version and the current database version => check for overlapping changes
                    var sb = new StringBuilder("Data has been edited but has already been changed since editing started. Are you missing an entity list initialization in the constructor?");
                    sb.AppendLine();

                    var hasDbOverlaps = new List<bool>
                             {
                                  dbChanges.WriteOverlapDifferences(clientChanges, sb)
                             };

                    if (hasDbOverlaps.Any(x => x))
                    {
                        return new SaveResult(false, 0, new Exception(sb.ToString()));
                    }
                }

                var query = databaseContext.Query<TEntity>();
                if (modelIncludes != null)
                {
                    foreach (var include in modelIncludes)
                    {
                        if (!String.IsNullOrEmpty(include))
                            query = query.Include(include);
                    }
                }

                var entity = query.Where(x => x.Id == current.Id).ToList().FirstOrDefault();
                //save
                return saveFunc(entity, clientChanges, databaseContext, entityInitializerFactory, entityDeleterFactory);
            }
        }
    }
}
