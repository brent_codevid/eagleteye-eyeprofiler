﻿using Orbb.Data.Common;
using Orbb.Data.Common.Tools;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Attributes;
using Orbb.Data.ViewModels.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Dynamic;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Orbb.Data.EF.Utility
{
    internal class ViewModelToEntityMapper
    {
        private readonly IDatabaseContext db;
        private readonly AddMap addMap = new AddMap();
        private readonly IEntityInitializerFactory initializerFactory;
        private readonly IEntityDeleterFactory deleterFactory;
        private readonly ICurrentUser currentUser;

        public ViewModelToEntityMapper(IDatabaseContext db,
                IEntityInitializerFactory initializerFactory,
                IEntityDeleterFactory deleterFactory,
                ICurrentUser currentUser)
        {
            this.db = db;
            this.initializerFactory = initializerFactory;
            this.deleterFactory = deleterFactory;
            this.currentUser = currentUser;
        }

        public void Map(IEntity entityRoot, IEnumerable<ViewModelDifference> differences)
        {
            try
            {
                MapInternal(entityRoot, differences);
            }
            catch (Exception ex)
            {
                throw new Exception("Mapping error: " + ex.Message, ex);
            }
        }

        private void MapInternal(IEntity entityRoot, IEnumerable<ViewModelDifference> differences)
        {
            //sort difference from top of hierarchy to deepest nesting
            var differencesSorted = differences.Where(x => String.IsNullOrWhiteSpace(x.Path)).ToList();
            differencesSorted.AddRange(differences.Except(differencesSorted).OrderBy(x => x.Path.Count(y => y == '.')).ThenBy(x => x.Path));

            foreach (var difference in differencesSorted)
            {
                var viewModel = difference.NewVersion;
                var entity = entityRoot;
                if (!String.IsNullOrWhiteSpace(difference.Path))
                    foreach (var s in difference.Path.Split('.'))
                        entity = GetEntityPropertyValue(entity, s) as IEntity;

                foreach (var piViewModel in difference.Properties.Where(x => x.GetCustomAttribute<IgnoreSave>() == null))
                {
                    try
                    {
                        var newValue = piViewModel.GetValue(viewModel, null);
                        var entityPropertyResult = GetEntityProperty(entity, piViewModel.Name);
                        if (newValue is ICollection collection)
                        {
                            var clientCollection = collection.Cast<IViewModel>().ToList();
                            var oldValue = entityPropertyResult.Item2.GetValue(entityPropertyResult.Item1, null);
                            if (!(oldValue is ICollection))
                            {
                                throw new Exception(
                                    $"comparing client collection property {difference.Path}.{piViewModel.Name} to non-collection entity property {entityPropertyResult.Item1.GetNonProxiedType().Name}.{entityPropertyResult.Item2.Name}");
                            }
                            var databaseCollection = ((ICollection)oldValue).Cast<IEntity>();
                            var databaseCollectionEntityType = entityPropertyResult.Item2.PropertyType.GetGenericArguments()[0];

                            //check adds
                            var adds = clientCollection.Where(x => databaseCollection.All(y => y.Id != x.Id)).ToList();
                            foreach (var add in adds)
                            {
                                if (add.Id <= 0)
                                {
                                    if (!addMap.Contains(add))
                                        AddNewObject(add, databaseCollectionEntityType);
                                    AddToCollection(addMap.GetEntity(add), databaseCollection as ICollection);
                                }
                                else
                                {
                                    var entities = db.Query(databaseCollectionEntityType).Where("Id == @0", add.Id);
                                    foreach (IEntity obj in entities)
                                        AddToCollection(obj, databaseCollection as ICollection);
                                }
                            }

                            //check deletes
                            var deletes = databaseCollection.Where(x => clientCollection.All(y => y.Id != x.Id)).ToList();
                            foreach (var delete in deletes)
                            {
                                // Call BeforeDeleteInViewModelMapper
                                IHasCanDelete deleter = (IHasCanDelete)deleterFactory.GetEntityDeleter(delete.GetNonProxiedType());
                                if (deleter != null)
                                {
                                    var res = deleter.CanDelete(db, delete.Id, deleterFactory, out var reasons);
                                    if (!res)
                                    {
                                        throw new Exception(
                                            $"Cannot delete {delete.GetNonProxiedType().Name}: {string.Join(", ", reasons)}");
                                    }

                                    var beforeDeleter = deleter as IBeforeDeleteOnSaveChanges;
                                    if (beforeDeleter != null)
                                    {
                                        var beforeDeleteResult = beforeDeleter.BeforeDeleteInViewModelMapper(db, delete.Id, currentUser, deleterFactory);
                                        if (!beforeDeleteResult)
                                        {
                                            throw new Exception(
                                                $"BeforeDelete error for {delete.GetNonProxiedType().Name}");
                                        }
                                    }
                                }

                                RemoveFromCollection(delete, databaseCollection as ICollection);

                                if (piViewModel.GetCustomAttribute<DeleteWhenRemovedFromCollection>() != null)
                                    db.Entry(delete).State = System.Data.Entity.EntityState.Deleted;
                            }
                            continue;
                        }

                        if (newValue is IViewModel vm) //reference property
                        {
                            if (vm.Id <= 0)
                            {
                                if (!addMap.Contains(vm))
                                    AddNewObject(vm, entityPropertyResult.Item2.PropertyType);
                                newValue = addMap.GetEntity(vm);
                            }
                            else
                            {
                                var entities = db.Query(entityPropertyResult.Item2.PropertyType).Where("Id == @0", ((IViewModel)newValue).Id);
                                foreach (var obj in entities)
                                    newValue = obj;
                            }
                        }
                        if (entityPropertyResult.Item2.Name != "Id")
                            entityPropertyResult.Item2.SetValue(entityPropertyResult.Item1, newValue, null);
                        else
                        {
                            Console.WriteLine("test");
                        }

                        //if (entityPropertyResult.Item1.Id > 0)
                        //    db.Update(entityPropertyResult.Item1);
                        //else
                        //    db.Add(entityPropertyResult.Item1);
                    }
                    catch (Exception propertyEx)
                    {
                        throw new Exception("Error while mapping property " + piViewModel.Name, propertyEx);
                    }
                }

                if (entity.Id <= 0)
                    db.Add(entity);
                else
                    db.Update(entity);
            }
        }

        private void RemoveFromCollection(IEntity delete, ICollection collection)
        {
            var removeMethod = collection.GetType().GetMethod("Remove");
            removeMethod.Invoke(collection, new object[] { delete });
        }

        private void AddToCollection(IEntity entity, ICollection collection)
        {
            var addMethod = collection.GetType().GetMethod("Add");
            addMethod.Invoke(collection, new object[] { entity });
        }

        private void AddNewObject(IViewModel vm, Type entityType)
        {
            Type typeToUse = entityType;
            var attr = vm.GetType().GetCustomAttributes<MapTo>().FirstOrDefault();
            if (attr != null)
            {
                Assembly assembly = typeof(Models.Models.Base.Country).Assembly;
                typeToUse = assembly.GetType(attr.Type);
            }
            var entity = Activator.CreateInstance(typeToUse) as IEntity;

            addMap.Add(vm, entity);
            //db.Add(entity);
            entity.Id = vm.Id;
        }
        private Tuple<IEntity, PropertyInfo> GetEntityProperty(IEntity entity, string viewModelPropertyPath)
        {
            if (viewModelPropertyPath.Contains("[")) //collection property
                viewModelPropertyPath = viewModelPropertyPath.Split('[')[0];
            var property = entity.GetType().GetProperties().SingleOrDefault(x => x.Name.ToLower() == viewModelPropertyPath.ToLower());

            if (property != null) return new Tuple<IEntity, PropertyInfo>(entity, property);

            var camelSplit = Regex.Replace(viewModelPropertyPath, @"(\B[A-Z]+?(?=[A-Z][^A-Z])|\B[A-Z]+?(?=[^A-Z]))", " $1").Split(' ');
            var skip = 1;
            do
            {
                var test = String.Join("", camelSplit.Take(camelSplit.Length - skip));
                property = entity.GetType().GetProperty(test);
                if (property == null) continue;

                var remainingPath = String.Join("", camelSplit.Skip(camelSplit.Length - skip));
                var intermediaryValue = property.GetValue(entity, null);

                if (intermediaryValue == null)
                {
                    if (typeof(IEntity).IsAssignableFrom((property.PropertyType)))
                    {
                        if (Activator.CreateInstance(property.PropertyType) is IEntity newentity)
                        {
                            initializerFactory.Initialize(newentity, property.PropertyType, db);

                            property.SetValue(entity, newentity);
                            intermediaryValue = newentity;
                        }
                    }
                    if (intermediaryValue == null)
                    {
                        throw new Exception(
                            $"VM-to-Entity mapping logic is expecting to find an even deeper nested item but has already run into a null reference.{Environment.NewLine}Expected path: {viewModelPropertyPath}, null reference at: {test}");
                    }
                }

                if (!(intermediaryValue is IEntity))
                    throw new Exception(
                        $"A nested item in an entity must also be an entity or a collection of entities.{Environment.NewLine}Property {property.Name} on entity type {entity.GetNonProxiedType().Name}");

                return GetEntityProperty(intermediaryValue as IEntity, remainingPath);
            } while (skip++ < camelSplit.Length);
            return new Tuple<IEntity, PropertyInfo>(entity, property);
        }

        private object GetEntityPropertyValue(IEntity entity, string viewModelPropertyPath)
        {
            var propertyResult = GetEntityProperty(entity, viewModelPropertyPath);
            int? collectionItemId = null;
            if (viewModelPropertyPath.Contains("[")) //collection property
                collectionItemId = Convert.ToInt32(viewModelPropertyPath.Split('[')[1].Split(']')[0]);

            var value = propertyResult.Item2.GetValue(propertyResult.Item1, null);
            if (collectionItemId.HasValue)
                value = ((ICollection)value).Cast<IEntity>().Single(x => x.Id == collectionItemId);
            return value;
        }

        private class AddMap
        {
            private readonly IDictionary<IViewModel, IEntity> addMap = new Dictionary<IViewModel, IEntity>(new ViewEntityEqualityComparer());
            private class ViewEntityEqualityComparer : IEqualityComparer<IViewModel>
            {
                public bool Equals(IViewModel x, IViewModel y)
                {
                    return x.Id == y.Id && x.GetType() == y.GetType();
                }

                public int GetHashCode(IViewModel obj)
                {
                    return obj.GetHashCode();
                }
            }

            public void Add(IViewModel vm, IEntity entity)
            {
                addMap[vm] = entity;
            }

            public bool Contains(IViewModel vm)
            {
                var comparer = new ViewEntityEqualityComparer();
                return addMap.Keys.Any(x => comparer.Equals(x, vm));
            }

            public IEntity GetEntity(IViewModel vm)
            {
                var comparer = new ViewEntityEqualityComparer();
                return addMap.Single(x => comparer.Equals(x.Key, vm)).Value;
            }
        }
    }
}