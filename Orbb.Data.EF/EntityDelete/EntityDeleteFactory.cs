﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityDelete
{
    public class EntityDeleteFactory : IEntityDeleterFactory
    {
        private readonly Dictionary<string, object> deleters = new Dictionary<string, object>();
        public void RegisterTrigger<T>(IEntityDelete<T> deleter) where T : IEntity, new()
        {
            deleters.Add(typeof(T).FullName, deleter);
        }

        public void RegisterTrigger(object deleter, Type type)
        {
            deleters.Add(type.FullName, deleter);
        }

        //private void BuildList()
        //{
        //    var types = Assembly.GetExecutingAssembly().GetTypes();
        //    var typesToRegister = types
        //                                  .Where(type => !String.IsNullOrEmpty(type.Namespace))
        //                                   .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
        //                                       type.BaseType.GetGenericTypeDefinition() ==
        //                                       typeof(EntityDeleteBase<>));
        //    foreach (var initializerType in typesToRegister)
        //    {
        //        Type initForType = initializerType.BaseType.GenericTypeArguments[0];//GetMethods()[0].GetParameters()[0].ParameterType;
        //        var deleter = Activator.CreateInstance(initializerType);
        //        deleters.Add(initForType.FullName, deleter);
        //    }
        //}

        public IEntityDelete<T> GetEntityDeleter<T>() where T : IEntity, new()
        {
            if (deleters.ContainsKey(typeof(T).FullName))
            {
                var deleter = deleters[typeof(T).FullName];
                return (IEntityDelete<T>)deleter;
            }

            return null;
        }

        public object GetEntityDeleter(Type type)
        {
            if (deleters.ContainsKey(type.FullName))
                return deleters[type.FullName];
            else return null;
        }

        public void Delete(IEntity entity, Type type, IDatabaseContext dbContext, ICurrentUser currentUser)
        {
            if (deleters.ContainsKey(type.FullName))
            {
                var deleter = deleters[type.FullName];
                var t = typeof(EntityDeleteBase<>).MakeGenericType(type);
                t.GetMethod("Delete").Invoke(deleter, new object[] { dbContext, entity, currentUser });
            }
        }
    }
}
