﻿using Orbb.Data.Common;

namespace Orbb.Data.EF.EntityDelete
{
    public interface IBeforeDeleteOnSaveChanges
    {
        bool BeforeDelete(IDatabaseContext dbContext, int id, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory);

        bool BeforeDeleteInViewModelMapper(IDatabaseContext dbContext, int id, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory);
    }
}
