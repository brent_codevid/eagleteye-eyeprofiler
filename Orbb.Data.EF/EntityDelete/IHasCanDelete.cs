﻿namespace Orbb.Data.EF.EntityDelete
{
    public interface IHasCanDelete
    {
        bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons);
    }
}
