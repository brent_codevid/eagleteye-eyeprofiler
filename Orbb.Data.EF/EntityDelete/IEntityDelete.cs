﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;

namespace Orbb.Data.EF.EntityDelete
{
    public interface IEntityDelete<TEntity> where TEntity : IEntity
    {
        bool CanDelete(IDatabaseContext dbContext, TEntity entity, IEntityDeleterFactory deleterFactory);
        bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory);
        bool CanDelete(IDatabaseContext dbContext, TEntity entity, IEntityDeleterFactory deleterFactory, out string[] reasons);
        bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons);
        bool Delete(IDatabaseContext dbContext, int id, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory);
    }
}