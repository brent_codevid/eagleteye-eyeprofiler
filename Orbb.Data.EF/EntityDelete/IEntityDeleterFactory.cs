﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.EF.EntityDelete
{
    public interface IEntityDeleterFactory
    {
        void RegisterTrigger<T>(IEntityDelete<T> deleter) where T : IEntity, new();
        void RegisterTrigger(object deleter, Type type);
        IEntityDelete<T> GetEntityDeleter<T>() where T : IEntity, new();
        object GetEntityDeleter(Type type);
        void Delete(IEntity entity, Type type, IDatabaseContext dbContext, ICurrentUser currentUser);
    }
}