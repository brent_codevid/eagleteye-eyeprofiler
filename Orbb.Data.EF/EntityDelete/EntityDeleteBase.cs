﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using System.Linq;

namespace Orbb.Data.EF.EntityDelete
{
    public abstract class EntityDeleteBase<T> : IHasCanDelete, IEntityDelete<T> where T : class, IEntity
    {
        public bool CanDelete(IDatabaseContext dbContext, T entity, IEntityDeleterFactory deleterFactory)
        {
            return CanDelete(dbContext, entity.Id, deleterFactory, out var reasons);
        }
        public bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory)
        {
            return CanDelete(dbContext, id, deleterFactory, out var reasons);
        }

        public bool CanDelete(IDatabaseContext dbContext, T entity, IEntityDeleterFactory deleterFactory,
            out string[] reasons)
        {
            return CanDelete(dbContext, entity.Id, deleterFactory, out reasons);
        }
        public abstract bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons);

        public virtual bool BeforeDelete(IDatabaseContext dbContext, T entity, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory) // Any logic, like deleting related child objects can be done by overriding this method
        {
            return true;
        }

        public bool Delete(IDatabaseContext dbContext, int id, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory)
        {
            if (id <= 0) // Not saved yet
                return true;

            var storedEntity = dbContext.Query<T>().Single(x => x.Id == id);

            //RETURN TRUE IF STOREDENTITY == NULL?

            /*string[] reasons;
            if (!CanDelete(dbContext, storedEntity, deleterFactory, out reasons))
                return false;*/

            if (BeforeDelete(dbContext, storedEntity, currentUser, deleterFactory))
            {
                dbContext.Delete(storedEntity);
                return dbContext.SaveChanges(currentUser, null, false).Success;
            }

            return false;
        }
    }
}