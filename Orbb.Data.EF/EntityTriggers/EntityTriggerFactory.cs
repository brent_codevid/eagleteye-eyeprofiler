﻿using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityTriggers
{
    public class EntityTriggerFactory : IEntityTriggerFactory
    {
        private readonly Dictionary<string, List<IEntityTrigger>> triggersDictionary = new Dictionary<string, List<IEntityTrigger>>();
        public void RegisterTrigger(IEntityTrigger trigger, Type type)
        {
            if (triggersDictionary.ContainsKey(type.FullName))
                triggersDictionary[type.FullName].Add(trigger);
            else
            {
                var list = new List<IEntityTrigger> { trigger };
                triggersDictionary.Add(type.FullName, list);
            }

        }

        public List<IEntityTrigger> GetTriggers<T>() where T : IEntity, new()
        {
            if (triggersDictionary.ContainsKey(typeof(T).FullName))
            {
                var triggers = triggersDictionary[typeof(T).FullName];
                return triggers;
            }

            return null;
        }

        public void Trigger(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext)
        {
            if (triggersDictionary.ContainsKey(type.FullName))
            {
                var triggers = triggersDictionary[type.FullName];
                var list = triggers as IList<IEntityTrigger>;
                foreach (var trigger in list)
                    TriggerSingle(operation, entity, type, dbContext, trigger);
            }
        }

        private void TriggerSingle(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext, IEntityTrigger trigger)
        {
            if (!trigger.Operations.Contains(operation))
                return;

            trigger.Trigger(entity, dbContext);
        }
    }
}
