﻿using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityTriggers
{
    public interface IEntityTriggerFactory
    {
        void RegisterTrigger(IEntityTrigger trigger, Type type);
        List<IEntityTrigger> GetTriggers<T>() where T : IEntity, new();

        void Trigger(EntityOperations operation, IEntity entity, Type type, IDatabaseContext dbContext);
    }
}
