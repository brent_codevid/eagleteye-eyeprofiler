﻿using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.EF.EntityTriggers
{
    public interface IEntityTrigger
    {
        List<EntityOperations> Operations { get; }
        Type OfEntityType { get; }
        void Trigger(IEntity entity, IDatabaseContext context);
    }
}
