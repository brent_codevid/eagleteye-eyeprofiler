﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.UI.Base;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.UI.User
{
    public class UserQuickMenu : IEntity
    {
        public int UserId { get; set; }

        public IList<MenuItem> MenuItems { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

    }
}
