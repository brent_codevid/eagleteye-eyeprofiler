﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Globalization;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.Models.Models.UI.Base
{
    public class MenuItem : IEntity
    {
        public MenuItem()
        {
            ChildMenuItems = new List<MenuItem>();
        }

        //TODO Role management
        public Translation Title { get; set; }
        public int OrderNumber { get; set; }
        [ForeignKey("ParentMenuItem")]
        public int? ParentMenuItemId { get; set; }
        public virtual MenuItem ParentMenuItem { get; set; }
        public virtual IList<MenuItem> ChildMenuItems { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

    }
}
