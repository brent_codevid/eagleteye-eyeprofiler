﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Security
{
    [Serializable]
    public class SecuredObjectPermission : IEntity
    {
        public int SecuredObjectId { get; set; }
        public SecuredObject SecuredObject { get; set; }
        public int UserGroupId { get; set; }
        public UserGroup UserGroup { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Add { get; set; }
        public bool Delete { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
