﻿using Orbb.Common.Utility;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Security.Types;
using Orbb.Data.Models.Models.System;
using System;
using System.Collections.Generic;
using System.Drawing;

namespace Orbb.Data.Models.Models.Security
{
    [Serializable]
    public class User : IEntity, IHasDivisions
    {
        public User()
        {
            Divisions = new List<Division>();
        }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
        public byte[] ProfileImageData { get; set; }
        public int? LanguageId { get; set; }
        public virtual Language Language { get; set; }

        public Image ProfileImage
        {
            get
            {
                if (ProfileImageData == null || ProfileImageData.Length < 2)
                    return null;

                Image returnImage = ImageConversion.ByteArrayToImage(ProfileImageData);
                return returnImage;
            }
        }

        public string Password { get; set; }
        public string Salt { get; set; }

        public DateTime? LastLoggedInAt { get; set; }
        public int? UserGroupId { get; set; }

        public virtual UserGroup UserGroup { get; set; }
        public string AnalyticalCode { get; set; }

        public int? DefaultDivisionId { get; set; }
        public virtual Division DefaultDivision { get; set; }

        public virtual List<Division> Divisions { get; set; }

        public int? CurrencyId { get; set; }
        public Currency Currency { get; set; }
        public int Gender { get; set; }
        public int TypeId { get; set; }
        public virtual UserType Type { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
