﻿using Newtonsoft.Json;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Orbb.Data.Models.Models.Security
{
    [Serializable]
    public class UserGroup : IEntity
    {
        public UserGroup()
        {
            Permissions = new List<SecuredObjectPermission>();
            Users = new List<User>();
        }
        public string Description { get; set; }
        public virtual List<SecuredObjectPermission> Permissions { get; set; }
        [XmlIgnore]
        [JsonIgnoreAttribute]
        public virtual List<User> Users { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
