﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Globalization;
using System;

namespace Orbb.Data.Models.Models.Security.Types
{
    public class UserType : IEntity
    {
        public int DescriptionId { get; set; }
        public virtual Translation Description { get; set; } // E.g. Administration / Technician
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
