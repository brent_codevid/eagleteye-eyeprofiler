﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Views
{
    public class UserView : IEntity
    {

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EmailAddress { get; set; }
        //public byte[] ProfileImageData { get; set; }
        public string Password { get; set; }
        public bool IsRepresentative { get; set; }


        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
