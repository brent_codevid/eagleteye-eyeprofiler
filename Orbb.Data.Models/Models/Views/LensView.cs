﻿using Orbb.Data.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using static Orbb.Common.Enumerations;

namespace Orbb.Data.Models.Models.Views
{
    public class LensView : IEntity
    {
        public string Name { get; set; }

        public int Type { get; set; }
        private string _typeName = null;
        [NotMapped]
        public string TypeName
        {
            get
            {
                if (_typeName == null)
                {
                    _typeName = ((LensTypes)Type).ToString();
                    if (_typeName == "0")
                    {
                        _typeName = "";
                    }
                }
                return _typeName;
            }
            set
            {
                _typeName = value;
            }
        }

        public string SupplierName { get; set; }

        public int LandingZoneBase { get; set; }
        public string LandingZoneBaseName
        {
            get
            {
                string name = ((LandingZoneBaseType)LandingZoneBase).ToString();
                if (name == "0")
                {
                    name = "";
                }

                return name;
            }

            private set
            {
            }
        }
        public double? LensDiameter
        {
            get;
            set;
        }
        public double? LensDiameterMax { get; set; }
        public double? LensDiameterMin { get; set; }
        public double? LensDiameterStepSize { get; set; }

        [NotMapped]
        public string ComputedLensCount
        {
            get
            {
                if (LensDiameter.HasValue)
                {
                    return LensDiameter.ToString();
                }
                else if (LensDiameterMax.HasValue && LensDiameterMin.HasValue && LensDiameterStepSize.HasValue)
                {
                    return $"{LensDiameterMin} - {LensDiameterMax} ({LensDiameterStepSize})";
                }

                return "";
            }
            private set { }
        }

        public int? CP1Id { get; set; }
        public int? CP2Id { get; set; }
        public int? CP4Id { get; set; }

        public string LandingCategory { get; set; }
        public string CenterCategory { get; set; }

        public int LandingZoneSections { get; set; }
        public int CenterZoneSections { get; set; }
        [NotMapped]
        public int SectionCount => LandingZoneSections + CenterZoneSections + 1;

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        public LensView() { }
    }
}
