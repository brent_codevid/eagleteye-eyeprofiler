﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Views
{
    public class ControlpointCategoryView : IEntity
    {
        public string Name { get; set; }

        public int NumberOfAxes { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        public ControlpointCategoryView() { }

        public ControlpointCategoryView(int id, string name)
        {
            Id = id;
            Name = name;
            NumberOfAxes = id;
        }
    }
}
