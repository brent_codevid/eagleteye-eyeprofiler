﻿using Orbb.Data.Models.Models.Orders;
using System;
using static Orbb.Common.Enumerations;

namespace Orbb.Data.Models.Models.Views
{
    public class OrderView
    {
        public DateTime Date { get; set; }
        public string CustomerName { get; set; }
        public string SupplierName { get; set; }
        public int Status { get; set; }
        private string _statusName;
        public string StatusName
        {
            get => _statusName ?? ((OrderStatus)Status).ToString();
            set => _statusName = value;
        }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        public OrderView() { }

        public OrderView(Order o)
        {
            Date = o.Date;
            CustomerName = o.Customer.Name;
            SupplierName = o.Supplier.Name;
            Status = o.Status;
        }
    }
}
