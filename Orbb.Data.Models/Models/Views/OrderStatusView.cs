﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Views
{
    public class OrderStatusView : IEntity
    {
        public string Name { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        public OrderStatusView() { }

        public OrderStatusView(int id, string name)
        {
            Id = id;
            Name = name;
        }
    }
}
