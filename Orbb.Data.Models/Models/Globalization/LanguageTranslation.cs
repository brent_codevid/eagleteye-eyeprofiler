﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Globalization
{
    public class LanguageTranslation : IEntity
    {
        public string ShortCode { get; set; }
        public string Value { get; set; }
        public int LanguageId { get; set; }
        public virtual Language Language { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

    }
}
