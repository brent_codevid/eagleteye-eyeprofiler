﻿using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.Globalization
{
    public class Translation : IEntity
    {
        public Translation()
        {
            Entries = new List<TranslationEntry>();
        }
        public string DefaultValue { get; set; }

        public virtual ICollection<TranslationEntry> Entries { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
