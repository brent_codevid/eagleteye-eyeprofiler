﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Globalization
{
    public class Language : IEntity
    {
        public string Code { get; set; }
        public string ShortCode { get; set; }
        public string EnglishDescription { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
