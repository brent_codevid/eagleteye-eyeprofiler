﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.Base
{
    public class Country : IEntity
    {
        public string Code { get; set; }
        public string Description { get; set; }
        public List<LensDetail> AvailableLenssets { get; set; } = new List<LensDetail>();

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
