﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.Customers
{
    public class Customer : IEntity
    {
        public List<LensDetail> UsedLenssets { get; set; } = new List<LensDetail>();
        public string Name { get; set; }
        public string Email { get; set; }

        public int? CountryId { get; set; }
        public Country Country { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
