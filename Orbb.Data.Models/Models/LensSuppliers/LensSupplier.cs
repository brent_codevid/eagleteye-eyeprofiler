﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.LensSuppliers
{
    public class LensSupplier : IEntity
    {
        public string Name { get; set; }
        public string EmailAdress { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
