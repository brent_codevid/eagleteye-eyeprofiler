﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.LensSuppliers;
using System;

namespace Orbb.Data.Models.Models.Orders
{
    public class Order : IEntity
    {
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
        public int SupplierId { get; set; }
        public LensSupplier Supplier { get; set; }
        public string FittedLens { get; set; }
        public int Status { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
