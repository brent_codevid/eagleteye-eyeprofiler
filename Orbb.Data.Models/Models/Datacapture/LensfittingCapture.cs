﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;

namespace Orbb.Data.Models.Models.Datacapture
{
    public class LensfittingCapture : IEntity
    {
        public int LensDetailId { get; set; }
        public LensDetail LensDetail { get; set; }

        public string FittedLensName { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
