﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Globalization;
using System;

namespace Orbb.Data.Models.Models.System.Types
{
    public class DocumentType : IEntity
    {
        public Translation Description { get; set; } // E.g. Mail - Invoice - Order
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
