﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.System.Types
{
    public class DocumentStoreState : IEntity
    {
        public string Description { get; set; } // E.g. Active - Read Only
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
