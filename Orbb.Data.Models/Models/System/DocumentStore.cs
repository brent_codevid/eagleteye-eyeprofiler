﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.System.Types;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.System
{
    public class DocumentStore : IEntity
    {
        public DocumentStore()
        {
            Documents = new List<Document>();
        }
        public int TypeId { get; set; }
        public DocumentStoreType Type { get; set; }
        public string Url { get; set; }
        public int Capacity { get; set; }
        public int SpaceUsed { get; set; }
        public int SpaceFree { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        public virtual List<Document> Documents { get; set; }
        #endregion
    }
}
