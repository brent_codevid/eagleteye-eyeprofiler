﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.System
{
    public class LogItem : IEntity
    {
        public string Action { get; set; }
        public string ByUser { get; set; }
        public string Description { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
