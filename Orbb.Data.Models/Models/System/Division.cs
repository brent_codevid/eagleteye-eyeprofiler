﻿using Newtonsoft.Json;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Security;
using System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Models.System
{
    public class Division : IEntity
    {
        public Division()
        {
            Users = new List<User>();
        }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public int FaxNumber { get; set; }
        public string Description { get; set; }
        public string ShortCode { get; set; }
        public string Logo { get; set; }
        public bool IsDefault { get; set; }
        public string ImagePath { get; set; }

        [JsonIgnoreAttribute]
        public virtual List<User> Users { get; set; }

        public virtual List<User> DefaultDivisionUsers { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
