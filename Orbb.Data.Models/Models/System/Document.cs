﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.System.Types;
using System;

namespace Orbb.Data.Models.Models.System
{
    public class Document : IEntity
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Description { get; set; }
        public int TypeId { get; set; }
        public virtual DocumentType Type { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public int StorageId { get; set; }
        public virtual DocumentStore Storage { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
