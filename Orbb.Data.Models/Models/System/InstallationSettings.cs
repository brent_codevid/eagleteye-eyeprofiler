﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.System
{
    public class InstallationSettings : IEntity, IHasDivision
    {
        public string CompanyName { get; set; }
        public string CompanyAddressline1 { get; set; }
        public string CompanyAddressline2 { get; set; }
        public string CompanyAddressline3 { get; set; }
        public string CompanyPostalCode { get; set; }
        public string CompanyCity { get; set; }
        public string CompanyCity2 { get; set; }
        public string CompanyCountry { get; set; }
        public string LogoFileName { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        #region Division
        public int DivisionId { get; set; }
        public virtual Division Division { get; set; }
        #endregion
    }
}
