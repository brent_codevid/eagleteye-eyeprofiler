﻿using Orbb.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class ControlPoint4 : ControlPoint
    {
        public bool CalculateLz { get; set; }
        public double? LzPercentage { get; set; }
        [Column("Threshold")]
        public double Threshold { get; set; }
        public string PositiveName { get; set; }
        public string NegativeName { get; set; }
        public Enumerations.DisplayLandingZoneStepType DisplayLandingZoneStepType { get; set; }
        public int DisplayLandingZoneCenter { get; set; }
        public int? DisplayLandingZonePlusExtrema { get; set; }
        public int? DisplayLandingZoneMinExtrema { get; set; }
        public ICollection<LensDetail> LensDetails { get; set; }
    }
}
