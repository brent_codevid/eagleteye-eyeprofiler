﻿using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class TangentList : List<CPTangentItem>, IEntity
    {
        public Common.Enumerations.TangentListPositioning TangentPosition { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
