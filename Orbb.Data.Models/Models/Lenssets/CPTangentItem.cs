﻿using Orbb.Data.Models.Interfaces;
using System;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class CPTangentItem : IEntity, IDesignListItem
    {
        public double CPTangentValue { get; set; }

        public string SupplierSpecificName { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
