﻿using Orbb.Common;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.LensSuppliers;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using Orbb.Data.Models.Implementations.LensFitting.NamingStrategies;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class LensDetail : IEntity
    {
        /* general lens data */
        public bool IsDraft { get; set; }
        public int? SupplierId { get; set; }
        public LensSupplier Supplier { get; set; }
        public int Type { get; set; }
        public string Name { get; set; }
        public bool RotateEyeDataBeforeFitting { get; set; }
        public bool UseNewMer1Selector { get; set; } //Select if the old system has to be used (flattest angle) or the new system (left,right,top, bottom, flat)
        public Enumerations.MeridianPosition Meridian1Position { get; set; }

        public List<Country> Countries { get; set; }

        public double? LensDiameter { get; set; }
        public double? LensDiameterMax { get; set; }
        public double? LensDiameterMin { get; set; }
        public double? LensDiameterStepSize { get; set; }
        public double? LensDiameterPerc { get; set; }
        public double? LensDiameterCp { get; set; }
        public double? HvidBuffer { get; set; }

        public int SuggestedApicalClearance { get; set; }
        public int SuggestedMidPeriphClearance { get; set; }
        public int SuggestedSettling { get; set; }

        public bool IncludeLzInLensName { get; set; }
        public double? LzInLensName1Correction { get; set; }
        public double? LzInLensName2Correction { get; set; }
        public double? LzInLensName3Correction { get; set; }
        public double? LzInLensName4Correction { get; set; }
        public double? LzInLensName5Correction { get; set; }

        public bool UseCP2ToCP1List { get; set; }
        public List<CP2ToCP1Item> CP2ToCP1List { get; set; } = new List<CP2ToCP1Item>();

        /*Limbus or HVID*/
        public int LandingZoneBase { get; set; }

        /* control point data */
        public bool UseCP1 { get; set; }
        public int? CP1Id { get; set; }
        public ControlPoint1 CP1 { get; set; }
        public bool UseCP2 { get; set; }
        public int? CP2Id { get; set; }
        public ControlPoint2 CP2 { get; set; }
        public bool UseCP3 { get; set; }
        public int? CP3Id { get; set; }
        public ControlPoint3 CP3 { get; set; }
        public bool UseCP4 { get; set; }
        public int? CP4Id { get; set; }
        public ControlPoint4 CP4 { get; set; }
        public bool UseCP5 { get; set; }
        public int? CP5Id { get; set; }
        public ControlPoint5 CP5 { get; set; }

        /* zone detail data */
        public int LandingzoneSectionCount { get; set; }
        public int CenterzoneSectionCount { get; set; }
        [NotMapped]
        public int SectionCount => LandingzoneSectionCount + CenterzoneSectionCount + 1;
        //bad but fast replace with list + grid in Gui if changes needed in future
        public bool UseHvidForDiameters { get; set; }
        public double HvidVal10 { get; set; }
        public double HvidVal105 { get; set; }
        public double HvidVal11 { get; set; }
        public double HvidVal115 { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

        public string GenerateFullSet()
        {
            StringBuilder result = new StringBuilder();
            result.Append(GeneralLensName());
            result.Append("\r\n");
            /* old version that only handles the original algo
            /*if (CP2ToCP1List.Count <= 0)
            {
                double cp1Counter = CP1.MinValue ?? 0;
                while (cp1Counter <= CP1.MaxValue)
                {
                    double cp2Counter = CP2.MinValue ?? 0;
                    while (cp2Counter <= CP2.MaxValue)
                    {
                        double cp4Counter = CP4.MinValue ?? 0;
                        while (cp4Counter <= CP4.MaxValue)
                        {
                            result.Append(GenerateSingleLine(cp1Counter, cp2Counter, cp4Counter, ";"));
                            cp4Counter += CP4.StepSize ?? 100;
                        }
                        cp2Counter += CP2.StepSize ?? 100;
                    }
                    cp1Counter += CP1.StepSize ?? 100;
                }
            }
            else
            {
                foreach (CP2ToCP1Item i in CP2ToCP1List)
                {
                    double cp4Counter = CP4.MinValue ?? 0;
                    while (cp4Counter <= CP4.MaxValue)
                    {
                        result.Append(GenerateSingleLine(i.CP1Value, i.CP2Value, cp4Counter, ";"));
                        cp4Counter += CP4.StepSize ?? 100;
                    }
                }
            }*/

            return result.ToString();
        }

        public string GenerateFittedLensName(LensFittingResults fittingData, bool includeName = true)
        {
            StringBuilder result = new StringBuilder();
            if (includeName)
            {
                result.Append(GeneralLensName(fittingData.Diameter));
                result.Append("\r\n");
            }
            result.Append(GenerateSingleLine(fittingData, " "));

            return result.ToString();
        }

        private string GenerateSingleLine(double cp1Vals, double cp2Vals, double cp3Vals, double cp4Vals, string delimiter)
        {
            LensFittingResults res = new LensFittingResults() { };
            res.CP1Result = new List<double>() { cp1Vals };
            res.CP2Result = new List<double>() { cp2Vals };
            res.CP3Result = new List<double>() { cp3Vals };
            res.CP4Result = new List<double>() { cp4Vals };
            return GenerateSingleLine(res, delimiter);
        }
        private string GenerateSingleLine(LensFittingResults lfr, string delimiter)
        {
            INamingStrategy namingStrategy;
            var output = "\t";
            if (UseCP1)
            {
                string cp1LinePart = "";

                namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp1);
                if (CP1.CP1SagCP2Percentage!=null)
                {
                    cp1LinePart = namingStrategy.GenerateCpLinePart(lfr.Cp1Diffs, CP1, CP1.DisplayValueFormat, lfr.Cp1Ids);
                }
                else
                {
                    cp1LinePart = namingStrategy.GenerateCpLinePart(lfr.CP1Result, CP1, CP1.DisplayValueFormat, lfr.Cp1Ids);
                }

                output = output + cp1LinePart + "\t" + delimiter;
            }
            if (UseCP2)
            {
                namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp2);

                string cp2LinePart = namingStrategy.GenerateCpLinePart(lfr.CP2Result, CP2, CP2.DisplayValueFormat, lfr.Cp2Ids);
                output = output + cp2LinePart + "\t" + delimiter;
            }
            if (UseCP3)
            {
                namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp3);

                string cp3LinePart = namingStrategy.GenerateCpLinePart(lfr.CP3Result, CP3, CP3.DisplayValueFormat, lfr.Cp3Ids);
                output = output + cp3LinePart + "\t" + delimiter;
            }
            if (UseCP4)
            {
                namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp4);
                string cp4LinePart = namingStrategy.GenerateCp4LinePart(CP4, lfr.CP4Result, lfr.Cp4Ids);
                output = output + cp4LinePart + "\r\n";
            }
            if (UseCP5)
            {
                namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp5);

                string cp5LinePart = namingStrategy.GenerateCpLinePart(lfr.CP5Result, CP5, CP5.DisplayValueFormat, lfr.Cp5Ids);
                output = output + cp5LinePart + "\t" + delimiter;
            }
            return output;
        }

        public IList<NameValueTuple> GenerateCP1FormattedList(IList<double> cp1Vals, LensFittingResults lfr, IList<int> cp1Ids)
        {
            INamingStrategy namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp1);
            IList<NameValueTuple> cpnames = namingStrategy.GenerateCpLinePartList(cp1Vals, CP1, CP1.DisplayValueFormat, lfr.Cp1Ids);
            var output = new List<NameValueTuple>();
            int i = 0;
            do
            {
                output.Add(cpnames[i]);
                i = ((i - 1) % cp1Vals.Count + cp1Vals.Count) % cp1Vals.Count;
            } while (i != 0);
            return cpnames;
        }

        public IList<NameValueTuple> GenerateCP2FormattedList(IList<double> cp2Vals, LensFittingResults lfr, IList<int> cp2Ids)
        {
            INamingStrategy namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp2);
            IList<NameValueTuple> cpnames = namingStrategy.GenerateCpLinePartList(lfr.CP2Result, CP2, CP2.DisplayValueFormat, lfr.Cp2Ids);
            var output = new List<NameValueTuple>();
            int i = 0;
            do
            {
                output.Add(cpnames[i]);
                i = ((i - 1) % cp2Vals.Count + cp2Vals.Count) % cp2Vals.Count;
            } while (i != 0);
            return cpnames;
        }

        public IList<NameValueTuple> GenerateCP3FormattedList(IList<double> cp3Vals, LensFittingResults lfr, IList<int> cp3Ids)
        {
            INamingStrategy namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp3);
            IList<NameValueTuple> cpnames = namingStrategy.GenerateCpLinePartList(lfr.CP3Result, CP3, CP3.DisplayValueFormat, lfr.Cp3Ids);
            var output = new List<NameValueTuple>();
            int i = 0;
            do
            {
                output.Add(cpnames[i]);
                i = ((i - 1) % cp3Vals.Count + cp3Vals.Count) % cp3Vals.Count;
            } while (i != 0);
            return cpnames;
        }

        public IList<NameValueTuple> GenerateCP4FormattedList(IList<double> cp4Vals, LensFittingResults lfr, IList<int> cp4Ids)
        {
            var output = new List<NameValueTuple>();
            var tmp = new List<double>();
            int i = 0;
            do
            {
                output.Add(new NameValueTuple()
                {
                    Value = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp4).ReplaceDisplayNameWildcardsCp4(cp4Vals[i], CP4, cp4Ids[i]),
                    Name = CP4.SupplierSpecificName
                }); 
                tmp.Add(cp4Vals[i]);
                i = ((i - 1) % cp4Vals.Count + cp4Vals.Count) % cp4Vals.Count;
            } while (i != 0);

            return output;
        }

        public IList<NameValueTuple> GenerateCP5FormattedList(IList<double> cp5Vals, LensFittingResults lfr, IList<int> cp5Ids)
        {
            INamingStrategy namingStrategy = SelectStrategyForCp(Enumerations.ControlPointEnum.Cp5);
            IList<NameValueTuple> cpnames = namingStrategy.GenerateCpLinePartList(lfr.CP5Result, CP5, CP5.DisplayValueFormat, lfr.Cp5Ids);
            var output = new List<NameValueTuple>();
            int i = 0;
            do
            {
                output.Add(cpnames[i]);
                i = ((i - 1) % cp5Vals.Count + cp5Vals.Count) % cp5Vals.Count;
            } while (i != 0);
            return cpnames;
        }

        public string GeneralLensName(double diameter = -1)
        {

            if (diameter != -1)
            {
                return $"{Supplier.Name} {Name} DIAM: {diameter}";
            }

            else if (LensDiameter != null)
            {
                return $"{Supplier.Name} {Name} DIAM:  {LensDiameter}";
            }
            else if (LensDiameterMin != null && LensDiameterMax != null && LensDiameterStepSize != null)
            {
                string diamRange = $"{LensDiameterMin} - {LensDiameterMax} ({LensDiameterStepSize})";
                return $"{Supplier.Name} {Name} DIAM:  {diamRange}";
            }
            return "";

        }

        public string GetTypeName()
        {
            foreach (Enumerations.LensTypes t in (Enumerations.LensTypes[])Enum.GetValues(typeof(Enumerations.LensTypes)))
            {
                if ((int)t == Type)
                {
                    return t.ToString();
                }
            }
            return "";
        }

        private INamingStrategy SelectStrategyForCp(Enumerations.ControlPointEnum CpType)
        {
            ControlPoint cp;
            switch (CpType)
            {
                case Enumerations.ControlPointEnum.Cp1:
                    cp = CP1;
                    break;
                case Enumerations.ControlPointEnum.Cp2:
                    cp = CP2;
                    break;
                case Enumerations.ControlPointEnum.Cp3:
                    cp = CP3;
                    break;
                case Enumerations.ControlPointEnum.Cp4:
                    cp = CP4;
                    break;
                case Enumerations.ControlPointEnum.Cp5:
                    cp = CP5;
                    break;
                default:
                    cp = null;
                    break;
            }
            switch (cp.CalculationMethod)
            {
                case Enumerations.CpCalculationMethod.Formula:
                    return new FormulaNamingStrategy();
                    break;
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    return new CurvatureDesignNamingStrategy();
                    break;
                case Enumerations.CpCalculationMethod.List:
                    return new ListNamingStrategy();
                    break;
                case Enumerations.CpCalculationMethod.TangentList:
                    return new TangentListNamingStrategy();
                    break;
                case Enumerations.CpCalculationMethod.AlignmentCurve:
                    return new AlignmentCurveNamingStrategy();
                    break;
                case Enumerations.CpCalculationMethod.Curvature:
                    return new CurvatureNamingStrategy();
                    break;
                default:
                    throw new NotImplementedException("No strategy defined for this calculationMethod");
                    break;
            }
        }
    }
}
