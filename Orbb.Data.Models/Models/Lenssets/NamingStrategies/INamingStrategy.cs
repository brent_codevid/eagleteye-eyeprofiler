﻿using Orbb.Common;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbb.Common.LensFitting;

namespace Orbb.Data.Models.Implementations.LensFitting.NamingStrategies
{
    public interface INamingStrategy
    {
        string GenerateCpLinePart(IList<double> cpVals, ControlPoint cp, string DisplayValueFormat, IList<int> ids);
        string GenerateCp4LinePart(ControlPoint4 cp4, IList<double> cp4Vals, IList<int> ids);
        string ReplaceDisplayValueFormatWildcards(string text, double value, ControlPoint cp, int id);
        string ReplaceDisplayNameWildcardsCp4(double value, ControlPoint4 cp4, int id);
        IList<NameValueTuple> GenerateCpLinePartList(IList<double> cpVals, ControlPoint cp, string displayValFormat, IList<int> ids);
    }
}
