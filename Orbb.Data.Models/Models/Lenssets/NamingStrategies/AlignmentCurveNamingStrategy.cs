﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.Data.Models.Implementations.LensFitting.NamingStrategies
{
    public class AlignmentCurveNamingStrategy : INamingStrategy
    {
        public string GenerateCpLinePart(IList<double> cpVals, ControlPoint cp, string displayValFormat, IList<int> ids)
        {
            StringBuilder formattedValueString = new StringBuilder();
            int i = 0;
            do
            {
                formattedValueString.Append(ReplaceDisplayValueFormatWildcards(displayValFormat, cpVals[i], cp, ids[i]) + " ");
                i = ((i - 1) % cpVals.Count + cpVals.Count) % cpVals.Count;
            } while (i != 0);

            return ReplaceDisplayCpFormatWildcards(cp.DisplayCpFormat, formattedValueString.ToString(), cp.SupplierSpecificName);
        }

        public IList<NameValueTuple> GenerateCpLinePartList(IList<double> cpVals, ControlPoint cp, string displayValFormat, IList<int> ids)
        {
            IList<NameValueTuple> resultList = new List<NameValueTuple>();
            int i = 0;
            do
            {
                string val;
                string name = cp.SupplierSpecificName;
                /*name = Regex.Replace(name, @"{name}", cp.SupplierSpecificName);
                name = Regex.Replace(name, @"{var}", "");
                name = Regex.Replace(name, @"{var:abs}", "");*/
                if (displayValFormat.Contains(":abs")){
                    val = Math.Abs(cpVals[i]).ToString();
                }
                else
                {
                    val = displayValFormat.Replace("{var}", cpVals[i].ToString());
                }

                resultList.Add(new NameValueTuple()
                {
                    Name = name,
                    Value = val

                });
                i = ((i - 1) % cpVals.Count + cpVals.Count) % cpVals.Count;
            } while (i != 0);

            return resultList;
        }

        public string GenerateCp4LinePart(ControlPoint4 cp4, IList<double> cp4Vals, IList<int> ids)
        {
            StringBuilder formattedValueString = new StringBuilder();
            int i = 0;
            do
            {

                formattedValueString.Append(ReplaceDisplayNameWildcardsCp4(cp4Vals[i], cp4, ids[i]) + " ");
                i = ((i - 1) % cp4Vals.Count + cp4Vals.Count) % cp4Vals.Count;
            } while (i != 0);
            return ReplaceDisplayCpFormatWildcards(cp4.DisplayCpFormat, formattedValueString.ToString(), cp4.SupplierSpecificName);
        }

        public string ReplaceDisplayValueFormatWildcards(string text, double value, ControlPoint cp, int id)
        {
            text = Regex.Replace(text, @"{name}", cp.SupplierSpecificName);
            text = Regex.Replace(text, @"{var:abs}", Math.Abs(value).ToString());
            text = Regex.Replace(text, @"{var}", value.ToString());

            return text;
        }

        private string ReplaceDisplayCpFormatWildcards(string displayCpFormat, string formattedValueString, string supplierSpecificName)
        {
            displayCpFormat = Regex.Replace(displayCpFormat, @"{vars}", formattedValueString);
            displayCpFormat = Regex.Replace(displayCpFormat, @"{name}", supplierSpecificName);
            return displayCpFormat;
        }

        private string ReplaceDisplayValueFormatWildcardscp4(IEnumerable<double> values, ControlPoint4 cp4, int id)
        {
            StringBuilder result = new StringBuilder();
            foreach (double value in values)
            {
                result.Append(ReplaceDisplayNameWildcardsCp4(value, cp4, id));
            }
            return result.ToString();
        }

        public string ReplaceDisplayNameWildcardsCp4(double value, ControlPoint4 cp4, int id)
        {
            StringBuilder result = new StringBuilder();
            string res;
            string nameFormat = value >= 0 ? cp4.PositiveName : cp4.NegativeName;
            switch (cp4.DisplayLandingZoneStepType)
            {
                case Enumerations.DisplayLandingZoneStepType.Steps:
                    {
                        double stepSize = 1;
                        if (cp4.DisplayLandingZoneMinExtrema != null && cp4.DisplayLandingZonePlusExtrema != null)
                        {
                            int oldSteps = (int)((cp4.MaxValue - cp4.MinValue) / cp4.StepSize);
                            stepSize = (double)(cp4.DisplayLandingZonePlusExtrema - cp4.DisplayLandingZoneMinExtrema) / oldSteps;
                        }
                        int steps = (int)(value / cp4.StepSize);
                        double valueInSteps = cp4.DisplayLandingZoneCenter + steps * stepSize;
                        res = Regex.Replace(nameFormat, @"{var}", valueInSteps.ToString());
                        res = Regex.Replace(res, @"{var:abs}", Math.Abs(valueInSteps).ToString());
                        break;
                    }
                case Enumerations.DisplayLandingZoneStepType.Microns:
                    res = Regex.Replace(nameFormat, @"{var}", value.ToString());
                    res = Regex.Replace(res, @"{var:abs}", Math.Abs(value).ToString());
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
            result.Append(Regex.Replace(res, @"{name}", cp4.SupplierSpecificName));
            return result.ToString();
        }

    }
}
