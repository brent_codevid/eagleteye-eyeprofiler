﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.Data.Models.Implementations.LensFitting.NamingStrategies
{
    public class TangentListNamingStrategy : INamingStrategy
    {
        public string GenerateCpLinePart(IList<double> cpVals, ControlPoint cp, string DisplayValueFormat, IList<int> ids)
        {
            StringBuilder formattedValueString = new StringBuilder();
            int i = 0;
            do
            {
                formattedValueString.Append(ReplaceDisplayValueFormatWildcards(DisplayValueFormat, cpVals[i], cp, ids[i]) + " ");
                i = ((i - 1) % cpVals.Count + cpVals.Count) % cpVals.Count;
            } while (i != 0);

            return ReplaceDisplayCpFormatWildcards(cp.DisplayCpFormat, formattedValueString.ToString(), cp.SupplierSpecificName);
        }

        public string GenerateCp4LinePart(ControlPoint4 cp4, IList<double> cp4Vals, IList<int> ids)
        {
            StringBuilder formattedValueString = new StringBuilder();
            int i = 0;
            do
            {
                formattedValueString.Append(ReplaceDisplayNameWildcardsCp4(cp4Vals[i], cp4, ids[i]) + " ");
                i = ((i - 1) % cp4Vals.Count + cp4Vals.Count) % cp4Vals.Count;
            } while (i != 0);
            return ReplaceDisplayCpFormatWildcards(cp4.DisplayCpFormat, formattedValueString.ToString(), cp4.SupplierSpecificName);
        }

        public string ReplaceDisplayValueFormatWildcards(string text, double value, ControlPoint cp, int id)
        {
            List<IDesignListItem> itemList = new List<IDesignListItem>();

            switch (cp.CalculationMethod)
            {
                case Enumerations.CpCalculationMethod.TangentList:
                    itemList = new List<IDesignListItem>(cp.CpTangentList.Cast<IDesignListItem>());
                    break;
                case Enumerations.CpCalculationMethod.List:
                    itemList = new List<IDesignListItem>(cp.CpList.Cast<IDesignListItem>());
                    break;
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    itemList = new List<IDesignListItem>(cp.CpCurvatureList.Cast<IDesignListItem>());
                    break;
            }


            text = Regex.Replace(text, @"{name}", cp.SupplierSpecificName);
            text = Regex.Replace(text, @"{var:name}", itemList.Single(c => c.Id == id).SupplierSpecificName);
            text = Regex.Replace(text, @"{var:abs}", Math.Abs(value).ToString());
            text = Regex.Replace(text, @"{var}", value.ToString());

            return text;
        }

        //layout for CpPart
        public string ReplaceDisplayCpFormatWildcards(string displayCpFormat, string formattedValueString, string supplierSpecificName)
        {
            displayCpFormat = Regex.Replace(displayCpFormat, @"{vars}", formattedValueString);
            displayCpFormat = Regex.Replace(displayCpFormat, @"{name}", supplierSpecificName);
            return displayCpFormat;
        }

        public string ReplaceDisplayValueFormatWildcardsCp4(IEnumerable<double> values, ControlPoint4 cp4, int id)
        {
            StringBuilder result = new StringBuilder();
            foreach (double value in values)
            {
                result.Append(ReplaceDisplayNameWildcardsCp4(value, cp4, id));
            }
            return result.ToString();
        }

        public string ReplaceDisplayNameWildcardsCp4(double value, ControlPoint4 cp4, int id)
        {
            StringBuilder result = new StringBuilder();
            string nameFormat = value >= 0 ? cp4.PositiveName : cp4.NegativeName;

            nameFormat = Regex.Replace(nameFormat, @"{var:name}", cp4.CpTangentList.Single(c => c.Id == id).SupplierSpecificName);
            nameFormat = Regex.Replace(nameFormat, @"{var:abs}", Math.Abs(value).ToString());
            nameFormat = Regex.Replace(nameFormat, @"{var}", value.ToString());
            nameFormat = Regex.Replace(nameFormat, @"{name}", cp4.SupplierSpecificName);

            result.Append(nameFormat);
            return result.ToString();
        }

        public IList<NameValueTuple> GenerateCpLinePartList(IList<double> cpVals, ControlPoint cp, string displayValFormat, IList<int> ids)
        {
            List<IDesignListItem> itemList = new List<IDesignListItem>();

            switch (cp.CalculationMethod)
            {
                case Enumerations.CpCalculationMethod.TangentList:
                    itemList = new List<IDesignListItem>(cp.CpTangentList.Cast<IDesignListItem>());
                    break;
                case Enumerations.CpCalculationMethod.List:
                    itemList = new List<IDesignListItem>(cp.CpList.Cast<IDesignListItem>());
                    break;
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    itemList = new List<IDesignListItem>(cp.CpCurvatureList.Cast<IDesignListItem>());
                    break;
            }
            IList<NameValueTuple> resultList = new List<NameValueTuple>();
            int i = 0;
            do
            {
                string val;
                string name = cp.SupplierSpecificName;
                /*name = Regex.Replace(name, @"{name}", cp.SupplierSpecificName);
                name = Regex.Replace(name, @"{var}", "");
                name = Regex.Replace(name, @"{var:abs}", "");*/

                val = itemList.Single(c => c.Id == ids[i]).SupplierSpecificName;

                resultList.Add(new NameValueTuple()
                {
                    Name = name,
                    Value = val

                });
                i = ((i - 1) % cpVals.Count + cpVals.Count) % cpVals.Count;
            } while (i != 0);

            return resultList;
        }
    }
}
