﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class ControlPoint3 : ControlPoint
    {
        [Column("RelativeSag")]
        public bool RelativeSag { get; set; }
        [Column("DisplayValueFormat")]
        public string DisplayValueFormat { get; set; }
        [Column("Threshold")]
        public double Threshold { get; set; }
        public bool? IncreaseClearance { get; set; }
        public double? MinimalClearance { get; set; }
        public double? SagCorrectionClearance { get; set; }

        public ICollection<LensDetail> LensDetails { get; set; }
    }
}
