﻿using Orbb.Common.Utility;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class CPCurvatureItem : IEntity, IDesignListItem
    {
        public string SupplierSpecificName { get; set; }
        public string RadiiString { get; set; }

        public List<double> Radii
        {
            private set { }
            get
            {
                if (RadiiString != null && RadiiString != "")
                {
                    return DataParser.ParseStringToList(RadiiString, ';');
                }
                else
                {
                    return new List<double>() { 0 };
                }
            }
        }

        public string ExcentricitiesString { get; set; }

        public List<double> Excentricities
        {
            private set { }
            get
            {
                if (ExcentricitiesString != null && ExcentricitiesString != "")
                {
                    return DataParser.ParseStringToList(ExcentricitiesString, ';');
                }
                else
                {
                    return new List<double>() { 0 };
                }
            }
        }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
