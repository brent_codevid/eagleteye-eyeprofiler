﻿using Orbb.Common;
using Orbb.Data.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class ControlPoint : IEntity
    {
        public int Category { get; set; }
        public bool RelativeChordLength { get; set; }
        public double? DiameterMultiplier { get; set; }
        public double? SemiChordLength { get; set; }

        public string SupplierSpecificName { get; set; }
        public string DisplayCpFormat { get; set; }
        public bool IncludeInApiResult { get; set; }

        public double? MinValue { get; set; }
        public double? MaxValue { get; set; }
        public double? StepSize { get; set; }

        public double Lift { get; set; }
        public double Correction { get; set; }

        public double? StandardVal1 { get; set; }
        public double? StandardVal2 { get; set; }
        public double? StandardVal3 { get; set; }
        public double? StandardVal4 { get; set; }

        public Enumerations.CpRoundingStrategy RoundingStrategy { get; set; }
        public Enumerations.CpCalculationMethod CalculationMethod { get; set; }


        public List<CPListItem> CpList { get; set; } = new List<CPListItem>();
        public List<CPTangentItem> CpTangentList { get; set; } = new List<CPTangentItem>();
        public Enumerations.TangentListPositioning TangentPosition { get; set; }
        public List<CPCurvatureItem> CpCurvatureList { get; set; } = new List<CPCurvatureItem>();

        public string CurvatureDiametersString { get; set; }
        public int CurvatureSectionCount { get; set; }

        public List<IDesignListItem> GetDesignList() {
            switch (CalculationMethod)
            {
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    return CpCurvatureList.Cast<IDesignListItem>().ToList();
                    break;
                case Enumerations.CpCalculationMethod.List:
                    return CpList.Cast<IDesignListItem>().ToList();
                    break;
                case Enumerations.CpCalculationMethod.TangentList:
                    return CpTangentList.Cast<IDesignListItem>().ToList();
                    break;
                default:
                    return null;
                    break;
            }

        }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion

    }
}
