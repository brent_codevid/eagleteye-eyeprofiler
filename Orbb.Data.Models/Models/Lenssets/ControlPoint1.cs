﻿using Orbb.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class ControlPoint1 : ControlPoint
    {
        [Column("RelativeSag")]
        public bool? RelativeSag { get; set; }
        public double? CP1SagCP2Percentage { get; set; }
        public double? CP1SagCP2Correction { get; set; }
        public double? CP1SagCP2MinValue { get; set; }
        public double? CP1SagCP2MaxValue { get; set; }
        public double? CP1SagCP2StepSize { get; set; }
        public bool? AddCalculatedCP1SagCP2PercentageToOtherCPs { get; set; }
        public Enumerations.MeasurementUnit MeasurementUnit { get; set; }
        public ICollection<LensDetail> LensDetails { get; set; }
        [Column("DisplayValueFormat")]
        public string DisplayValueFormat { get; set; }
    }
}
