﻿using System.Collections.Generic;
using static Orbb.Common.Enumerations;

namespace Orbb.Data.Models.Models.Lenssets
{
    public class LensInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Supplier { get; set; }
        public string Type { get; set; }
        public IList<double> Diameters { get; set; }
        public bool IsDraft { get; set; }

        public LensInfo(LensDetail ld)
        {

            Id = ld.Id;
            Name = ld.Name;
            Supplier = ld.Supplier.Name;
            IsDraft = ld.IsDraft;
            Diameters = new List<double>();
            Type = ((LensTypes)ld.Type).ToString();
            if (!ld.UseHvidForDiameters)
            {
                if (ld.LensDiameter != null)
                {
                    Diameters.Add((double)ld.LensDiameter);
                }
                else
                {
                    double t = (double)ld.LensDiameterMin;
                    while (t <= (double)ld.LensDiameterMax)
                    {
                        Diameters.Add(t);
                        t += (double)ld.LensDiameterStepSize;
                    }
                }
            }
        }
    }
}
