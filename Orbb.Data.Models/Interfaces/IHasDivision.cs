﻿using Orbb.Data.Models.Models.System;

namespace Orbb.Data.Models.Interfaces
{
    public interface IHasDivision
    {
        int DivisionId { get; set; }
        Division Division { get; set; }
    }
}
