﻿using System;

namespace Orbb.Data.Models.Interfaces
{
    public interface IEntity
    {
        int Id { get; set; }

        string CreatedBy { get; set; }
        DateTime? CreatedOn { get; set; }
        string LastModifiedBy { get; set; }
        DateTime? LastModifiedOn { get; set; }
    }
}
