﻿using Orbb.Data.Models.Models.System;
using System.Collections.Generic;

namespace Orbb.Data.Models.Interfaces
{
    public interface IHasDivisions
    {
        List<Division> Divisions { get; set; }
    }
}
