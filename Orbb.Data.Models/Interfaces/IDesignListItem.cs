﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.Data.Models.Interfaces
{
    public interface IDesignListItem
    {
        string SupplierSpecificName { get; set; }
        int Id { get; set; }
    }
}
