﻿using System;

namespace Orbb.Data.Models.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DivisionTypeAttribute : Attribute
    {
        public Type Type { get; set; }
        public DivisionTypeAttribute(Type type)
        {
            Type = type;
        }
    }
}
