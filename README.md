# Orbb project

## Requirements

- Visual Studio 2017
- Visual Studio 2017 extension 'Bundelr & Minifier' (Mads Kristensen)
- Asp.net core 2 SDK
- NuGet CLI: [download latest version](https://www.nuget.org/downloads)
	Add the folder where you placed nuget.exe to your PATH environment variable to use the CLI tool from anywhere ([moreinfo](https://docs.microsoft.com/en-us/nuget/install-nuget-client-tools#dotnetexe-cli)).
- Node.js: [download latest version](https://nodejs.org) ((npm is installed with Node.js)

## Getting Started

- Add Kendo private nuget repository: 
 ```sh
	nuget Sources Add -Name "telerik.com" -Source "https://nuget.telerik.com/nuget" -UserName "david@codevid.be" -Password "[Check with David]"
```
The above command stores a token in the %AppData%\NuGet\NuGet.config 
- Open project & build

- Run update-database EF command in package manager console to create empty database
 ```sh
	update-database -startup: Template.data.ef
```

- Add stored procedures (Orbb.Data.Models\Scripts):

```sh
	spAddMenuItem.txt
	spAddTranslationEntry.txt
	spWebViewSecuredObjectPermissionsForUserGroup.txt
	spWebViewUserGroupPermissions.txt
	spWebViewUserGroups.txt
	spWebViewUsers.txt
```

 - NLog logging framework used
	Configuration can be done in nlog.config, more info [NLog](http://nlog-project.org)
	Create log table & stored procedures (Orbb.Data.Models\Scripts):
```sh
	CreateLogTable.txt
	spAddLogEntry.txt
```
	
- Setup database: run /data/scripts/seed empty database.sql
- Modify connection string config.ini file
- Run project