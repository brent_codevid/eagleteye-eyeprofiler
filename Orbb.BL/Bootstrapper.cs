﻿using Orbb.BL.Implementations;
using Orbb.BL.Implementations.Base;
using Orbb.BL.Implementations.Country;
using Orbb.BL.Implementations.Customers;
using Orbb.BL.Implementations.Datacapture;
using Orbb.BL.Implementations.Document;
using Orbb.BL.Implementations.General;
using Orbb.BL.Implementations.LensFitting;
using Orbb.BL.Implementations.Lenssets;
using Orbb.BL.Implementations.LensSuppliers;
using Orbb.BL.Implementations.Media;
using Orbb.BL.Implementations.Orders;
using Orbb.BL.Implementations.Security;
using Orbb.BL.Implementations.Settings;
using Orbb.BL.Implementations.UISetup;
using Orbb.BL.Implementations.Utility.Security;
using Orbb.BL.Implementations.Utility.Validation;
using Orbb.BL.Interfaces;
using Orbb.BL.Interfaces.Base;
using Orbb.BL.Interfaces.Country;
using Orbb.BL.Interfaces.Customers;
using Orbb.BL.Interfaces.Datacapture;
using Orbb.BL.Interfaces.Document;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.LensFitting;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.BL.Interfaces.LensSuppliers;
using Orbb.BL.Interfaces.Media;
using Orbb.BL.Interfaces.Orders;
using Orbb.BL.Interfaces.Security;
using Orbb.BL.Interfaces.Settings;
using Orbb.BL.Interfaces.UISetup;
using Orbb.BL.Interfaces.Utility.Security;
using Orbb.BL.Interfaces.Utility.Validation;
using Orbb.BL.Tools.AutoMapper;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.EF.EntityInitializers;
using Orbb.Data.EF.EntityTriggers;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.EF.Utility;
using Orbb.Data.Models.Models.Security;
using StructureMap;
using System;
using System.Linq;
using System.Reflection;

namespace Orbb.BL
{
    public static class Bootstrapper
    {
        public static IContainer container;
        public static void RegisterWithContainer(IContainer container)
        {
            // When you receive the error that IDisposable is not defined => Include reference to %ProgramFiles(x86)%\Reference Assemblies\Microsoft\Framework\.NETFramework\v4.5\Facades\System.Runtime.dll
            container.Configure(c =>
            {
                c.For<IUserGroupsRepository>().Use<UserGroupsRepository>();
                c.For<IUserRepository>().Use<UserRepository>();
                c.For<IUserValidation>().Use<UserValidation>();
                c.For<ILookupProvider>().Use<LookupProvider>();
                c.For<IMenuRepository>().Use<MenuRepository>();
                c.For<IDataService>().Use<DataService>();
                c.For<IRepository>().Use<Repository<User>>(); // User is not really used, used here as dummy
                c.For(typeof(IRepository<>)).Use(typeof(Repository<>));
                c.For<IProcedureRepository>().Use<ProcedureRepository>();
                c.For<IInstallationSettingsRepository>().Use<InstallationSettingsRepository>();
                c.For<IDocumentRepository>().Use<DocumentRepository>();
                c.For<IEncryptionService>().Use<EncryptionService>();
                c.For<IEmailValidator>().Use<EmailValidator>();
                c.For<IDivisionRepository>().Use<DivisionRepository>();
                c.For<IMediaService>().Use<MediaService>();
                c.For<IMediaRepository>().Use<MediaRepository>();
                c.For<ILensTypeRepository>().Use<LensTypeRepository>();
                c.For<IControlpointCategoryRepository>().Use<ControlpointCategoryRepository>();
                c.For<ILensSupplierRepository>().Use<LensSupplierRepository>();
                c.For<ILensRepository>().Use<LensRepository>();
                c.For<ILensFitter>().Use<LensFitter>();
                c.For<ICustomerRepository>().Use<CustomerRepository>();
                c.For<IOrderRepository>().Use<OrderRepository>();
                c.For<IOrderStatusRepository>().Use<OrderStatusRepository>();
                c.For<ILensfittingCaptureRepository>().Use<LensfittingCaptureRepository>();
                c.For<ICountryRepository>().Use<CountryRepository>();
                c.For<IControlPointRepository>().Use<ControlPointRepository>();
            });

            if (container.TryGetInstance<DatabaseContext>() == null)
            {
                Orbb.Data.EF.Bootstrapper.RegisterWithContainer(container);
            }

            RegisterReverseMappings(container);
            RegisterValidators(container);
            RegisterTriggers(container);
            RegisterDeleters(container);
            RegisterInitializers(container);

            TranslationConverter.Container = container;
            Bootstrapper.container = container;
        }

        private static void RegisterInitializers(IContainer container)
        {
            IEntityInitializerFactory factory = container.GetInstance<IEntityInitializerFactory>();
            var types = Assembly.GetExecutingAssembly().GetTypes();
            var typesToRegister = types
                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                           .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                                               type.BaseType.GetGenericTypeDefinition() ==
                                               typeof(EntityInitializerBase<>));
            foreach (var initializerType in typesToRegister)
            {
                Type initForType = initializerType.GetMethods()[0].GetParameters()[0].ParameterType;
                //var initializer = Activator.CreateInstance(initializerType);
                factory.RegisterInitializer(initForType, initializerType);
            }
        }

        private static void RegisterDeleters(IContainer container)
        {
            IEntityDeleterFactory factory = container.GetInstance<IEntityDeleterFactory>();
            var types = Assembly.GetExecutingAssembly().GetTypes();
            var typesToRegister = types
                                          .Where(type => !String.IsNullOrEmpty(type.Namespace))
                                           .Where(type => type.BaseType != null && type.BaseType.IsGenericType &&
                                               type.BaseType.GetGenericTypeDefinition() ==
                                               typeof(EntityDeleteBase<>));
            foreach (var initializerType in typesToRegister)
            {
                Type initForType = initializerType.BaseType.GenericTypeArguments[0];//GetMethods()[0].GetParameters()[0].ParameterType;
                var deleter = container.GetInstance(initializerType);
                factory.RegisterTrigger(deleter, initForType);
            }
        }

        private static void RegisterTriggers(IContainer container)
        {
            IEntityTriggerFactory factory = container.GetInstance<IEntityTriggerFactory>();

            var types = Assembly.GetExecutingAssembly().GetTypes();
            var typesToRegister = types.Where(type => type.GetInterface("IEntityTrigger") != null).ToList();
            foreach (var t in typesToRegister)
            {
                IEntityTrigger trigger = (IEntityTrigger)container.GetInstance(t);
                factory.RegisterTrigger(trigger, trigger.OfEntityType);
            }
        }

        private static void RegisterValidators(IContainer container)
        {
            IEntityValidationRuleFactory factory = container.GetInstance<IEntityValidationRuleFactory>();

            var types = Assembly.GetExecutingAssembly().GetTypes();
            var typesToRegister = types.Where(type => type.GetInterface("IEntityValidationRule") != null).ToList();
            foreach (var t in typesToRegister)
            {
                IEntityValidationRule validator = (IEntityValidationRule)container.GetInstance(t);
                factory.RegisterRule(validator, validator.OfEntityType);
            }
        }

        private static void RegisterReverseMappings(IContainer container)
        {
            //IReverseMapperFactory reverseMapperFactory = new ReverseMapperFactory();
            //reverseMapperFactory.DefineReverseMapper<CustomerDetailVM, Customer>(container.Resolve<CustomerDetailVMToCustomerRevMap>());

            //container.RegisterInstance<IReverseMapperFactory>(reverseMapperFactory);

            //IReverseMapperFactory reverseMapperFactory1 = new ReverseMapperFactory();
            //reverseMapperFactory1.DefineReverseMapper<SupplierDetailVM, Supplier>(container.Resolve<SupplierDetailVMToSupplierRevMap>());

            //container.RegisterInstance<IReverseMapperFactory>(reverseMapperFactory1);
        }
    }
}
