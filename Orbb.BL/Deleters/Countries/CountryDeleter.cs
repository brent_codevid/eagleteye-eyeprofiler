﻿using Orbb.BL.Interfaces.Customers;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Customers;
using StructureMap.Attributes;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.LensDataSets
{
    public class CountryDeleter : EntityDeleteBase<Country>
    {

        [SetterProperty]
        public ICustomerRepository CustomerRepository { get; set; }

        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }

        public override bool BeforeDelete(IDatabaseContext dbContext, Country entity, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory)
        {
            List<Customer> customers = CustomerRepository.GetAll(c => c.CountryId == entity.Id, new string[] { });
            foreach (var customer in customers)
            {
                customer.CountryId = null;
                customer.Country = null;
                CustomerRepository.Update(customer, currentUser);
            }
            return true;
        }

    }
}
