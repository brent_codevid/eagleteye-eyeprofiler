﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Orders;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.Orders
{
    public class OrderDeleter : EntityDeleteBase<Order>
    {
        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }
    }
}
