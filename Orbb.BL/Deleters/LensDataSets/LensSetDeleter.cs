﻿using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Lenssets;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.LensDataSets
{
    public class LensDataSetDeleter : EntityDeleteBase<LensDetail>
    {
        [SetterProperty]
        public ILensRepository LensRepository { get; set; }

        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }

        public override bool BeforeDelete(IDatabaseContext dbContext, LensDetail entity, ICurrentUser currentUser, IEntityDeleterFactory deleterFactory)
        {
            var cpDeleter = deleterFactory.GetEntityDeleter<ControlPoint>();

            if (entity.CP1Id is int cp1Id)
            {
                cpDeleter.Delete(dbContext, cp1Id, currentUser, deleterFactory);
            }

            if (entity.CP2Id is int cp2Id)
            {
                cpDeleter.Delete(dbContext, cp2Id, currentUser, deleterFactory);
            }

            if (entity.CP4Id is int cp4Id)
            {
                cpDeleter.Delete(dbContext, cp4Id, currentUser, deleterFactory);
            }

            var cp2SagItemDeleter = deleterFactory.GetEntityDeleter<CP2ToCP1Item>();

            try
            {
                var results = LensRepository.GetCP2ToCP1ItemsByLensId(entity.Id);
                foreach (CP2ToCP1Item result in results)
                {
                    cp2SagItemDeleter.Delete(dbContext, result.Id, currentUser, deleterFactory);
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return true;
        }
    }
}
