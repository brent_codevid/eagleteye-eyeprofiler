﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Lenssets;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.LensDataSets
{
    public class CP2ToCP1ItemDeleter : EntityDeleteBase<CP2ToCP1Item>
    {
        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }
    }
}
