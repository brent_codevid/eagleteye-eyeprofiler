﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Customers;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.Customers
{
    public class CustomerDeleter : EntityDeleteBase<Customer>
    {
        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }
    }
}
