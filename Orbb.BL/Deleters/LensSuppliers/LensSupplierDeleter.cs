﻿using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.LensSuppliers;
using StructureMap.Attributes;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Deleters.LensSuppliers
{
    public class LensSupplierDeleterDeleter : EntityDeleteBase<LensSupplier>
    {
        [SetterProperty]
        public ILensRepository LensRepository { get; set; }

        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (LensRepository.CountLenssetWithSupplier(id) > 0)
            {
                reasonsList.Add("G_SUPPLIER_STILL_IN_USE");
            }

            if (!reasonsList.Any()) return true;

            reasons = reasonsList.ToArray();
            return false;
        }
    }
}
