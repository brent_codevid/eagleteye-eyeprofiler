﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Security;
using System.Linq;

namespace Orbb.BL.Deleters.Security
{
    public class SecuredObjectDeleter : EntityDeleteBase<SecuredObject>
    {
        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (reasons.Any())
            {
                return false;
            }

            return true;
        }

    }
}
