﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using System.Linq;

namespace Orbb.BL.Deleters.Media
{
    class MediaDeleter : EntityDeleteBase<Orbb.Data.Models.Models.Base.Media>
    {
        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };

            if (id <= 0)
            {
                reasons = new[] { "G_NOT_SAVED" };
                return false;
            }

            if (reasons.Any())
            {
                return false;
            }

            return true;
        }
    }
}
