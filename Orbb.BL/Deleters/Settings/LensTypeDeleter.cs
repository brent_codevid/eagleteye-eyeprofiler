﻿using System.Collections.Generic;
using System.Linq;
using Orbb.BL.Interfaces.LensDataSets;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.Models.Models.Settings;
using StructureMap.Attributes;

namespace Orbb.BL.Deleters.Base
{
    public class LensTypeDeleter : EntityDeleteBase<LensType>
    {
        [SetterProperty]
        public ILensRepository LensRepository { get; set; }

        public override bool CanDelete(IDatabaseContext dbContext, int id, IEntityDeleterFactory deleterFactory, out string[] reasons)
        {
            reasons = new string[] { };
            List<string> reasonsList = new List<string>();
            if (id <= 0)
            {
                reasons = new string[] { "G_NOT_SAVED" };
                return false;
            }

            if (LensRepository.CountLenssetWithType(id) > 0)
            {
                reasonsList.Add("G_TYPE_STILL_IN_USE");
            }

            if (reasonsList.Count() > 0)
            {
                reasons = reasonsList.ToArray();
                return false;
            }

            return true;
        }

    }
}
