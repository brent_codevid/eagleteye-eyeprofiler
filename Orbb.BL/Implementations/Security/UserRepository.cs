﻿using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Security;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.System;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Globalization;
using Orbb.Data.ViewModels.Models.Other;
using Orbb.Data.ViewModels.Models.Security;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using static Orbb.Data.Common.Enumerations;

namespace Orbb.BL.Implementations.Security
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        private readonly IDataService dataService;

        public UserRepository(IDatabaseContext dbContext, ICurrentUser currentUser, IDataService dataService) : base(dbContext)
        {
            this.CurrentUser = currentUser;
            this.dataService = dataService;
        }

        public IList<UserView> GetUsersOverview()
        {
            var languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<UserView>("spWebViewUsers @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }

        public string UpdateUserLanguage(int languageId)
        {
            Language lang = DbContext.Query<Language>().SingleOrDefault(l => l.Id == languageId);

            if (lang == null)
                throw new Exception("UpdateUserLanguage: language not found --> " + languageId.ToString());

            UserVm currentUserInstance = CurrentUser.GetCurrentUser();

            User user = GetSingle(u => u.Id == currentUserInstance.Id);
            user.LanguageId = languageId;
            DbContext.Update(user);
            DbContext.SaveChanges(CurrentUser);

            currentUserInstance.Language = Mapper.Map<LanguageVM>(lang);
            currentUserInstance.LanguageId = languageId;
            CurrentUser.SetCurrentUser(currentUserInstance);

            return lang.Code;
        }

        public int UpdateUserDivision(int? divisionId)
        {
            Division division = DbContext.Query<Division>().FirstOrDefault(d => d.Id == divisionId);

            UserVm currUserInstance = CurrentUser.GetCurrentUser();
            CurrentUser.SetCurrentUser(null);

            User user = GetSingle(u => u.Id == currUserInstance.Id);
            user.DefaultDivisionId = divisionId;
            DbContext.Update(user);
            DbContext.SaveChanges(CurrentUser);

            currUserInstance.DefaultDivisionId = divisionId;

            CurrentUser.SetCurrentUser(currUserInstance);

            return division?.Id ?? 0;
        }
        public IList<ComboBoxItemVm> GetUsersForCombobox()
        {
            var users = DbContext.Query<User>().Select(t => new ComboBoxItemVm() { Id = t.Id, Text = String.Concat(t.FirstName, " ", t.LastName) });
            return users.ToList();
        }

        public List<UserDetailVm> GetUsersByType(UserTypes userType)
        {
            return dataService.GetAll<User, UserDetailVm>(null).Where(u => u.TypeId == (int)userType).ToList();
        }
    }
}
