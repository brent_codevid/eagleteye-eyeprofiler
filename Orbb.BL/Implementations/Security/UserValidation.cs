﻿using AutoMapper;
using Orbb.BL.Data.Security;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Environment;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.ViewModels.Models.Security;
using Orbb.Data.ViewModels.Models.System;
using System.Data.Entity;
using System.Linq;
using static Orbb.Data.Common.Enumerations;

namespace Orbb.BL.Implementations.Security
{
    public class UserValidation : IUserValidation
    {
        private readonly ICurrentUser currentUser;
        private readonly IMapper mapper;

        public IDatabaseContext DbContext { get; set; }


        private readonly ICurrent<DivisionVM> currentDivision;
        public UserValidation(ICurrentUser currentUser,
            IDatabaseContext dbContext,
            ICurrent<DivisionVM> currentDivision,
            IMapper mapper)
        {
            this.currentUser = currentUser;
            this.DbContext = dbContext;
            this.currentDivision = currentDivision;
            this.mapper = mapper;
        }

        public bool ValidateUser(string username, string password)
        {
            var user = ValidateUserPassword(username, password);

            return user != null;
        }

        public bool ValidateUser(string username, string password, out User user)
        {
            user = ValidateUserPassword(username, password);

            var userVm = mapper.Map<UserVm>(user);

            currentUser.SetCurrentUser(userVm);

            user?.TouchUser(DbContext);

            return user != null;
        }

        private User ValidateUserPassword(string username, string password)
        {
            var user = DbContext.Query<User>().Where(u => u.UserName == username).Include(u => u.Language).FirstOrDefault();

            if (user == null)
                return null;

            // validate password with hash & salt
            var userSalt = user.Salt;
            var hash = new StringHash().HashWithSalt(password, userSalt);

            if (user.Password != hash)
                return null;

            return user;
        }

        public bool UserHasAccess(int userId, string securedType)
        {
            var user = DbContext.Query<User>().Where(u => u.Id == userId).Include("UserGroup.Permissions.SecuredObject").Include("Divisions").FirstOrDefault();
            if (user == null)
                return false;
            else
            {
                // do some role checking
                bool? access = user.UserGroup?.Permissions.Any(p => p.SecuredObject.Object == securedType.ToUpper() && p.Read);
                // do division checking
                var curDivision = currentDivision?.GetCurrent();
                if (Config.UseDivisions && curDivision != null)
                    access &= user.Divisions.Any(d => d.ShortCode == curDivision.ShortCode);

                return access == true;
            }
        }

        public void LogOut()
        {
            currentUser.SetCurrentUser(null);
        }

        public bool UserHasAccess(int userId, string securedType, PermissionType permissionType)
        {
            var user = DbContext.Query<User>().Where(u => u.Id == userId).Include("UserGroup.Permissions.SecuredObject").Include("Divisions").FirstOrDefault();
            if (user == null)
                return false;
            else
            {
                bool? access;

                // do some role checking
                switch (permissionType)
                {

                    case PermissionType.Add:
                        access = user.UserGroup?.Permissions.Any(p => p.SecuredObject.Object == securedType.ToUpper() && p.Add);
                        break;
                    case PermissionType.Update:
                        access = user.UserGroup?.Permissions.Any(p => p.SecuredObject.Object == securedType.ToUpper() && p.Update);
                        break;
                    case PermissionType.Delete:
                        access = user.UserGroup?.Permissions.Any(p => p.SecuredObject.Object == securedType.ToUpper() && p.Delete);
                        break;
                    default:
                        access = false;
                        break;
                }

                // do division checking
                var curDivision = currentDivision?.GetCurrent();
                if (Config.UseDivisions && curDivision != null)
                    access &= user.Divisions.Any(d => d.ShortCode == curDivision.ShortCode);

                return access == true;
            }
        }

        public bool UserHasReadOnlyAcces(int userId, string securedType)
        {
            var user = DbContext.Query<User>().Where(u => u.Id == userId).Include("UserGroup.Permissions.SecuredObject").Include("Divisions").FirstOrDefault();
            if (user == null)
                return false;
            else
            {
                // do some role checking               
                var readOnlyAccess = user.UserGroup?.Permissions
                    .Any(p => p.SecuredObject.Object == securedType.ToUpper() && !p.Add && !p.Update && !p.Delete);


                // do division checking
                var curDivision = currentDivision?.GetCurrent();
                if (Config.UseDivisions && curDivision != null)
                    readOnlyAccess &= user.Divisions.Any(d => d.ShortCode == curDivision.ShortCode);

                return readOnlyAccess == true;
            }
        }
    }
}
