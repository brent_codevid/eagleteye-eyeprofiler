﻿using Orbb.BL.Interfaces.Security;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Security;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Security
{
    public class UserGroupsRepository : Repository<UserGroup>, IUserGroupsRepository
    {
        public UserGroupsRepository(IDatabaseContext dbContext) : base(dbContext)
        {
        }

        public IList<UserGroupView> GetUserGroupsOverview()
        {
            var languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<UserGroupView>("spWebViewUserGroups @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }

        public IList<UserGroupDetailVM> GetUserGroupsAndPermissions()
        {
            var languageIdParameter = new SqlParameter("@LanguageId", 1);
            var userGroupIdParameter = new SqlParameter("@userGroupId", 1);
            var result = DbContext.QuerySql<UserGroupDetailVM>("spWebViewUserGroupPermissions @LanguageId @userGroupId", languageIdParameter, userGroupIdParameter)
                .ToList();
            return result;
        }

        public void CreateUserGroupObjectPermissions(int userGroupId)
        {
            var userGroupParameter = new SqlParameter("@userGroupId", userGroupId);
            var result = DbContext.QuerySql<SecuredObjectPermissionVm>("spWebViewSecuredObjectPermissionsForUserGroup @userGroupId", userGroupParameter)
                .ToList();
            foreach (var perm in result.Where(x => x.Id == 0))
            {
                // add this new permissions
                var entity = new SecuredObjectPermission
                {
                    Id = perm.Id,
                    SecuredObjectId = perm.SecuredObjectId,
                    UserGroupId = userGroupId,
                    Add = perm.Add,
                    Read = perm.Read,
                    Update = perm.Update,
                    Delete = perm.Delete,
                };
                DbContext.Add(entity);
                DbContext.SaveChanges(CurrentUser);
            }
        }
    }
}
