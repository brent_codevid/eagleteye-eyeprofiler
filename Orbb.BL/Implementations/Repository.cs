﻿using AutoMapper;
using Orbb.BL.Interfaces;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Orbb.BL.Implementations
{
    public class Repository<T> : IRepository<T> where T : class, IEntity
    {
        protected IDatabaseContext DbContext { get; set; }
        [SetterProperty]
        public ICurrent<DivisionVM> CurrentDivision { get; set; }
        [SetterProperty]
        public ICurrentUser CurrentUser { get; set; }
        [SetterProperty]
        public IMapper Mapper { get; set; }

        public Repository(IDatabaseContext dbContext)
        {
            this.DbContext = dbContext;
        }

        protected int CurrentUserLanguageId => CurrentUser?.GetCurrentUser()?.LanguageId ?? 1;

        protected int CurrentDivisionId => CurrentDivision?.GetCurrent()?.Id ?? 1;

        public T GetSingle(Expression<Func<T, bool>> predicate, string[] includeStrings = null)
        {
            return GetSingle<T>(predicate, includeStrings);
        }
        public Y GetSingle<Y>(Expression<Func<Y, bool>> predicate, string[] includeStrings = null) where Y : class, IEntity
        {
            IQueryable<Y> query = BuildQuery(predicate, includeStrings);
            return query.FirstOrDefault();
        }
        public List<Y> GetAll<Y>(Expression<Func<Y, bool>> predicate, string[] includeStrings) where Y : class, IEntity
        {
            var query = BuildQuery(predicate, includeStrings);
            return query.ToList();
        }
        public List<T> GetAll(Expression<Func<T, bool>> predicate, string[] includeStrings)
        {
            return GetAll<T>(predicate, includeStrings);
        }

        public List<T> GetAll(string[] includeStrings)
        {
            return GetAll<T>(includeStrings);
        }
        public List<Y> GetAll<Y>(string[] includeStrings) where Y : class, IEntity
        {
            var query = BuildQuery<Y>(null, includeStrings);
            return query.ToList();
        }
        private IQueryable<T> BuildQuery(Expression<Func<T, bool>> predicate, string[] includeStrings)
        {
            return BuildQuery<T>(predicate, includeStrings);
        }

        private IQueryable<Y> BuildQuery<Y>(Expression<Func<Y, bool>> predicate, string[] includeStrings) where Y : class, IEntity
        {
            var query = DbContext.Query<Y>();
            if (predicate != null)
                query = query.Where(predicate);
            if (typeof(IHasDivisions).IsAssignableFrom(typeof(Y)) && Config.UseDivisions && CurrentDivision?.GetCurrent() != null)
            {
                int divisionId = CurrentDivision.GetCurrent().Id;
                query = ((IQueryable<IHasDivisions>)query).Where(x => x.Divisions.Any(d => d.Id == divisionId)).Cast<Y>();
            }
            if (includeStrings != null)
                foreach (string include in includeStrings)
                    query = query.Include(include);
            return query;
        }

        public void Add(T entity, ICurrentUser currentUser)
        {
            Add<T>(entity, currentUser);
        }

        public void Add<Y>(Y entity, ICurrentUser currentUser) where Y : class, IEntity
        {
            DbContext.Add(entity);
            DbContext.SaveChanges(currentUser);
        }

        public void Update(T entity, ICurrentUser currentUser)
        {
            Update<T>(entity, currentUser);
        }

        public void Update<Y>(Y entity, ICurrentUser currentUser) where Y : class, IEntity
        {
            DbContext.Update(entity);
            DbContext.SaveChanges(currentUser);
        }

        V IRepository.GetSingle<Y, V>(Expression<Func<Y, bool>> predicate, string[] includeStrings)
        {
            Y getSingle = GetSingle(predicate, includeStrings);
            return Mapper.Map<V>(getSingle);
            //return AutoMapper.Mapper.Map<V>(getSingle);
        }

        List<V> IRepository.GetAll<Y, V>(Expression<Func<Y, bool>> predicate, string[] includeStrings)
        {
            return Mapper.Map<List<V>>(GetAll(predicate, includeStrings));
            // return AutoMapper.Mapper.Map<List<V>>(GetAll<Y>(predicate, includeStrings));
        }

        List<V> IRepository.GetAll<Y, V>(string[] includeStrings)
        {
            return Mapper.Map<List<V>>(GetAll<Y>(includeStrings));
            // return AutoMapper.Mapper.Map<List<V>>(GetAll<Y>(includeStrings));
        }
    }
}
