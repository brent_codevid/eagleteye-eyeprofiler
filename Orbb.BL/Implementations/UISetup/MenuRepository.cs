﻿using Orbb.BL.Interfaces.UISetup;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.UI.Base;
using Orbb.Data.ViewModels.Models.Security;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Orbb.BL.Implementations.UISetup
{
    public class MenuRepository : IMenuRepository
    {
        IDatabaseContext dbContext;
        public MenuRepository(IDatabaseContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public SaveResult AddQuickMenuItem(int id)
        {
            return new SaveResult(true, 1);
        }

        //public IList<MenuItemVM> GetMenu(int UserId, string languageCode)
        //{
        //    var result = new List<MenuItemVM>();
        //    var user = dbContext.Query<User>().Include(u => u.UserGroup)
        //                                    .Include("UserGroup.Permissions.SecuredObject").SingleOrDefault(u => u.Id == UserId);
        //    if (user == null)
        //        return result;
        //    var usergroup = user.UserGroup;
        //    var userpermissions = usergroup.Permissions.Where(p => p.SecuredObject.Type == "MENU" && p.Read).ToList();
        //    var permissions = userpermissions.Select(p => p.SecuredObject.Object).ToList();
        //    var items = dbContext.Query<MenuItem>()
        //        .Where(x => x.ParentMenuItemId == null && permissions.Contains(x.Title.DefaultValue.ToUpper()))
        //        .Include(x => x.Title)
        //        .Include(x => x.Title.Entries)
        //      //  .Include(x => x.ChildMenuItems)
        //        .OrderBy(x => x.OrderNumber)
        //        .ToList();
        //    items.ForEach(i =>
        //    {
        //        var vmitem = new MenuItemVM
        //        {
        //            Id = i.Id,
        //            Destination = i.Title.DefaultValue.Replace(" ", ""),
        //            Description = i.Title.Entries.SingleOrDefault(e => e.Language?.Code == languageCode)?.Value ?? i.Title.DefaultValue
        //        };
        //        result.Add(vmitem);
        //    });
        //    return result;
        //}

        //public IList<MenuItemVM> GetSubMenu(int userId, string languageCode)
        //{
        //    return GetSubMenuFor(userId, languageCode, "");
        //}

        public IList<MenuItemVM> GetMenu(int userId, string languageCode, string parent = null)
        {
            var result1 = new List<MenuItemVM>();
            var user = dbContext.Query<User>().Include(u => u.UserGroup)
                                            .Include("UserGroup.Permissions.SecuredObject").SingleOrDefault(u => u.Id == userId);
            if (user == null)
                return result1;
            var usergroup = user.UserGroup;
            var userpermissions = usergroup.Permissions.Where(p => p.SecuredObject.Type == "MENU" && p.Read).ToList();
            var permissions = userpermissions.Select(p => p.SecuredObject.Object).ToList();

            var items = dbContext.Query<MenuItem>()
            .Where(x => (
                (parent == null && x.ParentMenuItemId == null && permissions.Contains(x.Title.DefaultValue.ToUpper()))
                ||
                (parent != null && (x.ParentMenuItem != null) && (x.ParentMenuItem.Title.DefaultValue.ToLower() == parent) &&
                permissions.Contains(x.Title.DefaultValue.ToUpper()))
                )
                )
            .Include(x => x.Title)
            .Include(x => x.Title.Entries)
            .Include(x => x.ParentMenuItem)
            .Include(x => x.ParentMenuItem.Title)
            .OrderBy(x => x.OrderNumber)
            .ToList();

            items.ForEach(item =>
            {
                var vmitem = new MenuItemVM
                {
                    Id = item.Id,
                    Destination = GetMenuItemDestinationString(item).Replace(" ", ""), //item.Title.DefaultValue,
                    Description = item.Title.Entries.SingleOrDefault(e => e.Language?.Code == languageCode)?.Value ?? item.Title.DefaultValue
                };
                result1.Add(vmitem);
            });
            return result1;

        }

        public IList<MenuItem> GetQuickMenu(int userId, string languageCode)
        {
            return new List<MenuItem>()
            {
                new MenuItem() {
                     Title = new Translation()
                        {
                            DefaultValue = "customers",
                            Entries = new List<TranslationEntry>()
                        },
                        ParentMenuItemId = null
                    }
            };
        }

        public SaveResult RemoveQuickMenuItem(int id)
        {
            return new SaveResult(true, 1);
        }

        public SaveResult OrderQuickMenuItem(int id)
        {
            return new SaveResult(true, 1);
        }

        private string GetMenuItemDestinationString(MenuItem mnuItem)
        {
            var parent = mnuItem.ParentMenuItem;
            if (parent == null)
            {
                return mnuItem.Title.DefaultValue;
            }

            return $"{GetMenuItemDestinationString(parent)}_{mnuItem.Title.DefaultValue}";
        }
    }
}
