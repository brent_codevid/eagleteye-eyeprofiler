﻿using Orbb.BL.Interfaces.Settings;
using Orbb.Data.Models.Models.Views;
using System;
using System.Collections.Generic;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.Settings
{
    public class LensTypeRepository : ILensTypeRepository
    {
        public IList<LensTypeView> GetLensTypeOverview()
        {
            IList<LensTypeView> items = new List<LensTypeView>();
            var results = (LensTypes[])Enum.GetValues(typeof(LensTypes));
            foreach (var result in results)
            {
                items.Add(new LensTypeView((int)result, result.ToString()));
            }
            return items;
        }
    }
}