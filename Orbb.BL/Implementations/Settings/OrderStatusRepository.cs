﻿using Orbb.BL.Interfaces.Settings;
using Orbb.Data.Models.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.Settings
{
    public class OrderStatusRepository : IOrderStatusRepository
    {
        public IList<OrderStatusView> GetOrderStatusOverview()
        {
            return Enum.GetValues(typeof(OrderStatus))
                .OfType<OrderStatus>()
                .Select(result => new OrderStatusView((int)result, result.ToString()))
                .ToList();
        }
    }
}