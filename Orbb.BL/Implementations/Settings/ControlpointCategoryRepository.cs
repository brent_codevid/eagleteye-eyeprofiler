﻿using Orbb.BL.Interfaces.Settings;
using Orbb.Data.Models.Models.Views;
using System;
using System.Collections.Generic;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.Settings
{
    class ControlpointCategoryRepository : IControlpointCategoryRepository
    {
        public IList<ControlpointCategoryView> GetControlpointCategoryOverview()
        {
            IList<ControlpointCategoryView> items = new List<ControlpointCategoryView>();
            var results = (ControlPointCategories[])Enum.GetValues(typeof(ControlPointCategories));
            foreach (var result in results)
            {
                items.Add(new ControlpointCategoryView((int)result, result.ToString()));
            }
            return items;
        }
    }
}