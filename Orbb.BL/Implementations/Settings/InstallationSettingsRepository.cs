﻿using Orbb.BL.Interfaces.Settings;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.System;
using System.Linq;

namespace Orbb.BL.Implementations.Settings
{
    public class InstallationSettingsRepository : IInstallationSettingsRepository
    {
        IDatabaseContext dbContext;
        private readonly ICurrentUser currentUser;
        public InstallationSettingsRepository(IDatabaseContext dbContext, ICurrentUser currentUser)
        {
            this.dbContext = dbContext;
            this.currentUser = currentUser;
        }

        public InstallationSettings GetInstallationSettings(int divisionId)
        {
            return dbContext
                .Query<InstallationSettings>()
                .FirstOrDefault(i => i.DivisionId == divisionId);
        }

        public void SetInstallationSettings(InstallationSettings settings)
        {
            dbContext.Update(settings);
            dbContext.SaveChanges(currentUser);
        }
    }
}
