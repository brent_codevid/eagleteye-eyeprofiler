﻿using Orbb.BL.Interfaces;
using Orbb.Data.Common;
using Orbb.Data.EF;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations
{
    public class ProcedureRepository : IProcedureRepository
    {
        protected IDatabaseContext dbContext;

        public ProcedureRepository(IDatabaseContext dbContext)
        {
            this.dbContext = dbContext;
        }

        List<T> IProcedureRepository.Get<T>(string procedureName, List<Parameter> parameters)
        {
            List<SqlParameter> sqlparameters = new List<SqlParameter>();
            string parameternames = "";
            foreach (Parameter p in parameters)
            {
                if (p.Type != Parameter.Types.String && p.Value == string.Empty)
                    p.Value = DBNull.Value;

                sqlparameters.Add(new SqlParameter(p.Name, p.Value));
                parameternames += (parameternames.Length > 0 ? ", " : "") + p.Name;
            }

            var result = dbContext.QuerySql<T>(procedureName + " " + parameternames, sqlparameters.ToArray()).ToList();
            return result;
        }

        T IProcedureRepository.GetSingle<T>(string procedureName, List<Parameter> parameters)
        {
            List<SqlParameter> sqlparameters = new List<SqlParameter>();
            string parameternames = "";
            foreach (Parameter p in parameters)
            {
                if (p.Type != Parameter.Types.String && p.Value == string.Empty)
                    p.Value = DBNull.Value;

                sqlparameters.Add(new SqlParameter(p.Name, p.Value));
                parameternames += (parameternames.Length > 0 ? ", " : "") + p.Name;
            }

            var result = dbContext.QuerySql<T>(procedureName + " " + parameternames, sqlparameters.ToArray()).FirstOrDefault();
            return result;
        }

        int IProcedureRepository.GetInt(string procedureName, List<Parameter> parameters)
        {
            List<SqlParameter> sqlparameters = new List<SqlParameter>();
            string parameternames = "";
            foreach (Parameter p in parameters)
            {
                if (p.Type != Parameter.Types.String && p.Value == string.Empty)
                    p.Value = DBNull.Value;

                sqlparameters.Add(new SqlParameter(p.Name, p.Value));
                parameternames += (parameternames.Length > 0 ? ", " : "") + p.Name;
            }

            int result = dbContext.QuerySql<int>(procedureName + " " + parameternames, sqlparameters.ToArray()).First();
            return (result);
        }

        public void Execute(string procedureName, List<Parameter> parameters)
        {
            List<SqlParameter> sqlparameters = new List<SqlParameter>();
            string parameternames = "";
            foreach (Parameter p in parameters)
            {
                if (p.Type != Parameter.Types.String && p.Value == string.Empty)
                    p.Value = DBNull.Value;

                sqlparameters.Add(new SqlParameter(p.Name, p.Value));
                parameternames += (parameternames.Length > 0 ? ", " : "") + p.Name;
            }

            var result = dbContext.QuerySql<List<string>>(procedureName + " " + parameternames, sqlparameters.ToArray()).ToList();
        }
    }
}
