﻿using AutoMapper;
using Orbb.BL.Interfaces.General;
using Orbb.Common.Environment;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityDelete;
using Orbb.Data.EF.Utility;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using IHasDivision = Orbb.Data.Models.Interfaces.IHasDivision;

namespace Orbb.BL.Implementations.General
{
    public class DataService : IDataService
    {
        public IDatabaseContext DbContext
        {
            get; set;
        }

        private readonly ICurrentUser currentUser;
        private readonly IMapper mapper;
        private readonly IEntityInitializerFactory initializerFactory;
        private readonly IEntityDeleterFactory deleterFactory;

        [SetterProperty]
        public ICurrent<DivisionVM> CurrentDivision { get; set; }

        public DataService(IDatabaseContext dbcontext,
            ICurrentUser currentUser,
            IMapper mapper,
            IEntityInitializerFactory initializerFactory,
            IEntityDeleterFactory deleterFactory)
        {
            DbContext = dbcontext;
            this.currentUser = currentUser;
            this.mapper = mapper;
            this.initializerFactory = initializerFactory;
            this.deleterFactory = deleterFactory;
        }
        public SaveResult Update<T, Y>(T original, T newmodel, T currentDbVersion, string[] modelIncludes) where T : class, IViewModel where Y : class, IEntity, new()
        {
            var vms = new ViewModelSaver<T, Y>(DbContext, currentUser, CurrentDivision, initializerFactory, deleterFactory);
            return vms.Save(newmodel, original, currentDbVersion, modelIncludes);
        }

        public TViewModel Get<TEntity, TViewModel>(int id, string[] modelIncludes) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel
        {
            var q = DbContext.Query<TEntity>();
            if (modelIncludes != null)
                foreach (string include in modelIncludes.Where(i => !String.IsNullOrWhiteSpace(i)).ToList())
                    q = q.Include(include);

            var entity = q.Where(x => x.Id == id).ToList().FirstOrDefault();
            if (entity == null && id <= 0)
            {
                entity = (TEntity)Activator.CreateInstance(typeof(TEntity), null);

                var initializer = initializerFactory.GetEntityInitializer<TEntity>();
                initializer?.InitializeForNew(entity, DbContext);
            }
            var vm = mapper.Map<TViewModel>(entity);
            return vm;
        }

        public List<TViewModel> GetAll<TEntity, TViewModel>(string[] modelIncludes) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel
        {
            return GetAll<TEntity, TViewModel>(null, modelIncludes);
        }

        public List<TViewModel> GetAll<TEntity, TViewModel>(Expression<Func<TEntity, bool>> predicate, string[] modelIncludes) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel
        {
            List<TViewModel> vm = new List<TViewModel>();

            var q = DbContext.Query<TEntity>();
            if (predicate != null)
                q = q.Where(predicate);

            if (modelIncludes != null)
                foreach (string include in modelIncludes)
                    q = q.Include(include);

            if (typeof(IHasDivisions).IsAssignableFrom(typeof(TEntity)) && Config.UseDivisions && CurrentDivision?.GetCurrent() != null)
            {
                int divisionId = CurrentDivision.GetCurrent().Id;

                q = ((IQueryable<IHasDivisions>)q).Where(x => x.Divisions.Any(d => d.Id == divisionId)).Cast<TEntity>();
            }
            else if (typeof(IHasDivision).IsAssignableFrom(typeof(TEntity)) && Config.UseDivisions && CurrentDivision?.GetCurrent() != null)
            {
                int divisionId = CurrentDivision.GetCurrent().Id;
                q = ((IQueryable<IHasDivision>)q).Where(x => x.DivisionId == divisionId).Cast<TEntity>();
            }

            var entities = q.ToList();
            foreach (var item in entities)
            {
                vm.Add(mapper.Map<TViewModel>(item));
            }

            return vm;
        }

        public TViewModel GetNew<TEntity, TViewModel>() where TEntity : class, IEntity, new() where TViewModel : class, IViewModel
        {
            var vm = Get<TEntity, TViewModel>(0, new string[0]);

            return vm;
        }

        public bool CanDelete<TEntity>(int id, out string[] reasons) where TEntity : class, IEntity, new()
        {
            reasons = new string[] { };
            if (id <= 0)
            {
                reasons = new[] { "Not saved yet." };
                return false;
            }

            var deleter = deleterFactory.GetEntityDeleter<TEntity>();
            if (deleter == null)
                return true; // changed default to true. If no deleter to validate deletion, the object can be deleted.

            return deleter.CanDelete(DbContext, id, deleterFactory, out reasons);
        }

        bool IDataService.Delete<TEntity>(int id, out string[] reasons)
        {
            reasons = new string[] { };
            if (id <= 0)
            {
                reasons = new[] { "G_D_NOT_SAVED" };
                return false;
            }

            var deleter = deleterFactory.GetEntityDeleter<TEntity>();
            if (deleter == null)
            {
                reasons = new[] { "G_D_CONFLICT" };
                return false;
            }

            if (!deleter.CanDelete(DbContext, id, deleterFactory, out reasons))
                return false;

            return deleter.Delete(DbContext, id, currentUser, deleterFactory);
        }

        public void Clear()
        {
            DbContext.Clear();
        }

        TEntity IDataService.GetNew<TEntity>()
        {
            TEntity entity = (TEntity)Activator.CreateInstance(typeof(TEntity), null);

            var initializer = initializerFactory.GetEntityInitializer<TEntity>();
            initializer?.InitializeForNew(entity, DbContext);

            return entity;
        }

        TEntity IDataService.GetNew<TEntity>(IDatabaseContext context)
        {
            {
                TEntity entity = (TEntity)Activator.CreateInstance(typeof(TEntity), null);

                var initializer = initializerFactory.GetEntityInitializer<TEntity>();
                initializer?.InitializeForNew(entity, context);

                return entity;
            }
        }
    }
}
