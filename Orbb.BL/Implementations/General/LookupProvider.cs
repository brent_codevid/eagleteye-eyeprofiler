﻿using AutoMapper;
using Microsoft.Extensions.Caching.Distributed;
using Orbb.BL.Interfaces.General;
using Orbb.Common.Extensions;
using Orbb.Data.Common;
using Orbb.Data.Common.General;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.Security.Types;
using Orbb.Data.ViewModels.Models.Base;
using Orbb.Data.ViewModels.Models.Globalization;
using Orbb.Data.ViewModels.Models.Other;
using Orbb.Data.ViewModels.Models.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.General
{
    public class LookupProvider : ILookupProvider
    {
        IDatabaseContext dbContext;
        protected int? languageId;
        readonly ICurrent<DivisionVM> currentDivision;
        private readonly ICurrentUser currentUser;
        private readonly IDistributedCache cache;
        private readonly IMapper mapper;

        public LookupProvider(IDatabaseContext dbContext,
            ICurrentUser currentUser,
            ICurrent<DivisionVM> currentDivision,
            IMapper mapper,
            IDistributedCache cache)
        {
            this.dbContext = dbContext;
            languageId = currentUser.GetCurrentUser()?.LanguageId;
            this.currentDivision = currentDivision;
            this.currentUser = currentUser;
            this.cache = cache;
            this.mapper = mapper;
        }

        public IList<Language> GetArticleLanguagesList()
        {
            var list = dbContext.Query<Language>()
                .ToList();
            return list;
        }

        public IList<UserGroup> GetUserGroupsList()
        {
            var list = dbContext.Query<UserGroup>()
                .Include(x => x.Permissions)
                .ToList();
            return list;
        }

        public IList<LanguageTranslation> GetLanguageTranslationsList()
        {
            var list = dbContext.Query<LanguageTranslation>()
                .Include(x => x.Language)
                .ToList();
            return list;
        }

        public IList<LanguageTranslation> GetLanguageTranslationsMenuList()
        {
            var list = cache.Get<IList<LanguageTranslation>>(CacheKeys.LanguageTranslationsMenuList);

            if (list == null)
            {
                // Key not in cache
                var translations = GetLanguageTranslationsList();
                var fixedLanguagesCodesList = Enum.GetValues(typeof(Enumerations.FixedLanguages)).Cast<Enumerations.FixedLanguages>().Select(a => a.ToString());
                list = translations.Where(lang => fixedLanguagesCodesList.Any(l => l == lang.Language.ShortCode)).ToList();

                // Save data in cache.
                cache.Set(CacheKeys.LanguageTranslationsMenuList, list);
            }

            return list;
        }

        public IList<Language> GetLanguagesList()
        {
            var list = dbContext.Query<Language>()
                .ToList();
            return list;
        }

        public IList<LanguageVM> GetLanguages()
        {
            var query = dbContext.Query<Language>();
            var list = mapper.Map<List<LanguageVM>>(query);

            return list;
        }

        public IList<Orbb.Data.Models.Models.Base.Country> GetCountriesList()
        {
            var list = dbContext.Query<Orbb.Data.Models.Models.Base.Country>()
                .Include(x => x.Description)
                .ToList();
            return list;
        }
        public IList<CountryVM> GetCountriesVmList(int languageId)
        {
            var languageIdParameter = new SqlParameter("@LanguageId", languageId);
            var result = dbContext.QuerySql<CountryVM>("spWebListCountries @LanguageId", languageIdParameter).ToList();
            return result;
        }

        public IList<CountryListItem> GetCountries(int language)
        {
            var languageIdParameter = new SqlParameter("@LanguageId", languageId);
            var result = dbContext.QuerySql<CountryListItem>("spWebListCountries @LanguageId", languageIdParameter).ToList();
            return result;
        }

        public Language GetLanguage(string code)
        {
            return dbContext
                .Query<Language>()
                .FirstOrDefault(x => x.Code == code);
        }


        public IList<DivisionVM> GetDivisionByCategory(int entityId, int categoryId)
        {
            var entityIddParameter = new SqlParameter("@CategoryId", categoryId);
            var categoryIdParameter = new SqlParameter("@EntityId", entityId);
            var result = dbContext.QuerySql<DivisionVM>("spWebGetDivisionForCategory @CategoryId, @EntityId", categoryIdParameter, entityIddParameter).ToList();
            return result;
        }

        public IList<ComboBoxItemVm> GetUserTypesForCombobox()
        {
            var usertypes = dbContext.Query<UserType>().Select(t => new ComboBoxItemVm() { Id = t.Id, Text = t.Description.DefaultValue });
            return usertypes.ToList();
        }
    }
}
