﻿using Orbb.BL.Interfaces.General;
using System.Collections.Generic;

namespace Orbb.BL.Implementations.General
{
    public class ReverseMapperFactory : IReverseMapperFactory
    {
        readonly Dictionary<string, object> mappers = new Dictionary<string, object>();
        void IReverseMapperFactory.DefineReverseMapper<From, To>(IReverseMapper<From, To> mapper)
        {
            string key = typeof(From) + "-" + typeof(To);
            mappers.Add(key, mapper);
        }

        IReverseMapper<From, To> IReverseMapperFactory.GetReverseMapper<From, To>()
        {
            string key = typeof(From) + "-" + typeof(To);
            var mapper = mappers[key];
            return mapper as IReverseMapper<From, To>;
        }
    }
}
