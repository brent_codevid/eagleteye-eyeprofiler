﻿using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Lenssets
{
    public class LensRepository : Repository<LensDetail>, ILensRepository
    {
        private readonly IDataService dataService;

        public LensRepository(IDatabaseContext dbContext, IDataService dataService) : base(dbContext)
        {
            this.dataService = dataService;
        }

        public LensDetail GetLenssetById(int id)
        {
            return GetSingle(l => l.Id == id, new[] { "Supplier", "CP1", "CP2", "CP3", "CP4", "CP5", "CP2ToCP1List", 
                "CP1.CpList", "CP2.CpList", "CP3.CpList", "CP4.CpList", "CP5.CpList", 
                "CP1.CpTangentList", "CP2.CpTangentList", "CP3.CpTangentList", "CP4.CpTangentList", "CP5.CpTangentList", 
                "CP1.CpCurvatureList", "CP2.CpCurvatureList", "CP3.CpCurvatureList", "CP4.CpCurvatureList", "CP5.CpCurvatureList" });
        }

        public List<CP2ToCP1Item> GetCP2ToCP1ItemsByLensId(int id)
        {
            return GetSingle(l => l.Id == id, new[] { "CP2ToCP1List" }).CP2ToCP1List;
        }

        public int CountLenssetWithSupplier(int supplierId)
        {
            return GetAll(l => l.SupplierId == supplierId, null).Count;
        }

        public int CountLenssetsWithCategory(int category)
        {
            return GetAll(l => l.CP1.Category == category || l.CP2.Category == category || l.CP4.Category == category, null).Count;
        }

        public int CountLenssetWithType(int type)
        {
            return GetAll(l => l.Type == type, null).Count;
        }

        public IList<LensView> GetOverview()
        {
            SqlParameter languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<LensView>("spWebViewLenses @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }

        public IList<LensInfo> GetInfo()
        {
            var result = new List<LensInfo>();
            var lensDetails = GetAll(l => l.IsDraft == false, new[] { "Supplier" });
            foreach (LensDetail ld in lensDetails)
            {
                try
                {
                    result.Add(new LensInfo(ld));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            return result;
        }

        public IList<int> GetValidIds()
        {
            var result = new List<int>();
            var lensDetails = GetAll(l => true, null);
            foreach (LensDetail ld in lensDetails)
            {
                result.Add(ld.Id);
            }
            return result;
        }

        public IList<LensDetail> GetAllAvailableInCountry(List<int> lensIds, string[] includeString)
        {
            var outputLensList = GetAll<LensDetail>(includeString);
            return outputLensList.Where(l => lensIds.Any(i => i == l.Id)).ToList();
        }
    }
}