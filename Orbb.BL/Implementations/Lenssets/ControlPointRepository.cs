﻿using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Views;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Lenssets
{
    public class ControlPointRepository : Repository<ControlPoint>, IControlPointRepository
    {
        private readonly IDataService dataService;

        public ControlPointRepository(IDatabaseContext dbContext, IDataService dataService) : base(dbContext)
        {
            this.dataService = dataService;
        }


        public string GetChildClassName(int id)
        {
            return DbContext.QuerySql<string>($"select Discriminator from Lenses.ControlPoints where Id = {id}").FirstOrDefault();
        }
       
    }
}