﻿using Orbb.BL.Interfaces.Media;
using Orbb.Common.Environment;
using Orbb.Common.Utility;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;

namespace Orbb.BL.Implementations.Media
{
    public class MediaService : IMediaService
    {
        private const string DEFAULT_IMAGE_SUFFIX = "default.jpg";
        private const int IMAGE_RESIZE_WIDTH = 1024;
        private const int IMAGE_RESIZE_HEIGHT = 1024;
        public void SaveVideo(string relativefilePath, byte[] bytes)
        {
            var fullPath = GetMediaLocalPath(relativefilePath);
            var directory = Path.GetDirectoryName(fullPath);

            Directory.CreateDirectory(directory);

            File.WriteAllBytes(fullPath, bytes);
        }

        public void SaveImage(string relativefilePath, byte[] bytes, ImageResolution minimumResolution, bool resize = true)
        {
            Image image;

            using (var ms = new MemoryStream(bytes))
            {
                image = new Bitmap(Image.FromStream(ms));
            }

            ValidateImage(minimumResolution, image);

            if (resize)
                image = ResizeImage(image, new Size(IMAGE_RESIZE_WIDTH, IMAGE_RESIZE_HEIGHT), true);

            var fullPath = GetMediaLocalPath(relativefilePath);
            var directory = Path.GetDirectoryName(fullPath);

            Directory.CreateDirectory(directory);

            if (Path.GetExtension(fullPath).ToLower() == ".jpg" || Path.GetExtension(fullPath).ToLower() == ".jpeg")
                SaveJpeg(fullPath, image, 100);
            else
                image.Save(fullPath);
        }

        public void SaveImage(string relativefilePath, byte[] bytes)
        {
            SaveImage(relativefilePath, bytes, new ImageResolution { Width = 1024 });
        }

        public void CreateDefaultImage(string originalRelativefilePath, string prefix)
        {
            var fullPath = GetMediaLocalPath(originalRelativefilePath);
            var directory = Path.GetDirectoryName(fullPath);
            string defaultImagePath;

            defaultImagePath = Path.Combine(directory, !string.IsNullOrEmpty(prefix) ? $"{prefix}-{DEFAULT_IMAGE_SUFFIX}" : DEFAULT_IMAGE_SUFFIX);

            try
            {
                File.Copy(fullPath, defaultImagePath, true);
            }
            catch (Exception ex)
            {
                throw new IOException($"Error creating default image for {fullPath}. {ex.Message}", ex);
            }
        }

        public void Delete(string relativefilePath)
        {
            var localPath = GetMediaLocalPath(relativefilePath);
            File.Delete(localPath);
        }

        private static void ValidateImage(ImageResolution minimumResolution, Image image)
        {
            if (image == null)
                throw new ArgumentNullException("image", "Image cannot be empty");

            if (minimumResolution != null)
            {
                var validationMessages = new List<string>();

                if (minimumResolution.Width > 0 && image.Width < minimumResolution.Width)
                    validationMessages.Add($"Minimal image width is {minimumResolution.Width}px");

                if (minimumResolution.Height > 0 && image.Height < minimumResolution.Height)
                    validationMessages.Add($"Minimal image height is {minimumResolution.Height}px");

                if (validationMessages.Count > 0)
                    throw new Exception(string.Join(",", validationMessages.ToArray()));
            }
        }

        private string GetMediaLocalPath(string filePath)
        {
            var mediaPath = Config.MediaPath;
            return Path.Combine(mediaPath, filePath);
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private void SaveJpeg(string path, Image img, int quality)
        {
            EncoderParameter qualityParam = new EncoderParameter(Encoder.Quality, quality);
            ImageCodecInfo jpegCodec = GetEncoder(ImageFormat.Jpeg);
            EncoderParameters encoderParams = new EncoderParameters(1) { Param = { [0] = qualityParam } };


            using (MemoryStream ms = new MemoryStream())
            using (FileStream fs = new FileStream(path, FileMode.Create, FileAccess.ReadWrite))
            {
                img.Save(ms, jpegCodec, encoderParams);
                byte[] bytes = ms.ToArray();
                fs.Write(bytes, 0, bytes.Length);
            }
        }

        private Image ResizeImage(Image imgToResize, Size size, bool bestQuality)
        {
            int sourceWidth = imgToResize.Width;
            int sourceHeight = imgToResize.Height;

            float nPercent = 0;
            float nPercentW = 0;
            float nPercentH = 0;

            nPercentW = ((float)size.Width / (float)sourceWidth);
            nPercentH = ((float)size.Height / (float)sourceHeight);

            nPercent = nPercentH < nPercentW ? nPercentH : nPercentW;

            int destWidth = (int)(sourceWidth * nPercent);
            int destHeight = (int)(sourceHeight * nPercent);

            Bitmap newImage = new Bitmap(destWidth, destHeight);

            using (Graphics graphics = Graphics.FromImage(newImage))
            {
                if (bestQuality)
                {
                    graphics.InterpolationMode = InterpolationMode.High;
                    graphics.SmoothingMode = SmoothingMode.HighSpeed;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                }
                else
                {
                    graphics.InterpolationMode = InterpolationMode.Low;
                    graphics.SmoothingMode = SmoothingMode.HighSpeed;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighSpeed;
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                }

                graphics.DrawImage(imgToResize, 0, 0, destWidth, destHeight);
            }

            return newImage;
        }
    }
}
