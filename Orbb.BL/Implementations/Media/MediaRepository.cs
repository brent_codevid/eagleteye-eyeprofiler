﻿using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Media;
using Orbb.Common.Utility;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.ViewModels.Models.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using static Orbb.Data.Common.Enumerations;
using modelLocation = Orbb.Data.Models.Models.Base;


namespace Orbb.BL.Implementations.Media
{
    class MediaRepository : Repository<modelLocation.Media>, IMediaRepository
    {
        private const string DELETETYPE = "DELETE";
        //private const string IMAGESUBPATH = "rates";

        private readonly IDataService dataService;
        private readonly IMediaService mediaService;

        public MediaRepository(IDatabaseContext dbContext,
                    IDataService dataService,
                    IMediaService mediaService) : base(dbContext)
        {
            this.dataService = dataService;
            this.mediaService = mediaService;
        }

        public IList<MediaFileResult> GetFiles(int id, string deleteUrl, LinkedType type)
        {
            var fileResults = new List<MediaFileResult>();
            var baseDeleteUrl = deleteUrl.EndsWith("/") ? deleteUrl : deleteUrl + "/";
            var mediaFiles = DbContext.Query<modelLocation.Media>().Where(m => m.Id == id).OrderBy(m => m.DisplayOrder);
            switch (type)
            {
                /*
                case LinkedType.Event:
                    mediaFiles = dbContext.Query<modelLocation.Media>().Where(m => m.EventDetail.Id == id).OrderBy(m => m.DisplayOrder);
                    break;
                case LinkedType.News:
                    mediaFiles = dbContext.Query<modelLocation.Media>().Where(m => m.NewsDetail.Id == id).OrderBy(m => m.DisplayOrder);
                    break;
                    */
                default:
                    throw new NotImplementedException();
            }

            foreach (var file in mediaFiles)
            {
                var url = MediaUtility.ConvertToUrl(file.Filename);

                fileResults.Add(new MediaFileResult
                {
                    Id = file.Id,
                    Type = file.MimeType,
                    Video = file.MimeType.Contains("video") ? true : false,
                    Name = Path.GetFileName(file.Filename),
                    Url = url,
                    ThumbnailUrl = url,
                    DisplayOrder = file.DisplayOrder,
                    DeleteUrl = baseDeleteUrl + file.Id,
                    DeleteType = DELETETYPE
                });
            }

            return fileResults;
        }

        public MediaFileResult AddFile(byte[] binary, string prefix, string fileExtension, int externalId, string mimeType, string deleteUrl, LinkedType type)
        {
            var baseDeleteUrl = deleteUrl.EndsWith("/") ? deleteUrl : deleteUrl + "/";
            var mediaFile = dataService.GetNew<modelLocation.Media>();
            var divsionDescription = CurrentDivision.GetCurrent().Description;
            IQueryable<modelLocation.Media> mediaFiles = null;

            switch (type)
            {
                /*
                case LinkedType.Event:
                    mediaFile.EventDetail = dbContext.Query<EventDetail>().Where(e=>e.Id==externalId).FirstOrDefault();
                    mediaFile.Filename = Path.Combine("event", divsionDescription, $"{prefix}-{Guid.NewGuid()}{fileExtension}");
                    mediaFiles = dbContext.Query<modelLocation.Media>().Where(m => m.EventDetail.Id == externalId);

                    break;
                case LinkedType.News:
                    mediaFile.NewsDetail= dbContext.Query<NewsDetail>().Where(e => e.Id == externalId).FirstOrDefault();
                    mediaFile.Filename = Path.Combine( "news", divsionDescription, $"{prefix}-{Guid.NewGuid()}{fileExtension}");
                    mediaFiles = dbContext.Query<modelLocation.Media>().Where(m => m.NewsDetail.Id == externalId);
              
                    break;
                    */
                default:
                    throw new NotImplementedException();
            }


            var mediaType = mimeType.Contains("video") ? MediaTypes.Video : MediaTypes.Image;
            mediaFile.Type = (int)mediaType;
            mediaFile.MimeType = mimeType;


            if (mediaFiles.Any())
                mediaFile.DisplayOrder = mediaFiles.Max(m => m.DisplayOrder) + 1;

            var url = MediaUtility.ConvertToUrl(mediaFile.Filename);
            var fileResult = new MediaFileResult
            {
                Type = mediaFile.MimeType,
                Video = mediaType == MediaTypes.Video ? true : false,
                Name = Path.GetFileName(mediaFile.Filename),
                Url = url,
                ThumbnailUrl = url,
                DisplayOrder = mediaFile.DisplayOrder,
                DeleteType = DELETETYPE
            };

            try
            {
                // save file to filesystem
                SaveToFile(binary, mediaFile.Filename, mediaType);
            }
            catch (Exception ex)
            {
                fileResult.Error = ex.Message;
                return fileResult;
            }

            DbContext.Add(mediaFile);

            var result = DbContext.SaveChanges(CurrentUser, CurrentDivision);

            if (result.Success)
            {
                fileResult.Id = mediaFile.Id;
                fileResult.DeleteUrl = baseDeleteUrl + mediaFile.Id;
            }
            else
            {
                fileResult.Error = result.SaveException?.Message;
            }

            return fileResult;
        }

        public SaveResult SetDisplayOrder(int id, int[] ids)
        {
            var mediaFiles = DbContext.Query<modelLocation.Media>().Where(m => m.Id == id);

            foreach (var file in mediaFiles)
            {
                file.DisplayOrder = Array.IndexOf(ids, file.Id);
                DbContext.Update(file);
            }

            var result = DbContext.SaveChanges(CurrentUser, CurrentDivision);

            return result;
        }

        public MediaFileResult DeleteFile(int id)
        {
            var mediaFile = DbContext.Query<modelLocation.Media>().FirstOrDefault(m => m.Id == id);
            var result = new MediaFileResult { Id = id };

            if (mediaFile == null)
                throw new Exception("File not found");

            result.Name = mediaFile.Filename;

            if (!dataService.Delete<modelLocation.Media>(id, out string[] reasons))
            {
                var error = string.Join(",", reasons);
                throw new Exception(error);
            }

            try
            {
                mediaService.Delete(mediaFile.Filename);
            }
            catch (Exception ex)
            {
                result.Error = ex.Message;
            }

            return result;
        }

        private void SaveToFile(byte[] binary, string fileName, MediaTypes mediaType)
        {
            if (mediaType == MediaTypes.Image)
                mediaService.SaveImage(fileName, binary);
            else
                mediaService.SaveVideo(fileName, binary);
        }
    }
}
