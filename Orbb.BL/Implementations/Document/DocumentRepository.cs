﻿using Microsoft.AspNetCore.Hosting;
using Orbb.BL.Interfaces.Document;
using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.Document;
using System;
using System.IO;
using System.Linq;
using Model = Orbb.Data.Models.Models.System;

namespace Orbb.BL.Implementations.Document
{
    /// <summary>
    /// Manage Document storage.
    /// </summary>
    public class DocumentRepository : IDocumentRepository
    {
        IDatabaseContext dbContext;
        private readonly ICurrentUser currentUser;
        private readonly IHostingEnvironment hostingEnvironment;
        public DocumentRepository(IDatabaseContext dbContext, ICurrentUser currentUser, IHostingEnvironment hostingEnvironment)
        {
            this.dbContext = dbContext;
            this.currentUser = currentUser;
            this.hostingEnvironment = hostingEnvironment;
        }
        /// <summary>
        /// Storing documents on the file system.
        /// </summary>
        /// <param name="storageModel"></param>
        /// <returns></returns>
        public SaveResult Storedocument(DocumentStorageVM storageModel)
        {
            SaveResult result;
            var filePath = Path.Combine(hostingEnvironment.WebRootPath, "Documents");
            try
            {
                using (var fs = new FileStream(filePath + storageModel.FileName, FileMode.Create, FileAccess.Write))
                {
                    storageModel.FileStream.WriteTo(fs);
                    fs.Close();
                }
                result = Logdocument(storageModel, filePath);
            }
            catch (Exception ex)
            {
                result = new SaveResult(false, 0, ex, "No Storage");
            }
            return result;
        }
        /// <summary>
        /// Store and update document details on the database .
        /// </summary>
        /// <param name="storageModel"></param>
        /// <param name="filePath"></param>
        /// <returns></returns>
        private SaveResult Logdocument(DocumentStorageVM storageModel, string filePath)
        {
            SaveResult result;
            //HARD CODED 1 --> Implement storages in customer configuration
            DocumentStore storage = dbContext.Query<DocumentStore>().FirstOrDefault(so => so.TypeId == 1); // Goal is to have a number of storages that are configured when setting up the customer. For now you can get the storage with id 1.
            if (storage != null)
            {
                Model.Document documents;
                DocumentTypeRepository documentType = new DocumentTypeRepository(storageModel.TypeId);
                documents = documentType.LogDocumentType(dbContext, storageModel);
                storage.Documents.Add(documents);
                dbContext.Add(documents);
                dbContext.Update(storage);
                result = dbContext.SaveChanges(currentUser);
            }
            else
            {
                result = new SaveResult(false, 0, null, "No Storage");
            }
            return result;
        }
        //private Vayamundo.Data.Models.Models.System.Document LogPickingNote(DocumentStorageVM storageModel)
        //{
        //    Vayamundo.Data.Models.Models.System.Document Documents = new Vayamundo.Data.Models.Models.System.Document
        //    {
        //        Description = "Picking Note",
        //        FileName = storageModel.fileName,
        //        Extension = storageModel.fileExtension,
        //        TypeId = storageModel.typeId,
        //   };
        //    var pickingOrder = dbContext.Query<Picking>().Where(so => so.Id == storageModel.pickingId).FirstOrDefault();
        //    if (pickingOrder != null)
        //    {
        //        pickingOrder.PickingNoteId = Documents.Id;
        //        dbContext.Update(pickingOrder);
        //    }
        //    return Documents;
        //}
    }
}
