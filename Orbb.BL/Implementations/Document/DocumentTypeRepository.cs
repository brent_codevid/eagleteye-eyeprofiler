﻿using Orbb.BL.Interfaces.Document;
using Orbb.Data.EF;
using Orbb.Data.ViewModels.Models.Document;
using Model = Orbb.Data.Models.Models.System;

namespace Orbb.BL.Implementations.Document
{
    /// <summary>
    /// Repository to manage document types.
    /// </summary>
    public class DocumentTypeRepository
    {
        IDocumentTypeRepository documentType;
        public DocumentTypeRepository(int type)
        {
            //switch (type)
            //{
            //    case (int)Enumerations.DocumentTypes.StockMove:
            //        this.documentType = new StockMoveDocumentType();
            //        break;
            //}
        }
        /// <summary>
        /// Invoke the implementation based on the type.
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="storageModel"></param>
        /// <returns></returns>
        public Model.Document LogDocumentType(IDatabaseContext dbContext, DocumentStorageVM storageModel)
        {
            return documentType.LogDocumentType(dbContext, storageModel);
        }
    }

}
