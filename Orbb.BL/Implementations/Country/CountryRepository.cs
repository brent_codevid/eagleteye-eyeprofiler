﻿using Orbb.BL.Interfaces.Country;
using Orbb.BL.Interfaces.General;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Country
{
    public class CountryRepository : Repository<Orbb.Data.Models.Models.Base.Country>, ICountryRepository
    {
        private readonly IDataService dataService;

        public CountryRepository(IDatabaseContext dbContext, IDataService dataService) : base(dbContext)
        {
            this.dataService = dataService;
        }

        public IList<CountryView> GetOverview()
        {
            SqlParameter languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<CountryView>("spWebViewCountries @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }
    }
}