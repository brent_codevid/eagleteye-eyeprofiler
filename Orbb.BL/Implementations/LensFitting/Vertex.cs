﻿using MIConvexHull;
using Orbb.BL.Implementations.Base;

namespace Orbb.BL.Implementations.LensFitting
{
    public class Vertex : IVertex
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Vertex"/> class.
        /// </summary>
        /// <param name="x">The x position.</param>
        /// <param name="y">The y position.</param>
        /// <param name="z">The z value.</param>
        public Vertex(double x, double y, double z)
        {
            Position = new[] { x, y };
            Z = z;
            coordinates = new CarthesianCoordinates
            {
                XCoord = x,
                YCoord = y
            };
        }

        /// <summary>
        /// Gets or sets the Z.
        /// </summary>
        /// <value>The Z position.</value>
        public double Z { get; set; }

        /// <summary>
        /// Gets or sets the coordinates.
        /// </summary>
        /// <value>The coordinates.</value>
        public double[] Position { get; set; }

        /// <summary>
        /// Returns the coordinates.
        /// </summary>
        /// <value>The coordinates.</value>
        private readonly CarthesianCoordinates coordinates;
        public CarthesianCoordinates GetCoordinates()
        {
            return coordinates;
        }
    }
}