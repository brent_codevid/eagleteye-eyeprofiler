﻿using MathNet.Numerics.LinearAlgebra;
using MathNet.Spatial.Euclidean;
using MIConvexHull;
using Orbb.BL.Implementations.Base;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace Orbb.BL.Implementations.LensFitting
{
    public class SemiMeridians : List<SemiMeridian>
    {
        private const double DistanceFromLensEdge = 0.5;

        public SemiMeridians(int count) : base(count) { }

        public SemiMeridians() { }

        public void RotateData(double lensdiameter)
        {
            RotateData(CalculatePlaneVector(lensdiameter / 2 - DistanceFromLensEdge), lensdiameter);
        }

        public void RotateData(Vector3D planeVector, double lensdiameter)
        {
            Matrix<double> transformationMatrix = Matrix3D.RotationTo(planeVector, new Vector3D(0, 0, 1));

            List<Vertex> vertices = new List<Vertex>();
            Vertex origin = new Vertex(0, 0, 0);
            vertices.Add(origin);
            foreach (SemiMeridian semiMeridian in this)
            {
                foreach (var (rho, zValue) in semiMeridian.DataPoints)
                {
                    CarthesianCoordinates coords = MathHelpers.PolToCartWithDegrees(semiMeridian.Angle, rho);
                    if (Math.Abs(coords.XCoord) > 0.00001 || Math.Abs(coords.YCoord) > 0.00001)
                    {
                        Point3D rotatedValue = new Point3D(coords.XCoord, coords.YCoord, zValue).TransformBy(transformationMatrix);
                        vertices.Add(new Vertex(rotatedValue.X, rotatedValue.Y, rotatedValue.Z));
                    }
                }
            }

            /* var model1 = new PlotModel { Title = "ScatterSeries" };
             var scatterSeries1 = new ScatterSeries { MarkerType = MarkerType.Circle };
             foreach (SemiMeridian semiMeridian in this)
             {
                 foreach (var (rho, zValue) in semiMeridian.DataPoints)
                 {
                     CarthesianCoordinates coords = MathHelpers.PolToCartWithDegrees(semiMeridian.Angle, rho);
                     var x = coords.XCoord;
                     var y = coords.YCoord;
                     var size = 1;
                     var colorValue = zValue;
                     scatterSeries1.Points.Add(new ScatterPoint(x, y, size, colorValue));
                 }
             }
             model1.Series.Add(scatterSeries1);
             model1.Axes.Add(new LinearColorAxis { Position = AxisPosition.Right, Palette = OxyPalettes.Jet(200) });

             using (var stream = File.Create("plotbefore.pdf"))
             {
                 var pdfExporter = new PdfExporter { Width = 600, Height = 600 };
                 pdfExporter.Export(model1, stream);
             }





             var model = new PlotModel { Title = "ScatterSeries" };
             var scatterSeries = new ScatterSeries { MarkerType = MarkerType.Circle };
             foreach (var vert in vertices)
             {
                 var x = vert.GetCoordinates().XCoord;
                 var y = vert.GetCoordinates().YCoord;
                 var size = 1;
                 var colorValue = vert.Z;
                 scatterSeries.Points.Add(new ScatterPoint(x, y, size, colorValue));
             }
             model.Series.Add(scatterSeries);
             model.Axes.Add(new LinearColorAxis { Position = AxisPosition.Right, Palette = OxyPalettes.Jet(200) });

             using (var stream2 = File.Create("plot.pdf"))
             {
                 var pdfExporter = new PdfExporter { Width = 600, Height = 600 };
                 pdfExporter.Export(model, stream2);
             }*/

            DelaunayRepopulation(vertices, origin, lensdiameter);
        }

        private Vector3D CalculatePlaneVector(double correctedLensRadius)
        {
            //find rotationplane
            List<Point3D> pointsOnPlane = new List<Point3D>();

            List<SemiMeridian> rotationList = FindPointToLevel();
            rotationList.Add(this[0]);
            int startAngle = this[0].Angle;

            foreach (SemiMeridian semiMer in rotationList)
            {
                int distanceCount = (int)Math.Round((correctedLensRadius) / semiMer.RhoDistance);
                if (distanceCount > semiMer.DataPoints.Count)
                {
                    distanceCount = semiMer.DataPoints.Count - 1;
                }
                var (rho, zValues) = semiMer.DataPoints[distanceCount];

                CarthesianCoordinates coords = MathHelpers.PolToCartWithDegrees(semiMer.Angle, rho);
                pointsOnPlane.Add(new Point3D(coords.XCoord, coords.YCoord, zValues));
            }

            Matrix<double> matrixA = Matrix<double>.Build.Dense(3, 3);
            Matrix<double> matrixB = Matrix<double>.Build.Dense(1, 3);

            //sum_i x[i]*x[i],    sum_i x[i]*y[i],    sum_i x[i]
            //sum_i x[i]*y[i],    sum_i y[i]*y[i],    sum_i y[i]
            //sum_i x[i],         sum_i y[i],         n

            //sum_i x[i]*z[i],   sum_i y[i]*z[i],    sum_i z[i]
            foreach (Point3D point in pointsOnPlane)
            {
                matrixA[0, 0] += point.X * point.X;
                matrixA[1, 0] += point.X * point.Y;
                matrixA[2, 0] += point.X;

                matrixA[0, 1] += point.X * point.Y;
                matrixA[1, 1] += point.Y * point.Y;
                matrixA[2, 1] += point.Y;

                matrixA[0, 2] += point.X;
                matrixA[1, 2] += point.Y;
                matrixA[2, 2] += 1;

                matrixB[0, 0] += point.X * point.Z;
                matrixB[0, 1] += point.Y * point.Z;
                matrixB[0, 2] += point.Z;

            }
            var matrixInverseA = matrixA.Inverse();
            var planeParameters = matrixB.Multiply(matrixInverseA);
            // plane parameters Ax+By+C=z => for normal z-Ax-By
            if (double.IsNaN(planeParameters[0, 0]) || double.IsNaN(planeParameters[0, 1]))
            {
                throw new ArgumentException("This configuration of SemiMeridians resulted in an invalid Normal vector");
            }
            return new Vector3D(-planeParameters[0, 0], -planeParameters[0, 1], 1);
        }

        private List<SemiMeridian> FindPointToLevel()
        {
            int mercount = 8;
            List<SemiMeridian> output = new List<SemiMeridian>(8);
            try
            {
                for (int i = 0; i < mercount; i++)
                {
                    int angle = i * (360 / mercount);
                    SemiMeridian closest = this[0];
                    foreach (var semiMeri in this)
                    {
                        if (Math.Abs(closest.Angle - angle) > Math.Abs(semiMeri.Angle - angle))
                        {
                            closest = semiMeri;
                        }
                    }
                    output.Add(closest);
                }
            }
            catch (Exception ex)
            {
                throw new ArgumentException("This configuration of SemiMeridians resulted in an invalid Normal vector");
            }
            return output;
        }

        private void DelaunayRepopulation(IList<Vertex> vertices, Vertex origin, double lensdiameter)
        {
            var voronoiMesh = VoronoiMesh.Create<Vertex, Cell>(vertices);

            Cell originTriangle = voronoiMesh.Vertices.FirstOrDefault(c => c.Vertices.Contains(origin));
            foreach (SemiMeridian sm in this)
            {
                RecreateSemiMeridian(originTriangle, sm, lensdiameter);
            }
        }

        private const int DoNotRecreateFirstXValues = 10;
        private const int CreateXNewValues = 50;

        private static void RecreateSemiMeridian(Cell start, SemiMeridian sm, double lensdiameter)
        {
            //start at start, find triangle containing wanted point through randomized edge hopping, and use one of the vertexes of the triangle as starting point for the next round
            double endX = lensdiameter / 2;
            double startX = sm.DataPoints[DoNotRecreateFirstXValues].Item1;
            double step = (endX - startX) / CreateXNewValues;
            sm.DataPoints.RemoveRange(DoNotRecreateFirstXValues, sm.DataPoints.Count - DoNotRecreateFirstXValues);
            double rho = startX - step;
            for (int i = 0; i < CreateXNewValues; i++)
            {
                bool found = false;
                rho += step;
                CarthesianCoordinates coord = MathHelpers.PolToCartWithDegrees(sm.Angle, rho);

                while (!found)
                {
                    found = true;
                    for (int j = 0; j < 3; j++)
                    {
                        if (MathHelpers.IsOtherSide(start.Vertices[j].GetCoordinates(), start.Vertices[(j + 1) % 3].GetCoordinates(), coord, start.Vertices[(j + 2) % 3].GetCoordinates()))
                        {
                            found = false;
                            for (int k = 0; k < 3; k++)
                            {

                                if (start.Adjacency[k].Vertices.Contains(start.Vertices[j])
                                    && start.Adjacency[k].Vertices.Contains(start.Vertices[(j + 1) % 3]))
                                {
                                    start = start.Adjacency[k];
                                    break;
                                }

                            }
                            break;
                        }
                    }
                }

                Tuple<bool, double> w = CalculateZ(coord.XCoord, coord.YCoord, start);
                sm.DataPoints.Add((rho, Math.Abs(w.Item2)));
            }
        }

        //Calculates the Z value through interpolation given a triangle
        private static Tuple<bool, double> CalculateZ(double x, double y, Cell triangle)
        {
            Vertex p1 = triangle.Vertices[0];
            double x1 = p1.Position[0];
            double y1 = p1.Position[1];
            double z1 = p1.Z;
            Vertex p2 = triangle.Vertices[1];
            double x2 = p2.Position[0];
            double y2 = p2.Position[1];
            double z2 = p2.Z;
            Vertex p3 = triangle.Vertices[2];
            double x3 = p3.Position[0];
            double y3 = p3.Position[1];
            double z3 = p3.Z;

            double w1 = ((y2 - y3) * (x - x3) * 100000 + (x3 - x2) * (y - y3) * 100000) / ((y2 - y3) * (x1 - x3) * 100000 + (x3 - x2) * (y1 - y3) * 100000);
            double w2 = ((y3 - y1) * (x - x3) * 100000 + (x1 - x3) * (y - y3) * 100000) / ((y2 - y3) * (x1 - x3) * 100000 + (x3 - x2) * (y1 - y3) * 100000);
            double w3 = 1 - w1 - w2;

            double z = z1 * w1 + z2 * w2 + z3 * w3;

            bool valid = !(w1 < 0 || w2 < 0 || w3 < 0);

            return new Tuple<bool, double>(valid, z);
        }
    }
}
