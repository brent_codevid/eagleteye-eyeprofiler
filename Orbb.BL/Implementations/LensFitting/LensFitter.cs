﻿using MathNet.Numerics;
using MathNet.Numerics.Interpolation;
using MathNet.Numerics.Statistics;
using Orbb.BL.Implementations.LensFitting.LensTypeFitters;
using Orbb.BL.Interfaces.LensFitting;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.ViewModels.Models.LensFitter;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.LensFitting
{
    public class LensFitter : ILensFitter
    {
        private const int _MERCOUNT = 4;

        private readonly ILensRepository _lensRepository;
        private SemiMeridians _semiMeridians;
        private double diameter;
        public LensFitter(ILensRepository lensRepository)
        {
            _lensRepository = lensRepository;
        }

        //Returns the best fitting lens
        public IList<LensFittingResultVm> GetBestFittingLens(EyeData eyeData)
        {
            if (eyeData.LenssetIds == null || eyeData.LenssetIds.Count == 0)
            {
                return null;
            }

            IList<LensDetail> lensDetails = GenerateLensDetails(eyeData);
            var groupedLenses = lensDetails.GroupBy(l => l?.LensDiameter ?? (l?.CP4?.SemiChordLength * 2 ?? 0.0));

            IList<LensFittingResultVm> results = new List<LensFittingResultVm>();
            foreach (var lensesGroup in groupedLenses)
            {

                diameter = lensesGroup.Key;
                if (diameter != 0)
                {
                    _semiMeridians = GenerateSemiMeridians(eyeData, diameter);
                    var groupedLensesByRotation = lensesGroup.GroupBy(l => l.RotateEyeDataBeforeFitting);
                    //calculate unrotated first
                    var lensesWithoutRotation = groupedLensesByRotation.SingleOrDefault(l => l.Key == false);
                    if (lensesWithoutRotation != null)
                    {
                        foreach (LensDetail lensDetail in lensesWithoutRotation)
                        {
                            results.Add(CalculateResult(lensDetail, eyeData));
                        }
                    }
                    //rotate
                    _semiMeridians.RotateData(diameter);
                    //calculate rotated
                    var lensesWithRotation = groupedLensesByRotation.SingleOrDefault(l => l.Key == true);
                    if (lensesWithRotation != null)
                    {
                        foreach (LensDetail lensDetail in lensesWithRotation)
                        {
                            results.Add(CalculateResult(lensDetail, eyeData));
                        }
                    }
                }
                else
                {
                    results.Add(null);
                }
            }
            return results;
        }

        private LensFittingResultVm CalculateResult(LensDetail lensDetail, EyeData eyeData)
        {
            try
            {
                EyeAnalysisResults analysisResults = CalculateFittingParameters(eyeData, lensDetail, diameter);
                LensFittingResults fittingResult = CalculateFitting(analysisResults, lensDetail, eyeData);
                if (!lensDetail.UseNewMer1Selector)
                {//only use quadrotator for legacy lenses
                    fittingResult.PlaceMerOneOnTopForQuad();
                }


                string res = lensDetail.GenerateFittedLensName(fittingResult);
                LensFittingResultVm result = new LensFittingResultVm
                {
                    FittingAngle = fittingResult.Angle,
                    SelectedAngle = fittingResult.SelectedAngle,
                    Id = lensDetail.Id,
                    Name = res,
                    SupplierName = lensDetail.Supplier.Name,
                    Diameter = diameter,
                    Bcrs = new List<NameValueTuple>(),
                    Vaults = new List<NameValueTuple>(),
                    LandingZones = new List<NameValueTuple>()
                };

                if (lensDetail.UseCP1 && lensDetail.CP1.IncludeInApiResult)
                {
                    if (lensDetail.CP1.CP1SagCP2Percentage != null)
                    {
                        result.Bcrs = lensDetail.GenerateCP1FormattedList(fittingResult.Cp1Diffs, fittingResult, fittingResult.Cp1Ids);
                    }
                    else
                    {
                        result.Bcrs = lensDetail.GenerateCP1FormattedList(fittingResult.CP1Result, fittingResult, fittingResult.Cp1Ids);
                    }

                }

                if (lensDetail.UseCP2 && lensDetail.CP2.IncludeInApiResult)
                {
                    result.Vaults = lensDetail.GenerateCP2FormattedList(fittingResult.CP2Result, fittingResult, fittingResult.Cp2Ids);
                }

                if (lensDetail.UseCP4 && lensDetail.CP4.IncludeInApiResult)
                {
                    result.LandingZones = lensDetail.GenerateCP4FormattedList(fittingResult.CP4Result, fittingResult, fittingResult.Cp4Ids);
                }

                if (lensDetail.UseCP5 && lensDetail.CP5.IncludeInApiResult)
                {
                    result.Vaults = lensDetail.GenerateCP5FormattedList(fittingResult.CP5Result, fittingResult, fittingResult.Cp5Ids);
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //Calculates the data required for fitting a lens for the given eye
        public EyeAnalysisResults CalculateFittingParameters(EyeData eyedata, LensDetail lensDetail, double currentDiameter)
        {
            EyeAnalysisResults analysisResults = new EyeAnalysisResults()
            {
                Hvid = eyedata.Parameters?.Hvid?.CalculateDistance() ?? 0
            };

            double? cP1SemiChordLength = lensDetail.CP1?.SemiChordLength;
            double? cP2SemiChordLength = lensDetail.CP2?.SemiChordLength.Value;
            double? cP3SemiChordLength = lensDetail.CP3?.SemiChordLength.Value;
            double? cP4SemiChordLength = lensDetail.CP4?.SemiChordLength.Value;
            double? cP5SemiChordLength = lensDetail.CP5?.SemiChordLength.Value;


            ControlPointCategories category = (ControlPointCategories)(lensDetail.CP4?.Category ??
                                              lensDetail.CP3?.Category ??
                                              lensDetail.CP2?.Category ?? 
                                              lensDetail.CP1.Category);
            int flattestAngle = LensAngleFinder(currentDiameter / 2, category);

            //debug value
            //flattest_angle = 314;//173
            //flattest_angle = 138;

            analysisResults.Angle = flattestAngle;
            int primaryAngle = flattestAngle;
            var angleCorrection = flattestAngle;
            while (angleCorrection > 45)
            {
                angleCorrection -= 90;
            }

            if (lensDetail.UseNewMer1Selector)
            {
                switch (lensDetail.Meridian1Position)
                {
                    case MeridianPosition.Flat:
                        analysisResults.SelectedPosition = flattestAngle;
                        break;
                    case MeridianPosition.Top:
                        analysisResults.SelectedPosition = 90;
                        break;
                    case MeridianPosition.Right:
                        analysisResults.SelectedPosition = 0;
                        break;
                    case MeridianPosition.Bottom:
                        analysisResults.SelectedPosition = 270;
                        break;
                    case MeridianPosition.Left:
                        analysisResults.SelectedPosition = 180;
                        break;
                    default:
                        analysisResults.SelectedPosition = flattestAngle;
                        break;
                }
                primaryAngle = analysisResults.SelectedPosition;
            }
            analysisResults.SelectedPosition += angleCorrection;

            analysisResults.ApicalClearance = eyedata.Parameters.ApicalClearance;
            analysisResults.LimbalClearance = eyedata.Parameters.LimbalClearance;
            analysisResults.MidPeripheralClearance = eyedata.Parameters.MidPeripheralClearance;
            analysisResults.EdgeLift = eyedata.EdgeLift;

            List<SemiMeridian> semiMeridians = new List<SemiMeridian>(_MERCOUNT)
            {
                _semiMeridians.Single(item => item.Angle == primaryAngle),
                _semiMeridians.Single(item => item.Angle == (primaryAngle + 90) % 360),
                _semiMeridians.Single(item => item.Angle == (primaryAngle + 180) % 360),
                _semiMeridians.Single(item => item.Angle == (primaryAngle + 270) % 360)
            };

            //smoothing
            semiMeridians.ForEach(semiMer => { semiMer.SmoothData(); });

            List<IInterpolation> interpolations = new List<IInterpolation>(_MERCOUNT);
            for (int i = 0; i < _MERCOUNT; i++)
            {
                interpolations.Add(InterpolateMeridian(semiMeridians[i]));
                if (!lensDetail.UseCP2ToCP1List)
                {
                    if (cP1SemiChordLength != null)
                    {
                        analysisResults.CP1Val[i] = Math.Abs(interpolations[i].Interpolate(cP1SemiChordLength.Value));
                    }
                }
                if (cP2SemiChordLength != null)
                {
                    analysisResults.CP2Val[i] = Math.Abs(interpolations[i].Interpolate(cP2SemiChordLength.Value));
                }
                if (cP3SemiChordLength != null)
                {
                    analysisResults.CP3Val[i] = Math.Abs(interpolations[i].Interpolate(cP3SemiChordLength.Value));
                }
                if (cP4SemiChordLength != null)
                {
                    analysisResults.CP4Val[i] = Math.Abs(interpolations[i].Interpolate(cP4SemiChordLength.Value));
                }
                if (cP5SemiChordLength != null)
                {
                    analysisResults.CP5Val[i] = Math.Abs(interpolations[i].Interpolate(cP5SemiChordLength.Value));
                }
            }

            //calculate threshold (future feature when best lens from different lenssets)
            (ControlPointCategories vaultDesign, ControlPointCategories lzDesign) = Threshold(analysisResults, lensDetail);


            // most classic lens designs use a central spherical zone and a toric landing zone.In this case settling has to be guesstimated(see Settling2D.m)
            if (cP2SemiChordLength != null)
            {
                Dictionary<int, double> cP2Settling =
                    CalculateSettling(cP2SemiChordLength.Value, lensDetail.SuggestedSettling);

                for (int i = 0; i < _MERCOUNT; i++)
                {
                    int ii = flattestAngle + i * 90;
                    analysisResults.SettlingMerCP2[i] = cP2Settling[ii % 360];
                }
            }

            if (cP3SemiChordLength != null)
            {
                Dictionary<int, double> cP3Settling =
                    CalculateSettling(cP3SemiChordLength.Value, lensDetail.SuggestedSettling);

                for (int i = 0; i < _MERCOUNT; i++)
                {
                    int ii = flattestAngle + i * 90;
                    analysisResults.SettlingMerCP3[i] = cP3Settling[ii % 360];
                }
            }

            if (cP4SemiChordLength != null)
            {
                Dictionary<int, double> cP4Settling =
                    CalculateSettling(cP4SemiChordLength.Value, lensDetail.SuggestedSettling);

                for (int i = 0; i < _MERCOUNT; i++)
                {
                    int ii = flattestAngle + i * 90;
                    analysisResults.SettlingMerCP4[i] = cP4Settling[ii % 360];
                }
            }

            if (cP5SemiChordLength != null)
            {
                Dictionary<int, double> cP5Settling =
                    CalculateSettling(cP5SemiChordLength.Value, lensDetail.SuggestedSettling);

                for (int i = 0; i < _MERCOUNT; i++)
                {
                    int ii = flattestAngle + i * 90;
                    analysisResults.SettlingMerCP5[i] = cP5Settling[ii % 360];
                }
            }

            if (eyedata.Parameters.ApicalClearance != null)
            {
                analysisResults.ApicalClearance = (int)eyedata.Parameters.ApicalClearance;
            }
            if (eyedata.Parameters.MidPeripheralClearance != null)
            {
                analysisResults.MidPeripheralClearance = (int)eyedata.Parameters.MidPeripheralClearance;
            }

            return analysisResults;

        }


        //Finds the "best" lens for the described eye
        public LensFittingResults CalculateFitting(EyeAnalysisResults ear, LensDetail lensDetail, EyeData eyeData)
        {

            bool? unbound = eyeData.Unbound;
            bool? unlistedCp2 = eyeData.UnlistedCP2;
            //HVID = corneal diameter. Might be in use to choose the lens size
            //next values only needed for ortho_K calculations so far
            //MC = myopia control 1 = yes 2 = no
            //S = spherical power
            //C = cylinder
            //ax = axis of the cylinder
            //OZ = optical zone

            //variables
            //double diam_CP = 6;   //one of the CPs should be chosen if diam_min is not Nan (CP2 here)
            double? cp1Corr = lensDetail.CP1?.Correction;
            double? cp2Corr = lensDetail.CP2?.Correction;
            double? cp3Corr = lensDetail.CP3?.Correction;
            double? cp4Corr = lensDetail.CP4?.Correction;
            double? cp5Corr = lensDetail.CP5?.Correction;
            // end variables

            //prepare values
            cp1Corr = cp1Corr / 1000;
            cp2Corr = cp2Corr / 1000;
            cp3Corr = cp3Corr / 1000;
            cp4Corr = cp4Corr / 1000;
            cp5Corr = cp5Corr / 1000;
            for (int i = 0; i < ear.CP1Val.Length; i++)
            {
                if (cp1Corr != null && lensDetail.CP1.SemiChordLength != null)
                {
                    ear.CP1Val[i] = ear.CP1Val[i] + cp1Corr.Value;
                }
                if (cp2Corr != null)
                {
                    ear.CP2Val[i] = ear.CP2Val[i] + cp2Corr.Value;      // extra control if correction is required.So far only known issue for DRL(ortho - k)
                }
                if (lensDetail.CP2?.LimbalClearance != null)
                {
                    ear.CP2Val[i] = ear.CP2Val[i] + lensDetail.CP2.LimbalClearance.Value/1000;
                }
                if (cp3Corr != null)
                {
                    ear.CP3Val[i] = ear.CP3Val[i] + cp3Corr.Value;
                }
                if (cp4Corr != null)
                {
                    ear.CP4Val[i] = ear.CP4Val[i] + cp4Corr.Value;
                }
                if (cp5Corr != null)
                {
                    ear.CP5Val[i] = ear.CP5Val[i] + cp5Corr.Value;
                }
            }

            ILensTypeFitter fitter;
            //should be different, but client works with 1 multi-use algo
            switch (lensDetail.Type)
            {
                case (int)LensTypes.scleral:
                    fitter = new ScleralFitter();
                    break;
                case (int)LensTypes.orthok:
                    //ortho_k_DRL(HVID, MC, S, C, OZ, CP2_Z, CP4_Z, diam, apiccl);
                    fitter = new OrthoKFitter();
                    break;
                case (int)LensTypes.rgp:
                    //rgp_fitter;
                    fitter = new ScleralFitter();
                    break;
                case (int)LensTypes.soft:
                    //soft_fitter;
                    fitter = new ScleralFitter();
                    break;
                case (int)LensTypes.hybrid:
                    //hybrid_fitter(CP1_Z, CP);
                    fitter = new ScleralFitter();
                    break;
                default:
                    fitter = new ScleralFitter();
                    break;
            }

            fitter.Unbound = unbound ?? false;
            fitter.UnlistedCP2 = unlistedCp2 ?? false;
            ear.CP1Corr = cp1Corr ?? 0;
            return fitter.CalculateBestFit(ear, lensDetail, eyeData);
        }

        /*
         *---------------------------------------------------------
         *Private functions           
         *---------------------------------------------------------
         */

        //method return a list of LensDetail loaded from the database and supplemented by the data in EyeData
        private IList<LensDetail> GenerateLensDetails(EyeData eyeData)
        {
            IList<LensDetail> lensDetails = new List<LensDetail>();
            foreach (int id in eyeData.LenssetIds)
            {
                try
                {
                    LensDetail lens = _lensRepository.GetLenssetById(id);
                    if (eyeData.EdgeLift != null)
                    {
                        lens.CP4.Lift = -(double)eyeData.EdgeLift;
                    }

                    double diameter = CalculateDiameter(lens, eyeData, eyeData?.Parameters?.Hvid?.CalculateDistance() ?? 0);


                    if (lens.CP1?.RelativeChordLength ?? false)
                    {
                        lens.CP1.SemiChordLength = diameter * lens.CP1.DiameterMultiplier;
                    }
                    if (lens.CP2?.RelativeChordLength ?? false)
                    {
                        lens.CP2.SemiChordLength = diameter * lens.CP2.DiameterMultiplier;
                    }
                    if (lens.CP3?.RelativeChordLength ?? false)
                    {
                        lens.CP3.SemiChordLength = diameter * lens.CP3.DiameterMultiplier;
                    }
                    if (lens.CP4?.RelativeChordLength ?? false)
                    {
                        lens.CP4.SemiChordLength = diameter * lens.CP4.DiameterMultiplier;
                    }

                    lens.LensDiameter = diameter;
                    lensDetails.Add(lens);
                }
                catch (Exception e)
                {
                    lensDetails.Add(null);
                }

            }
            return lensDetails;
        }

        //method generates SemiMeridians from the meridians in eyeData
        private SemiMeridians GenerateSemiMeridians(IEyeData eyeData, double diameter)
        {
            SemiMeridians semiMeridians = new SemiMeridians(eyeData.Meridians.Count * 2);
            foreach (IMeridian mer in eyeData.Meridians)
            {
                //add first half
                SemiMeridian semiMer = new SemiMeridian
                {
                    DataPoints = new List<(double Rho, double zValues)>(),
                    Angle = mer.Angle + 180,
                    RhoDistance = mer.Rho
                };

                for (int i = mer.Apex; i >= 0; i--)
                {
                    if (i < 5) //not enough data left to detect a rise
                    {
                        semiMer.DataPoints.Add(((mer.Apex - i) * mer.Rho, mer.Points[i]));
                    }
                    else if (mer.Points[i] > mer.Points[i - 5])
                    {
                        semiMer.DataPoints.Add(((mer.Apex - i) * mer.Rho, mer.Points[i]));
                    }
                    else // rise detected
                    {
                        break;
                    }
                }
                semiMer.Extrapolate(diameter);//add some reseve for delauney triangulation (amount to add should be calculated from rotation plane vector
                semiMeridians.Add(semiMer);

                //add second half
                semiMer = new SemiMeridian
                {
                    DataPoints = new List<(double Rho, double zValues)>(),
                    Angle = mer.Angle,
                    RhoDistance = mer.Rho
                };
                for (int i = mer.Apex; i < mer.Points.Count; i++)
                {
                    if (i > mer.Points.Count - 6) //not enough data left to detect a rise
                    {
                        semiMer.DataPoints.Add(((i - mer.Apex) * mer.Rho, mer.Points[i]));
                    }
                    else if (mer.Points[i] > mer.Points[i + 5])
                    {
                        semiMer.DataPoints.Add(((i - mer.Apex) * mer.Rho, mer.Points[i]));
                    }
                    else // rise detected
                    {
                        break;
                    }
                }
                semiMer.Extrapolate(diameter);
                semiMeridians.Add(semiMer);
            }
            return semiMeridians;
        }

        //method returns a Tuple containing vault_design and the LZ_design

        private (ControlPointCategories vault_design, ControlPointCategories LZ_design) Threshold(EyeAnalysisResults ear, LensDetail lensDetail)
        {
            ControlPointCategories vaultDesign = ControlPointCategories.sferic;
            ControlPointCategories lzDesign = ControlPointCategories.sferic;
            if (lensDetail.CP2 != null)
            {
                double cp2T = lensDetail.CP2.Threshold / 1000; // threshold for CP2 in microns.

                if (ear.GetCP2Diff().MaximumAbsolute() >= cp2T)
                    vaultDesign = ControlPointCategories.quad;
                else if (Math.Abs(ear.GetCP2Mean()[1] - ear.GetCP2Mean()[0]) >= cp2T) // not sure if Math.Abs is correct here
                    vaultDesign = ControlPointCategories.toric;
                else
                    vaultDesign = ControlPointCategories.sferic;
            }

            if (lensDetail.CP4 != null)
            {
                double cp4T = lensDetail.CP4.Threshold / 1000; // threshold for CP4 in microns.

                if (ear.GetCP4Diff().MaximumAbsolute() >= cp4T)
                    lzDesign = ControlPointCategories.quad;
                else if (Math.Abs(ear.GetCP4Mean()[1] - ear.GetCP4Mean()[0]) >= cp4T) // not sure if Math.Abs is correct here
                    lzDesign = ControlPointCategories.toric;
                else
                    lzDesign = ControlPointCategories.sferic;
            }

            return (vaultDesign, lzDesign);
        }

        //method calculates settling, normally only used when CP2 is Spheric and CP4 is Toric
        private Dictionary<int, double> CalculateSettling(double cp, int avgSettling)
        {
            // calculate fit
            double sumSettling = _semiMeridians.Count * avgSettling / 1000; // total number for settling
            double sumSettling2 = sumSettling;

            //keys range from 0 to 359
            Dictionary<int, double> z = new Dictionary<int, double>();
            foreach (SemiMeridian semi in _semiMeridians)
            {
                IInterpolation interpolation = InterpolateMeridian(semi);
                z[semi.Angle] = -1 * Math.Abs(interpolation.Interpolate(cp));
            }

            double zMax = z.Aggregate((l, r) => l.Value > r.Value ? l : r).Value;
            z = z.ToDictionary(item => item.Key, item => item.Value - zMax);

            while (sumSettling2 > 0)
            {
                z = z.ToDictionary(item => item.Key, item => item.Value + 0.01);
                double tempSettling = z.Count(x => x.Value >= 0);
                sumSettling2 = sumSettling - tempSettling;
            }
            return z;
        }

        //method returns the flattest angle
        //The flattest chord is the chord perpendicular to the lowest chord (the chord with the highest z-values)
        private int LensAngleFinder(double semiDiam, ControlPointCategories design)
        {
            double rho = semiDiam - 0.5; // 0.5 is an arbitrary distance

            //keys range from 0 to 359
            Dictionary<int, double> zValues = new Dictionary<int, double>();
            //keys range from 0 to 179
            Dictionary<int, double> zp2 = new Dictionary<int, double>(); //array containing the mean z value


            foreach (SemiMeridian semiMeridian in _semiMeridians)
            {
                IInterpolation interpolation = InterpolateMeridian(semiMeridian);
                zValues.Add(semiMeridian.Angle, interpolation.Interpolate(rho));
            }
            for (int i = 0; i < 180; i++)
            {
                if (zValues.ContainsKey(i))
                    zp2.Add(i, (zValues[i] + zValues[i + 180]) / 2);
            }

            //keys range from 0 to 179
            Dictionary<int, double> degreesAngles = new Dictionary<int, double>(); //array containing the height difference between a point and the point on the circle perpendicular to it
            for (int i = 0; i < 180; i++)
            {
                if (zp2.ContainsKey(i))
                    degreesAngles.Add(i, zp2[i] - zp2[(i + 90) % 180]);
            }

            int degreesAngle = degreesAngles.Aggregate((l, r) => l.Value < r.Value ? l : r).Key; //index of the point where it's height difference with it's perpendicular point is greatest

            //chooses which semi meridian is the flattest
            if (design != ControlPointCategories.quad)
                return degreesAngle;

            //we are searching for the lowest value, aren't we?
            if (zValues[degreesAngle + 180] < zValues[degreesAngle])
            {
                degreesAngle = degreesAngle + 180;
            }

            return degreesAngle;
        }

        //method returns an interpolator defined on the given semiMeridian
        private static IInterpolation InterpolateMeridian(SemiMeridian semiMeridian)
        {
            (double[] rhos, double[] zValues) = semiMeridian.GetRhosAndValues();
            return Interpolate.Linear(rhos, zValues);
        }

        //has to be revised
        private double CalculateDiameter(LensDetail lensDetail, EyeData eyedata, double hvid)
        {
            double diam = 0.0;
            if (lensDetail.LensDiameter.HasValue && !lensDetail.UseHvidForDiameters)
            {
                diam = lensDetail.LensDiameter.Value;
            }
            // check if lens set contains more than one diameter
            else if (lensDetail.LensDiameterMin.HasValue && lensDetail.LensDiameterMax.HasValue && lensDetail.LensDiameterStepSize.HasValue)
            {
                double diamMin = lensDetail.LensDiameterMin.Value;
                double diamMax = lensDetail.LensDiameterMax.Value;
                double diamStep = lensDetail.LensDiameterStepSize.Value;

                if (lensDetail.LensDiameterPerc.HasValue)
                {
                    double diamPerc = lensDetail.LensDiameterPerc.Value;
                    diam = diamPerc * hvid;
                }
                else if (lensDetail.UseHvidForDiameters)// Leah Johnsons HVID rule
                {
                    if (hvid < lensDetail.HvidVal10)
                    {
                        diam = 10;
                    }
                    else if (hvid >= lensDetail.HvidVal10 & hvid <= lensDetail.HvidVal105)
                    {
                        diam = 10.5;
                    }
                    else if (hvid >= lensDetail.HvidVal105 & hvid <= lensDetail.HvidVal11)
                    {
                        diam = 11;
                    }
                    else if (hvid >= lensDetail.HvidVal11 & hvid <= lensDetail.HvidVal115)
                    {
                        diam = 11.5;
                    }
                    else if (hvid > lensDetail.HvidVal115)
                    {
                        diam = 12;
                    }
                }
                else
                {
                    //diam_CP = diam_CP * 2; //use full chord length
                    diam = lensDetail.HvidBuffer.Value + hvid;
                }

                diam = Math.Round(diam / diamStep) * diamStep;
                if (diam < diamMin)
                {
                    diam = diamMin;
                }
                else if (diam > diamMax)
                {
                    diam = diamMax;
                }
            }
            else if (lensDetail.CP4?.SemiChordLength != null)
            {
                diam = lensDetail.CP4.SemiChordLength.Value * 2;
            }
            else if (lensDetail.CP2?.SemiChordLength != null)
            {
                diam = lensDetail.CP2.SemiChordLength.Value * 2;
            }

            return diam;
        }
    }
}