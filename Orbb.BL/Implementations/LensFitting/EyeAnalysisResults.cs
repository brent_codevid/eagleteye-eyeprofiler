﻿using MathNet.Numerics.Statistics;
using Orbb.Data.Models.Models.Lenssets;
using System;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.LensFitting
{
    public class EyeAnalysisResults
    {
        public int Angle { get; set; }
        public int SelectedPosition { get; set; }

        public double[] CP1Val { get; set; }

        public double[] CP2Val { get; set; }

        public double[] CP3Val { get; set; }

        public double[] CP4Val { get; set; }

        public double[] CP5Val { get; set; }

        public double[] SettlingMerCP2 { get; set; }
        public double[] SettlingMerCP3 { get; set; }
        public double[] SettlingMerCP4 { get; set; }
        public double[] SettlingMerCP5 { get; set; }


        public double? ApicalClearance { get; set; }
        public double? LimbalClearance { get; set; }
        public double? MidPeripheralClearance { get; set; }
        public double? EdgeLift { get; set; }

        public double CP1Corr { get; set; }

        public double Hvid { get; set; }

        //extra clearance
        public double MinimalClearanceChord { get; set; }
        public double MinimalClearanceResidual{ get; set; }
        public double MinimalClearanceOriginalSAG { get; set; }

        public EyeAnalysisResults(int meridiansCount = 4)
        {
            CP1Val = new double[meridiansCount];

            CP2Val = new double[meridiansCount];

            CP3Val = new double[meridiansCount];

            CP4Val = new double[meridiansCount];

            CP5Val = new double[meridiansCount];

            SettlingMerCP2 = new double[meridiansCount];
            SettlingMerCP3 = new double[meridiansCount];
            SettlingMerCP4 = new double[meridiansCount];
            SettlingMerCP5 = new double[meridiansCount];
        }

        public double[] GetCP1Mean()
        {
            return new[] {
                (CP1Val[0] + CP1Val[2]) / 2 ,
                (CP1Val[1] + CP1Val[3]) / 2
            };
        }

        public double[] GetCP1Diff()
        {
            return new[] {
                Math.Abs(CP1Val[0] - CP1Val[2]),
                Math.Abs(CP1Val[1] - CP1Val[3])
            };
        }

        public double GetCP1Value(ControlPoint1 cp1, int axis)
        {
            switch (cp1.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        return CP1Val[axis];
                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        int meanIndex = (axis) % 2;
                        return GetCP1Mean()[meanIndex];
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        return CP1Val.Mean();
                        break;
                    }
            }
            throw new InvalidOperationException("Couldn't get correct value for cp.");
        }

        public double[] GetCP2Mean()
        {
            return new[] {
                (CP2Val[0] + CP2Val[2]) / 2 ,
                (CP2Val[1] + CP2Val[3]) / 2
            };
        }

        public double[] GetCP2Diff()
        {
            return new[] {
                Math.Abs(CP2Val[0] - CP2Val[2]),
                Math.Abs(CP2Val[1] - CP2Val[3])
            };
        }

        public double GetCP2Value(ControlPoint2 cp2, int axis)
        {
            switch (cp2.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        return CP2Val[axis];
                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        int meanIndex = (axis) % 2;
                        return GetCP2Mean()[meanIndex];
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        return CP2Val.Mean();
                        break;
                    }
            }
            throw new InvalidOperationException("Couldn't get correct value for cp.");
        }

        public double[] GetCP3Mean()
        {
            return new[] {
                (CP3Val[0] + CP3Val[2]) / 2 ,
                (CP3Val[1] + CP3Val[3]) / 2
            };
        }

        public double[] GetCP3Diff()
        {
            return new[] {
                Math.Abs(CP3Val[0] - CP3Val[2]),
                Math.Abs(CP3Val[1] - CP3Val[3])
            };
        }


        public double GetCP3Value(ControlPoint3 cp3, int axis)
        {
            switch (cp3.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        return CP3Val[axis];
                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        int meanIndex = (axis) % 2;
                        return GetCP3Mean()[meanIndex];
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        return CP3Val.Mean();
                        break;
                    }
            }
            throw new InvalidOperationException("Couldn't get correct value for cp.");
        }


        public double[] GetCP4Mean()
        {
            return new[] {
                (CP4Val[0] + CP4Val[2]) / 2 ,
                (CP4Val[1] + CP4Val[3]) / 2
            };
        }

        public double[] GetCP4Diff()
        {
            return new[] {
                Math.Abs(CP4Val[0] - CP4Val[2]),
                Math.Abs(CP4Val[1] - CP4Val[3])
            };
        }

        public double GetCP4Value(ControlPoint4 cp4, int axis)
        {
            switch (cp4.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        return CP4Val[axis];
                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        int meanIndex = (axis) % 2;
                        return GetCP4Mean()[meanIndex];
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        return CP4Val.Mean();
                        break;
                    }
            }
            throw new InvalidOperationException("Couldn't get correct value for cp.");
        }

        public double[] GetCP5Mean()
        {
            return new[] {
                (CP5Val[0] + CP5Val[2]) / 2 ,
                (CP5Val[1] + CP5Val[3]) / 2
            };
        }

        public double[] GetCP5Diff()
        {
            return new[] {
                Math.Abs(CP5Val[0] - CP5Val[2]),
                Math.Abs(CP5Val[1] - CP5Val[3])
            };
        }

        public double GetCP5Value(ControlPoint5 cp5, int axis)
        {
            switch (cp5.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        return CP5Val[axis];
                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        int meanIndex = (axis) % 2;
                        return GetCP5Mean()[meanIndex];
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        return CP5Val.Mean();
                        break;
                    }
            }
            throw new InvalidOperationException("Couldn't get correct value for cp.");
        }
    }
}
