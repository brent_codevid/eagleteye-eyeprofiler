﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.BL.Implementations.LensFitting
{
    public class OrthoK
    {
        public double Sphere { get; set; }
        public double Cylinder { get; set; }
        public double Axis { get; set; }
    }
}
