﻿namespace Orbb.BL.Implementations.LensFitting
{
    public class Parameters
    {

        public Hvid Hvid { get; set; }
        public Limbus Limbus { get; set; }
        public OrthoK OrthoK { get; set; }
        public SimK SimK { get; set; }
        public double? Bfs { get; set; }
        public int? ApicalClearance { get; set; }
        public int? LimbalClearance { get; set; }
        public int? MidPeripheralClearance { get; set; }

    }
}
