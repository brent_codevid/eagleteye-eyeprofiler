﻿using MathNet.Numerics;
using Orbb.BL.Implementations.Base;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Implementations.LensFitting
{
    //Representation of a semiMeridian
    public class SemiMeridian
    {
        public double RhoDistance { get; set; }
        public int Angle { get; set; }
        //list of rho and Z value tuples
        public List<(double Rho, double zValues)> DataPoints { get; set; }

        public double ExtrapolateDistance { get; set; } = 2;
        public double SmoothDataMinimumRho { get; set; } = 6.25;

        //Returns a tuple containing an array with the rho's and an array with the values
        public (double[] rhos, double[] zValues) GetRhosAndValues()
        {
            List<double> rhos = new List<double>(DataPoints.Count);
            List<double> values = new List<double>(DataPoints.Count);
            foreach (var (rho, zValue) in DataPoints)
            {
                rhos.Add(rho);
                values.Add(zValue);
            }
            return (rhos.ToArray(), values.ToArray());
        }

        //Returns an array containing all the Rho-values
        public double[] GetRhos()
        {
            List<double> output = new List<double>(DataPoints.Count);
            foreach ((double rho, _) in DataPoints)
            {
                output.Add(rho);
            }
            return output.ToArray();
        }

        //Returns an array containing all the z-values
        public double[] GetZValues()
        {
            List<double> output = new List<double>(DataPoints.Count);
            foreach ((_, double zValue) in DataPoints)
            {
                output.Add(zValue);
            }
            return output.ToArray();
        }

        //Returns the z-value interpolated by the semiMeridian
        public double GetInterpolatedValue(double rhoVal)
        {
            return Interpolate.Linear(GetRhos(), GetZValues()).Interpolate(rhoVal);
        }

        //Formerly known as CustomInterpolation
        //Almost known as SuperAwsomeDataImprovementAreaIncrease
        public void Extrapolate(double diameter)
        {
            double[] xData = DataPoints.Where(item => item.Rho >= 5).Select(item => item.Rho).ToArray();
            double[] yData = DataPoints.Where(item => item.Rho >= 5).Select(item => item.zValues).ToArray();

            var (intercept, slope) = Fit.Line(xData, yData);
            double lastRho = DataPoints.Last().Rho;
            while (lastRho < diameter / 2 + ExtrapolateDistance) // Making sure we have enough data
            {
                lastRho = lastRho + RhoDistance;
                DataPoints.Add((lastRho, intercept + slope * lastRho));
            }
        }

        //Smoothes the data by using a MovingStatistic
        public void SmoothData()
        {
            int span = DataPoints.Count / 2;
            var smoothedData = MathHelpers.SimpleMovingAverageSmoothing(DataPoints.Where(item => item.Rho >= SmoothDataMinimumRho).Select(item => item.zValues).ToList(), span);
            int index = 0;
            for (int i = DataPoints.Count - smoothedData.Length; i < DataPoints.Count; i++)
            {
                DataPoints[i] = (DataPoints[i].Rho, smoothedData[index]);
                index++;
            }
        }
    }
}
