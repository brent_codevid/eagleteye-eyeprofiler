﻿using MIConvexHull;

namespace Orbb.BL.Implementations.LensFitting
{
    /// <summary>
    /// A vertex is a simple class that stores the position of a point, node or vertex.
    /// </summary>
    public class Cell : TriangulationCell<Vertex, Cell>
    {
        /*
        public static double Det(double[,] m)
        {
            return m[0, 0] * ((m[1, 1] * m[2, 2]) - (m[2, 1] * m[1, 2])) - m[0, 1] * (m[1, 0] * m[2, 2] - m[2, 0] * m[1, 2]) + m[0, 2] * (m[1, 0] * m[2, 1] - m[2, 0] * m[1, 1]);
        }

        public static double LengthSquared(double[] v)
        {
            return v.Sum(t => t * t);
        }
        */
    }
}