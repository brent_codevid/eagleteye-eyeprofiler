﻿using Orbb.BL.Interfaces.LensFitting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.Implementations.LensFitting
{
    public class Meridian : IMeridian
    {
        public int Angle { get; set; }
        public double Rho { get; set; }
        public int Apex { get; set; }
        public IList<double> Points { get; set; }
        public IList<double> Rhos { get; set; }
    }

    public class EyeData : IEyeData
    {
        IList<IMeridian> IEyeData.Meridians { get => new List<IMeridian>(Meridians); set { } }
        public IList<Meridian> Meridians { get; set; }
        public IList<int> LenssetIds { get; set; }
        public int LensType { get; set; }

        public double? EdgeLift { get; set; }

        public bool? Unbound { get; set; }
        public bool? UnlistedCP2 { get; set; }

        public Parameters Parameters { get; set; }

        public bool ValidMeridians()
        {
            List<int> angles = Meridians.Select(mer => mer.Angle % 180).ToList();

            //Efficient way to check if all elements in angles are unique
            var uniques = new HashSet<int>();
            if (!angles.All(uniques.Add))
                return false;

            //Checking if all angles have exactly one corresponding perpendicular angle
            var perpendiculars = new Dictionary<int, int>();
            foreach (var angle in angles)
            {
                if (perpendiculars.ContainsKey(angle % 90))
                {
                    perpendiculars[angle % 90] += 1;
                }
                else
                {
                    perpendiculars.Add(angle % 90, 1);
                }
            }
            return perpendiculars.All(item => item.Value == 2);
        }

        public IList<Meridian> GetAllDataWithin(double chordLengthInside, double chordLengthOutside)
        {
            IList<Meridian> filteredMeridians = new List<Meridian>();
            foreach (Meridian mer in Meridians)
            {
                Meridian newMer = new Meridian();
                newMer.Angle = mer.Angle;

                newMer.Rho = mer.Rho;

                int stepCount = (int)Math.Floor(chordLengthOutside / mer.Rho);
                int toSkip = stepCount <= mer.Apex ? mer.Apex - stepCount : 0;
                int totake = stepCount * 2 + 1;

                newMer.Apex = mer.Apex - toSkip;


                if (totake + toSkip >= mer.Points.Count)
                {
                    newMer.Points = mer.Points.Skip(toSkip).ToList();
                }
                else
                {
                    newMer.Points = mer.Points.Skip(toSkip).Take(totake).ToList();
                }

                newMer.Rhos = new List<double>();
                for (int i = 0; i < newMer.Apex; i++)
                {
                    newMer.Rhos.Add((newMer.Apex - i) * newMer.Rho);
                }
                for (int i = newMer.Apex; i < newMer.Points.Count(); i++)
                {
                    newMer.Rhos.Add((i - newMer.Apex) * newMer.Rho);
                }

                var tmplistpts = new List<double>();
                var tmplistrhos = new List<double>();
                for (int i = 0; i < newMer.Points.Count; i++) {
                    if(newMer.Rhos[i]>= chordLengthInside)
                    {
                        tmplistpts.Add(newMer.Points[i]);
                        tmplistrhos.Add(newMer.Rhos[i]);
                    }
                }

                newMer.Points = tmplistpts;
                newMer.Rhos = tmplistrhos;

                filteredMeridians.Add(newMer);
            }

            return filteredMeridians;
        }
        public IList<Meridian> RemoveCetralZone(double chordLength)
        {
            IList<Meridian> filteredMeridians = new List<Meridian>();
            foreach (Meridian mer in Meridians)
            {
                Meridian newMer = new Meridian();
                newMer.Angle = mer.Angle;

                newMer.Rho = mer.Rho;

                int stepCount = (int)Math.Floor(chordLength / mer.Rho);
                int toSkip = stepCount <= mer.Apex ? mer.Apex - stepCount : 0;
                int totake = stepCount * 2 + 1;

                if (totake + toSkip >= mer.Points.Count)
                {
                    newMer.Points = mer.Points.Skip(toSkip).ToList();
                }
                else
                {
                    newMer.Points = mer.Points.Skip(toSkip).Take(totake).ToList();
                }
                newMer.Apex = mer.Apex - toSkip;

                newMer.Rhos = new List<double>();
                for (int i = 0; i < newMer.Apex; i++)
                {
                    newMer.Rhos.Add((newMer.Apex - i) * newMer.Rho);
                }
                for (int i = newMer.Apex; i < newMer.Points.Count(); i++)
                {
                    newMer.Rhos.Add((i - newMer.Apex) * newMer.Rho);
                }
                filteredMeridians.Add(newMer);
            }

            return filteredMeridians;
        }
    }
}
