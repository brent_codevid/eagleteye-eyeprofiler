﻿using System;

namespace Orbb.BL.Implementations.LensFitting
{
    public class Hvid : Circle
    {
        public double CalculateDistance()
        {
            //return Math.Sqrt(Math.Pow(Center.XCoord - Radius.XCoord, 2) + Math.Pow(Center.YCoord - Radius.YCoord, 2)); 
            return Radius.XCoord * 2; //no coordinates but chordlength is passed
        }
    }
}