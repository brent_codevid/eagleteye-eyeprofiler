﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Common.Utility;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class CurvatureDesignStrategy : ICalculationStrategy
    {
        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            ControlPoint controlPoint = null;
            switch (cp)
            {
                case Enumerations.ControlPointEnum.Cp1:
                    controlPoint = lensdetail.CP1;
                    break;
                case Enumerations.ControlPointEnum.Cp2:
                    controlPoint = lensdetail.CP2;
                    break;
                case Enumerations.ControlPointEnum.Cp4:
                    controlPoint = lensdetail.CP4;
                    break;
            }

            int RadiusOptionsCount = controlPoint.CpCurvatureList.Count();
            int SectionCount = controlPoint.CpCurvatureList.First().Excentricities.Count();

            List<(int id, double result)> results = new List<(int, double)>();

            for (int i = 0; i < RadiusOptionsCount; i++) {
                double result = 0;

                for (int j = 0; j < SectionCount; j++)
                {
                    var curvatureList = controlPoint.CpCurvatureList;
                    List<double> diameterList = DataParser.ParseStringToList(controlPoint.CurvatureDiametersString, ';');

                    double sectionResult =
                        (curvatureList[i].Radii[j] - (Math.Sqrt(Math.Pow(curvatureList[i].Radii[j], 2) - (1 - Math.Pow(curvatureList[i].Excentricities[j], 2)) *Math.Pow(diameterList[j + 1], 2) / (1 - Math.Pow(curvatureList[i].Excentricities[j], 2)))))
                        - 
                        (curvatureList[i].Radii[j] - (Math.Sqrt(Math.Pow(curvatureList[i].Radii[j], 2) - (1 - Math.Pow(curvatureList[i].Excentricities[j], 2)) * Math.Pow(diameterList[j], 2) / (1 - Math.Pow(curvatureList[i].Excentricities[j], 2)))));

                    result += sectionResult;
                }
                results.Add((controlPoint.CpCurvatureList[i].Id, result));
            }

            var closestValue = calculatedValue;
            var id = 0;
            switch (controlPoint.RoundingStrategy)
            {
                case Enumerations.CpRoundingStrategy.Nearest:
                    (id, closestValue) = results.Aggregate((x, y) => Math.Abs(x.result - calculatedValue) < Math.Abs(y.result - calculatedValue) ? x : y);

                    break;
                case Enumerations.CpRoundingStrategy.NearestLarger:
                    (id, closestValue) = results.OrderBy(r => r.result - calculatedValue).First(r => r.result > calculatedValue);
                    break;
            }
            return (id, closestValue);
        }
    }
}
