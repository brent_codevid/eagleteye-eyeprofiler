﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class TangentStrategy : ICalculationStrategy
    {
        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            ControlPoint controlPoint = null;
            double CenterDiam = 0;
            double OwnDiam = 0;
            switch (cp)
            {
                case Enumerations.ControlPointEnum.Cp1:
                    controlPoint = lensdetail.CP1;
                    CenterDiam = 0;
                    OwnDiam = lensdetail.CP1.SemiChordLength ?? 0;
                    break;
                case Enumerations.ControlPointEnum.Cp2:
                    controlPoint = lensdetail.CP2;
                    CenterDiam = lensdetail.CP1.SemiChordLength ?? 0;
                    OwnDiam = lensdetail.CP2.SemiChordLength ?? 0;
                    break;
                case Enumerations.ControlPointEnum.Cp4:
                    controlPoint = lensdetail.CP4;
                    CenterDiam = lensdetail.CP2.SemiChordLength ?? 0;
                    OwnDiam = lensdetail.CP4.SemiChordLength ?? 0;
                    break;
            }

            double betweenDiameter = (OwnDiam - CenterDiam);
            double tangentRad = Math.Atan(calculatedValue / betweenDiameter);
            double tangentDeg = tangentRad * 180 / Math.PI;
            if (controlPoint.TangentPosition == Enumerations.TangentListPositioning.Vertical)
            {
                tangentDeg = 90 - tangentDeg;
            }

            var closestValue = tangentDeg;
            int id = 0;
            switch (controlPoint.RoundingStrategy)
            {
                case Enumerations.CpRoundingStrategy.Nearest:
                    //closestValue = controlPoint.CpTangentList.Aggregate((x, y) => Math.Abs(x.CPTangentValue - calculatedValue) < Math.Abs(y.CPTangentValue - calculatedValue) ? x : y).CPTangentValue;
                    var absvals = controlPoint.CpTangentList.Select(c => new { c, distance = Math.Abs(c.CPTangentValue - closestValue) });
                    var orderedabsvals = absvals.OrderBy(s => s.distance);

                    closestValue = orderedabsvals.First().c.CPTangentValue;
                    id = orderedabsvals.First().c.Id;
                    break;
                case Enumerations.CpRoundingStrategy.NearestLarger:
                    closestValue = controlPoint.CpTangentList.OrderBy(c => c.CPTangentValue - tangentDeg).First(c => c.CPTangentValue > tangentDeg).CPTangentValue;
                    id = controlPoint.CpTangentList.OrderBy(c => c.CPTangentValue - tangentDeg).First(c => c.CPTangentValue > tangentDeg).Id;
                    break;
            }

            return (id, closestValue);
        }
    }
}
