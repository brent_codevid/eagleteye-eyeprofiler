﻿using Orbb.Common;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public interface ICalculationStrategy
    {
        (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis);
    }
}
