﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Common.Utility;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class AlignmentCurveStrategy : ICalculationStrategy
    {
        public EyeAnalysisResults ear;

        public AlignmentCurveStrategy(EyeAnalysisResults ear)
        {
            this.ear = ear;
        }

        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            //AC= sqrt((2*CP2 chord)^2+((((CP4_chord^2- (2*CP2 chord) ^2)+CP4_sag_diff^2)/(2*CP4_sag_diff))^2));
            double CPSagDiff = 0.0;// ear.CP5Val[axis] + (lensdetail.CP5.EdgeLift ?? 0.0);
            double CPChordLength = 0.0;
            if(cp == Enumerations.ControlPointEnum.Cp5)
            {
                CPSagDiff = ear.GetCP5Value(lensdetail.CP5, axis) * 1000 + (lensdetail.CP5.EdgeLift ?? 0.0);
                CPChordLength = lensdetail.CP5.SemiChordLength ?? 0.0;
            } else
            {
                CPSagDiff = ear.GetCP4Value(lensdetail.CP4, axis + 1) * 1000 + (lensdetail.CP4.Lift);
                CPChordLength = lensdetail.CP4.SemiChordLength ?? 0.0;
            }

            double previousChordLength = 0.0;
            if (cp == Enumerations.ControlPointEnum.Cp5 && lensdetail.UseCP4)
            {
                previousChordLength = lensdetail.CP4.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP4Value(lensdetail.CP4, axis) * 1000;
            }
            else if (lensdetail.UseCP3)
            {
                previousChordLength = lensdetail.CP3.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP3Value(lensdetail.CP3, axis) * 1000;
            }
            else if (lensdetail.UseCP2)
            {
                previousChordLength = lensdetail.CP2.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP2Value(lensdetail.CP2, axis) * 1000;
            }
            else if (lensdetail.UseCP1)
            {
                previousChordLength = lensdetail.CP1.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP1Value(lensdetail.CP1, axis) * 1000;
            }
            
            var ac = Math.Sqrt(
                Math.Pow(previousChordLength, 2)
                +
                Math.Pow(
                    (CPChordLength * CPChordLength - Math.Pow(previousChordLength, 2) + Math.Pow(CPSagDiff / 1000, 2)) 
                    /
                    (2* (CPSagDiff / 1000))
                , 2)
            );
            return (0, ac);
        }
    }
}
