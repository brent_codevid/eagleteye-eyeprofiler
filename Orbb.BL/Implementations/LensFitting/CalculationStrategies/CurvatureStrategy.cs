﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Orbb.Common;
using Orbb.Common.Utility;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class CurvatureStrategy : ICalculationStrategy
    {
        public EyeAnalysisResults ear;

        public CurvatureStrategy(EyeAnalysisResults ear)
        {
            this.ear = ear;
        }

        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            double CPSagDiff = 0.0;// ear.CP5Val[axis] + (lensdetail.CP5.EdgeLift ?? 0.0);
            double CPChordLength = 0.0;
            if (cp == Enumerations.ControlPointEnum.Cp3)
            {
                CPSagDiff = ear.GetCP3Value(lensdetail.CP3, axis);
                CPChordLength = lensdetail.CP3.SemiChordLength ?? 0.0;
            } else
            {
                CPSagDiff = ear.GetCP2Value(lensdetail.CP2, axis);
                CPChordLength = lensdetail.CP2.SemiChordLength ?? 0.0;
            }
            double previousChordLength = 0.0;
            if (cp == Enumerations.ControlPointEnum.Cp3 && lensdetail.UseCP2)
            {
                previousChordLength = lensdetail.CP2.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP2Value(lensdetail.CP2, axis);
            }
            else if (lensdetail.UseCP1)
            {
                previousChordLength = lensdetail.CP1.SemiChordLength ?? 0.0;
                CPSagDiff -= ear.GetCP1Value(lensdetail.CP1, axis);
            }

            //RC CP3 = sqrt((CP2 chord)^2+((((CP3_chord^2- (CP2 chord) ^2)+CP3_sag_diff^2)/(2*CP3_sag_diff))^2));
            //RC CP2 = sqrt((CP1 chord)^2+((((CP2_chord^2- (CP1 chord) ^2)+CP2_sag_diff^2)/(2*CP2_sag_diff))^2));
            var rc = Math.Sqrt(
                Math.Pow(previousChordLength, 2)
                +
                Math.Pow(
                    (CPChordLength * CPChordLength - Math.Pow(previousChordLength, 2) + Math.Pow(CPSagDiff, 2)) 
                    /
                    (2* (CPSagDiff))
                , 2)
            );
            return (0, rc);
        }
    }
}
