﻿using Orbb.Common;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class FormulaStrategy : ICalculationStrategy
    {
        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            return (0, calculatedValue);
        }
    }
}
