﻿using Orbb.Common;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.BL.Implementations.LensFitting.CalculationStrategies
{
    public class ListStrategy : ICalculationStrategy
    {
        public (int id, double result) CalculateValue(LensDetail lensdetail, double calculatedValue, Enumerations.ControlPointEnum cp, int axis)
        {
            ControlPoint controlPoint = null;
            switch (cp)
            {
                case Enumerations.ControlPointEnum.Cp1:
                    controlPoint = lensdetail.CP1;
                    break;
                case Enumerations.ControlPointEnum.Cp2:
                    controlPoint = lensdetail.CP2;
                    break;
                case Enumerations.ControlPointEnum.Cp4:
                    controlPoint = lensdetail.CP4;
                    break;
            }

            var closestValue = calculatedValue;
            int id = 0;
                switch (controlPoint.RoundingStrategy)
                {
                    case Enumerations.CpRoundingStrategy.Nearest:
                        closestValue = controlPoint.CpList.Aggregate((x, y) => Math.Abs(x.CPValue - calculatedValue) < Math.Abs(y.CPValue - calculatedValue) ? x : y).CPValue;
                        id = controlPoint.CpList.Aggregate((x, y) => Math.Abs(x.CPValue - calculatedValue) < Math.Abs(y.CPValue - calculatedValue) ? x : y).Id;
                        break;
                    case Enumerations.CpRoundingStrategy.NearestLarger:
                        closestValue = controlPoint.CpList.OrderBy(c => c.CPValue - calculatedValue).First(c => c.CPValue >= calculatedValue).CPValue;
                        id = controlPoint.CpList.OrderBy(c => c.CPValue - calculatedValue).First(c => c.CPValue >= calculatedValue).Id;
                        break;
                }

            return (id, closestValue);
        }

    }
}
