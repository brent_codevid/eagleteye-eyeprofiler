﻿using Orbb.BL.Implementations.Base;

namespace Orbb.BL.Implementations.LensFitting
{
    public class Circle
    {
        public CarthesianCoordinates Center { get; set; }
        public CarthesianCoordinates Radius { get; set; }
    }
}