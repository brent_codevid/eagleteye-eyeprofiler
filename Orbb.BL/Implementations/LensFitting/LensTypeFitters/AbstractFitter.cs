﻿using MathNet.Numerics.Statistics;
using Orbb.BL.Interfaces.LensFitting;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;

namespace Orbb.BL.Implementations.LensFitting.LensTypeFitters
{
    public abstract class AbstractFitter : ILensTypeFitter
    {
        public bool Unbound { get; set; } = false;
        public bool UnlistedCP2 { get; set; } = false;

        public abstract LensFittingResults CalculateBestFit(EyeAnalysisResults ear, LensDetail lensDetail, EyeData eyeData);
        public static EyeAnalysisResults CalculateExtraClearance(EyeAnalysisResults ear, LensDetail lensDetail, EyeData eyeData, List<double> cp1s, List<double> cp2s)
        {
            double excentricity = 0; //temp default val
            IList<Meridian> pointsWithinCP1 = eyeData.GetAllDataWithin(3, lensDetail.CP1.SemiChordLength ?? 0);


            var cp1mean = cp1s.Mean();

            IList<double> cp1Sags = new List<double>();
            IList<IList<double>> residuals = new List<IList<double>>();
            ear.MinimalClearanceResidual = double.MaxValue;

            foreach (Meridian mer in pointsWithinCP1)
            {
                IList<double> residual = new List<double>();
                for (int i = 0;i<mer.Points.Count;i++)
                {
                    //convert baseCurve to SAG 
                    //Z_ctl = BC - sqrt(BC ^ 2 - (1 - ecc ^ 2) * rho.^ 2) / (1 - ecc ^ 2);
                    double cp1Sag = cp1mean - Math.Sqrt((cp1mean * cp1mean) - (1 - (excentricity * excentricity)) * (mer.Rhos[i]*mer.Rhos[i])) / (1 - excentricity * excentricity);
                    cp1Sag = (cp1Sag * 1000) - lensDetail.SuggestedApicalClearance;
                    cp1Sags.Add(cp1Sag);

                    //calc residuals
                    double res = Math.Abs(mer.Points[i] * 1000) - cp1Sag;
                    residual.Add(res);
                    if(res < ear.MinimalClearanceResidual)
                    {
                        ear.MinimalClearanceResidual = res;
                        ear.MinimalClearanceChord = mer.Rhos[i];
                        ear.MinimalClearanceOriginalSAG = mer.Points[i] * 1000;
                    }
                }
                residuals.Add(residual);
            }
            return ear;
        }

    }
}