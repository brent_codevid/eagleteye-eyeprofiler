﻿using MathNet.Numerics.Statistics;
using Orbb.BL.Implementations.LensFitting.CalculationStrategies;
using Orbb.Common;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.Implementations.LensFitting.LensTypeFitters
{
    public class ScleralFitter : AbstractFitter
    {
        //Finds the best scleral lens
        public override LensFittingResults CalculateBestFit(EyeAnalysisResults ear, LensDetail lensDetail, EyeData eyeData)
        {
            LensFittingResults lfd = new LensFittingResults
            {
                Angle = ear.Angle,
                SelectedAngle = ear.SelectedPosition,
                Diameter = lensDetail.LensDiameter.Value
            };

            int settling = lensDetail.SuggestedSettling;

            //EdgeLift && LimbalClearance not used yet

            //prepare values

            int lzSettling = settling;



            //calculates the SAG. CP2
            if (lensDetail.UseCP2)
            {
                //calculates settling
                if (lensDetail.CP2.Category == (int)ControlPointCategories.sferic) //most classic lens designs use a central spherical zone and a toric landing zone.In this case settling has to be guestimated(see Settling2D.m)
                {
                    //might want to check this, maybe
                    //ear.CP4Val = ear.CP4Val.Zip(ear.SettlingMerCP4, (a, b) => a + b).ToArray();
                    ear.CP2Val = ear.CP2Val.Zip(ear.SettlingMerCP2, (a, b) => a + b).ToArray();
                    settling = 0;
                }

                List<(int, double)> cp2r = CalculateCP2Result(lensDetail, ear, settling);
                lfd.Cp2Ids = cp2r.Select(x => x.Item1).ToList();
                lfd.CP2Result = cp2r.Select(x => x.Item2).ToList();
            }

            //calculates the SAG. CP3
            if (lensDetail.UseCP3)
            {
                //calculates settling
                if (lensDetail.CP3.Category == (int)ControlPointCategories.sferic) //most classic lens designs use a central spherical zone and a toric landing zone.In this case settling has to be guestimated(see Settling2D.m)
                {
                    //might want to check this, maybe
                    //ear.CP4Val = ear.CP4Val.Zip(ear.SettlingMerCP4, (a, b) => a + b).ToArray();
                    ear.CP3Val = ear.CP3Val.Zip(ear.SettlingMerCP3, (a, b) => a + b).ToArray();
                    settling = 0;
                }

                List<(int, double)> cp3r = CalculateCP3Result(lensDetail, ear, lensDetail.SuggestedApicalClearance, settling);
                lfd.Cp3Ids = cp3r.Select(x => x.Item1).ToList();
                lfd.CP3Result = cp3r.Select(x => x.Item2).ToList();
            }

            //end calculations SAG

            //calculates the LZ CP2 and CP4
            if (lensDetail.UseCP4)
            {
                List<(int, double)> cp4r = CalculateCP4Result(lensDetail, ear, settling, lzSettling, lfd.CP2Result);

                lfd.Cp4Ids = cp4r.Select(x => x.Item1).ToList();
                lfd.CP4Result = cp4r.Select(x => x.Item2).ToList();
            }
            //end calculations LZ

            // calculates the BC. Calculations related to CP1
            if (lensDetail.UseCP1)
            {
                List<(int, double)> cp1r = CalculateCP1Result(lensDetail, ear, lfd);
                lfd.Cp1Ids = cp1r.Select(x => x.Item1).ToList();
                lfd.CP1Result = cp1r.Select(x => x.Item2).ToList();
            }
            //end calculations BC

            //final modifications
            lfd.ApplyFinalValuesToAdd();


            if (lensDetail.CP2?.IncreaseClearance ?? false)
            {
                var originalCP1s = lfd.CP1Result;
                ear = CalculateExtraClearance(ear, lensDetail, eyeData, lfd.CP1Result, lfd.CP2Result);
                if (ear.MinimalClearanceResidual < lensDetail.CP2.MinimalClearance)
                {
                    double diam = ear.MinimalClearanceChord * 2;
                    double sagDiff = 0;
                    double sagPlus = 0;

                    if (lensDetail.UseCP1 || lensDetail.UseCP2ToCP1List)
                    {
                        double sag3 = -ear.MinimalClearanceOriginalSAG;
                        double flattestBC = ((diam * diam) + 4 * (sag3 * sag3)) / (8 * sag3);
                        if (lensDetail.CP1.MaxValue !=null && flattestBC > lensDetail.CP1.MaxValue)
                        {
                            flattestBC = lensDetail.CP1.MaxValue.Value;
                        }
                        //recalc with new bc
                        for (int i = 0; i < lfd.CP1Result.Count; i++)
                        {
                            lfd.CP1Result[i] = flattestBC;
                        }
                        ear = CalculateExtraClearance(ear, lensDetail, eyeData, lfd.CP1Result, lfd.CP2Result);


                        if (ear.MinimalClearanceResidual < lensDetail.CP2.MinimalClearance)
                        {
                            sagDiff = lensDetail.CP2.MinimalClearance.Value - ear.MinimalClearanceResidual;
                            sagPlus = Math.Ceiling(sagDiff / lensDetail.CP2.StepSize ?? 1) * lensDetail.CP2.StepSize ?? 1;
                            sagPlus = sagPlus + lensDetail.CP2.SagCorrectionClearance ?? 0;
                        }
                        else
                        {
                            sagPlus = 0;
                        }

                        for (int i = 0; i < lfd.CP1Result.Count; i++)
                        {
                            if(flattestBC>= lfd.CP1Result[i])
                            {
                                lfd.CP1Result[i] = flattestBC;
                            }
                            if (eyeData.Parameters.Bfs != null && (eyeData.Parameters.Bfs.Value) > lfd.CP1Result[i])
                            {
                                lfd.CP1Result[i] = eyeData.Parameters.Bfs.Value;
                            }
                        }
                    }
                    else
                    {
                        sagDiff = lensDetail.CP2.MinimalClearance.Value - ear.MinimalClearanceResidual;
                        sagPlus = Math.Ceiling(sagDiff / lensDetail.CP2.StepSize ?? 1) * lensDetail.CP2.StepSize ?? 1;
                        sagPlus = sagPlus + lensDetail.CP2.SagCorrectionClearance ?? 0;
                    }

                    for (int i = 0; i < lfd.CP2Result.Count; i++)
                    {
                        lfd.CP2Result[i] += sagPlus;
                    }

                   
                    if (lensDetail.CP1.CalculationMethod == CpCalculationMethod.List && !lensDetail.UseCP2ToCP1List)
                    {
                        List<(int, double)> cp1r = CalculateSpecialCalculationMethods(lensDetail, ear, lfd.CP1Result, Enumerations.ControlPointEnum.Cp1);
                        lfd.Cp1Ids = cp1r.Select(x => x.Item1).ToList();
                        lfd.CP1Result = cp1r.Select(x => x.Item2).ToList();
                    }
                    if (lensDetail.CP2.CalculationMethod == CpCalculationMethod.List && !lensDetail.UseCP2ToCP1List)
                    {
                        List<(int, double)> cp2r = CalculateSpecialCalculationMethods(lensDetail, ear, lfd.CP2Result, Enumerations.ControlPointEnum.Cp2);
                        lfd.Cp2Ids = cp2r.Select(x => x.Item1).ToList();
                        lfd.CP2Result = cp2r.Select(x => x.Item2).ToList();
                    }
                }

            }

            //rounding of values
            lfd = RoundResults(lfd, lensDetail);
            return lfd;
        }

        private List<(int id, double res)> CalculateCP2Result(LensDetail lensDetail, EyeAnalysisResults ear, double settling)
        {
            List<double> CP2Result = new List<double>();
            double apicCl = ear?.ApicalClearance ?? (double)lensDetail?.SuggestedApicalClearance; ;
            double stdLz = lensDetail.IncludeLzInLensName ? lensDetail.CP4.StandardVal1.Value : 0.0; //(LZ_name_incl == 1)
            stdLz += apicCl + settling;//if settling is null it's already added by settling2d to the cp2vals

            switch (lensDetail.CP2.Category)
            {
                case (int)ControlPointCategories.quad:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP2Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.CP2Val[1] * 1000));
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName3Correction ?? 0) + ear.CP2Val[2] * 1000));
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName4Correction ?? 0) + ear.CP2Val[3] * 1000));
                    }
                    else
                    {
                        CP2Result.Add(Math.Round(stdLz + ear.CP2Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP2Result.Add(Math.Round(stdLz + ear.CP2Val[1] * 1000));
                        CP2Result.Add(Math.Round(stdLz + ear.CP2Val[2] * 1000));
                        CP2Result.Add(Math.Round(stdLz + ear.CP2Val[3] * 1000));
                    }

                    break;
                case (int)ControlPointCategories.toric:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.GetCP2Mean()[0] * 1000));
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.GetCP2Mean()[1] * 1000));
                    }
                    else
                    {
                        CP2Result.Add(Math.Round(stdLz + ear.GetCP2Mean()[0] * 1000));
                        CP2Result.Add(Math.Round(stdLz + ear.GetCP2Mean()[1] * 1000));
                    }
                    break;
                case (int)ControlPointCategories.sferic:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP2Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP2Val.Mean() * 1000));
                    }
                    else
                    {
                        CP2Result.Add(Math.Round(stdLz + ear.CP2Val.Mean() * 1000));
                    }
                    break;
            }

            List<(int id, double res)> CP2ResultOut = new List<(int id, double res)>();
            /*if (lensDetail.UseCP2ToCP1List && !UnlistedCP2)
            {
                for (int i = 0; i < CP2Result.Count; i++)
                {
                    IEnumerable<double> cp2Values = lensDetail.CP2ToCP1List.Select(x => x.CP2Value);
                    double closestValue = cp2Values.Aggregate((x, y) => Math.Abs(x - CP2Result[i]) < Math.Abs(y - CP2Result[i]) ? x : y);
                    CP2ResultOut.Add((0, closestValue));
                }
            }
            else
            {*/
            CP2ResultOut = CalculateSpecialCalculationMethods(lensDetail, ear, CP2Result, Enumerations.ControlPointEnum.Cp2);
            /*}*/



            return CP2ResultOut;
        }



        private List<(int id, double res)> CalculateCP4Result(LensDetail lensDetail, EyeAnalysisResults ear, double settling, double lzSettling, List<double> cp2Results)
        {
            List<double> cp4Result = new List<double>();

            //Takes fraction of SAG1 if needed
            if (lensDetail.CP4.CalculateLz)
            {
                lensDetail.CP4.StandardVal1 = ear.CP2Val[0] * 1000 * lensDetail.CP4.LzPercentage;
                lensDetail.CP4.StandardVal2 = lensDetail.CP4.StandardVal1;
                lensDetail.CP4.StandardVal3 = lensDetail.CP4.StandardVal1;
                lensDetail.CP4.StandardVal4 = lensDetail.CP4.StandardVal1;
            }
            switch (lensDetail.CP4.Category)
            {
                case (int)ControlPointCategories.quad:
                    {
                        //we can remove LZ_settling and settling because they are the same, but i'm leaving them here to illustrate what is happening
                        double land1 = ear.CP4Val[0] * 1000 + lzSettling - ear.CP3Val[0] * 1000 - settling;
                        double land2 = ear.CP4Val[1] * 1000 + lzSettling - ear.CP3Val[1] * 1000 - settling - lensDetail.CP4.Lift;
                        double land3 = ear.CP4Val[2] * 1000 + lzSettling - ear.CP3Val[2] * 1000 - settling;
                        double land4 = ear.CP4Val[3] * 1000 + lzSettling - ear.CP3Val[3] * 1000 - settling - lensDetail.CP4.Lift;

                        if (land2 < land1) //make sure that edge lift doesn't make the SAG lower than meridian 1
                        {
                            land2 = Math.Min(land1, land2 + lensDetail.CP4.Lift);
                        }
                        if (land4 < land1) //make sure that edge lift doesn't make the SAG lower than meridian 1
                        {
                            land4 = Math.Min(land1, land4 + lensDetail.CP4.Lift);
                        }

                        cp4Result.Add(lensDetail.CP4.StandardVal1.Value - land1);
                        cp4Result.Add(lensDetail.CP4.StandardVal2.Value - land2);
                        cp4Result.Add(lensDetail.CP4.StandardVal3.Value - land3);
                        cp4Result.Add(lensDetail.CP4.StandardVal4.Value - land4);

                        break;
                    }
                case (int)ControlPointCategories.toric:
                    {
                        //we can remove LZ_settling and settling because they are the same, but i'm leaving them here to illustrate what is happening
                        double land1 = ear.GetCP4Mean()[0] * 1000 + lzSettling - ear.GetCP3Mean()[0] * 1000 - settling;
                        double land2 = ear.GetCP4Mean()[1] * 1000 + lzSettling - ear.GetCP3Mean()[1] * 1000 - settling - lensDetail.CP4.Lift;
                        if (land2 < land1) //make sure that edge lift doesn't make the SAG lower than meridian 1
                        {
                            land2 = Math.Min(land1, land2 + lensDetail.CP4.Lift);
                        }
                        cp4Result.Add(lensDetail.CP4.StandardVal1.Value - land1);
                        cp4Result.Add(lensDetail.CP4.StandardVal2.Value - land2);
                        break;
                    }
                case (int)ControlPointCategories.sferic:
                    {
                        //we can remove LZ_settling and settling because they are the same, but i'm leaving them here to illustrate what is happening
                        double land1 = ear.CP4Val.Mean() * 1000 + lzSettling - ear.CP3Val.Mean() * 1000 - settling;

                        cp4Result.Add(lensDetail.CP4.StandardVal1.Value - land1);
                        break;
                    }
            }
            return CalculateSpecialCalculationMethods(lensDetail, ear, cp4Result, Enumerations.ControlPointEnum.Cp4);
        }

        private List<(int id, double res)> CalculateCP1Result(LensDetail lensDetail, EyeAnalysisResults ear, LensFittingResults lfr)
        {
            List<double> cp2Vals = lfr.CP2Result;
            List<double> cp1Result = new List<double>();
            double apicCl = ear?.ApicalClearance ?? (double)lensDetail?.SuggestedApicalClearance;
            double midCl = ear?.MidPeripheralClearance ?? (double)lensDetail?.SuggestedMidPeriphClearance;


            if (lensDetail.CP1.SemiChordLength != null)
            {
                double cp1 = (double)lensDetail.CP1.SemiChordLength * 2;
                double? bcMin = lensDetail.CP1.MinValue;
                double? bcMax = lensDetail.CP1.MaxValue;
                double? bcStep = lensDetail.CP1.StepSize;

                double cp1Mean1 = (ear.CP1Val[0] + ear.CP1Val[2]) / 2;
                cp1Mean1 += ear.CP1Corr / 1000;
                switch (lensDetail.CP1.MeasurementUnit)
                {
                    case MeasurementUnit.BaseCurve:
                        {
                            cp1Result.Add((Math.Pow(cp1, 2) + 4 * Math.Pow(cp1Mean1 - ((apicCl - midCl) / 1000), 2)) / (8 * cp1Mean1));

                            //this part will be used only for Zenlens BX and RGP lenses
                            if (lensDetail.CP1.Category == (int)ControlPointCategories.toric)
                            {
                                double cp1Lift = lensDetail.CP1.Lift / 1000;
                                double cp1Mean2 = (ear.CP1Val[1] + ear.CP1Val[3]) / 2;
                                cp1Mean2 += (ear.CP1Corr / 1000) - cp1Lift;  //CP lift reduces SAG
                                if (cp1Mean2 < cp1Mean1) //make sure that edge lift does't make the SAG lower than meridian 1
                                {
                                    cp1Mean2 = Math.Min(cp1Mean1, cp1Mean2 + cp1Lift);
                                }
                                cp1Result.Add((Math.Pow(cp1, 2) + 4 * Math.Pow(cp1Mean2 - ((apicCl - midCl) / 1000), 2)) / (8 * cp1Mean2));
                            }

                            break;
                        }
                    case MeasurementUnit.SAG:
                        {
                            lensDetail.CP1.StandardVal1 = ear.CP2Val.Mean() * 1000 * (double)lensDetail.CP1.CP1SagCP2Percentage;
                            lensDetail.CP1.StandardVal2 = lensDetail.CP1.StandardVal1;
                            lensDetail.CP1.StandardVal3 = lensDetail.CP1.StandardVal1;
                            lensDetail.CP1.StandardVal4 = lensDetail.CP1.StandardVal1;
                            double cp1Correction = ((ear.CP2Val.Mean() * 1000) + lensDetail.CP1.CP1SagCP2Correction ?? 0) * (lensDetail.CP1.CP1SagCP2Percentage ?? 1) - (cp1Mean1 * 1000);
                            double offset = cp1Correction % lensDetail.CP1.CP1SagCP2StepSize ?? cp1Correction;
                            cp1Correction = Math.Floor((cp1Correction) / (lensDetail.CP1.CP1SagCP2StepSize ?? 1)) * (lensDetail.CP1.CP1SagCP2StepSize ?? 1) + offset;
                            cp1Correction = Math.Floor(cp1Correction);
                            if (lensDetail.CP1.CP1SagCP2MinValue != null && cp1Correction < lensDetail.CP1.CP1SagCP2MinValue)
                            {
                                cp1Correction = lensDetail.CP1.CP1SagCP2MinValue.Value;
                            }
                            else if (lensDetail.CP1.CP1SagCP2MaxValue != null && cp1Correction > lensDetail.CP1.CP1SagCP2MaxValue)
                            {
                                cp1Correction = lensDetail.CP1.CP1SagCP2MaxValue.Value;
                            }
                            switch (lensDetail.CP1.Category)
                            {
                                case (int)ControlPointCategories.quad:
                                    {
                                        cp1Result.Add(lensDetail.CP1.StandardVal1.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        cp1Result.Add(lensDetail.CP1.StandardVal2.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        cp1Result.Add(lensDetail.CP1.StandardVal3.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        cp1Result.Add(lensDetail.CP1.StandardVal4.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        break;
                                    }
                                case (int)ControlPointCategories.toric:
                                    {
                                        cp1Result.Add(lensDetail.CP1.StandardVal1.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        cp1Result.Add(lensDetail.CP1.StandardVal2.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        break;
                                    }
                                case (int)ControlPointCategories.sferic:
                                    {
                                        cp1Result.Add(lensDetail.CP1.StandardVal1.Value - cp1Correction);
                                        lfr.Cp1Diffs.Add(cp1Correction);
                                        break;
                                    }
                            }
                            if (lensDetail.CP1.AddCalculatedCP1SagCP2PercentageToOtherCPs ?? false)
                            {
                                for (int i = 0; i < lfr.CP2FinalValueToAdd.Count(); i++)
                                {
                                    lfr.CP2FinalValueToAdd[i] += cp1Correction;
                                }
                                for (int i = 0; i < lfr.CP4FinalValueToAdd.Count(); i++)
                                {
                                    lfr.CP4FinalValueToAdd[i] += cp1Correction;
                                }
                            }

                            if (lensDetail.CP1.RelativeSag ?? false)
                            {
                                if (lensDetail.CP1.RelativeSag ?? false)
                                {
                                    cp1Result[0] -= lensDetail.CP1.StandardVal1.Value;
                                }
                            }

                            break;
                        }
                    case MeasurementUnit.Munnerlyn:
                        {
                            throw new NotSupportedException("Munnerlyn currently not supported for scleral");
                        }
                    case MeasurementUnit.Jessen:
                        {
                            throw new NotSupportedException("Jessen currently not supported for scleral");
                        }
                }
            }
            else if (!UnlistedCP2)
            {
                //match sag with cp2sag and get BC
                CP2ToCP1Item closestItem = lensDetail.CP2ToCP1List.OrderBy(i => Math.Abs(i.CP2Value - cp2Vals[0])).First();
                cp1Result.Add(closestItem.CP1Value);
            }
            else
            {
                IEnumerable<double> cp2Values = lensDetail.CP2ToCP1List.Select(x => x.CP2Value);
                double closestValue = cp2Values.Aggregate((x, y) => Math.Abs(x - ear.CP2Val[0] * 1000) < Math.Abs(y - ear.CP2Val[0] * 1000) ? x : y);

                // match sag with cp2sag and get BC
                foreach (CP2ToCP1Item si in lensDetail.CP2ToCP1List)
                {
                    if (closestValue.Equals(si.CP2Value))
                    {
                        cp1Result.Add(si.CP1Value);
                        break;
                    }
                }
            }
            return CalculateSpecialCalculationMethods(lensDetail, ear, cp1Result, Enumerations.ControlPointEnum.Cp1);
        }
        private List<(int id, double res)> CalculateCP3Result(LensDetail lensDetail, EyeAnalysisResults ear, double apicCl, double settling)
        {
            List<double> CP3Result = new List<double>();

            double stdLz = lensDetail.IncludeLzInLensName ? lensDetail.CP4.StandardVal1.Value : 0.0; //(LZ_name_incl == 1)
            stdLz += apicCl + settling;//if settling is null it's already added by settling2d to the cp3vals

            switch (lensDetail.CP3.Category)
            {
                case (int)ControlPointCategories.quad:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP3Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.CP3Val[1] * 1000));
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName3Correction ?? 0) + ear.CP3Val[2] * 1000));
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName4Correction ?? 0) + ear.CP3Val[3] * 1000));
                    }
                    else
                    {
                        CP3Result.Add(Math.Round(stdLz + ear.CP3Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP3Result.Add(Math.Round(stdLz + ear.CP3Val[1] * 1000));
                        CP3Result.Add(Math.Round(stdLz + ear.CP3Val[2] * 1000));
                        CP3Result.Add(Math.Round(stdLz + ear.CP3Val[3] * 1000));
                    }

                    break;
                case (int)ControlPointCategories.toric:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.GetCP3Mean()[0] * 1000));
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.GetCP3Mean()[1] * 1000));
                    }
                    else
                    {
                        CP3Result.Add(Math.Round(stdLz + ear.GetCP3Mean()[0] * 1000));
                        CP3Result.Add(Math.Round(stdLz + ear.GetCP3Mean()[1] * 1000));
                    }
                    break;
                case (int)ControlPointCategories.sferic:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP3Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP3Val.Mean() * 1000));
                    }
                    else
                    {
                        CP3Result.Add(Math.Round(stdLz + ear.CP3Val.Mean() * 1000));
                    }
                    break;
            }

            List<(int id, double res)> CP3ResultOut = new List<(int id, double res)>();
            /*if (lensDetail.UseCP3ToCP1List && !UnlistedCP3)
            {
                for (int i = 0; i < CP3Result.Count; i++)
                {
                    IEnumerable<double> cp3Values = lensDetail.CP3ToCP1List.Select(x => x.CP3Value);
                    double closestValue = cp3Values.Aggregate((x, y) => Math.Abs(x - CP3Result[i]) < Math.Abs(y - CP3Result[i]) ? x : y);
                    CP3ResultOut.Add((0, closestValue));
                }
            }
            else
            {*/
            CP3ResultOut = CalculateSpecialCalculationMethods(lensDetail, ear, CP3Result, Enumerations.ControlPointEnum.Cp3);
            /*}*/



            return CP3ResultOut;
        }
        private List<(int id, double res)> CalculateCP5Result(LensDetail lensDetail, EyeAnalysisResults ear, double settling)
        {
            List<double> CP5Result = new List<double>();

            double apicCl = ear?.ApicalClearance ?? (double)lensDetail?.SuggestedApicalClearance;
            double stdLz = lensDetail.IncludeLzInLensName ? lensDetail.CP4.StandardVal1.Value : 0.0; //(LZ_name_incl == 1)
            stdLz += apicCl + settling;//if settling is null it's already added by settling2d to the cp2vals

            switch (lensDetail.CP5.Category)
            {
                case (int)ControlPointCategories.quad:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP5Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.CP5Val[1] * 1000));
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName3Correction ?? 0) + ear.CP5Val[2] * 1000));
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName4Correction ?? 0) + ear.CP5Val[3] * 1000));
                    }
                    else
                    {
                        CP5Result.Add(Math.Round(stdLz + ear.CP5Val[0] * 1000)); //most lens designs give the SAG values in microns
                        CP5Result.Add(Math.Round(stdLz + ear.CP5Val[1] * 1000));
                        CP5Result.Add(Math.Round(stdLz + ear.CP5Val[2] * 1000));
                        CP5Result.Add(Math.Round(stdLz + ear.CP5Val[3] * 1000));
                    }

                    break;
                case (int)ControlPointCategories.toric:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.GetCP5Mean()[0] * 1000));
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName2Correction ?? 0) + ear.GetCP5Mean()[1] * 1000));
                    }
                    else
                    {
                        CP5Result.Add(Math.Round(stdLz + ear.GetCP5Mean()[0] * 1000));
                        CP5Result.Add(Math.Round(stdLz + ear.GetCP5Mean()[1] * 1000));
                    }
                    break;
                case (int)ControlPointCategories.sferic:
                    if (lensDetail.IncludeLzInLensName)
                    {
                        CP5Result.Add(Math.Round(stdLz + (lensDetail.LzInLensName1Correction ?? 0) + ear.CP5Val.Mean() * 1000));
                    }
                    else
                    {
                        CP5Result.Add(Math.Round(stdLz + ear.CP5Val.Mean() * 1000));
                    }
                    break;
            }

            List<(int id, double res)> CP5ResultOut = new List<(int id, double res)>();
            /*if (lensDetail.UseCP2ToCP1List && !UnlistedCP2)
            {
                for (int i = 0; i < CP2Result.Count; i++)
                {
                    IEnumerable<double> cp2Values = lensDetail.CP2ToCP1List.Select(x => x.CP2Value);
                    double closestValue = cp2Values.Aggregate((x, y) => Math.Abs(x - CP2Result[i]) < Math.Abs(y - CP2Result[i]) ? x : y);
                    CP2ResultOut.Add((0, closestValue));
                }
            }
            else
            {*/
            CP5ResultOut = CalculateSpecialCalculationMethods(lensDetail, ear, CP5Result, Enumerations.ControlPointEnum.Cp5);
            /*}*/



            return CP5ResultOut;
        }

        private LensFittingResults RoundResults(LensFittingResults lfd, LensDetail lensDetail)
        {
            //round CP2
            //------------------------------------------------------------------------
            if (lensDetail.UseCP2 && lensDetail.CP2.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                //round values to the nearest SAGstep(most design use 50 microns steps)
                int sagStep = (int)lensDetail.CP2.StepSize;
                double offset = (double)lensDetail.CP2.MaxValue % sagStep;
                for (int i = 0; i < lfd.CP2Result.Count; i++)
                {
                    if (lensDetail.CP2.RoundingStrategy == CpRoundingStrategy.Nearest)
                    {
                        lfd.CP2Result[i] = Math.Round((lfd.CP2Result[i] - offset) / sagStep) * sagStep + offset;
                    }
                    else if (lensDetail.CP2.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                    {
                        lfd.CP2Result[i] = Math.Ceiling((lfd.CP2Result[i] - offset) / sagStep) * sagStep + offset;
                    }
                }

                // min/max limits checking
                if (!Unbound)
                {
                    for (int i = 0; i < lfd.CP2Result.Count; i++)
                    {
                        lfd.CP2Result[i] = Math.Min(lfd.CP2Result[i], lensDetail.CP2.MaxValue ?? lfd.CP2Result[i]);
                        lfd.CP2Result[i] = Math.Max(lfd.CP2Result[i], lensDetail.CP2.MinValue ?? lfd.CP2Result[i]);
                    }
                }

                //some designs give the SAG relative to the standard SAG
                if (lensDetail.CP2.RelativeSag)
                {
                    double[] cp2StandardVals = {
                        lensDetail.CP2.StandardVal1.Value,
                        lensDetail.CP2.StandardVal2.Value,
                        lensDetail.CP2.StandardVal3.Value,
                        lensDetail.CP2.StandardVal4.Value
                    };
                    for (int i = 0; i < lfd.CP2Result.Count; i++)
                    {
                        lfd.CP2Result[i] = lfd.CP2Result[i] - cp2StandardVals[i];
                    }
                }

            }

            //round CP4
            //------------------------------------------------------------------------
            if (lensDetail.UseCP4 && lensDetail.CP4.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                for (int i = 0; i < lfd.CP4Result.Count; i++)
                {
                    if (lensDetail.CP4.StepSize != null)
                    {
                        if (lensDetail.CP4.RoundingStrategy == CpRoundingStrategy.Nearest)
                        {
                            lfd.CP4Result[i] = Math.Round(lfd.CP4Result[i] / (double)lensDetail.CP4.StepSize) * (double)lensDetail.CP4.StepSize;
                        }
                        else if (lensDetail.CP4.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                        {
                            lfd.CP4Result[i] = Math.Ceiling(lfd.CP4Result[i] / (double)lensDetail.CP4.StepSize) * (double)lensDetail.CP4.StepSize;
                        }

                    }
                    //min/max check
                    if (!Unbound)
                    {
                        lfd.CP4Result[i] = Math.Min(lfd.CP4Result[i], lensDetail.CP4.MaxValue ?? lfd.CP4Result[i]);
                        lfd.CP4Result[i] = Math.Max(lfd.CP4Result[i], lensDetail.CP4.MinValue ?? lfd.CP4Result[i]);
                    }
                }
            }

            //round CP1
            //------------------------------------------------------------------------
            if (lensDetail.UseCP1 && lensDetail.CP1.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                for (int i = 0; i < lfd.CP1Result.Count; i++)
                {
                    if (!Unbound)
                    {
                        lfd.CP1Result[i] = Math.Min(lfd.CP1Result[i], lensDetail.CP1.MaxValue ?? lfd.CP1Result[i]);
                        lfd.CP1Result[i] = Math.Max(lfd.CP1Result[i], lensDetail.CP1.MinValue ?? lfd.CP1Result[i]);
                        if (lensDetail.CP1.CP1SagCP2Percentage != null)
                        {
                            lfd.Cp1Diffs[i] = Math.Min(lfd.Cp1Diffs[i], lensDetail.CP1.CP1SagCP2MaxValue ?? lfd.Cp1Diffs[i]);
                            lfd.Cp1Diffs[i] = Math.Max(lfd.Cp1Diffs[i], lensDetail.CP1.CP1SagCP2MinValue ?? lfd.Cp1Diffs[i]);
                        }
                    }
                    if (lensDetail.CP1.StepSize != null)
                    {
                        if (lensDetail.CP1.RoundingStrategy == CpRoundingStrategy.Nearest)
                        {
                            lfd.CP1Result[i] = Math.Round(lfd.CP1Result[i] / (double)lensDetail.CP1.StepSize) * (double)lensDetail.CP1.StepSize;
                        }
                        else if (lensDetail.CP1.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                        {
                            lfd.CP1Result[i] = Math.Ceiling(lfd.CP1Result[i] / (double)lensDetail.CP1.StepSize) * (double)lensDetail.CP1.StepSize;
                        }
                        if (lensDetail.CP1.CP1SagCP2Percentage != null)
                        {
                            if (lensDetail.CP1.RoundingStrategy == CpRoundingStrategy.Nearest)
                            {
                                lfd.Cp1Diffs[i] = Math.Round(lfd.Cp1Diffs[i] / (double)lensDetail.CP1.CP1SagCP2StepSize) * (double)lensDetail.CP1.CP1SagCP2StepSize + lensDetail.CP1.CP1SagCP2MinValue ?? 0;
                            }
                            else if (lensDetail.CP1.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                            {
                                lfd.Cp1Diffs[i] = Math.Ceiling(lfd.Cp1Diffs[i] / (double)lensDetail.CP1.CP1SagCP2StepSize) * (double)lensDetail.CP1.CP1SagCP2StepSize + lensDetail.CP1.CP1SagCP2MinValue ?? 0;
                            }

                        }
                    }
                }
            }


            //round CP3
            //------------------------------------------------------------------------
            if (lensDetail.UseCP3 && lensDetail.CP3.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                for (int i = 0; i < lfd.CP3Result.Count; i++)
                {
                    if (lensDetail.CP3.StepSize != null)
                    {
                        if (lensDetail.CP3.RoundingStrategy == CpRoundingStrategy.Nearest)
                        {
                            lfd.CP3Result[i] = Math.Round(lfd.CP3Result[i] / (double)lensDetail.CP3.StepSize) * (double)lensDetail.CP3.StepSize;
                        }
                        else if (lensDetail.CP3.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                        {
                            lfd.CP3Result[i] = Math.Ceiling(lfd.CP3Result[i] / (double)lensDetail.CP3.StepSize) * (double)lensDetail.CP3.StepSize;
                        }

                    }
                    //min/max check
                    if (!Unbound)
                    {
                        lfd.CP3Result[i] = Math.Min(lfd.CP3Result[i], lensDetail.CP3.MaxValue ?? lfd.CP3Result[i]);
                        lfd.CP3Result[i] = Math.Max(lfd.CP3Result[i], lensDetail.CP3.MinValue ?? lfd.CP3Result[i]);
                    }
                }
            }

            //round CP5
            //------------------------------------------------------------------------
            if (lensDetail.UseCP5 && lensDetail.CP5.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                for (int i = 0; i < lfd.CP5Result.Count; i++)
                {
                    if (lensDetail.CP5.StepSize != null)
                    {
                        if (lensDetail.CP5.RoundingStrategy == CpRoundingStrategy.Nearest)
                        {
                            lfd.CP5Result[i] = Math.Round(lfd.CP5Result[i] / (double)lensDetail.CP5.StepSize) * (double)lensDetail.CP5.StepSize;
                        }
                        else if (lensDetail.CP5.RoundingStrategy == CpRoundingStrategy.NearestLarger)
                        {
                            lfd.CP5Result[i] = Math.Ceiling(lfd.CP5Result[i] / (double)lensDetail.CP5.StepSize) * (double)lensDetail.CP5.StepSize;
                        }

                    }
                    //min/max check
                    if (!Unbound)
                    {
                        lfd.CP5Result[i] = Math.Min(lfd.CP5Result[i], lensDetail.CP5.MaxValue ?? lfd.CP5Result[i]);
                        lfd.CP5Result[i] = Math.Max(lfd.CP5Result[i], lensDetail.CP5.MinValue ?? lfd.CP5Result[i]);
                    }
                }
            }

            return lfd;
        }

        private List<(int id, double res)> CalculateSpecialCalculationMethods(LensDetail lensdetail, EyeAnalysisResults ear, List<double> cPResult, Enumerations.ControlPointEnum cp)
        {
            ControlPoint controlPoint = null;
            double[] rawVals = new double[0];
            double[] rawValsInnerCP = new double[0];
            switch (cp)
            {
                case Enumerations.ControlPointEnum.Cp1:
                    controlPoint = lensdetail.CP1;
                    rawVals = ear.GetCP1Mean();
                    break;
                case Enumerations.ControlPointEnum.Cp2:
                    controlPoint = lensdetail.CP2;
                    rawVals = ear.GetCP2Mean();
                    break;
                case Enumerations.ControlPointEnum.Cp3:
                    controlPoint = lensdetail.CP3;
                    rawVals = ear.GetCP3Mean();
                    break;
                case Enumerations.ControlPointEnum.Cp4:
                    controlPoint = lensdetail.CP4;
                    rawVals = ear.GetCP4Mean();
                    rawValsInnerCP = ear.GetCP2Mean();
                    break;
                case Enumerations.ControlPointEnum.Cp5:
                    controlPoint = lensdetail.CP5;
                    rawVals = ear.GetCP5Mean();
                    break;
            }

            List<(int id, double res)> calculatedResult = new List<(int id, double res)>();
            ICalculationStrategy strategy;
            switch (controlPoint.CalculationMethod)
            {
                case Enumerations.CpCalculationMethod.List:
                    strategy = new ListStrategy();
                    break;
                case Enumerations.CpCalculationMethod.TangentList:
                    strategy = new TangentStrategy();
                    break;
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    strategy = new CurvatureDesignStrategy();
                    break;
                case Enumerations.CpCalculationMethod.Formula:
                    strategy = new FormulaStrategy();
                    break;
                case Enumerations.CpCalculationMethod.AlignmentCurve:
                    if (cp != ControlPointEnum.Cp4 && cp != ControlPointEnum.Cp5)
                        throw new NotImplementedException("No strategy defined for this calculationMethod");
                    strategy = new AlignmentCurveStrategy(ear);
                    break;
                case Enumerations.CpCalculationMethod.Curvature:
                    if (cp != ControlPointEnum.Cp2 && cp != ControlPointEnum.Cp3)
                        throw new NotImplementedException("No strategy defined for this calculationMethod");
                    strategy = new CurvatureStrategy(ear);
                    break;
                default:
                    throw new NotImplementedException("No strategy defined for this calculationMethod");
                    break;
            }
            for (int i = 0; i < cPResult.Count(); i++)
            {
                if (controlPoint.CalculationMethod == Enumerations.CpCalculationMethod.TangentList || controlPoint.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
                {
                    if (cp == Enumerations.ControlPointEnum.Cp4)
                    {
                        cPResult[i] = rawVals[i] - rawValsInnerCP[i];
                    }
                }

                (int id, double res) = strategy.CalculateValue(lensdetail, cPResult[i], cp, i);
                calculatedResult.Add((id, res));
            }
            return calculatedResult;
        }
    }
}
