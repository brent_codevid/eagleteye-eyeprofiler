﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.BL.Implementations.LensFitting
{
    public class SimK
    {
        public double Flat { get; set; }
        public double Steep { get; set; }
        public double Angle { get; set; }
    }
}
