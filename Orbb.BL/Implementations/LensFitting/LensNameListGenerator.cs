﻿using Orbb.BL.Implementations.LensFitting;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.BL.LensFitting
{
    public class LensNameListGenerator
    {
        public string GenerateFullSet(LensDetail lensDetail, LensFitter fitter)
        {
            List<string> unfilteredRes = new List<string>();
            StringBuilder result = new StringBuilder();
            result.Append(lensDetail.GeneralLensName());
            result.Append("\r\n");
            EyeAnalysisResults analysisResults = new EyeAnalysisResults()
            {
                Angle = 0,
                CP1Val = new double[4],
                CP2Val = new double[4],
                CP4Val = new double[4]
            };
            for (int cp1val = 5; cp1val <= 20; cp1val++)
            {
                for (int cp2val = 20; cp2val <= 35; cp2val++)
                {
                    for (int cp4val = 35; cp4val <= 60; cp4val++)
                    {
                        for(int i = 0; i < 4; i++)
                        {
                            analysisResults.CP1Val[i] = cp1val / 10;
                            analysisResults.CP2Val[i] = cp2val / 10;
                            analysisResults.CP4Val[i] = cp4val / 10;
                        }
                        
                        try
                        {
                            LensFittingResults fittingResult = fitter.CalculateFitting(analysisResults, lensDetail, new EyeData() {Unbound = false, UnlistedCP2 = false });
                            string name = lensDetail.GenerateFittedLensName(fittingResult, false);
                            unfilteredRes.Add(name);
                        }
                        catch (Exception x)
                        {
                            //DO NOTHING
                        }
                    }
                }
            }
            List<string> filteredRes = unfilteredRes.Distinct().ToList();
            foreach (string res in filteredRes)
            {
                result.Append(res);
            }
            return result.ToString();
        }
    }
}

