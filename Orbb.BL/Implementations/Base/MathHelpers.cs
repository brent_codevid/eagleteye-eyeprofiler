﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Orbb.BL.Implementations.Base
{
    public static class MathHelpers
    {
        public static CarthesianCoordinates PolToCart(double radianAngle, double rho)
        {
            CarthesianCoordinates output = new CarthesianCoordinates
            {
                XCoord = rho * Math.Cos(radianAngle),
                YCoord = rho * Math.Sin(radianAngle)
            };
            return output;
        }

        public static CarthesianCoordinates PolToCartWithDegrees(double degreeAngle, double rho)
        {
            return PolToCart(degreeAngle / 180 * Math.PI, rho);
        }

        public static PolarCoordinates CartToPol(double xCoord, double yCoord)
        {
            PolarCoordinates output = new PolarCoordinates
            {
                RadianAngle = (Math.Atan2(yCoord, xCoord) + 2 * Math.PI) % (Math.PI * 2),
                Rho = Math.Sqrt(xCoord * xCoord + yCoord * yCoord)
            };
            return output;
        }

        //Functions found here http://phrogz.net/angle-between-three-points
        //angle formed by p1 -> p2 -> p3
        public static double AngleBetween(CarthesianCoordinates p1, CarthesianCoordinates p2, CarthesianCoordinates p3)
        {
            double a = Math.Pow(p2.XCoord - p1.XCoord, 2) + Math.Pow(p2.YCoord - p1.YCoord, 2);
            double b = Math.Pow(p2.XCoord - p3.XCoord, 2) + Math.Pow(p2.YCoord - p3.YCoord, 2);
            double c = Math.Pow(p3.XCoord - p1.XCoord, 2) + Math.Pow(p3.YCoord - p1.YCoord, 2);

            //calculate angle in radians
            return Math.Acos((a + b - c) / Math.Sqrt(4 * a * b));
        }

        //Determines whether p and reference lay on opposite side of line ab (https://math.stackexchange.com/questions/162728/how-to-determine-if-2-points-are-on-opposite-sides-of-a-line)
        public static bool IsOtherSide(CarthesianCoordinates a, CarthesianCoordinates b, CarthesianCoordinates p, CarthesianCoordinates reference)
        {
            double ax = a.XCoord;
            double ay = a.YCoord;
            double bx = b.XCoord;
            double by = b.YCoord;
            return ((ay - by) * (p.XCoord - ax) + (bx - ax) * (p.YCoord - ay))
                * ((ay - by) * (reference.XCoord - ax) + (bx - ax) * (reference.YCoord - ay)) < 0;
        }

        //Calculates smoothed data by using a simple moving average
        //span has to be 3 or larger for smoothing to occur
        //even span values wil be lowered by one to make uneven
        public static double[] SimpleMovingAverageSmoothing(List<double> data, int span)
        {
            if (data == null || data.Count == 0 || span <= 0)
            {
                return new double[0];
            }
            double[] smoothed = new double[data.Count];
            if (span % 2 == 0)
            {
                span -= 1;
            }
            int windowSize = 1;
            double windowSum = data[0];
            smoothed[0] = windowSum;

            for (int i = 1; i < data.Count; i++)
            {
                if (i + windowSize / 2 >= data.Count)
                {
                    windowSum -= data[i - windowSize / 2 - 1];
                    windowSum -= data[i - windowSize / 2];
                    windowSize -= 2;
                }
                else if (windowSize < span && i + windowSize / 2 + 1 < data.Count)
                {
                    windowSum += data[i + windowSize / 2];
                    windowSum += data[i + windowSize / 2 + 1];
                    windowSize += 2;
                }
                else
                {
                    windowSum -= data[i - windowSize / 2 - 1];
                    windowSum += data[i + windowSize / 2];
                }
                smoothed[i] = windowSum / windowSize;
            }

            return smoothed;
        }
    }

    public struct CarthesianCoordinates
    {
        [JsonProperty(PropertyName = "x")]
        public double XCoord { get; set; }
        [JsonProperty(PropertyName = "y")]
        public double YCoord { get; set; }
    }

    public struct PolarCoordinates
    {
        public double RadianAngle { get; set; }
        public double Rho { get; set; }
        public double DegreeAngle => RadianAngle * 180 / Math.PI;
    }
}
