﻿using Orbb.BL.Interfaces.Base;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Media;
using Orbb.Common.Utility;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.Other;
using Orbb.Data.ViewModels.Models.System;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;

namespace Orbb.BL.Implementations.Base
{
    public class DivisionRepository : Repository<Division>, IDivisionRepository
    {
        private const string IMAGESUBPATH = "header";
        private IDataService dataService;
        private readonly IMediaService imageService;

        public DivisionRepository(IDatabaseContext dbContext,
            IDataService dataService,
            IMediaService imageService) : base(dbContext)
        {
            this.dataService = dataService;
            this.imageService = imageService;
        }

        public IList<DivisionVM> GetDivisions(int userId)
        {
            var result = new List<DivisionVM>();

            var user = DbContext.Query<User>().Include(u => u.Divisions).SingleOrDefault(u => u.Id == userId);
            if (user?.Divisions == null || user.Divisions.Count == 0)
                return result;

            foreach (var d in user.Divisions)
            {
                result.Add(Mapper.Map<DivisionVM>(d));
                //result.Add(AutoMapper.Mapper.Map<DivisionVM>(d));
            }

            return result;
        }

        public IList<DivisionVM> GetDivisions()
        {
            var result = new List<DivisionVM>();

            var divisons = DbContext.Query<Division>().ToList();

            foreach (var d in divisons)
            {
                result.Add(Mapper.Map<DivisionVM>(d));
                //result.Add(AutoMapper.Mapper.Map<DivisionVM>(d));
            }

            return result;
        }

        public MediaFileResult UpdateDivsionImage(byte[] binary, string fileExtension)
        {
            var division = DbContext.Query<Division>().FirstOrDefault(d => d.Id == CurrentDivisionId);
            var imagePath = Path.Combine(IMAGESUBPATH, $"{division.Description}-{Guid.NewGuid()}{fileExtension}");
            division.ImagePath = imagePath;

            var url = MediaUtility.ConvertToUrl(division.ImagePath);

            var fileResult = new MediaFileResult
            {
                Name = imagePath,
                Url = url,
                ThumbnailUrl = url
            };

            try
            {
                // save file to filesystem
                imageService.SaveImage(imagePath, binary);
            }
            catch (Exception ex)
            {
                fileResult.Error = ex.Message;
                return fileResult;
            }

            DbContext.Update(division);

            var result = DbContext.SaveChanges(CurrentUser, CurrentDivision);

            if (!result.Success)
            {
                fileResult.Error = result.SaveException?.Message;
            }

            return fileResult;
        }
    }
}
