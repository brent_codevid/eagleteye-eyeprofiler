﻿using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Orders;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Orders;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Orders
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        private readonly IDataService dataService;

        public OrderRepository(IDatabaseContext dbContext, IDataService dataService) : base(dbContext)
        {
            this.dataService = dataService;
        }

        public IList<OrderView> GetOverview()
        {
            var languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<OrderView>("spWebViewOrders @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }

        public List<Order> GetOrdersByCustomerId(int id)
        {
            return GetAll(l => l.CustomerId == id, new[] { "Customer", "Supplier" });
        }
    }
}
