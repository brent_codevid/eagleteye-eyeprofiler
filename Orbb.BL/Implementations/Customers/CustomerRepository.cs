﻿using Microsoft.Extensions.Logging;
using Orbb.BL.Interfaces.Customers;
using Orbb.BL.Interfaces.General;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Customers;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.Customers
{
    public class CustomerRepository : Repository<Customer>, ICustomerRepository
    {
        private readonly IDataService dataService;
        private readonly ILogger<CustomerRepository> _logger;

        public CustomerRepository(IDatabaseContext dbContext, IDataService dataService, ILogger<CustomerRepository> logger) : base(dbContext)
        {
            this.dataService = dataService;
            _logger = logger;
        }

        public IList<CustomerView> GetOverview()
        {
            SqlParameter languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<CustomerView>("spWebViewCustomers @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }

        public void RemoveUnavailableLensesFromCustomers(int countryId, IList<LenssetVM> availableLenssets)
        {
            List<Customer> customersToEdit = GetAll(c => c.CountryId == countryId, new string[] { "UsedLenssets" });
            foreach (var customer in customersToEdit)
            {
                try
                {
                    customer.UsedLenssets.RemoveAll(l => !availableLenssets.Any(s => s.Id == l.Id));
                    Update(customer, CurrentUser);
                }
                catch (Exception e)
                {
                    _logger.LogError(e, "Failed to update lenses of customerId:" + customer.Id);
                    throw;
                }

            }
        }
    }
}