﻿using Orbb.BL.Interfaces.Datacapture;
using Orbb.BL.Interfaces.General;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Datacapture;
using System;

namespace Orbb.BL.Implementations.Datacapture
{
    public class LensfittingCaptureRepository : Repository<LensfittingCapture>, ILensfittingCaptureRepository
    {
        private IDataService dataService;

        public LensfittingCaptureRepository(IDatabaseContext dbContext,
            IDataService dataService) : base(dbContext)
        {
            this.dataService = dataService;
        }

        public void CaptureFitting(int lensId, string fittedName, string userName)
        {
            LensfittingCapture capture = new LensfittingCapture()
            {
                Id = 0,
                LensDetailId = lensId,
                FittedLensName = fittedName,
                CreatedBy = userName,
                CreatedOn = DateTime.Now
            };
            this.Add(capture, CurrentUser);
        }
    }


}
