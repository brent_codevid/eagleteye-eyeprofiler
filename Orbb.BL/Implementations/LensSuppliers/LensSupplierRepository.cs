﻿using Orbb.BL.Interfaces.LensSuppliers;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Orbb.BL.Implementations.LensSuppliers
{
    public class LensSupplierRepository : Repository<LensSupplier>, ILensSupplierRepository
    {
        public LensSupplierRepository(IDatabaseContext dbContext) : base(dbContext) { }

        public IList<LensSupplierView> GetOverview()
        {
            var languageIdParameter = new SqlParameter("@LanguageId", 1);
            var result = DbContext.QuerySql<LensSupplierView>("spWebViewLensSuppliers @LanguageId", languageIdParameter)
                .ToList();
            return result;
        }
    }
}