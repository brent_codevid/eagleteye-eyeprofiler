﻿using AutoMapper;
using Orbb.BL.Tools.AutoMapper;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.Models.Models.Orders;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.Base;
using Orbb.Data.ViewModels.Models.Customers;
using Orbb.Data.ViewModels.Models.Document;
using Orbb.Data.ViewModels.Models.Globalization;
using Orbb.Data.ViewModels.Models.Lenssets;
using Orbb.Data.ViewModels.Models.LensSuppliers;
using Orbb.Data.ViewModels.Models.Orders;
using Orbb.Data.ViewModels.Models.Security;
using Orbb.Data.ViewModels.Models.System;

namespace Orbb.BL.Mappings
{
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<Translation, string>().ConvertUsing<TranslationConverter>();
            CreateMap<User, UserVm>();
            CreateMap<UserGroup, UserGroupDetailVM>();
            CreateMap<SecuredObject, SecuredObjectVM>();
            CreateMap<SecuredObjectPermission, SecuredObjectPermissionVm>();
            CreateMap<User, UserDetailVm>();
            CreateMap<Division, DivisionVM>();
            CreateMap<Language, LanguageVM>();
            CreateMap<TranslationEntry, TranslationEntryVM>();
            CreateMap<Country, CountryVM>();
            CreateMap<Country, CountryListItem>();
            CreateMap<Translation, TranslationVM>();
            CreateMap<Document, DocumentVm>();
            CreateMap<LensSupplier, LensSupplierVM>();
            CreateMap<LensDetail, LensDetailVM>();
            CreateMap<LensDetail, LenssetVM>();
            CreateMap<ControlPoint, ControlPointVM>();
            CreateMap<ControlPoint1, ControlPoint1VM>();
            CreateMap<ControlPoint2, ControlPoint2VM>();
            CreateMap<ControlPoint3, ControlPoint3VM>();
            CreateMap<ControlPoint4, ControlPoint4VM>();
            CreateMap<ControlPoint5, ControlPoint5VM>();
            CreateMap<CP2ToCP1Item, CP2ToCP1ItemVM>();
            CreateMap<CPListItem, CPListItemVM>();
            CreateMap<CPTangentItem, CPTangentItemVM>();
            CreateMap<CPCurvatureItem, CPCurvatureItemVM>();
            CreateMap<CPCurvatureItemVMForKendo, CPCurvatureItemVM>();
            CreateMap<Customer, CustomerVM>();
            CreateMap<LensSupplier, string>().ConvertUsing(s => s?.Name ?? "");
            CreateMap<Order, OrderVM>(); 
            CreateMap<TangentList, TangentListVM>();
        }
    }
}
