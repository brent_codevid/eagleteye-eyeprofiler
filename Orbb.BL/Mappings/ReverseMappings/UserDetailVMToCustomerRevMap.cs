﻿using Orbb.BL.Interfaces.General;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.ViewModels.Models.Security;
using System.Data.Entity;
using System.Linq;

namespace Orbb.BL.Mappings.ReverseMappings
{
    public class UserDetailVMToCustomerRevMap : IReverseMapper<UserDetailVm, User>
    {
        IDatabaseContext dbContext;
        public UserDetailVMToCustomerRevMap(IDatabaseContext dbContext)
        {
            this.dbContext = dbContext;
        }
        public User ReverseMap(UserDetailVm source)
        {
            User data;
            if (source.Id > 0)
            {
                data = dbContext.Query<User>()
                    .Include(x => x.UserGroup)
                    .Single(x => x.Id == source.Id);
            }
            else
            {
                data = new User();
            }

            return data;
        }
    }
}