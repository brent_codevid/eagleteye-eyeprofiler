﻿using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Orbb.BL.Interfaces
{
    public interface IRepository<T> : IRepository where T : class
    {
        T GetSingle(Expression<Func<T, bool>> predicate, string[] includeStrings);
        List<T> GetAll(Expression<Func<T, bool>> predicate, string[] includeStrings);
        List<T> GetAll(string[] includeStrings);
        void Add(T entity, ICurrentUser currentUser);
        void Update(T entity, ICurrentUser currentUser);
    }

    public interface IRepository
    {
        Y GetSingle<Y>(Expression<Func<Y, bool>> predicate, string[] includeStrings = null) where Y : class, IEntity;
        V GetSingle<Y, V>(Expression<Func<Y, bool>> predicate, string[] includeStrings = null) where Y : class, IEntity where V : class, IViewModel;
        List<Y> GetAll<Y>(Expression<Func<Y, bool>> predicate, string[] includeStrings) where Y : class, IEntity;
        List<V> GetAll<Y, V>(Expression<Func<Y, bool>> predicate, string[] includeStrings) where Y : class, IEntity where V : class, IViewModel;
        List<Y> GetAll<Y>(string[] includeStrings) where Y : class, IEntity;
        List<V> GetAll<Y, V>(string[] includeStrings) where Y : class, IEntity where V : class, IViewModel;
        void Add<Y>(Y entity, ICurrentUser currentUser) where Y : class, IEntity;
        void Update<Y>(Y entity, ICurrentUser currentUser) where Y : class, IEntity;
    }
}