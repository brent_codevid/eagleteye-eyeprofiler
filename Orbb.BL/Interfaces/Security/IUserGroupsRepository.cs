﻿using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Security;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Security
{
    public interface IUserGroupsRepository : IRepository<UserGroup>
    {
        IList<UserGroupView> GetUserGroupsOverview();
        IList<UserGroupDetailVM> GetUserGroupsAndPermissions();

        void CreateUserGroupObjectPermissions(int userGroupId);
    }
}
