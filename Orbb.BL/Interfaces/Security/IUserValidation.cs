﻿using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;

namespace Orbb.BL.Interfaces.Security
{
    public interface IUserValidation
    {
        IDatabaseContext DbContext { get; set; }
        bool ValidateUser(string userName, string password);
        bool ValidateUser(string userName, string password, out User user);
        bool UserHasAccess(int userId, string securedType);
        bool UserHasAccess(int userId, string securedType, Enumerations.PermissionType permissionType);
        void LogOut();
        bool UserHasReadOnlyAcces(int userId, string securedType);
    }
}

