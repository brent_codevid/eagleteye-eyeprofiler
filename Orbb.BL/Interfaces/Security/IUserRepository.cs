﻿using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Other;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Security
{

    public interface IUserRepository : IRepository<User>
    {
        IList<UserView> GetUsersOverview();
        IList<ComboBoxItemVm> GetUsersForCombobox();
        string UpdateUserLanguage(int languageId);
        int UpdateUserDivision(int? divisionId);
        //List<UserDetailVm> GetUsersByType(UserTypes userType);
    }
}
