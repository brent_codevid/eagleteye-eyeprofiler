﻿using Orbb.BL.Implementations.LensFitting;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.ViewModels.Models.LensFitter;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.LensFitting
{
    public interface ILensFitter
    {
        EyeAnalysisResults CalculateFittingParameters(EyeData eyedata, LensDetail lens, double diameter);
        IList<LensFittingResultVm> GetBestFittingLens(EyeData eyedata);
    }
}
