﻿using Orbb.BL.Implementations.LensFitting;
using Orbb.Common.LensFitting;
using Orbb.Data.Models.Models.Lenssets;

namespace Orbb.BL.Interfaces.LensFitting
{
    public interface ILensTypeFitter
    {
        bool Unbound { get; set; }
        bool UnlistedCP2 { get; set; }
        LensFittingResults CalculateBestFit(EyeAnalysisResults ear, LensDetail lensDetail, EyeData eyeData);
    }
}
