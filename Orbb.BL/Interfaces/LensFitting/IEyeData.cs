﻿using System.Collections.Generic;

namespace Orbb.BL.Interfaces.LensFitting
{
    public interface IEyeData
    {
        IList<IMeridian> Meridians { get; set; }
        IList<int> LenssetIds { get; set; }
        int LensType { get; set; }

        double? EdgeLift { get; set; }

        bool? Unbound { get; set; }
        bool? UnlistedCP2 { get; set; }

        bool ValidMeridians();
    }
}
