﻿using System.Collections.Generic;

namespace Orbb.BL.Interfaces.LensFitting
{
    public interface IMeridian
    {
        int Angle { get; set; }
        double Rho { get; set; }
        int Apex { get; set; }
        IList<double> Points { get; set; }
    }
}
