﻿using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces
{
    public interface IProcedureRepository
    {
        List<T> Get<T>(string procedureName, List<Parameter> parameters) where T : class, IViewModel;
        T GetSingle<T>(string procedureName, List<Parameter> parameters) where T : class, IViewModel;
        int GetInt(string procedureName, List<Parameter> parameters);
        void Execute(string procedureName, List<Parameter> parameters);
    }
}