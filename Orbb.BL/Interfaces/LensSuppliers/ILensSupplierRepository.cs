﻿using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.LensSuppliers
{
    public interface ILensSupplierRepository : IRepository<LensSupplier>
    {
        IList<LensSupplierView> GetOverview();
    }
}
