﻿using Orbb.Data.Models.Models.Orders;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Orders
{
    public interface IOrderRepository : IRepository<Order>
    {
        IList<OrderView> GetOverview();
        List<Order> GetOrdersByCustomerId(int id);
    }
}
