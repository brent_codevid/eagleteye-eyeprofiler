﻿using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Other;
using System.Collections.Generic;
using static Orbb.Data.Common.Enumerations;

namespace Orbb.BL.Interfaces.Media
{
    public interface IMediaRepository
    {
        IList<MediaFileResult> GetFiles(int id, string deleteUrl, LinkedType type);

        MediaFileResult AddFile(byte[] binary, string fileNamePrefix, string fileExtension, int id, string mimeType, string deleteUrl, LinkedType type);

        MediaFileResult DeleteFile(int id);

        SaveResult SetDisplayOrder(int id, int[] sortedIds);
    }
}