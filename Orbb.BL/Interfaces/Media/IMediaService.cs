﻿using Orbb.Common.Utility;

namespace Orbb.BL.Interfaces.Media
{
    public interface IMediaService
    {
        void SaveVideo(string relativefilePath, byte[] bytes);

        void SaveImage(string relativefilePath, byte[] bytes);

        void SaveImage(string relativefilePath, byte[] bytes, ImageResolution minimumResolution, bool resize = true);

        void Delete(string relativefilePath);

        void CreateDefaultImage(string originalRelativefilePath, string prefix);
    }
}