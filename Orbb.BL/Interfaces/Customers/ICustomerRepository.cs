﻿using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Customers;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Customers
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        IList<CustomerView> GetOverview();
        void RemoveUnavailableLensesFromCustomers(int countryId, IList<LenssetVM> availableLenssets);
    }
}
