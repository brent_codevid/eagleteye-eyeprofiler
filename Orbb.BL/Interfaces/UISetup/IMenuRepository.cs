﻿using Orbb.Data.Common;
using Orbb.Data.Models.Models.UI.Base;
using Orbb.Data.ViewModels.Models.Security;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.UISetup
{
    public interface IMenuRepository
    {
        //IList<MenuItemVM> GetMenu(int UserId, string languageCode);
        IList<MenuItem> GetQuickMenu(int userId, string languageCode);
        //IList<MenuItemVM> GetSubMenu(int UserId, string languageCode);
        IList<MenuItemVM> GetMenu(int userId, string languageCode, string parent);
        SaveResult AddQuickMenuItem(int id);
        SaveResult RemoveQuickMenuItem(int id);
        SaveResult OrderQuickMenuItem(int id);
    }
}
