﻿using Orbb.Data.Common;
using Orbb.Data.EF;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Orbb.BL.Interfaces.General
{
    public interface IDataService
    {
        IDatabaseContext DbContext { get; set; }

        TViewModel Get<TEntity, TViewModel>(int id, string[] modelIncludes) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel;
        TViewModel GetNew<TEntity, TViewModel>() where TEntity : class, IEntity, new() where TViewModel : class, IViewModel;
        SaveResult Update<T, Y>(T original, T newmodel, T currentDbVersion, string[] modelIncludes) where T : class, IViewModel where Y : class, IEntity, new();
        bool CanDelete<TEntity>(int id, out string[] reasons) where TEntity : class, IEntity, new();
        bool Delete<TEntity>(int id, out string[] reasons) where TEntity : class, IEntity, new();
        List<TViewModel> GetAll<TEntity, TViewModel>(string[] modelIncludes) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel;
        List<TViewModel> GetAll<TEntity, TViewModel>(Expression<Func<TEntity, bool>> predicate, string[] includeStrings) where TEntity : class, IEntity, new() where TViewModel : class, IViewModel;
        void Clear();
        TEntity GetNew<TEntity>() where TEntity : class, IEntity, new();
        TEntity GetNew<TEntity>(IDatabaseContext context) where TEntity : class, IEntity, new();
    }
}
