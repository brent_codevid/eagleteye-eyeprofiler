﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;

namespace Orbb.BL.Interfaces.General
{
    public interface IReverseMapper<From, To> where From : class, IViewModel where To : class, IEntity
    {
        To ReverseMap(From source);
    }
}
