﻿using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;

namespace Orbb.BL.Interfaces.General
{
    public interface IReverseMapperFactory
    {
        IReverseMapper<From, To> GetReverseMapper<From, To>() where From : class, IViewModel where To : class, IEntity;
        void DefineReverseMapper<From, To>(IReverseMapper<From, To> mapper) where From : class, IViewModel where To : class, IEntity;
    }
}
