﻿using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.ViewModels.Models.Base;
using Orbb.Data.ViewModels.Models.Globalization;
using Orbb.Data.ViewModels.Models.Other;
using Orbb.Data.ViewModels.Models.System;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.General
{
    public interface ILookupProvider
    {
        IList<Language> GetArticleLanguagesList();
        IList<UserGroup> GetUserGroupsList();
        IList<LanguageTranslation> GetLanguageTranslationsList();
        IList<LanguageTranslation> GetLanguageTranslationsMenuList();
        IList<Language> GetLanguagesList();
        IList<LanguageVM> GetLanguages();
        IList<Orbb.Data.Models.Models.Base.Country> GetCountriesList();
        IList<CountryVM> GetCountriesVmList(int languageId);
        IList<CountryListItem> GetCountries(int languageId);
        Language GetLanguage(string code);
        IList<DivisionVM> GetDivisionByCategory(int entityId, int categoryId);
        IList<ComboBoxItemVm> GetUserTypesForCombobox();
    }
}
