﻿using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Lenssets
{
    public interface ILensRepository : IRepository<LensDetail>
    {
        IList<LensView> GetOverview();
        LensDetail GetLenssetById(int id);
        List<CP2ToCP1Item> GetCP2ToCP1ItemsByLensId(int id);
        int CountLenssetWithSupplier(int supplierId);
        int CountLenssetsWithCategory(int category);
        int CountLenssetWithType(int typeid);
        IList<LensInfo> GetInfo();
        IList<int> GetValidIds();
        IList<LensDetail> GetAllAvailableInCountry(List<int> lensIds, string[] includeString);
    }
}
