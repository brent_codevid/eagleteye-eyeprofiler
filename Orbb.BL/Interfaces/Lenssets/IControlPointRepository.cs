﻿using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Lenssets
{
    public interface IControlPointRepository : IRepository<ControlPoint>
    {
        string GetChildClassName(int id);
        
    }
}
