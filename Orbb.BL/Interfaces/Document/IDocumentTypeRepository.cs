﻿using Orbb.Data.EF;
using Orbb.Data.ViewModels.Models.Document;
using Model = Orbb.Data.Models.Models.System;

namespace Orbb.BL.Interfaces.Document
{
    public interface IDocumentTypeRepository
    {
        Model.Document LogDocumentType(IDatabaseContext dbContext, DocumentStorageVM storageModel);
    }
}
