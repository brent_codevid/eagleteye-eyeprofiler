﻿using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Document;

namespace Orbb.BL.Interfaces.Document
{
    public interface IDocumentRepository
    {
        SaveResult Storedocument(DocumentStorageVM storageModel);
    }
}
