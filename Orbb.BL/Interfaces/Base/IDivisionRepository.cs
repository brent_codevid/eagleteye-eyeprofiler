﻿using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.Other;
using Orbb.Data.ViewModels.Models.System;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Base
{
    public interface IDivisionRepository : IRepository<Division>
    {
        IList<DivisionVM> GetDivisions(int userId);
        IList<DivisionVM> GetDivisions();
        MediaFileResult UpdateDivsionImage(byte[] binary, string fileExtension);
    }
}
