﻿using Orbb.Data.Models.Models.Datacapture;

namespace Orbb.BL.Interfaces.Datacapture
{
    public interface ILensfittingCaptureRepository : IRepository<LensfittingCapture>
    {
        void CaptureFitting(int lensId, string fittedName, string userName);
    }
}
