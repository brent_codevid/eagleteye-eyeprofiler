﻿using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Settings
{
    public interface ILensTypeRepository
    {
        IList<LensTypeView> GetLensTypeOverview();
    }
}
