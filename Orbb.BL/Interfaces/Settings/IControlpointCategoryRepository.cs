﻿using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Settings
{
    public interface IControlpointCategoryRepository
    {
        IList<ControlpointCategoryView> GetControlpointCategoryOverview();
    }
}
