﻿using Orbb.Data.Models.Models.System;

namespace Orbb.BL.Interfaces.Settings
{
    public interface IInstallationSettingsRepository
    {
        InstallationSettings GetInstallationSettings(int divisionId);
        void SetInstallationSettings(InstallationSettings settings);
    }
}