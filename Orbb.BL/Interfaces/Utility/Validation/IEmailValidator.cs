﻿using System.Text.RegularExpressions;

namespace Orbb.BL.Interfaces.Utility.Validation
{
    public interface IEmailValidator
    {
        bool IsValidEmail(string strIn);

        string DomainMapper(Match match);
    }
}
