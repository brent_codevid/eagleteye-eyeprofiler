﻿using System.Text;

namespace Orbb.BL.Interfaces.Utility.Security
{
    public interface IEncryptionService
    {
        string CalculateSHA512(string text, Encoding enc);
        byte[] EncryptStringToBytes_Aes(string plainText, byte[] Key, byte[] IV);
        string DecryptStringFromBytes_Aes(byte[] cipherText, byte[] Key, byte[] IV);
    }
}
