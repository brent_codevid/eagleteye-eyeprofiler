﻿using Orbb.Data.Models.Models.Views;
using System.Collections.Generic;

namespace Orbb.BL.Interfaces.Country
{
    public interface ICountryRepository : IRepository<Orbb.Data.Models.Models.Base.Country>
    {
        IList<CountryView> GetOverview();
    }
}
