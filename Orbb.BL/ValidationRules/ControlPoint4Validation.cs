﻿using Orbb.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.ValidationRules
{
    public class ControlPoint4Validation : ControlPointValidation, IEntityValidationRule
    {
        public new List<EntityOperations> Operations { get; }

        public new Type OfEntityType { get; }

        public ControlPoint4Validation()
        {
            OfEntityType = typeof(ControlPoint4);

            Operations = new List<EntityOperations>
            {
                EntityOperations.Add,
                EntityOperations.Update
            };
        }

        public new ValidationRuleResult Validate(IEntity entity, IDatabaseContext context)
        {
            return ValidateControlPoint4(entity, context);
        }

        public virtual ValidationRuleResult ValidateControlPoint4(IEntity entity, IDatabaseContext context)
        {
            ValidationRuleResult result = base.Validate(entity, context);
            if (!(entity is ControlPoint4))
            {
                result.Valid = false;
                return result;
            }

            ControlPoint4 cp = (ControlPoint4)entity;
            if (cp.LensDetails.Count != 1)
            {
                result.Valid = false;
                return result;
            }

            if (cp.LensDetails.First().IsDraft)
            {
                result.Valid = true;
                return result;
            }
            if (!cp.LensDetails.First().UseCP4)
            {
                result.Valid = true;
                return result;
            }
            /*
            int containsLbCount = cp.PositiveName.Count(f => f == '{');
            int containsRbCount = cp.PositiveName.Count(f => f == '}');
            if (!(containsLbCount == containsRbCount &&
                (containsLbCount == 0 ||
                (containsLbCount == 1 && (cp.PositiveName.Contains("{var}") || cp.PositiveName.Contains("{var:abs}") || cp.PositiveName.Contains("{name}"))) ||
                (containsLbCount == 2 && (cp.PositiveName.Contains("{var}") || cp.PositiveName.Contains("{var:abs}")) && cp.PositiveName.Contains("{name}")))))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CP4VM_INVALID_FORMAT_POS");
            }
            containsLbCount = cp.NegativeName.Count(f => f == '{');
            containsRbCount = cp.NegativeName.Count(f => f == '}');
            if (!(containsLbCount == containsRbCount &&
                (containsLbCount == 0 ||
                (containsLbCount == 1 && (cp.NegativeName.Contains("{var}") || cp.NegativeName.Contains("{var:abs}") || cp.NegativeName.Contains("{name}"))) ||
                (containsLbCount == 2 && (cp.NegativeName.Contains("{var}") || cp.NegativeName.Contains("{var:abs}")) && cp.NegativeName.Contains("{name}")))))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CP4VM_INVALID_FORMAT_NEG");
            }*/
            if (cp.CalculateLz && cp.LzPercentage == null)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGLZPERCENTAGE");
            }
            /*if (cp.CalculateLz == false &&
                (cp.StandardVal1 == null ||
                    (cp.Category >= 2 && cp.StandardVal2 == null) ||
                    (cp.Category >= 3 && cp.StandardVal3 == null) ||
                    (cp.Category >= 4 && cp.StandardVal4 == null)
                ))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CP4VM_MISSINGSTANDARDVAL");
            }*/
            //Uncomment this when CP2ToCP4List is added
            /*
            LensDetail ld = cp.LensDetails.OfType<LensDetail>().FirstOrDefault();
            if (ld.CP2ToCP4List == null || ld.CP2ToCP4List.Count == 0)
            {
            */
            if (cp.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                if (cp.MaxValue == null && entity.Id != 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CP4VM_MISSINGMAXVALUE");
                }
                if (cp.MinValue == null && entity.Id != 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CP4VM_MISSINGMINVALUE");
                }
                if (cp.StepSize == null && entity.Id != 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CP4VM_MISSINGSTEPSIZE");
                }
            }
            if (cp.RelativeChordLength == false && cp.SemiChordLength == null)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGSEMICHORDLENGTH");
            }
            if (cp.RelativeChordLength && cp.DiameterMultiplier == null)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGDIAMETERMULTIPLIER");
            }
            /*
            }
            */
            return result;
        }
    }
}
