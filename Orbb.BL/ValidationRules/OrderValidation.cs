﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Orders;
using System;
using System.Collections.Generic;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.ValidationRules
{
    public class OrderValidation : IEntityValidationRule
    {
        public List<EntityOperations> Operations { get; }

        public Type OfEntityType { get; }

        public OrderValidation()
        {
            OfEntityType = typeof(Order);

            Operations = new List<EntityOperations>
            {
                EntityOperations.Add,
                EntityOperations.Update
            };
        }

        public ValidationRuleResult Validate(IEntity entity, IDatabaseContext context)
        {
            return ValidateOrder(entity, context);
        }

        public virtual ValidationRuleResult ValidateOrder(IEntity entity, IDatabaseContext context)
        {
            var result = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
            if (!(entity is Order))
            {
                result.Valid = false;
                return result;
            }

            Order o = (Order)entity;

            if (!Enum.IsDefined(typeof(OrderStatus), o.Status))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_O_INVALIDSTATUS");
            }

            return result;
        }
    }
}
