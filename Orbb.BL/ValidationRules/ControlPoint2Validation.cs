﻿using Orbb.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.BL.ValidationRules
{
    public class ControlPoint2Validation : ControlPointValidation, IEntityValidationRule
    {
        public new List<EntityOperations> Operations { get; }

        public new Type OfEntityType { get; }

        public ControlPoint2Validation()
        {
            OfEntityType = typeof(ControlPoint2);

            Operations = new List<EntityOperations>
            {
                EntityOperations.Add,
                EntityOperations.Update
            };
        }

        public new ValidationRuleResult Validate(IEntity entity, IDatabaseContext context)
        {
            return ValidateControlPoint2(entity, context);
        }

        public virtual ValidationRuleResult ValidateControlPoint2(IEntity entity, IDatabaseContext context)
        {
            ValidationRuleResult result = base.Validate(entity, context);
            if (!(entity is ControlPoint2))
            {
                result.Valid = false;
                return result;
            }

            ControlPoint2 cp = (ControlPoint2)entity;
            if (cp.LensDetails.Count != 1)
            {
                result.Valid = false;
                return result;
            }
            if (cp.LensDetails.First().IsDraft)
            {
                result.Valid = true;
                return result;
            }
            if (!cp.LensDetails.First().UseCP2)
            {
                result.Valid = true;
                return result;
            }
            if (cp.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                if (cp.MaxValue == null && entity.Id != 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CP2VM_MISSINGMAXVALUE");
                }
                if (cp.MinValue == null && entity.Id != 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CP2VM_MISSINGMINVALUE");
                }
            }
            LensDetail ld = cp.LensDetails.FirstOrDefault();
            if (!ld.UseCP2ToCP1List)
            {
                if (cp.StepSize == null && entity.Id != 0)
                {
                    if (cp.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
                    {
                        result.Valid = false;
                        result.Remarks.Add("VAL_CP2VM_MISSINGSTEPSIZE");
                    }
                }
            }

            /*int containsLbCount = cp.DisplayValueFormat.Count(f => f == '{');
            int containsRbCount = cp.DisplayValueFormat.Count(f => f == '}');

            if (!(containsLbCount == containsRbCount &&
                (containsLbCount == 0 ||
                (containsLbCount == 1 && (cp.DisplayValueFormat.Contains("{var}") || cp.DisplayValueFormat.Contains("{var:abs}") || cp.DisplayValueFormat.Contains("{name}"))) ||
                (containsLbCount == 2 && (cp.DisplayValueFormat.Contains("{var}") || cp.DisplayValueFormat.Contains("{var:abs}")) && cp.DisplayValueFormat.Contains("{name}")))))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CP2VM_INVALID_FORMAT");
            }*/

            if (cp.RelativeChordLength == false && cp.SemiChordLength == null)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGSEMICHORDLENGTH");
            }
            if (cp.RelativeChordLength && cp.DiameterMultiplier == null)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGDIAMETERMULTIPLIER");
            }

            if (cp.RelativeSag &&
                (cp.StandardVal1 == null ||
                    (cp.Category >= 2 && cp.StandardVal2 == null) ||
                    (cp.Category >= 3 && cp.StandardVal3 == null) ||
                    (cp.Category >= 4 && cp.StandardVal4 == null)
                 ))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_RELATIVESAGSTANDARDVAL");
            }
            return result;
        }
    }
}
