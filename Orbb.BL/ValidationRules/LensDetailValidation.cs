﻿using Orbb.Data.EF;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.ValidationRules
{
    public class LensDetailValidation : IEntityValidationRule
    {
        public List<EntityOperations> Operations { get; }

        public Type OfEntityType { get; }

        public LensDetailValidation()
        {
            OfEntityType = typeof(LensDetail);

            Operations = new List<EntityOperations>
            {
                EntityOperations.Add,
                EntityOperations.Update
            };
        }

        public ValidationRuleResult Validate(IEntity entity, IDatabaseContext context)
        {
            return ValidateLensDetail(entity, context);
        }

        public virtual ValidationRuleResult ValidateLensDetail(IEntity entity, IDatabaseContext context)
        {
            var result = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
            if (!(entity is LensDetail))
            {
                result.Valid = false;
                return result;
            }

            LensDetail ld = (LensDetail)entity;
            if (ld.IsDraft)
            {
                result.Valid = true;
                return result;
            }

            if (ld.LensDiameter == null 
                && (ld.LensDiameterMax == null || ld.LensDiameterMin == null || (ld.LensDiameterStepSize == null && ld.LensDiameterPerc == null))
                && (ld.UseHvidForDiameters &&ld.HvidVal10 == null && ld.HvidVal105 == null && ld.HvidVal11 == null && ld.HvidVal115 == null))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_LDVM_LENSDIAMETER");
            }
            if (ld.LensDiameterMin != null && ld.LensDiameterMax != null && ld.LensDiameterMin >= ld.LensDiameterMax)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_LDVM_MINMAX");
            }
            if (ld.LensDiameterStepSize != null)
            {
                if (ld.LensDiameterStepSize <= 0)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CPVM_STEP");
                }
                //lensDiameterStepSize > 0
                else if (ld.LensDiameterMin != null && ld.LensDiameterMax != null && ld.LensDiameterStepSize != null &&
                        ((ld.LensDiameterMax - ld.LensDiameterMin) / ld.LensDiameterStepSize - Math.Round((double)((ld.LensDiameterMax - ld.LensDiameterMin) / ld.LensDiameterStepSize))) > 0.000001)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CPVM_MINMAXSTEP");
                }
            }

            if (!Enum.IsDefined(typeof(LensTypes), ld.Type))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_LD_INVALIDTYPE");
            }

            if (ld.UseCP2ToCP1List && ld.CP2ToCP1List.Count == 0)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_MISSINGCP2TOCP1ITEMS");
            }
            return result;
        }
    }
}