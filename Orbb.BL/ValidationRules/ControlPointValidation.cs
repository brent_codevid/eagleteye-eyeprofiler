﻿using Orbb.Common;
using Orbb.Data.EF;
using Orbb.Data.EF.EntityValidation;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.Models.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.BL.ValidationRules
{
    public class ControlPointValidation : IEntityValidationRule
    {
        public List<EntityOperations> Operations { get; }

        public Type OfEntityType { get; }

        public ControlPointValidation()
        {
            OfEntityType = typeof(Orbb.Data.Models.Models.Lenssets.ControlPoint);

            Operations = new List<EntityOperations>
            {
                EntityOperations.Add,
                EntityOperations.Update
            };
        }

        public ValidationRuleResult Validate(IEntity entity, IDatabaseContext context)
        {
            return ValidateControlPoint(entity, context);
        }

        public virtual ValidationRuleResult ValidateControlPoint(IEntity entity, IDatabaseContext context)
        {
            var result = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
            if (!(entity is ControlPoint))
            {
                result.Valid = false;
                return result;
            }

            ControlPoint cp = (ControlPoint)entity;
            result = ValidateDisplayCpFormat(entity, context);

            if (cp.SemiChordLength != null && cp.SemiChordLength < 0)
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_SEMICHORDLENGTH");
            }
            if (cp.CalculationMethod == Enumerations.CpCalculationMethod.Formula)
            {
                if (cp.MinValue != null && cp.MaxValue != null && cp.MinValue > cp.MaxValue)
                {
                    result.Valid = false;
                    result.Remarks.Add("VAL_CPVM_MINMAX");
                }
                if (cp.StepSize != null)
                {
                    if (cp.StepSize <= 0)
                    {
                        result.Valid = false;
                        result.Remarks.Add("VAL_CPVM_STEP");
                    }
                    //StepSize > 0
                    else if (cp.MinValue != null && cp.MaxValue != null &&
                        ((cp.MaxValue - cp.MinValue) / cp.StepSize - Math.Round((double)((cp.MaxValue - cp.MinValue) / cp.StepSize))) > 0.000001)
                    {
                        result.Valid = false;
                        result.Remarks.Add("VAL_CPVM_MINMAXSTEP");
                    }
                }
            }
            

            if (!Enum.IsDefined(typeof(ControlPointCategories), cp.Category))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CP_INVALIDCATEGORY");
            }
            return result;
        }

        public virtual ValidationRuleResult ValidateDisplayCpFormat(IEntity entity, IDatabaseContext context)
        {
            ValidationRuleResult result = new ValidationRuleResult { Valid = true, Remarks = new List<string>() };
            if (!(entity is ControlPoint))
            {
                result.Valid = false;
                return result;
            }

            ControlPoint cp = (ControlPoint)entity;
            int containsLbCount = cp.DisplayCpFormat.Count(f => f == '{');
            int containsRbCount = cp.DisplayCpFormat.Count(f => f == '}');

           /* if (!(containsLbCount == containsRbCount &&
                    ((containsLbCount == 0) ||
                        (containsLbCount == 1 && (cp.DisplayCpFormat.Contains("{vars}") || cp.DisplayCpFormat.Contains("{name}"))) ||
                        (containsLbCount == 2 && cp.DisplayCpFormat.Contains("{vars}") && cp.DisplayCpFormat.Contains("{name}"))
                    )))
            {
                result.Valid = false;
                result.Remarks.Add("VAL_CPVM_INVALID_FORMAT");
            }*/
            return result;
        }
    }
}
