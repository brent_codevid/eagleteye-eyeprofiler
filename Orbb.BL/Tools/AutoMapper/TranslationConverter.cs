﻿using AutoMapper;
using Orbb.Data.EF;
using Orbb.Data.Models.Models.Globalization;
using StructureMap;
using System;
using System.Linq;

namespace Orbb.BL.Tools.AutoMapper
{
    public class TranslationConverter : ITypeConverter<Translation, string>
    {
        public static IContainer Container { get; set; }

        public string Convert(Translation source, string destination, ResolutionContext context)
        {
            try
            {
                //string culture = CultureInfo.CurrentCulture.Name;
                IDatabaseContext databaseContext = Container.GetInstance<IDatabaseContext>();
                string culture = System.Threading.Thread.CurrentThread.CurrentCulture.Name;
                var language = databaseContext.Query<Language>().First(l => l.Code == culture);
                //Translation source = (Translation)context.SourceValue;
                if (source == null) return "";
                Translation translation = databaseContext.Query<Translation>().Single(t => t.Id == source.Id);
                if (translation == null)
                    return "";
                var entry = translation.Entries?.Where(e => e.LanguageId == language.Id).FirstOrDefault();
                if (entry == null)
                    return translation.DefaultValue ?? "";

                return entry.Value;
            }
            catch (Exception e)
            {
                Console.WriteLine("TranslationConverter exception: " + e.Message);
                return "";
            }
        }
    }
}
