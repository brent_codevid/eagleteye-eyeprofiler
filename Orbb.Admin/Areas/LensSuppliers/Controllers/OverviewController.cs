﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.LensSuppliers;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.ViewModels.Models.LensSuppliers;

namespace Orbb.Admin.Areas.LensSuppliers.Controllers
{
    [Area("LensSuppliers")]
    [SecuredObject("Lens Supplier")]
    public class OverviewController : BaseOverviewController<LensSupplier, LensSupplierVM>
    {
        private readonly ILensSupplierRepository lensSupplierRepository;

        public OverviewController(ILensSupplierRepository lensSupplierRepository)
        {
            this.lensSupplierRepository = lensSupplierRepository;
        }

        public IActionResult LensSupplier_Overview([DataSourceRequest]DataSourceRequest request)
        {
            return Json(lensSupplierRepository.GetOverview().ToDataSourceResult(request));
        }
    }
}
