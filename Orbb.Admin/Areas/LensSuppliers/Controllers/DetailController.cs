﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.LensSuppliers;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.ViewModels.Models.LensSuppliers;

namespace Orbb.Admin.Areas.LensSuppliers.Controllers
{
    [Area("LensSuppliers")]
    [SecuredObject("Lens Supplier")]
    public class DetailController : DetailBaseController<LensSupplier, LensSupplierVM>
    {
        private ILensSupplierRepository lensSupplierRepository;
        private const string sessionCurrentObjectKey = "Base.LensSupplierDetail";


        public DetailController(ILensSupplierRepository lensSupplierRepository) : base("LensSupplierDetail")
        {
            redirectTo = "/LensSuppliers/detail/index/{id}";
            OverviewURL = "/LensSuppliers/overview";
            this.lensSupplierRepository = lensSupplierRepository;
        }

    }
}
