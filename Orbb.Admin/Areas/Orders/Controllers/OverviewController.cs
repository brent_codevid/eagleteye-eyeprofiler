﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Orders;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Orders;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Orders;
using System.Collections.Generic;

namespace Orbb.Admin.Areas.Orders.Controllers
{
    [Area("Orders")]
    [SecuredObject("Orders")]
    public class OverviewController : BaseOverviewController<Order, OrderVM>
    {
        private readonly IOrderRepository _orderRepository;

        public OverviewController(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository;
        }
        public IActionResult Orders_Overview([DataSourceRequest]DataSourceRequest request)
        {
            IList<OrderView> list = _orderRepository.GetOverview();
            foreach (OrderView ov in list)
            {
                ov.StatusName = stringLocalizer[ov.StatusName];
            }
            ModifyDateFilters(request.Filters, "Date");
            return Json(list.ToDataSourceResult(request));
        }
    }
}
