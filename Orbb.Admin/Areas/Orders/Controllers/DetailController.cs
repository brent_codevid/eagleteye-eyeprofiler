﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Orders;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Orders;
using Orbb.Data.ViewModels.Models.Orders;

namespace Orbb.Admin.Areas.Orders.Controllers
{
    [Area("Orders")]
    [SecuredObject("Orders")]
    public class DetailController : DetailBaseController<Order, OrderVM>
    {

        private IOrderRepository _orderRepository;

        public DetailController(IOrderRepository orderRepository) : base("Order")
        {
            modelIncludes = new string[] { };
            redirectTo = "/Orders/detail/index/{id}";
            OverviewURL = "/Orders/overview";
            _orderRepository = orderRepository;
        }
    }
}
