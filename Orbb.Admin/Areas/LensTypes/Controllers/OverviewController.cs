﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Settings;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Views;

namespace Orbb.Admin.Areas.LensTypes.Controllers
{
    [Area("LensTypes")]
    [SecuredObject("Lens Types")]
    public class OverviewController : BaseController
    {
        private readonly ILensTypeRepository lensTypeRepository;

        public OverviewController(ILensTypeRepository lensTypeRepository)
        {
            this.lensTypeRepository = lensTypeRepository;
        }
        public IActionResult LensType_Overview([DataSourceRequest]DataSourceRequest request)
        {
            var lensTypes = lensTypeRepository.GetLensTypeOverview();
            foreach (LensTypeView lensType in lensTypes)
            {
                lensType.Name = stringLocalizer[lensType.Name];
            }
            return Json(lensTypes.ToDataSourceResult(request));
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }
            return View();
        }
    }
}
