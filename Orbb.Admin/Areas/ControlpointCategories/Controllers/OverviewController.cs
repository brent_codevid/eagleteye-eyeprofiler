﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Settings;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Views;

namespace Orbb.Admin.Areas.ControlpointCategories.Controllers
{
    [Area("ControlpointCategories")]
    [SecuredObject("Controlpoint categories")]
    public class OverviewController : BaseController
    {
        private readonly IControlpointCategoryRepository controlpointCategoryRepository;

        public OverviewController(IControlpointCategoryRepository controlpointCategoryRepository)
        {
            this.controlpointCategoryRepository = controlpointCategoryRepository;
        }

        public IActionResult ControlpointCategory_Overview([DataSourceRequest]DataSourceRequest request)
        {
            var controlpointCategories = controlpointCategoryRepository.GetControlpointCategoryOverview();
            foreach (ControlpointCategoryView controlpointCategory in controlpointCategories)
            {
                controlpointCategory.Name = stringLocalizer[controlpointCategory.Name];
            }
            return Json(controlpointCategories.ToDataSourceResult(request));
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }
            return View();
        }
    }
}
