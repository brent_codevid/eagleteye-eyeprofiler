﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Common;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.Lenssets;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.Admin.Areas.Lenssets.Controllers
{
    [Area("Lenssets")]
    [SecuredObject("lens dataset")]
    public class DetailController : DetailBaseController<LensDetail, LensDetailVM>
    {
        private readonly IControlPointRepository _controlPointRepository;
        private readonly List<int> ControlPoints = new List<int>() { 1, 3, 4 };
        private readonly IMapper _mapper;

        public DetailController(IControlPointRepository controlPointRepository, IMapper mapper) : base("LensDetail")
        {
            modelIncludes = new[] { "Supplier", "CP1", "CP2", "CP3", "CP4", "CP5", 
                "CP2ToCP1List", "CP1.CpList", "CP2.CpList", "CP3.CpList", "CP4.CpList", "CP5.CpList",
                "CP1.CpTangentList", "CP2.CpTangentList", "CP3.CpTangentList", "CP4.CpTangentList", "CP5.CpTangentList", 
                "CP1.CpCurvatureList", "CP2.CpCurvatureList", "CP3.CpCurvatureList", "CP4.CpCurvatureList", "CP5.CpCurvatureList" };
            redirectTo = "/Lenssets/detail/index/{id}";
            OverviewURL = "/Lenssets/overview";
            _controlPointRepository = controlPointRepository;
            _mapper = mapper;
        }

        /*grid methods*/
        override
        public LensDetailVM AfterLoad(LensDetailVM model, string extraData = null, bool isCopy = false)
        {
            PopulateSessionStorage(model);
            if (model.CP1 != null && model.CP1.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in model.CP1.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(model.CP1.CurvatureDiameters);
                }
            }

            if (model.CP2 != null && model.CP2.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in model.CP2.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(model.CP2.CurvatureDiameters);
                }
            }

            if (model.CP3 != null && model.CP3.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in model.CP3.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(model.CP3.CurvatureDiameters);
                }
            }

            if (model.CP4 != null && model.CP4.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in model.CP4.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(model.CP4.CurvatureDiameters);
                }
            }

            if (model.CP5 != null && model.CP5.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in model.CP5.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(model.CP5.CurvatureDiameters);
                }
            }

            return model;
        }
        override
        public LensDetailVM BeforeUpdate(LensDetailVM model)
        {
            model.Cleanup();

            if (model.CP4.DisplayLandingZoneStepType.Equals(Enumerations.DisplayLandingZoneStepType.Microns))
            {
                ModelState["CP4.DisplayLandingZoneCenter"].Errors.Clear();
                ModelState.Remove("CP4.DisplayLandingZoneCenter");
            }

            if (model.IsDraft)
            {
                ModelState.Clear();
            }

            if (!model.UseCP1)
            {
                model.CP1Id = null;
                model.CP1 = null;
                foreach (var name in ModelState.Keys.Where(k => k.Contains("CP1.")))
                {
                    ModelState.Remove(name);
                }
            }
            if (!model.UseCP2)
            {
                model.CP2Id = null;
                model.CP2 = null;
                foreach (var name in ModelState.Keys.Where(k => k.Contains("CP2.")))
                {
                    ModelState.Remove(name);
                }
            }
            if (!model.UseCP3)
            {
                model.CP3Id = null;
                model.CP3 = null;
                foreach (var name in ModelState.Keys.Where(k => k.Contains("CP3.")))
                {
                    ModelState.Remove(name);
                }
            }
            if (!model.UseCP4)
            {
                model.CP4Id = null;
                model.CP4 = null;
                foreach (var name in ModelState.Keys.Where(k => k.Contains("CP4.")))
                {
                    ModelState.Remove(name);
                }
            }
            if (!model.UseCP5)
            {
                model.CP5Id = null;
                model.CP5 = null;
                foreach (var name in ModelState.Keys.Where(k => k.Contains("CP5.")))
                {
                    ModelState.Remove(name);
                }
            }

            PopulateModelFromSessionStorage(model);

            return model;
        }

        [AcceptVerbs("Post")]
        public JsonResult ListItems_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPListItemVM> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.List.ToString());
            foreach (var listItem in listItems)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, listItem);
            }

            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public JsonResult ListItems_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPListItemVM> listItems, int cpNum)
        {
            int counter = -1;
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.List.ToString());
            foreach (var listItem in listItems)
            {
                listItem.Id = int.Parse($"-{cpNum}{-counter}");
                counter--;
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, listItem);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public JsonResult ListItems_Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPListItemVM> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.List.ToString());
            foreach (var listItem in listItems)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Remove(sessionKey);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs("Post")]
        public JsonResult TangentItems_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPTangentItemVM> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.TangentList.ToString());
            foreach (var listItem in listItems)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, listItem);
            }

            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public JsonResult TangentItems_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPTangentItemVM> listItems, int cpNum)
        {
            int counter = -1;
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.TangentList.ToString());
            foreach (var listItem in listItems)
            {
                listItem.Id = int.Parse($"-{cpNum}{-counter}");
                counter--;
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, listItem);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));

        }
        [AcceptVerbs("Post")]


        [AcceptVerbs("Post")]
        public JsonResult TangentItems_Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPTangentItemVM> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.TangentList.ToString());
            foreach (var listItem in listItems)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Remove(sessionKey);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));
        }
        [AcceptVerbs("Post")]
        public JsonResult CurvatureItems_Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPCurvatureItemVMForKendo> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.CurvatureDesign.ToString());
            foreach (var listItem in listItems)
            {
                CPCurvatureItemVM curvatureItem = _mapper.Map<CPCurvatureItemVM>(listItem);
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, curvatureItem);
            }

            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public JsonResult CurvatureItems_Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPCurvatureItemVMForKendo> listItems, int cpNum)
        {
            int counter = -1;
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.CurvatureDesign.ToString());
            foreach (var listItem in listItems)
            {
                listItem.Id = int.Parse($"-{cpNum}{-counter}");
                CPCurvatureItemVM curvatureItem = _mapper.Map<CPCurvatureItemVM>(listItem);
                counter--;
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, curvatureItem);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        [AcceptVerbs("Post")]
        public JsonResult CurvatureItems_Destroy([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]ICollection<CPCurvatureItemVMForKendo> listItems, int cpNum)
        {
            string sessionKeyString = GenerateSessionKey(cpNum, Enumerations.CpCalculationMethod.CurvatureDesign.ToString());
            foreach (var listItem in listItems)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Remove(sessionKey);
            }
            return Json(listItems.ToDataSourceResult(request, ModelState));
        }

        public ActionResult GetCpValueFormPartial(int cpId, Enumerations.CpCalculationMethod listType, string cpNum)
        {
            ViewBag.listType = listType.ToString();
            ControlPointVM result = _controlPointRepository.GetSingle<ControlPoint, ControlPointVM>(c => c.Id == cpId, new string[] { "CpList", "CpTangentList", "CpCurvatureList" });
            if (result.CalculationMethod == Enumerations.CpCalculationMethod.CurvatureDesign)
            {
                foreach (var curvatureItem in result.CpCurvatureList)
                {
                    curvatureItem.CalculateSagitta(result.CurvatureDiameters);
                }
            }

            string childType;
            if (result != null)
            {
                childType = _controlPointRepository.GetChildClassName(result.Id);
            }
            else
            {
                result = new ControlPointVM();
                childType = $"ControlPoint{cpNum}";
            }

            switch (childType)
            {
                case "ControlPoint1":
                    ViewBag.CpNum = "1";
                    break;
                case "ControlPoint2":
                    ViewBag.CpNum = "2";
                    break;
                case "ControlPoint3":
                    ViewBag.CpNum = "3";
                    break;
                case "ControlPoint4":
                    ViewBag.CpNum = "4";
                    break;
                case "ControlPoint5":
                    ViewBag.CpNum = "5";
                    break;
            }

            string partialName = "";
            switch (listType)
            {
                case Enumerations.CpCalculationMethod.List:
                    partialName = "Partial_CPList";
                    break;
                case Enumerations.CpCalculationMethod.TangentList:
                    partialName = "Partial_CPTangentList";
                    break;
                case Enumerations.CpCalculationMethod.CurvatureDesign:
                    partialName = "Partial_CPCurvatureList";
                    break;
                case Enumerations.CpCalculationMethod.Formula:
                    partialName = "Partial_CPFormula";
                    break;
                case Enumerations.CpCalculationMethod.AlignmentCurve:
                    partialName = "Partial_CPFormula";
                    break;
                case Enumerations.CpCalculationMethod.Curvature:
                    partialName = "Partial_CPFormula";
                    break;
            }
            return PartialView(partialName, result);


        }

        private string GenerateSessionKey(int cp, string type)
        {
            return $"sessionlistkeyCP{cp}For{type}";
        }

        private void PopulateSessionStorage(LensDetailVM model)
        {
            PopulateSessionStorageFor<CPListItemVM>(model?.CP1?.CpList ?? new List<CPListItemVM>(), 1, Enumerations.CpCalculationMethod.List);
            PopulateSessionStorageFor<CPListItemVM>(model?.CP2?.CpList ?? new List<CPListItemVM>(), 2, Enumerations.CpCalculationMethod.List);
            PopulateSessionStorageFor<CPListItemVM>(model?.CP3?.CpList ?? new List<CPListItemVM>(), 3, Enumerations.CpCalculationMethod.List);
            PopulateSessionStorageFor<CPListItemVM>(model?.CP4?.CpList ?? new List<CPListItemVM>(), 4, Enumerations.CpCalculationMethod.List);
            PopulateSessionStorageFor<CPListItemVM>(model?.CP5?.CpList ?? new List<CPListItemVM>(), 5, Enumerations.CpCalculationMethod.List);

            PopulateSessionStorageFor<CPCurvatureItemVM>(model?.CP1?.CpCurvatureList ?? new List<CPCurvatureItemVM>(), 1, Enumerations.CpCalculationMethod.CurvatureDesign);
            PopulateSessionStorageFor<CPCurvatureItemVM>(model?.CP2?.CpCurvatureList ?? new List<CPCurvatureItemVM>(), 2, Enumerations.CpCalculationMethod.CurvatureDesign);
            PopulateSessionStorageFor<CPCurvatureItemVM>(model?.CP3?.CpCurvatureList ?? new List<CPCurvatureItemVM>(), 3, Enumerations.CpCalculationMethod.CurvatureDesign);
            PopulateSessionStorageFor<CPCurvatureItemVM>(model?.CP4?.CpCurvatureList ?? new List<CPCurvatureItemVM>(), 4, Enumerations.CpCalculationMethod.CurvatureDesign);
            PopulateSessionStorageFor<CPCurvatureItemVM>(model?.CP5?.CpCurvatureList ?? new List<CPCurvatureItemVM>(), 5, Enumerations.CpCalculationMethod.CurvatureDesign);

            PopulateSessionStorageFor<CPTangentItemVM>(model?.CP1?.CpTangentList ?? new List<CPTangentItemVM>(), 1, Enumerations.CpCalculationMethod.TangentList);
            PopulateSessionStorageFor<CPTangentItemVM>(model?.CP2?.CpTangentList ?? new List<CPTangentItemVM>(), 2, Enumerations.CpCalculationMethod.TangentList);
            PopulateSessionStorageFor<CPTangentItemVM>(model?.CP3?.CpTangentList ?? new List<CPTangentItemVM>(), 3, Enumerations.CpCalculationMethod.TangentList);
            PopulateSessionStorageFor<CPTangentItemVM>(model?.CP4?.CpTangentList ?? new List<CPTangentItemVM>(), 4, Enumerations.CpCalculationMethod.TangentList);
            PopulateSessionStorageFor<CPTangentItemVM>(model?.CP5?.CpTangentList ?? new List<CPTangentItemVM>(), 5, Enumerations.CpCalculationMethod.TangentList);
        }

        private void PopulateSessionStorageFor<H>(IEnumerable<H> CalculationList, int cp, Enumerations.CpCalculationMethod method) where H : IViewModel
        {
            string sessionKeyString = GenerateSessionKey(cp, method.ToString());
            //remove old listItems in storage
            var listItemKeys = this.SessionStorage.Keys().Where(k => k.Contains("SESSION-" + sessionKeyString)).ToList();
            foreach (var listItemKey in listItemKeys)
            {
                this.SessionStorage.Remove(listItemKey);
            }
            //add own listitems
            foreach (var listItem in CalculationList)
            {
                var sessionKey = GetSessionKey(sessionKeyString, listItem.Id);
                this.SessionStorage.Set(sessionKey, listItem);
            }
        }

        private void PopulateModelFromSessionStorage(LensDetailVM model)
        {
            if (model.CP1 != null)
            {
                switch (model.CP1.CalculationMethod)
                {
                    case Enumerations.CpCalculationMethod.CurvatureDesign:
                        model.CP1.CpCurvatureList = PopulateModelFor<CPCurvatureItemVM>(1, Enumerations.CpCalculationMethod.CurvatureDesign);
                        break;
                    case Enumerations.CpCalculationMethod.TangentList:
                        model.CP1.CpTangentList = PopulateModelFor<CPTangentItemVM>(1, Enumerations.CpCalculationMethod.TangentList);
                        break;
                    case Enumerations.CpCalculationMethod.List:
                        model.CP1.CpList = PopulateModelFor<CPListItemVM>(1, Enumerations.CpCalculationMethod.List);
                        break;
                }
            }

            if (model.CP2 != null)
            {
                switch (model.CP2.CalculationMethod)
                {
                    case Enumerations.CpCalculationMethod.CurvatureDesign:
                        model.CP2.CpCurvatureList = PopulateModelFor<CPCurvatureItemVM>(2, Enumerations.CpCalculationMethod.CurvatureDesign);
                        break;
                    case Enumerations.CpCalculationMethod.TangentList:
                        model.CP2.CpTangentList = PopulateModelFor<CPTangentItemVM>(2, Enumerations.CpCalculationMethod.TangentList);
                        break;
                    case Enumerations.CpCalculationMethod.List:
                        model.CP2.CpList = PopulateModelFor<CPListItemVM>(2, Enumerations.CpCalculationMethod.List);
                        break;
                }
            }

            if (model.CP3 != null)
            {
                switch (model.CP3.CalculationMethod)
                {
                    case Enumerations.CpCalculationMethod.CurvatureDesign:
                        model.CP3.CpCurvatureList = PopulateModelFor<CPCurvatureItemVM>(3, Enumerations.CpCalculationMethod.CurvatureDesign);
                        break;
                    case Enumerations.CpCalculationMethod.TangentList:
                        model.CP3.CpTangentList = PopulateModelFor<CPTangentItemVM>(3, Enumerations.CpCalculationMethod.TangentList);
                        break;
                    case Enumerations.CpCalculationMethod.List:
                        model.CP3.CpList = PopulateModelFor<CPListItemVM>(3, Enumerations.CpCalculationMethod.List);
                        break;
                }
            }

            if (model.CP4 != null)
            {
                switch (model.CP4.CalculationMethod)
                {
                    case Enumerations.CpCalculationMethod.CurvatureDesign:
                        model.CP4.CpCurvatureList = PopulateModelFor<CPCurvatureItemVM>(4, Enumerations.CpCalculationMethod.CurvatureDesign);
                        break;
                    case Enumerations.CpCalculationMethod.TangentList:
                        model.CP4.CpTangentList = PopulateModelFor<CPTangentItemVM>(4, Enumerations.CpCalculationMethod.TangentList);
                        break;
                    case Enumerations.CpCalculationMethod.List:
                        model.CP4.CpList = PopulateModelFor<CPListItemVM>(4, Enumerations.CpCalculationMethod.List);
                        break;
                }
            }

            if (model.CP5 != null)
            {
                switch (model.CP5.CalculationMethod)
                {
                    case Enumerations.CpCalculationMethod.CurvatureDesign:
                        model.CP5.CpCurvatureList = PopulateModelFor<CPCurvatureItemVM>(5, Enumerations.CpCalculationMethod.CurvatureDesign);
                        break;
                    case Enumerations.CpCalculationMethod.TangentList:
                        model.CP5.CpTangentList = PopulateModelFor<CPTangentItemVM>(5, Enumerations.CpCalculationMethod.TangentList);
                        break;
                    case Enumerations.CpCalculationMethod.List:
                        model.CP5.CpList = PopulateModelFor<CPListItemVM>(5, Enumerations.CpCalculationMethod.List);
                        break;
                }
            }
        }

        private List<H> PopulateModelFor<H>(int cp, Enumerations.CpCalculationMethod method) where H : IViewModel
        {
            List<H> result = new List<H>();

            string sessionKeyString = GenerateSessionKey(cp, method.ToString());

            var listItemKeys = this.SessionStorage.Keys().Where(k => k.Contains("SESSION-" + sessionKeyString)).ToList();
            foreach (var listItem in listItemKeys)
            {
                var item = this.SessionStorage.GetAsync<H>(listItem).Result;
                result.Add(item);
            }
            return result;
        }
    }
}
