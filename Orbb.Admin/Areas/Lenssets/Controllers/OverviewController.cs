﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Lenssets;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Orbb.Admin.Areas.Lenssets.Controllers
{
    [Area("Lenssets")]
    [SecuredObject("Lens Dataset")]
    public class OverviewController : BaseOverviewController<LensDetail, LensDetailVM>
    {

        private readonly ILensRepository lensRepository;

        public OverviewController(ILensRepository lensRepository)
        {
            this.lensRepository = lensRepository;
        }

        public IActionResult LensDatasets_Overview([DataSourceRequest]DataSourceRequest request)
        {
            IList<LensView> lenses = lensRepository.GetOverview();
            foreach (LensView lens in lenses)
            {
                string typename = lens.TypeName;
                lens.TypeName = stringLocalizer[typename];
            }
            var orderedLenses = lenses.OrderBy(x => x.SupplierName).ThenBy(x => x.Name).ThenBy(x => x.LensDiameter);
            return Json(orderedLenses.ToDataSourceResult(request));
        }

        public IActionResult Export(int id)
        {
            var lens = lensRepository.GetLenssetById(id);
            var name = lens.GeneralLensName() + ".csv";
            var data = lens.GenerateFullSet();

            return File(Encoding.UTF8.GetBytes(data), "text/plain", name);

            /*new version does not work yet :(
            LensNameListGenerator generator = new LensNameListGenerator();
            var lens = lensRepository.GetLenssetById(id);
            var name = lens.GeneralLensName() + ".csv";
            var data = generator.GenerateFullSet(lens, new LensFitter(null));

            return File(Encoding.UTF8.GetBytes(data), "text/plain", name);
            */
        }

        override
        public LensDetailVM BeforeDelete(LensDetailVM model)
        {
            foreach (var key in ModelState)
            {
                ModelState[key.Key].Errors.Clear();
                ModelState.Remove(key.Key);
            }
            return model;
        }
    }
}
