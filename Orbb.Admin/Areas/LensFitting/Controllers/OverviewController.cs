﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Implementations.LensFitting;
using Orbb.BL.Interfaces.LensFitting;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.ViewModels.Models.LensFitter;
using Orbb.Data.ViewModels.Models.Lenssets;
using Orbb.Data.ViewModels.Models.Other;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.Admin.Areas.LensFitting.Controllers
{
    [Area("Lensfitting")]
    [SecuredObject("algorithm testing")]
    public class OverviewController : BaseOverviewController<LensDetail, LensDetailVM>
    {
        private readonly ILensRepository _lensRepository;
        private readonly ILensFitter _lensFitter;
        private readonly IMapper _mapper;

        public OverviewController(ILensRepository lensRepository, ILensFitter lensFitter, IMapper mapper)
        {
            _lensRepository = lensRepository;
            _lensFitter = lensFitter;
            _mapper = mapper;
        }

        [HttpPost]
        public async Task<JsonResult> UploadFileAsync(List<int> lensIds, bool unbound = false, bool unlistedCP2 = false)
        {
            var files = new List<EyeFittingTestResult>();

            foreach (IFormFile file in Request.Form.Files)
            {
                if (lensIds == null || lensIds.Count == 0)
                {
                    files.Add(new EyeFittingTestResult { Name = file.FileName, Error = stringLocalizer["E_NOLENS"] });
                }
                else
                {
                    byte[] fileData;
                    string jsonFile;
                    using (MemoryStream stream = new MemoryStream())
                    {
                        await file.CopyToAsync(stream);
                        fileData = stream.ToArray();
                        jsonFile = Encoding.UTF8.GetString(fileData, 0, fileData.Length);
                    }

                    EyeData obj = null;
                    obj = JsonConvert.DeserializeObject<EyeData>(jsonFile);
                    if (obj.ValidMeridians())
                    {
                        obj.LenssetIds = lensIds;
                        obj.Unbound = unbound;
                        obj.UnlistedCP2 = unlistedCP2;
                        try
                        {
                            IList<LensFittingResultVm> fittings = _lensFitter.GetBestFittingLens(obj);
                            //for each fitting in fittings, generate a new EyeFittingTestResult and add this to files
                            foreach (var fitting in fittings)
                            {
                                if (fitting != null)
                                {
                                    files.Add(new EyeFittingTestResult { Name = file.FileName, FittedLens = fitting, Size = fileData.Length });
                                }
                                else
                                {
                                    files.Add(new EyeFittingTestResult { Name = file.FileName, Error = stringLocalizer["E_ALGOPROB"] });
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            //LensDetail lens = _lensRepository.GetLenssetById(lensId);
                            //files.Add(new EyeFittingTestResult() { name = file.FileName, error = lens.Supplier.Name + " " + lens.Name + ": " + stringLocalizer["E_ALGOPROB"] });
                            files.Add(new EyeFittingTestResult { Name = file.FileName, Error =  stringLocalizer["E_ALGOPROB"]});
                        }
                    }
                    else
                    {
                        files.Add(new EyeFittingTestResult { Name = file.FileName, Error = stringLocalizer["E_INVALIDMERIDIANS"] });
                    }
                }
            }
            return Json(new { files });
        }
    }
}
