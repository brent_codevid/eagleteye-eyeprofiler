﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.UISetup;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Admin.Areas.Settings.Controllers
{
    [Area("Settings")]
    [SecuredObject("Settings")]
    public class MenuController : MenuBaseController
    {
        public MenuController(IMenuRepository menuFactory, ICurrent<UserMenuVM> userMenu) : base(userMenu, "settings")
        {
        }
    }
}