﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Customers;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.ViewModels.Models.Base;
using Orbb.Data.ViewModels.Models.Customers;
using System.Collections.Generic;

namespace Orbb.Admin.Areas.Countries.Controllers
{
    [Area("Countries")]
    [SecuredObject("Countries")]
    public class DetailController : DetailBaseController<Country, CountryVM>
    {
        private readonly ICustomerRepository _customerRepository;
        public DetailController(ICustomerRepository customerRepository) : base("Country")
        {
            modelIncludes = new[] { "AvailableLenssets", "AvailableLenssets.Supplier" };
            redirectTo = "/Countries/detail/index/{id}";
            OverviewURL = "/Countries/overview";

            _customerRepository = customerRepository;
        }

        public override CountryVM BeforeUpdate(CountryVM model)
        {
            if (model.AvailableLenssets == null)
            {
                model.AvailableLenssets = new List<LenssetVM>();
            }
            return model;
        }
        public override CountryVM AfterUpdate(CountryVM model, bool success)
        {
            _customerRepository.RemoveUnavailableLensesFromCustomers(model.Id, model.AvailableLenssets);
            return model;
        }

    }
}
