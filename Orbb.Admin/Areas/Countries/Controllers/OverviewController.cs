﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Country;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.ViewModels.Models.Base;

namespace Orbb.Admin.Areas.Countries.Controllers
{
    [Area("Countries")]
    [SecuredObject("Countries")]
    public class OverviewController : BaseOverviewController<Country, CountryVM>
    {
        private readonly ICountryRepository _countryRepository;

        public OverviewController(ICountryRepository countryRepository)
        {
            _countryRepository = countryRepository;
        }
        public IActionResult Countries_Overview([DataSourceRequest]DataSourceRequest request)
        {
            var countries = _countryRepository.GetOverview();
            return Json(countries.ToDataSourceResult(request));
        }
    }
}
