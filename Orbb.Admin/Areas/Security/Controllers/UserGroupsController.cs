﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    [SecuredObject("Usergroups")]
    public class UserGroupsController : BaseController
    {
        readonly IUserGroupsRepository userGroupsOverview;

        public UserGroupsController(IUserGroupsRepository userGroupsOverview)
        {
            this.userGroupsOverview = userGroupsOverview;
        }
        public IActionResult UserGroups_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(userGroupsOverview.GetUserGroupsOverview().ToDataSourceResult(request));
        }

        public IActionResult Index()
        {
            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }
            return View();
        }
    }
}