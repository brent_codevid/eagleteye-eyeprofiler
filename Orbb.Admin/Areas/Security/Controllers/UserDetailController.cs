﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    [SecuredObject("Users")]
    public class UserDetailController : DetailBaseController<User, UserDetailVm>
    {
        private readonly IUserRepository userRepository;

        public UserDetailController(ILookupProvider lookupProvider, IDataService dataService, IUserRepository userRepository) : base("SystemUser")
        {
            modelIncludes = new[] { "UserGroup" };
            redirectTo = "/security/users";
            LookupProvider = lookupProvider;
            DataService = dataService;
            this.userRepository = userRepository;
        }

        public override UserDetailVm BeforeUpdate(UserDetailVm model)
        {
            // set hash if new user
            if (model.Id <= 0)
            {
                var hashSalt = new StringHash().Hash(model.Password);
                model.Password = hashSalt.Hash;
                model.Salt = hashSalt.Salt;
            }
            else if (model.Password != model.OldPassword)
            {
                var hash = new StringHash().HashWithSalt(model.Password, model.Salt);
                model.Password = hash;
            }

            return base.BeforeUpdate(model);
        }

        public override void LoadViewBagData(int id = 0)
        {
            base.LoadViewBagData(id);

            ViewBag.UserGroups = LookupProvider.GetUserGroupsList();
            ViewBag.UserTypes = LookupProvider.GetUserTypesForCombobox();
            ViewBag.PartialLanguages = LookupProvider.GetLanguageTranslationsMenuList();
        }


        public IActionResult DeleteUser(UserDetailVm model)
        {
            redirectTo = "/security/users";
            return Delete(new UserDetailVm() { Id = model.Id });
        }
    }
}