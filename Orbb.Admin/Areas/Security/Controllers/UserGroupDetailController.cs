﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    [SecuredObject("Usergroups")]
    public class UserGroupDetailController : DetailBaseController<UserGroup, UserGroupDetailVM>
    {
        private readonly IUserGroupsRepository userGroupsRepository;

        public UserGroupDetailController(ILookupProvider lookupProvider, IDataService dataService, IUserGroupsRepository userGroupsRepository)
        {
            modelIncludes = new[] { "Permissions" };
            redirectTo = "/security/usergroups";
            LookupProvider = lookupProvider;
            DataService = dataService;
            this.userGroupsRepository = userGroupsRepository;
        }

        public override void BeforeLoad(int id = 0)
        {
            if (id > 0) userGroupsRepository.CreateUserGroupObjectPermissions(id);
            base.BeforeLoad(id);
        }

        public IActionResult UserGroupPermissions_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(userGroupsRepository.GetUserGroupsOverview().ToDataSourceResult(request));
        }
    }
}