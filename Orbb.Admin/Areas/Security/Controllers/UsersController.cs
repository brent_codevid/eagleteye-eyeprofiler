﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    [SecuredObject("Users")]
    public class UsersController : BaseController
    {
        private readonly IUserRepository usersOverview;

        public UsersController(IUserRepository usersOverview)
        {
            this.usersOverview = usersOverview;
        }
        public IActionResult Users_Read([DataSourceRequest]DataSourceRequest request)
        {
            return Json(usersOverview.GetUsersOverview().ToDataSourceResult(request));
        }

        public IActionResult Index()
        {
            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }
            return View();
        }
    }
}