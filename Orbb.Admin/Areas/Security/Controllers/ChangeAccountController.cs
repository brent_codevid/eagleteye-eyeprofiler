﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.ViewEngines;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Orbb.Admin.Extensions;
using Orbb.Admin.Utility.Mvc;
using Orbb.Admin.Utility.SessionStorage;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Security;
using StructureMap.Attributes;
using System.IO;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    [SecuredObject("Security")]
    public class ChangeAccountController : BaseController
    {
        private readonly ICurrent<UserMenuVM> userMenu;

        [SetterProperty]
        public ISessionStorage SessionStorage { get; set; }

        public ChangeAccountController(ICurrent<UserMenuVM> userMenu)
        {
            this.userMenu = userMenu;
        }

        //public string Index()
        //{
        //    Guid code = Guid.NewGuid();
        //    SessionStorage.Set<Guid>("Verification", code);
        //    string html = "<div id=\"code\" value=\""+code+"\">"+code+"</div><br>Geef uw paswoord in <br><input type=\"password\" name=\"passwordField\" id=\"passwordField\">";
        //    return html;
        //}
        public JsonResult CheckPassword(string password, int userIdToEdit)
        {
            if (!userValidation.ValidateUser(currentUser.GetCurrentUser().UserName, password))
                return Json(new { succes = false });

            var sessionKey = GetSessionKey("SystemUser", userIdToEdit);
            var model = SessionStorage.Get<UserDetailVm>(sessionKey);
            var data = new { succes = true, data = RenderPartialViewToString("_EditUserCredentials", model) };
            return Json(data);
        }

        protected string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (StringWriter sw = new StringWriter())
            {
                var engine = HttpContext.RequestServices.GetService(typeof(ICompositeViewEngine)) as ICompositeViewEngine;
                ViewEngineResult viewResult = engine.FindView(ControllerContext, viewName, false);

                var viewContext = new ViewContext(
                    ControllerContext,
                    viewResult.View,
                    ViewData,
                    TempData,
                    sw,
                    new HtmlHelperOptions()
                );

                //Everything is async now!
                var task = viewResult.View.RenderAsync(viewContext);
                task.Wait();

                return sw.GetStringBuilder().ToString();
            }
        }
        public string Change(string username, string pw, string pw2)
        {
            return "";
        }
    }
}