﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Orbb.Admin.Extensions;
using Orbb.Admin.Utility;
using Orbb.Admin.Utility.Mvc;
using Orbb.Admin.ViewModels;
using Orbb.Admin.ViewModels.Security;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.Common.General;
using Orbb.Data.Models.Models.Security;
using Orbb.Data.Models.Models.System;
using Orbb.Data.ViewModels.Models.Globalization;
using Orbb.Data.ViewModels.Models.Security;
using Orbb.Data.ViewModels.Models.System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orbb.Admin.Areas.Security.Controllers
{
    [Area("Security")]
    public class LoginController : BaseController
    {
        private readonly StringEncryption encrypt;
        private readonly IStringLocalizer locString;
        private readonly ICurrent<UserMenuVM> currentUserMenu;
        private readonly IUserRepository userRepository;
        private readonly IMapper mapper;

        public LoginController(IUserValidation userValidation,
            ICurrent<UserMenuVM> currentUserMenu,
            StringEncryption encrypt,
            IStringLocalizer locString,
            IUserRepository userRepository,
            IMapper mapper
            )
        {
            this.userValidation = userValidation;
            this.encrypt = encrypt;
            this.locString = locString;
            this.currentUserMenu = currentUserMenu;
            this.userRepository = userRepository;
            this.mapper = mapper;
        }

        public IActionResult Index(string returnUrl, string languageCode = null)
        {
            if (languageCode != null)
                currentLanguageVM.SetCurrent(new LanguageModel { ShortCode = languageCode });

            ViewBag.LoginLanguages = GetLoginLanguages();

            var model = new LogIn { ReturnUrl = returnUrl };
            return View(model);
        }

        [HttpPost]
        public JsonResult ChangeLanguage(string returnUrl, string languageCode)
        {
            if (languageCode != null)
                currentLanguageVM.SetCurrent(new LanguageModel { ShortCode = languageCode });
            return Json("ok");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogIn(LogIn login, string languageCode = null)
        {
            if (languageCode != null)
                currentLanguageVM.SetCurrent(new LanguageModel { ShortCode = languageCode });

            ViewBag.LoginLanguages = GetLoginLanguages();

            if (!ModelState.IsValid)
            {
                return View("Index", login);
            }

            if (!userValidation.ValidateUser(login.Username, login.Password, out User user))
            {
                var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

                alertMessages.Add(new AlertMessage() { AlertMessageType = Enumerations.AlertMessageTypes.Error, Message = locString["LN_LOGINERROR"] });

                TempData.Set(Constants.AlertMessages, alertMessages);
                return View("Index", login);
            }

            await HttpContext.DoLogin(user.UserName, user.Id);

            if (user.DefaultDivisionId != 0 && user.DefaultDivisionId != null)
                currentDivision.SetCurrent(divisionRepository.GetSingle<Division, DivisionVM>(d => d.Id == user.DefaultDivisionId));
            else
            {
                var def = divisionRepository.GetSingle<Division, DivisionVM>(d => d.IsDefault, new string[] { });
                if (def != null)
                    currentDivision?.SetCurrent(def);
            }

            return RedirectToLocal(login.ReturnUrl);
        }

        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Load(string token, string targetController, string targetAction, int divisionId, string languageCode = "nl")//David zijn versie is zonder controller
        {
            ViewBag.LoginLanguages = GetLoginLanguages();

            currentLanguageVM.SetCurrent(new LanguageModel { ShortCode = languageCode });

            ////MASTER DB CHECK

            ////IF OK
            ////GET API USER FOR CUSTOMER
            //// ELSE VIEW DIE TOONT DAT MEN GEEN TOEGANG GEEFT

            User user = new User() { UserName = "admin", Password = "admin" };

            if (!userValidation.ValidateUser(user.UserName, user.Password, out user))
            {
                var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

                alertMessages.Add(new AlertMessage() { AlertMessageType = Enumerations.AlertMessageTypes.Error, Message = locString["LN_LOGINERROR"] });

                TempData.Set(Constants.AlertMessages, alertMessages);
                return View("Index", new LogIn()); //VIEW DIE TOONT DAT MEN GEEN TOEGANG GEEFT
            }

            await HttpContext.DoLogin(user.UserName, user.Id);

            var def = divisionRepository.GetSingle<Division, DivisionVM>(d => d.Id == divisionId, new[] { "Currency" });
            if (def != null)
                currentDivision?.SetCurrent(def);

            HttpContext.Session.Set("isIFrame", true);
            var isIFrame = HttpContext.Session.Get<bool>("isIFrame");

            return RedirectToAction(targetAction, targetController);
        }

        public IActionResult SwitchDivision(int? divisionId, string returnUrl)
        {
            var id = userRepository.UpdateUserDivision(divisionId);
            var division = userRepository.GetSingle<Division, DivisionVM>(d => d.Id == divisionId);
            currentDivision.SetCurrent(division);

            if (!string.IsNullOrEmpty(returnUrl))
                return RedirectToLocal(returnUrl);
            else
                return RedirectToAction("index", "home", new { area = "" });
        }

        public async Task<IActionResult> Exit()
        {
            userValidation.LogOut();

            await HttpContext.DoLogout();

            HttpContext.Session.Clear();

            // Delete cookies
            currentDivision.Remove();
            currentUser.Remove();
            currentUserMenu.Remove();
            currentLanguageVM.Remove();

            return RedirectToAction("Index", "Login");
        }

        private IList<LanguageVM> GetLoginLanguages()
        {
            return new List<LanguageVM>()
            {
                new LanguageVM
                {
                    Code = "nl-BE",
                    ShortCode ="NL",
                    EnglishDescription="Dutch"
                },
                new LanguageVM
                {
                    Code = "fr-FR",
                    ShortCode ="FR",
                    EnglishDescription="French"
                },
                new LanguageVM
                {
                    Code = "en-US",
                    ShortCode ="EN",
                    EnglishDescription="English"
                }
            };
        }

    }
}
