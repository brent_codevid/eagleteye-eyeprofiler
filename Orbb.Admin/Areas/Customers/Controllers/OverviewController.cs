﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Customers;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.ViewModels.Models.Customers;

namespace Orbb.Admin.Areas.Customers.Controllers
{
    [Area("Customers")]
    [SecuredObject("Customers")]
    public class OverviewController : BaseOverviewController<Customer, CustomerVM>
    {

        private readonly ICustomerRepository _customerRepository;

        public OverviewController(ICustomerRepository customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IActionResult Customers_Overview([DataSourceRequest]DataSourceRequest request)
        {
            return Json(_customerRepository.GetOverview().ToDataSourceResult(request));
        }
    }
}
