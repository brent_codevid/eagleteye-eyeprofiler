﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.BL.Interfaces.Customers;
using Orbb.Common.Security;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.ViewModels.Models.Customers;

namespace Orbb.Admin.Areas.Customers.Controllers
{
    [Area("Customers")]
    [SecuredObject("Customers")]
    public class DetailController : DetailBaseController<Customer, CustomerVM>
    {

        private readonly ICustomerRepository _customerRepository;

        public DetailController(ICustomerRepository customerRepository) : base("Customer")
        {
            modelIncludes = new[] { "UsedLenssets", "UsedLenssets.Supplier" };
            redirectTo = "/Customers/detail/index/{id}";
            OverviewURL = "/Customers/overview";
            _customerRepository = customerRepository;
        }
    }
}
