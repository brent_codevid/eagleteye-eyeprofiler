﻿//using System.Diagnostics;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Builder;
//using Microsoft.AspNetCore.Http;
//using Orbb.BL.Interfaces.Base;
//using Orbb.Data.Common;
//using Orbb.Data.Models.Models.System;
//using Orbb.Data.ViewModels.Models.System;

//namespace Template.Admin.Middleware
//{
//    public class DivisionMiddleware
//    {
//        private readonly RequestDelegate _next;
//        public static readonly object Key = new { Name = "DivisionMiddleware" };

//        public DivisionMiddleware(RequestDelegate next)
//        {
//            _next = next;
//        }

//        public async Task Invoke(HttpContext httpContext, 
//            IDivisionRepository divisionRepository, 
//            ICurrent<DivisionVM> currentDivision)
//        {
//            httpContext.Items[Key] = "DivisionMiddleware";

//            if (httpContext.Request.Path.Value.EndsWith("ChangeDivision"))
//            {
//                var current = currentDivision?.GetCurrent()?.ShortCode;
//                var requested = httpContext.Request.Headers["divisionShortCode"];

//                if (current != requested && requested.ToString() != null)
//                {
//                    var reqDivision = divisionRepository.GetSingle<Division, DivisionVM>(d => d.ShortCode == requested);

//                    if (reqDivision != null)
//                        currentDivision.SetCurrent(reqDivision);
//                }

//                httpContext.Response.Redirect("/");
//            }

//            Debug.WriteLine($"{Key}: Request for {httpContext.Request.Path} received ({httpContext.Request.ContentLength ?? 0} bytes)");

//            await _next(httpContext);
//        }
//    }

//    // Extension method used to add the middleware to the HTTP request pipeline.
//    public static class DivisionMiddlewareExtensions
//    {
//        public static IApplicationBuilder UseDivisionMiddleware(this IApplicationBuilder builder)
//        {
//            return builder.UseMiddleware<DivisionMiddleware>();
//        }
//    }
//}