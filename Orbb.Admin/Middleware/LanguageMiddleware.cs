﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Orbb.Admin.ViewModels;
using Orbb.Data.Common;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Orbb.Admin.Middleware
{
    public class LanguageMiddleware
    {
        private readonly RequestDelegate _next;
        public static readonly object Key = new { Name = "LanguageMiddleware" };

        public LanguageMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext,
            ICurrentUser currentUser,
            ICurrent<LanguageModel> currentLanguage)
        {
            httpContext.Items[Key] = "LanguageMiddleware";

            var language = currentUser?.GetCurrentUser()?.Language;
            if (!string.IsNullOrEmpty(language?.Code))
            {
                var storedLanguage = currentLanguage?.GetCurrent();
                if (storedLanguage == null || storedLanguage.ShortCode != language.Code)
                {
                    var langModel = new LanguageModel { ShortCode = language.Code };
                    currentLanguage.SetCurrent(langModel);
                }
            }

            Debug.WriteLine($"{Key}: Request for {httpContext.Request.Path} received ({httpContext.Request.ContentLength ?? 0} bytes)");

            await _next(httpContext);
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class LanguageMiddlewareExtensions
    {
        public static IApplicationBuilder UseLanguageMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<LanguageMiddleware>();
        }
    }
}