﻿// open details link on grid detail button click
// has to be loaded before document ready! (if not ok here - put js before grids)
function openDetail(e) {
    e.preventDefault();

    var dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    var url = $(e.currentTarget).closest(".k-widget.k-grid").data("url-detail");

    if (!url) {
        showNotification("data-url-detail attribute not defined for grid!", 2);
        return;
    }
    
    if (typeof String.format != "function") {
        showNotification("js file where String.format() function is defined not included!", 2);
    }
    url = String.format(url, dataItem.Id);

    document.location.href = url;
}

jQuery(document).ready(function () {

    $('.CloseModalOnClick').click(
        function (e) {
            var el = e.target;
            while ((el = el.parentElement) && !el.classList.contains('modal'));
            if (el !== null) $(el).modal('hide');
        });

    // add "+" button on .has-add-button class
    if ('.has-add-button') {
        var lastChild = $('.has-add-button').data("colshift-add"); // if the button will be before last child, the value for data-colshift-add will be = 2, else default = 1
        if (!lastChild) {
            lastChild = 1;
        }

        $('.has-add-button .k-filter-row th:nth-last-child(' + lastChild + ')').append('<span id="addButton"> <i class="fas fa-plus"></i> </span>');
        $('.has-add-button #addButton').click(function (e) {
            var url = $(this).closest(".k-widget.k-grid.has-add-button").data("url-add");
            if (url != undefined) {
                document.location.href = url;
            }
        });
    }

    // add "bookmarks" button, onclick open for edit filtered items with "<" and ">" buttons
    $(".has-bookmarks-button .k-filter-row th:last-child").append('<a class="icon fas fa-bookmark" id="multiSelect"></a>');
    $(".has-bookmarks-button #multiSelect").click(function (e) {
        var grid = $(this).closest(".k-widget.k-grid.has-bookmarks-button");
        if (grid) {
            displayFilterResults(grid);
        }
    });

    //dbl click on "ondblclick-opendetail" selector opens detail view
    $(".ondblclick-opendetail").delegate("tbody>tr", "dblclick", function (e) {
        $(e.currentTarget).find(".k-grid-Detail").click()
    });

    //dbl click on "ondblclick-opendetail-new" class
    $(".ondblclick-opendetail-new").delegate("tbody>tr", "dblclick", function (e) {
        $(e.currentTarget).find(".k-grid-Detail").click()
    });

    // grid filter persistance
    if ($(".persist-filter").length > 0) {

        // save filter on beforeunload event
        $(window).bind('beforeunload', function () {
            var grid = getKendoGrid($(".k-widget.k-grid.persist-filter"));
            var filter = grid.dataSource.filter();
            var url = "/data/SaveGridFilter";
            var data = {
                gridName: grid.element[0].id,
                serializedData: kendo.stringify(filter)
            }
            $.ajax({
                url: url,
                data: data,
                type: "POST",
                success: function (e) {
                },
                error: function (e) {
                    showNotification(e.responseText);
                }
            });
        });

        // load grid filter
        var grid = getKendoGrid($($(".k-widget.k-grid.persist-filter")[0]));
        var gridName = grid.element[0].id;
        var url = "/data/LoadGridFilter";
        var data = {
            gridName: gridName
        }
        $.ajax({
            url: url,
            data: data,
            type: "POST",
            success: function (e) {
                var filter = "";
                if (e && e.length > 0) {
                    filter = JSON.parse(e);
                }
                grid.dataSource.filter(filter);
            },
            error: function (e) {
                showNotification(e.responseText);
            }
        });

    }

    $(".detailContainerHeader").css("display", "none");
    /*
    if (window.location.href.replace("http://localhost:5000/customer/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $("#tabBarAddresses").click(function () {

            $(".k-grid-toolbar").children().css("display", "none");
            $(".k-grid-toolbar").children().first().css("display", "inline-block");

        });

        $("#tabBarCustomerDiscountSets").click(function () {

            $(".k-grid-toolbar").children().css("display", "none");
            $(".k-grid-toolbar").children('#addButton').css("display", "inline-block");

        });

    }

    else if (window.location.href.replace("http://localhost:5000/supplier/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $(".TabBarStyle").children().first().css("display", "block");
        $(".TabBarStyle").children().css("display", "none");
        $("#commandButtons").append('<button type="submit" id="submitButton" class="btn btn-default"></button><button type="button" id="cancelButton" class="btn btn-default"></button>');
        $("#cancelButton").css("clear", "both");
        $("#cancelButton").click(function () { document.location.href = "/supplier/overview/" });

    }

    else if (window.location.href.replace("http://localhost:5000/article/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $(".TabBarStyle").children().first().css("display", "block");
        $(".TabBarStyle").children().css("display", "none");
        $("#commandButtons").append('<button type="submit" id="submitButton" class="btn btn-default"></button><button type="button" id="cancelButton" class="btn btn-default"></button>');
        $("#cancelButton").css("clear", "both");
        $("#cancelButton").click(function () { document.location.href = "/article/overview/" });

    }

    else if (window.location.href.replace("http://localhost:5000/user/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $(".TabBarStyle").children().first().css("display", "block");
        $(".TabBarStyle").children().css("display", "none");
        $("#commandButtons").append('<button type="submit" id="submitButton" class="btn btn-default"></button><button type="button" id="cancelButton" class="btn btn-default"></button>');
        $("#cancelButton").css("clear", "both");
        $("#cancelButton").click(function () { document.location.href = "/user/overview/" });

    }
    else if (window.location.href.replace("http://localhost:5000/salesorder/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $(".TabBarStyle").children().first().css("display", "block");
        $(".TabBarStyle").children().css("display", "none");
        $("#commandButtons").append('<button type="submit" id="submitButton" class="btn btn-default"></button><button type="button" id="cancelButton" class="btn btn-default"></button>');
        $("#cancelButton").css("clear", "both");
        $("#cancelButton").click(function () { document.location.href = "/salesorder/overview/" });

    }
    else if (window.location.href.replace("http://localhost:5000/purchaseorder/detail/index/", "") == 0) {

        $(".detailContainerHeader").css("display", "none");
        $(".detailContainerContentDetails").removeClass("fixedheight93");
        $(".TabBarStyle").children().first().css("display", "block");
        $(".TabBarStyle").children().css("display", "none");
        $("#commandButtons").append('<button type="submit" id="submitButton" class="btn btn-default"></button><button type="button" id="cancelButton" class="btn btn-default"></button>');
        $("#cancelButton").css("clear", "both");
        $("#cancelButton").click(function () { document.location.href = "/purchaseorder/overview/" });

    }

    else {
    */
        $("#tabBarAddresses").click(function () {

            $("#tabBarAddresses .k-grid-toolbar").children().css("display", "none")
            $("#tabBarAddresses .k-grid-toolbar").children().first().css("display", "inline-block")


        });

        $("#tabBarCustomerDiscountSets").click(function () {

            $("#tabBarCustomerDiscountSets .k-grid-toolbar").children().css("display", "none");
            $("#tabBarCustomerDiscountSets .k-grid-toolbar").children('#addButton').css("display", "inline-block");

        });

        $(".detailContainerHeader").css("display", "block");
    /*
    }
    */
});

function displayFilterResults(grid) {
    var action = grid.data("url-multiselect");

    // Gets the data source from the grid.
    var dataSource = $(grid).data("kendoGrid").dataSource;

    // Gets the full set of data from the data source
    var allData = dataSource.data();

    if (allData.length == 0) {
        showNotification($.validator.messages.General.G_NO_SELECTION);
        return;
    }
    var rowIds = "";
    // Output the results
    $.each(allData, function (index, item) {
        rowIds += item.Id + ",";
    });

    var frm = document.getElementById('listSelectionForm') || null;
    if (frm) {
        frm.action = action
        var rowIdsField = document.getElementsByName('Ids')
        $(rowIdsField).val(rowIds.slice(0, -1));
        $(frm).submit();
    }
    else {
        showNotification($.validator.messages.General.G_MISSING_ELEMENT);
    }
}

function showListItem(newItem, action) {

    var res = $("#Ids").val().split(',');
    if (res.length == newItem)
        newItem = 0;
    else if (newItem == -1)
        newItem = res.length - 1;

    var frm = document.getElementById('listSelectionForm') || null;
    if (frm) {
        frm.action = action
        $("#currentListItem").val(newItem);
        $(frm).submit();
    }
    else {
        showNotification($.validator.messages.General.G_MISSING_ELEMENT);
    }
}