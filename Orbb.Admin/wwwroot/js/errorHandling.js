﻿function error_handler(e) {
    if (e.errors) {
        var t = this;
        //var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ('errors' in value) {
                if (key == 'Delete') {
                    t.cancelChanges();
                }
                $.each(value.errors, function (idx, val) {
                    showNotification(val, 2);// message += this + "\n";
                });
            }
        });
        //alert(message);
    }
}

function alertmessage_handler(e) {
    for (index = 0; index < e.length; ++index) {
        showNotification(e[index].Message, e[index].AlertMessageType);
    }
}