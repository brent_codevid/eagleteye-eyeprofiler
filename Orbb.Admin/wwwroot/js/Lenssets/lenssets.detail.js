﻿var map = new Map();

$(function () {
    $(".detailContainerContentTabBar").kendoTabStrip({
        animation: { open: { effects: "fadeIn" } },
        contentUrls: [
            null,
            null
        ]
    }).data("kendoTabStrip");

    const id = $("#Id").val();

    $(".TabBarStyle").append('<li class="tabBarCommandButtons"> </li>');

    if (id > 0) {
        $(".tabBarCommandButtons").append('<button type="button" id="canDeleteButton" class="icon far fa-trash-alt iconOnlyButtonRed" onclick="deleteLens()"></button>');
    } else {
        $(".hideOnNew").hide();
    }

    // create Editor from textarea HTML element with default set of tools
    $(".kendo-editor").kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        },
        encoded: false,
        change: function () {
            processChanges();
        }
    });

    BindPartialViewToRadioButtons();
});

$(document).ready(function () {
    fixDelimitersDoubles();
    $(".pasteModal").on("shown.bs.modal",
        function () {
            const textarea = $("#excelPasteArea");
            textarea.focus();
        });

    for (let i = 1; i <= 5; i++) {
        for (let j = 1; j <= 4; j++) {
            $(`#CP${i}_StandardVal${j}Div`).on("changeData", function (e) { setComponentVisibility(e); });
        }
    }

    for (let i = 1; i <= 5; i++) {
        $(`#LzInLensName${i}CorrectionDiv`).on("changeData", function (e) { setComponentVisibility(e); });
    }

    if (typeof $("#CP1_Category")[0].value !== "undefined" && $("#CP1_Category")[0].value !== "") {
        map.set("CP1", parseInt($("#CP1_Category")[0].value));
    } else {
        map.set("CP1", 0);
    }
    if (typeof $("#CP2_Category")[0].value !== "undefined" && $("#CP2_Category")[0].value !== "") {
        map.set("CP2", parseInt($("#CP2_Category")[0].value));
    } else {
        map.set("CP2", 0);
    }
    if (typeof $("#CP3_Category")[0].value !== "undefined" && $("#CP3_Category")[0].value !== "") {
        map.set("CP3", parseInt($("#CP3_Category")[0].value));
    } else {
        map.set("CP3", 0);
    }
    if (typeof $("#CP4_Category")[0].value !== "undefined" && $("#CP4_Category")[0].value !== "") {
        map.set("CP4", parseInt($("#CP4_Category")[0].value));
    } else {
        map.set("CP4", 0);
    }
    if (typeof $("#CP5_Category")[0].value !== "undefined" && $("#CP5_Category")[0].value !== "") {
        map.set("CP5", parseInt($("#CP5_Category")[0].value));
    } else {
        map.set("CP5", 0);
    }

    for (let i = 1; i <= 4; i++) {
        var divName = `#CP1_StandardVal${i}Div`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP1") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }

        divName = `#CP2_StandardVal${i}Div`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP2") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }

        divName = `#CP3_StandardVal${i}Div`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP3") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }

        divName = `#CP4_StandardVal${i}Div`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP4") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }

        divName = `#CP5_StandardVal${i}Div`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP5") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }
    }

    for (let i = 1; i <= 5; i++) {
        divName = `#LzInLensName${i}CorrectionDiv`;
        $(divName).data("invisCounter", 0);
        if (map.get("CP2") < i) {
            $(divName).data("invisCounter", $(divName).data("invisCounter") + 1).trigger("changeData");
        }
    }


    //initialise
    $("#tabBarCP2ToCP1Items").data("invisCounter", 0);
    $("#tabBarCP2ToCP1Items").on("changeData", function (e) { setComponentVisibility(e); });

    $("#HvidRangeDiv").data("invisCounter", 0);
    $("#HvidRangeDiv").on("changeData", function (e) { setComponentVisibility(e); });

    $("#DiameterDiv").data("invisCounter", 0);
    $("#DiameterDiv").on("changeData", function (e) { setComponentVisibility(e); });
   /* $("#DiameterIntervalDiv").data("invisCounter", 0);
    $("#DiameterIntervalDiv").on("changeData", function (e) { setComponentVisibility(e); });*/

    $("#hideBcUnit").data("invisCounter", 0);
    $("#hideBcUnit").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP1_StandardValsDiv").data("invisCounter", 0);
    $("#CP1_StandardValsDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP1_SemiChordLengthDiv").data("invisCounter", 0);
    $("#CP1_SemiChordLengthDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP1_ChordPercentDiv").data("invisCounter", 0);
    $("#CP1_ChordPercentDiv").on("changeData", function (e) { setComponentVisibility(e); });
   
    $("#NotUsingCP2ToCP1Div2").data("invisCounter", 0);
    $("#NotUsingCP2ToCP1Div2").on("changeData", function (e) { setComponentVisibility(e); });

    $("#CP2_SemiChordLengthDiv").data("invisCounter", 0);
    $("#CP2_SemiChordLengthDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP2_ChordPercentDiv").data("invisCounter", 0);
    $("#CP2_ChordPercentDiv").on("changeData", function (e) { setComponentVisibility(e); });

    $("#CP3_SemiChordLengthDiv").data("invisCounter", 0);
    $("#CP3_SemiChordLengthDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP3_ChordPercentDiv").data("invisCounter", 0);
    $("#CP3_ChordPercentDiv").on("changeData", function (e) { setComponentVisibility(e); });

    $("#CP4_SemiChordLengthDiv").data("invisCounter", 0);
    $("#CP4_SemiChordLengthDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP4_ChordPercentDiv").data("invisCounter", 0);
    $("#CP4_ChordPercentDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP4_LzPercentageDiv").data("invisCounter", 0);
    $("#CP4_LzPercentageDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP4_StandardValsDiv").data("invisCounter", 0);
    $("#CP4_StandardValsDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP4_DisplayLandingZoneFields").data("invisCounter", 0);
    $("#CP4_DisplayLandingZoneFields").on("changeData", function (e) { setComponentVisibility(e); });

    $("#CP2_IncreaseClearanceVis").data("invisCounter", 0);
    $("#CP2_IncreaseClearanceVis").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP3_IncreaseClearanceVis").data("invisCounter", 0);
    $("#CP3_IncreaseClearanceVis").on("changeData", function (e) { setComponentVisibility(e); });

    $("#CP5_SemiChordLengthDiv").data("invisCounter", 0);
    $("#CP5_SemiChordLengthDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP5_ChordPercentDiv").data("invisCounter", 0);
    $("#CP5_ChordPercentDiv").on("changeData", function (e) { setComponentVisibility(e); });
    $("#CP5_IncreaseClearanceVis").data("invisCounter", 0);
    $("#CP5_IncreaseClearanceVis").on("changeData", function (e) { setComponentVisibility(e); });

    $("#LzInLensNameCorrectionDiv").data("invisCounter", 0);
    $("#LzInLensNameCorrectionDiv").on("changeData", function (e) { setComponentVisibility(e); }); 

    $("#DynamicMerDiv").data("invisCounter", 0);
    $("#DynamicMerDiv").on("changeData", function (e) { setComponentVisibility(e); });

    $(".hideCP1Details").each(function () {
        $(this).data("invisCounter", 0);
    });
    $(".hideCP1Details").each(function () {
        $(this).on("changeData", function (e) { setComponentVisibility(e); });
    });

    $(".hideCP2Details").each(function () {
        $(this).data("invisCounter", 0);
    });
    $(".hideCP2Details").each(function () {
        $(this).on("changeData", function (e) { setComponentVisibility(e); });
    });

    $(".hideCP3Details").each(function () {
        $(this).data("invisCounter", 0);
    });
    $(".hideCP3Details").each(function () {
        $(this).on("changeData", function (e) { setComponentVisibility(e); });
    });

    $(".hideCP4Details").each(function () {
        $(this).data("invisCounter", 0);
    });
    $(".hideCP4Details").each(function () {
        $(this).on("changeData", function (e) { setComponentVisibility(e); });
    });

    $(".hideCP5Details").each(function () {
        $(this).data("invisCounter", 0);
    });
    $(".hideCP5Details").each(function () {
        $(this).on("changeData", function (e) { setComponentVisibility(e); });
    });

    checkInitialVisibility();

    /*$("#DiameterInterval").change(function () {
        if (this.checked) {
            $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") + 1).trigger("changeData");
            $("#DiameterIntervalDiv").data("invisCounter", $("#DiameterIntervalDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") - 1).trigger("changeData");
            $("#DiameterIntervalDiv").data("invisCounter", $("#DiameterIntervalDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });*/

    $("#UseHvidForDiameters").change(function () {
    if (this.checked) {
        $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") + 1).trigger("changeData");
        $("#HvidRangeDiv").data("invisCounter", $("#HvidRangeDiv").data("invisCounter") - 1).trigger("changeData");
    } else {
        $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") - 1).trigger("changeData");
        $("#HvidRangeDiv").data("invisCounter", $("#HvidRangeDiv").data("invisCounter") + 1).trigger("changeData");
    }
    });

    $("#CP1_MeasurementUnit_2").change(function () {
        $("#hideBcUnit").data("invisCounter", $("#hideBcUnit").data("invisCounter") - 1).trigger("changeData");
        $("#CP1_RelativeSag_False").prop("checked", true);
    });
    $("#CP1_MeasurementUnit_1").change(function () {
        $("#CP1_RelativeSag_True").prop("checked", false);
        $("#CP1_RelativeSag_False").prop("checked", false);
        $("#hideBcUnit").data("invisCounter", $("#hideBcUnit").data("invisCounter") + 1).trigger("changeData");
    });

    $("#CP1_RelativeSag").change(function () {
        if (this.checked) {
            $("#CP1_StandardValsDiv").data("invisCounter", $("#CP1_StandardValsDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP1_StandardValsDiv").data("invisCounter", $("#CP1_StandardValsDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#CP1_RelativeChordLength").change(function () {
        if (this.checked) {
            $("#CP1_SemiChordLengthDiv").data("invisCounter", $("#CP1_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP1_ChordPercentDiv").data("invisCounter", $("#CP1_ChordPercentDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP1_SemiChordLengthDiv").data("invisCounter", $("#CP1_SemiChordLengthDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP1_ChordPercentDiv").data("invisCounter", $("#CP1_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#CP2_RelativeChordLength").change(function () {
        if (this.checked) {
            $("#CP2_SemiChordLengthDiv").data("invisCounter", $("#CP2_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP2_ChordPercentDiv").data("invisCounter", $("#CP2_ChordPercentDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP2_SemiChordLengthDiv").data("invisCounter", $("#CP2_SemiChordLengthDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP2_ChordPercentDiv").data("invisCounter", $("#CP2_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#CP3_RelativeChordLength").change(function () {
        if (this.checked) {
            $("#CP3_SemiChordLengthDiv").data("invisCounter", $("#CP3_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP3_ChordPercentDiv").data("invisCounter", $("#CP3_ChordPercentDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP3_SemiChordLengthDiv").data("invisCounter", $("#CP3_SemiChordLengthDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP3_ChordPercentDiv").data("invisCounter", $("#CP3_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#CP4_RelativeChordLength").change(function () {
        if (this.checked) {
            $("#CP4_SemiChordLengthDiv").data("invisCounter", $("#CP4_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP4_ChordPercentDiv").data("invisCounter", $("#CP4_ChordPercentDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP4_SemiChordLengthDiv").data("invisCounter", $("#CP4_SemiChordLengthDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP4_ChordPercentDiv").data("invisCounter", $("#CP4_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#CP4_CalculateLz").change(function () {
        if (this.checked) {
            $("#CP4_LzPercentageDiv").data("invisCounter", $("#CP4_LzPercentageDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP4_StandardValsDiv").data("invisCounter", $("#CP4_StandardValsDiv").data("invisCounter") + 1).trigger("changeData");
        } else {
            $("#CP4_LzPercentageDiv").data("invisCounter", $("#CP4_LzPercentageDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP4_StandardValsDiv").data("invisCounter", $("#CP4_StandardValsDiv").data("invisCounter") - 1).trigger("changeData");
        }
    });

    $("#CP4_DisplayLandingZoneStepType_2").change(function () {
        $("#CP4_DisplayLandingZoneFields").data("invisCounter", $("#CP4_DisplayLandingZoneFields").data("invisCounter") - 1).trigger("changeData");
    });
    $("#CP4_DisplayLandingZoneStepType_1").change(function () {
        $("#CP4_DisplayLandingZoneFields").data("invisCounter", $("#CP4_DisplayLandingZoneFields").data("invisCounter") + 1).trigger("changeData");
    });

    $("#CP5_RelativeChordLength").change(function () {
        if (this.checked) {
            $("#CP5_SemiChordLengthDiv").data("invisCounter", $("#CP5_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
            $("#CP5_ChordPercentDiv").data("invisCounter", $("#CP5_ChordPercentDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#CP5_SemiChordLengthDiv").data("invisCounter", $("#CP5_SemiChordLengthDiv").data("invisCounter") - 1).trigger("changeData");
            $("#CP5_ChordPercentDiv").data("invisCounter", $("#CP5_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });


    $("#IncludeLzInLensName").change(function () {
        if (this.checked) {
            $("#LzInLensNameCorrectionDiv").data("invisCounter", $("#LzInLensNameCorrectionDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#LzInLensNameCorrectionDiv").data("invisCounter", $("#LzInLensNameCorrectionDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    $("#UseNewMer1Selector").change(function () {
        if (this.checked) {
            $("#DynamicMerDiv").data("invisCounter", $("#DynamicMerDiv").data("invisCounter") - 1).trigger("changeData");
        } else {
            $("#DynamicMerDiv").data("invisCounter", $("#DynamicMerDiv").data("invisCounter") + 1).trigger("changeData");
        }
    });

    

    $("#UseCP1").change(function () {
        if (this.checked) {
            $(".hideCP1Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $(".hideCP1Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });
    $("#UseCP2").change(function () {
        if (this.checked) {
            $(".hideCP2Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $(".hideCP2Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });
    $("#CP2_IncreaseClearance").change(function () {
        if (this.checked) {
            $("#CP2_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $("#CP2_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });

    $("#UseCP3").change(function () {
        if (this.checked) {
            $(".hideCP3Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $(".hideCP3Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });
    $("#CP3_IncreaseClearance").change(function () {
        if (this.checked) {
            $("#CP3_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $("#CP3_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });
    


    $("#UseCP4").change(function () {
        if (this.checked) {
            $(".hideCP4Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $(".hideCP4Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });

    $("#UseCP5").change(function () {
        if (this.checked) {
            $(".hideCP5Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $(".hideCP5Details").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });
    $("#CP5_IncreaseClearance").change(function () {
        if (this.checked) {
            $("#CP5_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        } else {
            $("#CP5_IncreaseClearanceVis").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        }
    });

    $("#UseCP2ToCP1List").change(function () {
        if (this.checked) {
            $("#tabBarCP2ToCP1Items").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
            $("#NotUsingCP2ToCP1Div2").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
        } else {
            $("#tabBarCP2ToCP1Items").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") + 1).trigger("changeData");
            })
            $("#NotUsingCP2ToCP1Div2").each(function () {
                $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
            })
        }
    });

    $(function () {
        $('[data-toggle="namingTooltip"]').tooltip({ html: true });
    });
});

function checkInitialVisibility() {
    /*if ($("#DiameterInterval")[0].checked) {
        $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#DiameterIntervalDiv").data("invisCounter", $("#DiameterIntervalDiv").data("invisCounter") + 1).trigger("changeData");
    }*/

    if ($("#UseHvidForDiameters")[0].checked) {
        $("#DiameterDiv").data("invisCounter", $("#DiameterDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#HvidRangeDiv").data("invisCounter", $("#HvidRangeDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP1_MeasurementUnit_1")[0].checked) {
        $("#hideBcUnit").data("invisCounter", $("#hideBcUnit").data("invisCounter") + 1).trigger("changeData");
    } else if (!($("#CP1_MeasurementUnit_2")[0].checked)) {
        $("#CP1_MeasurementUnit_1")[0].checked = true;
        $("#hideBcUnit").data("invisCounter", $("#hideBcUnit").data("invisCounter") + 1).trigger("changeData");
    }

    

    if (!$("#CP1_RelativeSag")[0].checked) {
        $("#CP1_StandardValsDiv").data("invisCounter", $("#CP1_StandardValsDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP1_RelativeChordLength")[0].checked) {
        $("CP1_SemiChordLengthDiv").data("invisCounter", $("#CP1_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP1_ChordPercentDiv").data("invisCounter", $("#CP1_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP2_RelativeChordLength")[0].checked) {
        $("#CP2_SemiChordLengthDiv").data("invisCounter", $("#CP2_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP2_ChordPercentDiv").data("invisCounter", $("#CP2_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP3_RelativeChordLength")[0].checked) {
        $("#CP3_SemiChordLengthDiv").data("invisCounter", $("#CP3_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP3_ChordPercentDiv").data("invisCounter", $("#CP3_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP4_RelativeChordLength")[0].checked) {
        $("#CP4_SemiChordLengthDiv").data("invisCounter", $("#CP4_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP4_ChordPercentDiv").data("invisCounter", $("#CP4_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP4_CalculateLz")[0].checked) {
        $("#CP4_StandardValsDiv").data("invisCounter", $("#CP4_StandardValsDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP4_LzPercentageDiv").data("invisCounter", $("#CP4_LzPercentageDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP4_DisplayLandingZoneStepType_1")[0].checked) {
        $("#CP4_DisplayLandingZoneFields").data("invisCounter", $("#CP4_DisplayLandingZoneFields").data("invisCounter") + 1).trigger("changeData");
    } else if (!($("#CP4_DisplayLandingZoneStepType_2")[0].checked)) {
        $("#CP4_DisplayLandingZoneStepType_1")[0].checked = true;
        $("#CP4_DisplayLandingZoneFields").data("invisCounter", $("#CP4_DisplayLandingZoneFields").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#CP5_RelativeChordLength")[0].checked) {
        $("#CP5_SemiChordLengthDiv").data("invisCounter", $("#CP5_SemiChordLengthDiv").data("invisCounter") + 1).trigger("changeData");
    } else {
        $("#CP5_ChordPercentDiv").data("invisCounter", $("#CP5_ChordPercentDiv").data("invisCounter") + 1).trigger("changeData");
    }

    if ($("#IncludeLzInLensName")[0].checked) {
    } else {
        $("#LzInLensNameCorrectionDiv").data("invisCounter", $("#LzInLensNameCorrectionDiv").data("invisCounter") + 1)
            .trigger("changeData");
    }

    if ($("#UseNewMer1Selector")[0].checked) {
    } else {
        $("#DynamicMerDiv").data("invisCounter", $("#DynamicMerDiv").data("invisCounter") + 1)
            .trigger("changeData");
    }

    if ($("#UseCP1")[0].checked) {
    } else {
        $(".hideCP1Details").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
    }
    if ($("#UseCP2")[0].checked) {
    } else {
        $(".hideCP2Details").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
    }
    if ($("#UseCP3")[0].checked) {
    } else {
        $(".hideCP3Details").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
    }
    if ($("#UseCP4")[0].checked) {
    } else {
        $(".hideCP4Details").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
    }
    if ($("#UseCP5")[0].checked) {
    } else {
        $(".hideCP5Details").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
    }
    if ($("#UseCP2ToCP1List")[0].checked) {
    } else {
        $("#tabBarCP2ToCP1Items").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") + 1)
                .trigger("changeData")
        });
        $("#NotUsingCP2ToCP1Div2").each(function () {
            $(this).data("invisCounter", $(this).data("invisCounter") - 1).trigger("changeData");
        })
    }

    if (!$("#CP2_IncreaseClearance")[0].checked) {
        $("#CP2_IncreaseClearanceVis").data("invisCounter", $("#CP2_IncreaseClearanceVis").data("invisCounter") + 1).trigger("changeData");
    }
    if (!$("#CP3_IncreaseClearance")[0].checked) {
        $("#CP3_IncreaseClearanceVis").data("invisCounter", $("#CP3_IncreaseClearanceVis").data("invisCounter") + 1).trigger("changeData");
    }
    if (!$("#CP5_IncreaseClearance")[0].checked) {
        $("#CP5_IncreaseClearanceVis").data("invisCounter", $("#CP5_IncreaseClearanceVis").data("invisCounter") + 1).trigger("changeData");
    }
}

function setComponentVisibility(e) {
    if ($(e.target).data("invisCounter") > 0) {
        $(e.target).hide();
    } else {
        $(e.target).show();
    }
}

function deleteLens() {
    showConfirmationDialog(
        $.validator.messages.General.G_DELETE,
        function () {
            const form = $("#canDeleteButton").closest("form")[0];
            if (form == null || form == "undefined" || form == "") {
                return;
            }
            var action = $(form).attr("action").toLowerCase();

            action = action.replace("update", "delete");
            if (action.indexOf("delete") > -1) {
                $(form).attr("action", action);
            }
            $(form).submit();
        },
        function () {
        });
}


var currentIndex = 0;
var firstTime = true;
var newRow = false;
var currentItems = 0;
var newId = 0;

var currentPage = 1;

function onDataBound() {
    const page = $("#grid").data("kendoGrid").dataSource.page();
    if (firstTime) {
        rebuildFormList();
        firstTime = false;
        checkEmptyList();
    } else if (page != currentPage) {
        currentPage = page;
    } else {
        rebuildFormList();
        processChanges();
        checkEmptyList();
    }
}

function onCellClose(e) {
    var uid = e.model.uid;
    var list = document.getElementById("CP2ToCP1List");
    var exists = document.getElementById(`CP2ToCP1List_${uid}__Id`);
    var input;

    //if a new entry is added
    if (exists === null) {
        input = document.createElement('input');
        list.appendChild(input);
        input.type = "number";
        input.id = `CP2ToCP1List_${uid}__Id`;
        input.name = `CP2ToCP1List[${currentIndex}].Id`;
        input.value = newId;
        input.class = "form-control";


        input = document.createElement("input");
        list.appendChild(input);
        input.type = "text";
        input.id = `CP2ToCP1List_${uid}__CP2Value`;
        input.name = `CP2ToCP1List[${currentIndex}].CP2Value`;
        input.value = kendo.toString(e.model.CP2Value, "n");
        input.class = "form-control";


        input = document.createElement("input");
        list.appendChild(input);
        input.type = "text";
        input.id = `CP2ToCP1List_${uid}__CP1Value`;
        input.name = `CP2ToCP1List[${currentIndex}].CP1Value`;
        input.value = kendo.toString(e.model.CP1Value, "n");
        input.class = "form-control";

        currentIndex = currentIndex + 1;
        currentItems = currentItems + 1;
        newId = newId - 1;

        processChanges();
    }
    //if an existing entry is edited (doesn't have to be changed)
    else if (exists) {
        input = document.getElementById(`CP2ToCP1List_${uid}__CP2Value`);
        if (input.value !== kendo.toString(e.model.CP2Value, "n")) {
            input.value = kendo.toString(e.model.CP2Value, "n");
            processChanges();
        }

        input = document.getElementById(`CP2ToCP1List_${uid}__CP1Value`);
        if (input.value !== kendo.toString(e.model.CP1Value, "n")) {
            input.value = kendo.toString(e.model.CP1Value, "n");
            processChanges();
        }
    }

    checkEmptyList();
}

function onRowRemove(e) {
    //if an entry is destroyed
    rebuildFormList(e.model.uid);
    processChanges();
    checkEmptyList();
}

function checkEmptyList() {
    const list = document.getElementById("CP2ToCP1List");
    var input;
    if (currentItems === 0) {
        input = document.createElement("input");
        list.appendChild(input);
        input.type = "text";
        input.id = "EmptyCP2ToCP1List";
        input.name = "CP2ToCP1List";
        input.value = "System.Collections.Generic.List`1[Orbb.Data.ViewModels.Models.Lens.CP2ToCP1ItemVM]";
    } else {
        input = document.getElementById("EmptyCP2ToCP1List");
        if (input) {
            input.remove();
        }
    }
}

function rebuildFormList(ignoreUid) {
    const list = document.getElementById("CP2ToCP1List");
    while (list.firstChild) {
        list.removeChild(list.firstChild);
    }
    currentIndex = 0;
    currentItems = 0;
    newId = 0;
    const data = $("#grid").data("kendoGrid").dataSource.data();
    for (let i = 0; i < data.length; i++) {
        const d = data[i];
        const uid = d.uid;
        if (uid !== ignoreUid) {
            let input = document.createElement("input");
            list.appendChild(input);
            input.type = "number";
            input.id = `CP2ToCP1List_${uid}__Id`;
            input.name = `CP2ToCP1List[${currentIndex}].Id`;
            if (d.Id <= 0) {
                input.value = newId;
                newId = newId - 1;
            } else {
                input.value = d.Id;
            }

            input = document.createElement("input");
            list.appendChild(input);
            input.type = "text";
            input.id = `CP2ToCP1List_${uid}__CP2Value`;
            input.name = `CP2ToCP1List[${currentIndex}].CP2Value`;
            input.value = kendo.toString(d.CP2Value, "n");


            input = document.createElement("input");
            list.appendChild(input);
            input.type = "text";
            input.id = `CP2ToCP1List_${uid}__CP1Value`;
            input.name = `CP2ToCP1List[${currentIndex}].CP1Value`;
            input.value = kendo.toString(d.CP1Value, "n");

            currentItems = currentItems + 1;
            currentIndex = currentIndex + 1;
        }
    }
}

function addNewRowCurvatureDesign(gridObj) {
    addNewRow(gridObj)

    var cp = $(gridObj).attr('id').split('CP')[1]
    var inputsToAdd = $('#CP' + cp + '_CurvatureSectionCount').val() - 3; // 3 is default template length
    var kendoGrid = $(gridObj).data("kendoGrid");

    if (inputsToAdd > 0) {
        var currentOrderNr = 3;
        for (var i = 0; i < inputsToAdd; i++) {
            addInputFieldRow(gridObj, 0, currentOrderNr, kendoGrid.dataSource._data[0].uid);
            currentOrderNr++;
        }
    } else if (inputsToAdd < 0) {
        for (var i = 0; i > inputsToAdd; i--) {
            $("#RadiiContainer_" + kendoGrid.dataSource._data[0].uid + " input:last-child").remove();
            kendoGrid.dataSource._data[0].Radii.pop();
            $("#ExcentricitiesContainer_" + kendoGrid.dataSource._data[0].uid + " input:last-child").remove();
            kendoGrid.dataSource._data[0].Excentricities.pop();
        }
    }
}
function addNewRow(gridObj) {
    const grid = $(gridObj).data("kendoGrid");
    grid.addRow();
    processChanges();
}

function error_handler(e) {
    if (e.errors) {
        var message = "Errors:\n";
        $.each(e.errors, function (key, value) {
            if ("errors" in value) {
                $.each(value.errors, function () {
                    message += this + "\n";
                });
            }
        });
        alert(message);
    }
}

function nthIndex(str, pat, n) {
    const l = str.length;
    var i = -1;
    while (n-- && i++ < l) {
        i = str.indexOf(pat, i);
        if (i < 0) break;
    }
    return i;
}

function onDropDownClosed() {
    var name = this.element[0].name;
    name = name.substring(0, name.indexOf("."));
    const id = this.element[0].value;

    const resultNumber = Number(id);
    var i;
    const oldResult = map.get(name);
    var divI;
    if (oldResult > resultNumber) {
        for (i = resultNumber + 1; i <= oldResult; i++) {
            divI = document.getElementById(name + "_StandardVal" + i + "Div");
            $(divI).data("invisCounter", $(divI).data("invisCounter") + 1).trigger("changeData");
            divI = document.getElementById("LzInLensName" + i + "CorrectionDiv");
            $(divI).data("invisCounter", $(divI).data("invisCounter") + 1).trigger("changeData");
        }
    } else if (oldResult < resultNumber) {
        for (i = oldResult + 1; i <= resultNumber; i++) {
            divI = document.getElementById(name + "_StandardVal" + i + "Div");
            $(divI).data("invisCounter", $(divI).data("invisCounter") - 1).trigger("changeData");
            divI = document.getElementById("LzInLensName" + i + "CorrectionDiv");
            $(divI).data("invisCounter", $(divI).data("invisCounter") - 1).trigger("changeData");
        }
    }
    map.set(name, resultNumber);
}

function fixValidationRequiredBug() {
    for (let index = 0; index < document.getElementsByClassName("form-control").length; index++) {
        $(`#${document.getElementsByClassName("form-control")[index].id}`).valid();
    }
}

function checkJqueryLoaded() {
    if (window.jQuery) {
        window.setTimeout("fixValidationRequiredBug();", 1000);
    }
    else {
        window.setTimeout("checkJqueryLoaded();", 100);
    }
}
checkJqueryLoaded();

function fixDelimitersDoubles() {
    for (let index = 0; index < document.getElementsByClassName("double").length; index++) {
        const element = $(`#${document.getElementsByClassName("double")[index].id}`)[0];
        if (element.addEventListener) {
            element.addEventListener("change", function (e) {
                e = e || event;
                if (kendo.culture().numberFormat["."] === ",")
                    e.target.value = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                else
                    e.target.value = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
            }, false);
        }
        else if (element.attachEvent) { //IE fallback
            element.attachEvent("onchange", function (e) {
                e = e || event;
                if (kendo.culture().numberFormat["."] === ",")
                    e.target.value = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                else
                    e.target.value = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
            });
        }
    }
}

function fixDelimitersDouble(element) {
    if (element.addEventListener) {
        element.addEventListener("change", function (e) {
            e = e || event;
            if (kendo.culture().numberFormat["."] === ",")
                e.target.value = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
            else
                e.target.value = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
        }, false);
    }
    else if (element.attachEvent) { //IE fallback
        element.attachEvent("onchange", function (e) {
            e = e || event;
            if (kendo.culture().numberFormat["."] === ",")
                e.target.value = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
            else
                e.target.value = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
        });
    }
}

function fixDelimitersIntegers() {
    for (let index = 0; index < document.getElementsByClassName("integer").length; index++) {
        const element = $(`#${document.getElementsByClassName("integer")[index].id}`)[0];
        if (element.addEventListener) {
            element.addEventListener("change", function (e) {
                e = e || event;
                var v;
                var n;
                if (kendo.culture().numberFormat["."] === ",") {
                    v = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                    n = v.indexOf(",");
                }
                else {
                    v = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
                    n = v.indexOf(".");
                }
                e.target.value = v.substring(0, n !== -1 ? n : v.length);
            }, false);
        }
        else if (element.attachEvent) { //IE fallback
            element.attachEvent("onchange", function (e) {
                e = e || event;
                var v;
                var n;
                if (kendo.culture().numberFormat["."] === ",") {
                    v = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                    n = v.indexOf(",");
                }
                else {
                    v = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
                    n = v.indexOf(".");
                }
                e.target.value = v.substring(0, n !== -1 ? n : v.length);
            });
        }
    }
}
fixDelimitersIntegers();

function fixDelimitersInteger(element) {
    if (element.addEventListener) {
        element.addEventListener("change", function (e) {
            e = e || event;
            var v;
            var n;
            if (kendo.culture().numberFormat["."] === ",") {
                v = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                n = v.indexOf(",");
            }
            else {
                v = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
                n = v.indexOf(".");
            }
            e.target.value = v.substring(0, n !== -1 ? n : v.length);
        }, false);
    }
    else if (element.attachEvent) { //IE fallback
        element.attachEvent("onchange", function (e) {
            e = e || event;
            var v;
            var n;
            if (kendo.culture().numberFormat["."] === ",") {
                v = e.target.value.replaceAllInString(".", ",").replaceAllInString(" ", "");
                n = v.indexOf(",");
            }
            else {
                v = e.target.value.replaceAllInString(",", ".").replaceAllInString(" ", "");
                n = v.indexOf(".");
            }
            e.target.value = v.substring(0, n !== -1 ? n : v.length);
        });
    }
}
function openModal(gridname) {
    $(".pasteModal").modal("toggle");
    $("#excelPasteArea").data("gridname", gridname);
    $("#formatAlert").hide();
};
function pasteExcel() {
    // create a textarea element which will act as a clipboard
    const textarea = $("#excelPasteArea");
    // the the pasted content
    const value = $.trim(textarea.val());
    // get instance to the grid
    var gridName = $("#excelPasteArea").data("gridname");
    const grid = $("#" + gridName).data("kendoGrid");
    // get the pasted rows - split the text by new line
    const rows = value.split("\n");

    for (let i = 0; i < rows.length; i++) {
        const cells = rows[i].split("\t");
        if (gridName.toLowerCase().indexOf("curvature") >= 0) { //order is important, most specific to most general!
            var cpNum = gridName.match(/CP(\d)/)[1];
            pasteCurvature(cells, grid, cpNum);
        }
        else if (gridName.toLowerCase().indexOf("tangent") >= 0) {
            var cpNum = gridName.match(/CP(\d)/)[1];
            pasteTangent(cells, grid, cpNum);
        }
        else if (gridName.toLowerCase().indexOf("list") >= 0) {
            var cpNum = gridName.match(/CP(\d)/)[1];
            pasteList(cells, grid, cpNum);
        }
        else if (gridName.toLowerCase().indexOf("grid") >= 0) {
            pasteCP2ToCP1(cells, grid);
        }
       
    }
    textarea[0].value = "";
    processChanges();
}

function pasteCP2ToCP1(cells, grid) {
    if (cells.length !== 2) {
        $("#formatAlert").show();
    }
    else {
        cells[0] = removeAllButLast(cells[0].replaceAllInString(",", ".").replaceAllInString(" ", ""), ".");
        cells[1] = removeAllButLast(cells[1].replaceAllInString(",", ".").replaceAllInString(" ", ""), ".");

        grid.dataSource.add({
            CP2Value: parseFloat(cells[0]),
            CP1Value: parseFloat(cells[1]),
            Id: newId
        });
        newId = newId - 1;
        $(".pasteModal").modal("hide");
    }
}

function pasteList(cells, grid) {
    if (cells.length !== 2) {
        $("#formatAlert").show();
    }
    else {
        cells[1] = removeAllButLast(cells[1].replaceAllInString(",", ".").replaceAllInString(" ", ""), ".");

        grid.dataSource.add({
            SupplierSpecificName: cells[0],
            CPValue: parseFloat(cells[1]),
        });
        $(".pasteModal").modal("hide");
    }
}

function pasteTangent(cells, grid) {
    if (cells.length !== 2) {
        $("#formatAlert").show();
    }
    else {
        cells[1] = removeAllButLast(cells[1].replaceAllInString(",", ".").replaceAllInString(" ", ""), ".");

        grid.dataSource.add({
            SupplierSpecificName: cells[0],
            CPTangentValue: parseFloat(cells[1]),
        });
        $(".pasteModal").modal("hide");
    }
}

function pasteCurvature(cells, grid, cpNum) {
    var sectionCount = parseInt($("#CP" + cpNum + "_CurvatureSectionCount").val());
    if (cells.length !== 1 + 2 * sectionCount) {
        $("#formatAlert").show();
    }
    else {
        var radii = cells.slice(1, sectionCount + 1)
        radii.forEach(function (currentval, index) {
            radii[index] =  parseFloat(removeAllButLast(radii[index].replaceAllInString(",", ".").replaceAllInString(" ", ""), "."));
        })

        var excentricities = cells.slice(sectionCount + 1, sectionCount * 2 + 1)
        excentricities.forEach(function (currentval, index) {
            excentricities[index] = parseFloat(removeAllButLast(excentricities[index].replaceAllInString(",", ".").replaceAllInString(" ", ""), "."));
        })

        grid.dataSource.add({
            SupplierSpecificName: cells[0],
            Radii: radii,
            Excentricities: excentricities,
            Sagitta: " "
        });
        $(".pasteModal").modal("hide");
    }
}

function removeAllButLast(string, token) {
    const parts = string.split(token);
    if (parts[1] == undefined)
        return string;
    else
        return parts.slice(0, -1).join("") + token + parts.slice(-1);
}

String.prototype.replaceAllInString = function (search, replacement) {
    return this.split(search).join(replacement);
}



function onBeforeSubmit(submitAndSave) {
    var cp1listSavedPromise = saveallGrids();

    $.when(cp1listSavedPromise).done(function (e) {
        submitAndSave();
    }).fail(function () {
        showNotification($.validator.messages.General.G_SAVE_ERROR, 2);
    });
}

function saveallGrids() {
    var promises = [];

    var grids = $('.SaveWithPromise')
    grids.each(function (index) {
        var saved = new $.Deferred();
        promises.push(saved);
        var grid = $(grids[index]).data('kendoGrid');
        if (!grid.dataSource.hasChanges()) {
            saved.resolve();
            return saved.promise();
        }
        grid.dataSource.bind("sync", function (e) {
            saved.resolve();
        });

        grid.saveChanges()
    })


    return Promise.all(promises);
}

function BindPartialViewToRadioButtons() {
    $(".CalculationMethod").each(
        function () {
            $(this).change(function () {
                var cpId = 0;
                var cpNum = $(this).attr("data-cp");
                switch (cpNum) {
                    case "1":
                        cpId = $("#CP1Id").val();
                        break;
                    case "2":
                        cpId = $("#CP2Id").val();
                        break;
                    case "3":
                        cpId = $("#CP3Id").val();
                        break;
                    case "4":
                        cpId = $("#CP4Id").val();
                        break;
                    case "5":
                        cpId = $("#CP5Id").val();
                        break;
                }
                console.log(cpId);
                console.log($(this).val())
                console.log(cpNum);
                $.ajax({
                    url: '/Lenssets/detail/GetCpValueFormPartial',
                    type: 'GET',
                    data: { cpId: cpId, listType: $(this).val(), cpNum: cpNum },
                    success: function (result) {
                        $('#cp' + cpNum + 'ValueContainer').html(result);
                        fixDelimitersDoubles();
                    }
                });
            });

        }
    )
    $(".CalculationMethod:checked").each(
        function () {
            $(this).change()
        }
    )
}

function iterateListForColumn(list, uId, listId, gridId) {
    var NumberOfSections = list.length;
    var resultString = "";
    for (var i = 0; i < NumberOfSections; i++) {
        resultString = resultString + '<input id="' + listId + '_' + uId + '_' + i + '" type="number" class="k-input customGridInput" value=' + list[i] + ' onchange=\"changelistItem(this, \'' + gridId + '\',\'' + listId + '\', ' + i + ')\"></input>';
    }
    return resultString;
}

function changeField(caller, grid) {
    var dataEntry = getGridDataSourceObject(grid, caller)

    dataEntry.dirty = true;
    dataEntry["SupplierSpecificName"] = $(caller).val();

    processChanges();
}

function changeDiam(caller, grid) {
    var dataEntry = getGridDataSourceObject(grid, caller)

    dataEntry.dirty = true;
    dataEntry.Diameter = $(caller).val();
    processChanges();
}

function changelistItem(caller, grid, list, index) {
    var dataEntry = getGridDataSourceObject(grid, caller)

    dataEntry.dirty = true;
    dataEntry[list][index] = parseFloat($(caller).val());

    processChanges();
}


/*function changeNumberOfSections(caller, grid) {
    var dataEntry = getGridDataSourceObject(grid, caller)

    var inputsToAdd = $(caller).val() - dataEntry.NumberOfSections;
    dataEntry.dirty = true;
    dataEntry.NumberOfSections = $(caller).val();
    if (inputsToAdd > 0) {
        for (var i = 0; i < inputsToAdd; i++) {
            addInputFieldTo('RadiiContainer_' + getUidFromId(caller), 0);
            addInputFieldTo('ExcentricitiesContainer_' + getUidFromId(caller), 0);
            dataEntry.Excentricities.push(0);
            dataEntry.Radii.push(0);
        }
    } else if (inputsToAdd < 0) {
        for (var i = 0; i > inputsToAdd; i--) {
            $('#RadiiContainer_' + getUidFromId(caller) + ' input:last-child').remove();
            $('#ExcentricitiesContainer_' + getUidFromId(caller) + ' input:last-child').remove();
            dataEntry.Excentricities.pop();
            dataEntry.Radii.pop();
        }
    }
    processChanges();
}*/

function changeNumberOfSections(caller, cp) {

    var inputsToAdd = parseInt($(caller).val()) - caller.oldValue;
    var grid = $("div[id$='GridCP" + cp + "']")[0];
    var currentOrderNr = parseInt(caller.oldValue);
    if (inputsToAdd > 0) {
        currentOrderNr++;
        for (var i = 0; i < inputsToAdd; i++) {
            addDiameterInputFieldTo('DiameterListContainer' + cp, cp, currentOrderNr, 0);
            addInputFieldTo(grid, 0, currentOrderNr - 1);
            currentOrderNr++;
        }
    } else if (inputsToAdd < 0) {
        for (var i = 0; i > inputsToAdd; i--) { // oude min i
            kendo.destroy($('#CP' + cp +'_CurvatureDiameters_' + (currentOrderNr + i) + '_').parent().parent());
            $(document.getElementById('CP' + cp +'_CurvatureDiameters_' + (currentOrderNr + i) + '_')).parent().parent().remove()
            $(grid).data("kendoGrid").dataSource._data.forEach(function (object) {
                object["Excentricities"].pop();
                object["Radii"].pop();
                object.dirty = true;
            })
            $(grid).find("div[id^='RadiiContainer'] input:last-child").remove();
            $(grid).find("div[id^='ExcentricitiesContainer'] input:last-child").remove();
        }

    }
    processChanges();
}

function addInputFieldTo(grid, defValue, valueIndex) {
    var RadiiContainer = $(grid).find("div[id^='RadiiContainer']");
    var ExcentricitiesContainer = $(grid).find("div[id^='ExcentricitiesContainer']");
    RadiiContainer.each(function (index, item) {
        var uid = getUidFromId(item);
        var result = $('<input id =\"Radii_' + uid + '\" type="number" class="k-input customGridInput" value=' + defValue + ' onchange=\"changelistItem($(this)[0], \'' + $(grid).attr('id') + '\', \'Radii\', ' + valueIndex + ')\">').appendTo(item)

        var dataEntry = getGridDataSourceObject($(grid).attr('id'), result[0])
        dataEntry.dirty = true;
        dataEntry["Radii"].push(0);
    })
    ExcentricitiesContainer.each(function (index, item) {
        var uid = getUidFromId(item);
        var result = $('<input id =\"Excentricities_' + uid + '\" type="number" class="k-input customGridInput" value=' + defValue + ' onchange=\"changelistItem($(this)[0], \'' + $(grid).attr('id') + '\', \'Excentricities\',' + valueIndex + ' )\">').appendTo(item)

        var dataEntry = getGridDataSourceObject($(grid).attr('id'), result[0])
        dataEntry.dirty = true;
        dataEntry["Excentricities"].push(0);
    })

}

function addInputFieldRow(grid, defValue, valueIndex, rowGuid) {
    var RadiiContainer = $(grid).find("div[id^='RadiiContainer_" + rowGuid + "']");
    var ExcentricitiesContainer = $(grid).find("div[id^='ExcentricitiesContainer_" + rowGuid + "']");
    RadiiContainer.each(function (index, item) {
        var uid = getUidFromId(item);
        var result = $('<input id =\"Radii_' + uid + '\" type="number" class="k-input customGridInput" value=' + defValue + ' onchange=\"changelistItem($(this)[0], \'' + $(grid).attr('id') + '\', \'Radii\', ' + valueIndex + ')\">').appendTo(item)

        var dataEntry = getGridDataSourceObject($(grid).attr('id'), result[0])
        dataEntry.dirty = true;
        dataEntry["Radii"].push(0);
    })
    ExcentricitiesContainer.each(function (index, item) {
        var uid = getUidFromId(item);
        var result = $('<input id =\"Excentricities_' + uid + '\" type="number" class="k-input customGridInput" value=' + defValue + ' onchange=\"changelistItem($(this)[0], \'' + $(grid).attr('id') + '\', \'Excentricities\',' + valueIndex + ' )\">').appendTo(item)

        var dataEntry = getGridDataSourceObject($(grid).attr('id'), result[0])
        dataEntry.dirty = true;
        dataEntry["Excentricities"].push(0);
    })

}

function addDiameterInputFieldTo(inputContainerId, cp, orderNr, defValue) {
    $("#" + inputContainerId).append('<input id="CP' + cp + '_CurvatureDiameters_' + orderNr + '_" name="CP' + cp + '.CurvatureDiameters[' + orderNr + ']" type = "number"  value = "' + defValue + '" onchange="processChanges()" style="width: 7%;" >');
    $(document.getElementById("CP" + cp + "_CurvatureDiameters_" + orderNr + "_")).kendoNumericTextBox();
}

function getUidFromId(object) {
    return object.id.split('_')[1]
}

function getGridDataSourceObject(grid, object) {
    return $("#" + grid).data("kendoGrid").dataSource._data.filter(function (item) {
        return item.uid == getUidFromId(object);
    })[0];
}

function deleteRow(caller, gridName) {
    var grid = $("#" + gridName).data("kendoGrid");
    var tr = $(caller).closest("tr");
    grid.removeRow(tr);
}
