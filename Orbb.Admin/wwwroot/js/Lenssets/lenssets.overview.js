﻿function showDetails(e) {
    e.preventDefault();

    const dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    const url = `/Lenssets/detail/index/${dataItem.Id}`;

    document.location.href = url;
}

function delete_handler(e) {
    e.preventDefault();

    var grid = this;
    var row = $(e.currentTarget).closest("tr");

    showConfirmationDialog($.validator.messages.General.G_DELETE,
        function() {
            setTimeout(grid.removeRow(row), 8000);
        },
        function() {
            grid.cancelChanges();
        });
};

function Export_handler(e) {
    e.preventDefault();

    const dataItem = this.dataItem($(e.currentTarget).closest("tr"));
    const url = `/Lenssets/overview/export/${dataItem.Id}`;

    document.location.href = url;
};