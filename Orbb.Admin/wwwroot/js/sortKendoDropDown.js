﻿function sortDropDownListByText() {

    $("select").each(function () {

        var selectedValue = $(this).val();

        $(this).html($("option", $(this)).sort(function (a, b) {
            return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
        }));

        $(this).val(selectedValue);
    });
}

$(document).ready(sortDropDownListByText);