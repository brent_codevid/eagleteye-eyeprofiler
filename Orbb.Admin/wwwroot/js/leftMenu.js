﻿function removeQuickMenuItem(e) {

    var fastMenuItemId = $(e).parent().parent().attr("id");
    $.ajax({

        type: 'POST',
        url: '/Home/RemoveQuickMenuItem',
        data: { 'menuItemId': fastMenuItemId },
        dataType: 'json',
        success: function () { alert('Post Ok !!!'); },
        error: function () { alert('Post not OK ???'); }
    });

    $(e).parent().parent().remove();


    $("#" + fastMenuItemId.replace("fm-", "mm-")).kendoDraggable({
        ignore: ".disableDrag",
        scroll: true,
        hint: function (element) {
            var hintElement = element.clone();
            hintElement.css({
                "list-style": "none",
                "width": element.width(),
                "background": " #5692b1"
            });
            return hintElement;
        }
    });

    event.stopPropagation();
    window.event.cancelBubble = true;
}

$(document).ready(function () {
    $(".mainMenuItem").kendoDraggable({
        ignore: ".disableDrag",
        scroll: true,
        hint: function (element) {
            var hintElement = element.clone();
            hintElement.css({
                "list-style": "none",
                "width": element.width(),
                "background": "#5692b1"
            });
            return hintElement;
        }
    });

    $(".mainMenuItemSubMenuItem").kendoDraggable({
        ignore: ".disableDrag",
        scroll: true,
        hint: function (element) {
            var hintElement = element.clone();
            hintElement.css({
                "list-style": "none",
                "width": element.width(),
                "background": "#93d451"
            });
            return hintElement;
        }
    });
    

    $('#fastMenu .fastMenuItem').each(function () {

        var fastMenuItemId = $(this).attr("id");

        $("#" + fastMenuItemId.replace("fm-", "mm-")).off();

    });

    $("#fastMenuNavigation").kendoDropTarget({
        ignore: "#leftMenuFastMenu",
        drop: droptargetOnDrop
    });

    $("#fastMenuNavigation").kendoSortable({
        ignore: "#leftMenuFastMenu",
        filter: ">li:not(#filtered)",
        connectWith: "#mainMenuMenuNavigation",
        scroll: true,
        hint: function (element) {
            var hintElement = element.clone();
            hintElement.css({
                "list-style": "none",
                "width": element.width(),
                "background": "#5692b1"
            });
            return hintElement;
        }
    });
    
    function droptargetOnDrop(e) {
        if ($(e.draggable.element).attr("id") != "fastMenuNavigation") {
            fastMenuItemId = $(e.target).attr("id");
            $.ajax({

                type: 'POST',
                url: '/Home/AddQuickMenuItem',
                data: { 'menuItemId': fastMenuItemId },
                dataType: 'json',
                success: function () {
                    var el = $(e.draggable.element).clone();
                    $(el).children("button").css("font-size", "x-large");
                    $(el).removeClass("mainMenuItem");
                    $(el).addClass("fastMenuItem");
                    $(el).attr("id", $(el).attr("id").replace("mm-", "fm-"));
                    $(el).children("button").append('<span class="delete" onClick="removeQuickMenuItem(this)"></span>');
                    $(e.draggable.element).off();
                    $(e.dropTarget.context).append(el);
                },
                error: function () {
                    var el = $(e.draggable.element).clone();
                    $(el).removeClass("mainMenuItem");
                    $(el).addClass("fastMenuItem");
                    $(el).attr("id", $(el).attr("id").replace("mm-", "fm-"));
                    $(el).children("button").append('<span class="delete" onClick="removeQuickMenuItem(this)"></span>');
                    $(e.draggable.element).off();
                    $(e.dropTarget.context).append(el);
                }
            });


        }

    }
});












