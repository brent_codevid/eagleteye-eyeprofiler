﻿$(function () {
    $(".detailContainerContentTabBar").kendoTabStrip({
        animation: { open: { effects: "fadeIn" } },
        contentUrls: [
            null,
            null
        ]
    }).data("kendoTabStrip");

    const id = $("#Id").val();

    $(".TabBarStyle").append('<li class="tabBarCommandButtons"> </li>');

    if (id > 0) {
        $(".tabBarCommandButtons").append('<button type="button" id="canDeleteButton" class="icon far fa-trash-alt iconOnlyButtonRed" onclick="deleteCountry()"></button>');

    } else {
        $(".hideOnNew").hide();
    }

    // create Editor from textarea HTML element with default set of tools
    $(".kendo-editor").kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        },
        encoded: false,
        change: function () {
            processChanges();
        }
    });
});

function deleteCustomer() {
    showConfirmationDialog(
        $.validator.messages.General.G_DELETE,
        function () {
            const form = $("#canDeleteButton").closest("form")[0];
            if (form == null || form == "undefined" || form == "") {
                return;
            }
            var action = $(form).attr("action").toLowerCase();

            action = action.replace("update", "delete");
            if (action.indexOf("delete") > -1) {
                $(form).attr("action", action);
            }
            $(form).submit();
        },
        function () {
        });
}

var currentItems = 0;
var currentIndex = 0;
var adding = true;

var currentPageLenssets = 1;
var currentPageOrders = 1;

var firstLoad = true;

function onDataBound() {
    const pageLenssets = $("#grid").data("kendoGrid").dataSource.page();
    if (pageLenssets != currentPageLenssets) {
        currentPageLenssets = pageLenssets;
    }

    if (firstLoad) {
        adding = true;
        firstLoad = false;
        initialSelection();
        adding = false;
    }
}

function initialSelection() {
    const ini = document.getElementsByClassName("initialLensset");
    const grid = $("#grid").data().kendoGrid;

    const data = grid.dataSource.data();
    const pageSize = grid.dataSource.pageSize();
    const pages = Math.ceil(data.length / pageSize);

    for (let page = 1; page <= pages; page++) {
        grid.dataSource.page(page);
        for (let i = ini.length; i > 0; i--) {
            const uid = getGridUid(ini[i - 1].value);
            const select = grid.tbody.find(`tr[data-uid="${uid}"]`);
            grid.select(select);
            if (select.length != 0)
                ini[i - 1].remove();
        };
    }
    grid.dataSource.page(1);

    const ids = grid.selectedKeyNames();
    for (let i = 0; i < ids.length; i++) {
        addLensset(ids[i]);
    }
}

function getGridUid(id) {
    const grid = $("#grid").data().kendoGrid;
    const data = grid.dataSource.data(); //only get items on current page
    var uid;
    var d;
    for (let i = 0; i < data.length; i++) {
        if (data[i].Id == id) {
            d = data[i];
            uid = d.uid;
        }
    }

    return uid;
}

function addLensset(id) {
    const grid = $("#grid").data().kendoGrid;
    const data = grid.dataSource.data();
    var uid;
    var d;
    for (let i = 0; i < data.length; i++) {
        if (data[i].Id == id) {
            d = data[i];
            uid = d.uid;
        }
    }

    if (uid == null)
        return null;

    const list = document.getElementById("UsedLenssets");

    const input = document.createElement("input");
    list.appendChild(input);
    input.type = "number";
    input.id = `AvailableLenssets_${currentIndex}__Id`;
    input.name = `AvailableLenssets[${currentIndex}].Id`;
    input.value = id.toLocaleString("de-DE");

    currentItems = currentItems + 1;
    currentIndex = currentIndex + 1;

    checkEmptyList();

    return uid;
}

function checkEmptyList() {
    const list = document.getElementById("UsedLenssets");
    var input;
    if (currentItems == 0) {
        input = document.createElement('input');
        list.appendChild(input);
        input.type = "text";
        input.id = "EmptyAvailableLenssets";
        input.name = "AvailableLenssets";
        input.value = "System.Collections.Generic.List`1[Orbb.Data.ViewModels.Models.Customers.LenssetVM]";
    } else {
        input = document.getElementById('EmptyAvailableLenssets');
        if (input) {
            input.remove();
        }
    }
}

function onChange() {
    const pageLenssets = $("#grid").data("kendoGrid").dataSource.page();
    if (pageLenssets != currentPageLenssets) {
        currentPageLenssets = pageLenssets;
    } else if (!adding) {
        processChanges();
        const list = document.getElementById("UsedLenssets");
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }
        currentIndex = 0;
        const ids = this.selectedKeyNames();
        for (let i = 0; i < ids.length; i++) {
            addLensset(ids[i]);
        }
    }
}
