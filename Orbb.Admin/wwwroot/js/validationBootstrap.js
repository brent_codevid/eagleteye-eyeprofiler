﻿$(document).ready(function () {
    $('#loginScreenLoginForm').validate({

        rules: {
            Username: {
                required: true
            },
            Password: {
                required: true
            }
        },
        messages: {

            Username:{
                required: $('#UsernameValidation').val()
            },
            Password: {
                required: $('#PasswordValidation').val()
            }

        },
        highlight: function(element) {
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element) {
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorElement: 'span',
        errorClass: 'errorLogin',
        errorPlacement: function (error, element) {

            if ($('.validation-summary-errors').children().children().text() != '') {
                element.removeAttr('value')
                element.parent().addClass('.has-error');
                error = $('.validation-summary-errors').children().children().text();
                element.attr('placeholder',error);
            }
            else {
                element.removeAttr('value')
                error.appendTo(element.attr('placeholder',error.text()));
            }
        }
    });

    if ($('.validation-summary-errors').children().children().text() != '') {

        $('#loginScreenUsername').addClass('has-error');
        $('#loginScreenPassword').addClass('has-error');
        error = $('.validation-summary-errors').children().children().text();
        $('#loginScreenPassword').children('input').removeAttr('value')
        $('#loginScreenUsername').children('input').attr('placeholder', error);
        $('#loginScreenPassword').children('input').attr('placeholder', error);
        $('#loginScreenUsername').children('input').css('font-size', '12px');
        $('#loginScreenPassword').children('input').css('font-size', '12px');

    }

    else {

        $('#loginScreenUsername').children('input').css('font-size', 'large');
        $('#loginScreenPassword').children('input').css('font-size', 'large');

    }
});