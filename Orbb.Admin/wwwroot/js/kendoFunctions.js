﻿function getKendoGrid(element) {
    return element.data("kendoGrid");
}

function getKendoCombobox(element) {
    return element.data("kendoComboBox");
}

function getKendoAutoComplete(element) {
    return element.data("kendoAutoComplete");
}

function getKendoNumericTextbox(element) {
    return element.data("kendoNumericTextBox");
}

function getKendoDropDownList(element) {
    return element.data("kendoDropDownList");
}

function getKendoDatePicker(element) {
    return element.data("kendoDatePicker");
}

function getKendoDateTimePicker(element) {
    return element.data("kendoDateTimePicker");
}

function getKendoGridItem(grid, e) {
    return grid.dataItem($(e.currentTarget).closest("tr"))
}

function getKendoGridsSelectedItem(grid) {
    return grid.dataItem(grid.select());
}

function getKendoDataObject(kendoSource, e) {
    return kendoSource.dataItem(e.item.index());
}

function getKendoComboboxSelectedItem(combobox) {
    return combobox.dataItem(combobox.select());
}

function itemIndex(kendoControl, dataItem) {
    var data = kendoControl.dataSource.data();
    return data.indexOf(dataItem);
}

function KendoNumber(text) {
    if (text != null || text != '') {
        //Replace " " is a special code then normal space
        return kendo.toString(kendo.parseFloat(text), 'n');
    }
    return text;
}

function kendoNumberValue(text) {
    if (text == null || text == '')
        return "0";
    else {
        return KendoNumber(text).replace(kendo.culture().numberFormat[","], "");
    }
}

// Kendo Complex objects have issues with DateTime property
// If model has NameEntries or DescriptionEntries object => convert its LastModifiedOn and CreatedOn dates before send to controller
function convertDate(data) {
    $(data.models).each(function (index) {
        if (data.models[index].NameEntries != null) {
            var entries = data.models[index].NameEntries;

            for (var i = 0; i < entries.length; i++) {
                if (entries[i].LastModifiedOn != null) {
                    entries[i].LastModifiedOn = kendo.toString(kendo.parseDate(entries[i].LastModifiedOn, "G"));
                }
                if (entries[i].CreatedOn != null) {
                    entries[i].CreatedOn = kendo.toString(kendo.parseDate(entries[i].CreatedOn, "G"));
                }
            }
        }
        if (data.models[index].DescriptionEntries != null) {
            var entries = data.models[index].DescriptionEntries;

            for (var i = 0; i < entries.length; i++) {
                if (entries[i].LastModifiedOn != null) {
                    entries[i].LastModifiedOn = kendo.toString(kendo.parseDate(entries[i].LastModifiedOn, "G"));
                }
                if (entries[i].CreatedOn != null) {
                    entries[i].CreatedOn = kendo.toString(kendo.parseDate(entries[i].CreatedOn, "G"));
                }
            }
        }
    });
}