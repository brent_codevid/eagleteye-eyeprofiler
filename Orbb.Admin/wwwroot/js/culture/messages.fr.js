﻿$.extend($.validator.messages, {

    Username: {

        required: "Nom d'utilisateur est obligatoire"

    },
    Password: {

        required: "Mot de passe est obligatoire"

    },
    General: {
        welcome: "Bienvenue",
        unsavedChanges: "Vous avez des modifications non enregistrées. Êtes-vous sûr de vouloir quitter la page ?",
        changeDetected: "Un ajustement a été enregistré, n'oublieze pas de sauvegarder.",
        changeDetected_SaveAfterProcessAllRows: "A change was registered, don't forget to process all rows and save.",
        noDiscountSet: "Il n'y a pas de collections de réductions disponibles",
        unExpectedResult: "Un erreur inattendue s'est produit.",
        G_DELETE: "Are you sure you want to delete this object?",
        G_VAR_DELETED: "%NAME% has been deleted.",
        G_WARNING_DELETE_IN_EDIT: "Attention, you deleted an object while editing other rows. Please save your changes to confirm the delete",
        G_SAVED: "Successfuly saved!",
        G_DELETE_ERROR: "Erreur lors de la suppression!",
        G_SAVE_ERROR: "Error while saving!",
        G_UPDATE_CONFLICT: "Conflict while updating!",
        G_SAVING: "Saving, please wait...",
        G_Days: "jour(s)",
        G_DESCRIPTION_REQIURED: "Description is required",
        G_D_LINKED_CATEGORY: "Unable to delete because the category is linked to a productcategory.",
        G_USERRIGHTUSED: "Unable to delete because the user right is linked to a usergroup.",
        G_CANCEL: "Are you sure you want to cancel your changes?",
        G_VAL_ERROR: "Could not load comment.",
        G_NO_SELECTION: "No lines selected",
        G_MISSING_ELEMENT: "A configuration element is missing in order to perform this action",
        G_EMPTY_RESULT: "Er werden geen records gevonden voor uw zoekopdracht",
        G_NO_SEARCH_TERM: "S'il vous plaît entrer une recherche",
        G_GENERATENOTE_ERROR: "Error bij het genereren van het document.",
        G_IMAGE_ONLY_1_ALLOWED: "Maximum one image allowed",
        G_IMAGE_MIN_SIZE: "Minimal image resolution is %SIZE%",
        G_IMAGE_ONLY_PNG_ALLOWED: "Seuls les fichiers png sont autorisés"
    },
    Category: {
        CAT_CODEVAL: "The field Code must be a string with a maximum length of 10",
        CAT_LANG_DUPLICATION: "Sélection de la langue Dupliquer existe",
        CAT_CODE_DUPLICATION: "Code de la catégorie existe déjà. Code devrait être unique."
    },
    DiscountSets: {
        DS_ADDED: "A new discount set was added",
        DS_UNSAVED_DISCOUNTS: "Please save your discounts for  %DISCOUNTSET_NAME% first, before adding a new discount set"
    },
    EditUserCredentials: {
        INVALID_PASSWORD: "The entered password was invalid",
        ENTER_PASSWORD: "Enter your password",
        PASSWORDS_DONT_MATCH: "Passwords dont match",
        EMPTY_PASSWORD: "Password cannot be empty"
    },
    Pricelists: {
        pricelist_ADDED: "A new pricelist has been added"
    },
    ProductReturns: {
        incorrect_quantity: "Entered quantity is incorrect!"
    },
    ReturnIntakes: {
        INCORRECT_SUM_RETURNEDANDDECLINEDAMOUNTS: "The sum of Amount Accepted and Amount Declined has to be equal to returned product quantity!",
        REASONDECLINED_REQUIRED: "When the declined amount is added, the Reason Declined is mandatory!",
        LOCATION_REQUIRED: "When there is Amount Accepted and the Return To Stock is marked, the location is mandatory field!",
        SUPPLIER_REQUIRED: "Fournisseur requis",
        PURCHASEPRICE_REQUIRED: "Prix ​​d'achat requis"
    },
    Retour: {
        STOCKARTICLE_NOT_FOUND: "Could not find the stock article, make sure either the serial number or lot code is entered correctly"
    },
    OrderHistory: {
        OH_VALIDATE: "S'il vous plaît fournir %NAME% d'abord, avant de poursuivre la recherche.",
        //NEW TASK MAIKEL
        OH_SUPPLIERID_PRODUCTGROUP_PRODUCT: "Fournisseur ou Groupe de produits ou Produit",
        OH_STARTDATE_ENDDATE: "Dates de début ou Dates de fin",
        OH_INV_ENDDATE: "Dates de fin devrait avoir dd/mm/yyyy format",
        OH_INV_STARTDATE: "Dates de début devrait avoir dd/mm/yyyy format",
        OH_INV_DATES: "Dates de fin devrait plus grande que dates de début",
        OH_NO_ROWS: "Pas d'articles trouvés pour ordonner",
        //OLD
        OH_STARTDATE: "Dates de début",
        OH_ENDDATE: "Dates de fin",
        OH_STARTENDDATE: "Début et fin Dates"
    },
    SalesOrder: {
        WAREHOUSE_REQUIRED: "A warehouse is required for consignation orders",
        SO_DELIVERYADDRESS_REQUIRED: "L'adresse de livraison est nécessaire.",
        SO_INVOICEADDRESS_REQUIRED: "Adresse de facturation est nécessaire.",
        SO_CONFIRMBATCHPICKING: "Do you want to reservate the articles based on their default location?",
        CONSIGNATIONENDDATE_REQUIRED: "Consignation date de fin est nécessaire pour les commandes de consignation."
    },
    Delivery: {
        CURRENT_ORDER_CONFLICT: "The selected line is part of the selected order to deliver and is therefor a required line for this delivery",
        DEL_VALIDATE_LINES: "S'il vous plaît sélectionner les lignes de commande pour le traitement de la livraison.",
        DELIVERY_COST_UPDATE: "The delivery cost was changed based on the dispensation settings",
        SO_GENERATEPROFORMAINVOICE_ERROR: "Error on genrating proforma invoice."
    },
    Maintenance: {
        MAIN_ADD_CUSTOMER_TO_DIVISION_ERROR: "The customer is not available for this division, add the customer to this division first",
        MAIN_ADD_PRODUCT_TO_DIVISION_ERROR: "The product is not available for this division, add the product to this division first",
    },
    Supplier: {
        SUP_AddressInvaid: "Country and Type are required in Contact address",
        SUP_DeliveryCondition_REQUIRED: "Delivery condition is required",
        SUP_Name_REQUIRED: "Name is required",
        SUP_Code_REQUIRED: "Code is required",
        SUP_Contact_FirstName_REQUIRED: "First name is required"
    },
    Product:{
        PROD_Name_REQUIRED: "Name is required",
        PROD_Code_REQUIRED: "Code is required",
        PROD_Description_REQUIRED: "Description is required",
        PROD_ShortDescription_REQUIRED: "Short description is required",
        PROD_Division_REQUIRED: "Division is required"
    },
    Intake: {
        WAREHOUSE_REQUIRED: "Un entrepôt est nécessaire pour la prise de commandes.",
        LOCATION_REQUIRED: "Un emplacement est nécessaire pour la prise de commandes.",
        SUPPLIER_LOTCODE_REQUIRED: "Un code fournisseur du lot est nécessaire pour la prise de commandes.",
        INTAKE_AMOUNT_REQUIRED: "Un montant d'admission est nécessaire pour les commandes d'admission .",
    },
    Warehouse: {
        SUM_AVAILABLE_AND_TOCORRECT_INCORRECT: "If the serial number is entered, then the sum of available and to correct must be 0 or 1",
        NOT_SELECTED: "Aucun magasin sélectionné",
        SAVE_CURRENT_CHANGES: "Sauvegarder les modifications actuelles",
        TRANSFER_TO_SAME_WAREHOUSE: "Pas de transfert vers le même entrepôt",
        DESELECT_SUPPLIER: "Désélectionner d'abord le fournisseur",
        DESELECT_CUSTOMER: "Désélectionner d'abord le client",
        INVALID_DATE: "Date non valide",
        INVALID_PRICE: "Prix non valide"
    },
    Scan: {
        SCAN_NOTFOUND: "Pas de match trouvé.",
        LOC_VALIDATE: "S'il vous plaît fournir un premier emplacement, avant de poursuivre.",
        INAMOUNT_VALIDATE: "montant Intaked doit être inférieure ou égale à montant.",
        SCAN_PRODUCTFOUND: "produit scannée est tracée avec succès.",
        SCAN_LOTFOUND: "Scanned supplier lot is traced successfully.",
        SCAN_LOCATIONFOUND: "Scanned location is traced successfully.",
        CURRENTPRODUCTEMPTY: "Please provide product first before proceeding.",
        AMOUNTLESSTHAN0: "Amount is less than 0.",
        PRODUCTAMOUNTCHANGED: "Intake amount changed."
    },
    Customer: {
        CUS_TOOMANY_DEFAULT_ADDRESS: "une seule adresse peut être l'adresse par défaut de livraison ou de facturation.",
    },
    Trace: {
        TRACE_VALIDATE: "Please provide ateast a lot code, supplier lot code or an article serial number, before proceeding.",
        TRACE_NO_DOCUMENT: "Aucun document disponible."
    },
    ServiceOrder: {
        SERVICEORDER_ADJUSTSTOCK: "Close this service order and adjust stock if necessary?"
    },
    LensFitter: {
        CLEAR: "Effacer"
    }
});