﻿$.extend($.validator.messages, {

    Username: {

        required: "Gebruikersnaam is verplicht"

    },
    Password: {

        required: "Wachtwoord is verplicht"

    },
    General: {
        welcome: "Welkom",
        unsavedChanges: "U hebt onbewaarde wijzigingen. Bent u zeker dat u de pagina wilt verlaten?",
        changeDetected: "Er werd een wijziging waargenomen, vergeet deze niet op te slaan.",
        changeDetected_SaveAfterProcessAllRows: "A change was registered, don't forget to process all rows and save.",
        noDiscountSet: "Er is geen prijsafspraak geselecteerd",
        unExpectedResult: "Er heeft zich een onverwachte fout voorgedaan.",
        G_DELETE: "Bent u zeker dat u dit object wilt verwijderen?",
        G_VAR_DELETED: "%NAME% werd verwijderd.",
        G_WARNING_DELETE_IN_EDIT: "Opgelet, u verwijderde een rij terwijl er nog andere wijzigen zijn. Gelieve uw wijzigen op te slaan om de verwijderde rij te bevestigen",
        G_SAVED: "Opgeslaan!",
        G_DELETE_ERROR: "Fout bij het verwijderen!",
        G_SAVE_ERROR: "Fout bij het opslaan!",
        G_UPDATE_CONFLICT: "Conflict bij het updaten!",
        G_SAVING: "Bezig met opslaan, gelieve even te wachten...",
        G_Days: "dag(en)",
        G_DESCRIPTION_REQIURED: "Beschrijving is verplicht in te vullen",
        G_D_LINKED_CATEGORY: "Onmogelijk te verwijderen omdat de categorie gelinkt is aan een productcategorie.",
        G_USERRIGHTUSED: "Unable to delete because the user right is linked to a usergroup.",
        G_CANCEL: "Bent u zeker dat u uw wijzigingen wilt annuleren?",
        G_VAL_ERROR: "Commentaar kon niet geladen worden.",
        G_NO_SELECTION: "Er werden geen lijnen geselecteerd",
        G_MISSING_ELEMENT: "Er ontbreekt een configuratie element om de actie uit te voeren",
        G_EMPTY_RESULT: "Er werden geen records gevonden voor uw zoekopdracht",
        G_NO_SEARCH_TERM: "Gelieve een zoekopdracht in te geven",
        G_GENERATENOTE_ERROR: "Error bij het genereren van het document.",
        G_IMAGE_ONLY_1_ALLOWED: "Maximum één afbeelding toegelaten",
        G_IMAGE_MIN_SIZE: "Minimum resolutie afbeelding is %SIZE%",
        G_IMAGE_ONLY_PNG_ALLOWED: "Enkel png bestanden toegelaten"
    },
    Category: {
        CAT_CODEVAL: "Code moet een string zijn die maximaal 10 karakters lang is",
        CAT_LANG_DUPLICATION: "Dubbele taalkeuze bestaat",
        CAT_CODE_DUPLICATION: "Categorie code bestaat al. Code moet uniek zijn."
    },
    DiscountSets: {
        DS_ADDED: "De prijsafspraakverzameling werd toegevoegd",
        DS_UNSAVED_DISCOUNTS: "Gelieve de kortingen voor %DISCOUNTSET_NAME% op te slaan, alvorens een nieuwe prijsafspraakverzameling toe te voegen"
    },
    EditUserCredentials: {
        INVALID_PASSWORD: "Het paswoord is ongeldig",
        ENTER_PASSWORD: "Geef uw paswoord in",
        PASSWORDS_DONT_MATCH: "De paswoorden zijn niet identiek",
        EMPTY_PASSWORD: "Paswoord moet worden ingevuld"
    },
    Pricelists: {
        pricelist_ADDED: "Een nieuwe prijslijst werd toegevoegd"
    },
    ProductReturns: {
        incorrect_quantity: "Entered quantity is incorrect!"
    },
    ReturnIntakes: {
        INCORRECT_SUM_RETURNEDANDDECLINEDAMOUNTS: "The sum of Amount Accepted and Amount Declined has to be equal to returned product quantity!",
        REASONDECLINED_REQUIRED: "When the declined amount is added, the Reason Declined is mandatory!",
        LOCATION_REQUIRED: "When there is Amount Accepted and the Return To Stock is marked, the location is mandatory field!",
        SUPPLIER_REQUIRED: "Leverancier vereist",
        PURCHASEPRICE_REQUIRED: "Aankoopprijs vereist"
    },
    Retour: {
        STOCKARTICLE_NOT_FOUND: "Kan het stock artikel niet vinden, controleer de serie nummer en de lot code"
    },
    OrderHistory: {
        OH_VALIDATE: "Gelieve %NAME% eerste, alvorens verder te gaan zoeken.",
        //NEW TASK MAIKEL
        OH_SUPPLIERID_PRODUCTGROUP_PRODUCT: "Leverancier of artikelgroep of artikel",
        OH_STARTDATE_ENDDATE: "Start - of einddatum",
        OH_INV_ENDDATE: "Begindatum zou volgend formaat dd/mm/yyyy moeten hebben",
        OH_INV_STARTDATE: "Startdatum Date zou volgend formaat  dd/mm/yyyy moeten hebben",
        OH_INV_DATES: "Einddatum moet groter zijn dan startdatum",
        //OLD
        OH_STARTDATE: "Begindatum",
        OH_ENDDATE: "Einddatum",
        OH_STARTENDDATE: "Begin- en einddatum",
        OH_NO_ROWS: "Er werden geen te bestellen artikelen opgegeven"
},
    SalesOrder: {
        WAREHOUSE_REQUIRED: "Gelieve een warehouse op te geven bij opslaan van een consignatie",
        SO_DELIVERYADDRESS_REQUIRED: "Levering-mailadres is verplicht.",
        SO_INVOICEADDRESS_REQUIRED: "Factuur-mailadres is verplicht.",
        SO_CONFIRMBATCHPICKING: "Automatisch de artikelen reserveren en een magazijnbon aanmaken?",
        CONSIGNATIONENDDATE_REQUIRED: "Consignatie einddatum is vereist voor consignatie orders."
    },
    Delivery: {
        CURRENT_ORDER_CONFLICT: "De geselecteerde lijn maakt deel uit van het geselecteerd order en is daarom verplicht op te nemen bij deze levering",
        DEL_VALIDATE_LINES: "Selecteer de orderregels voor de verwerking van de levering.",
        DELIVERY_COST_UPDATE: "De leveringskost werd gewijzigd op basis van de vrijstellingsinstellingen",
        SO_GENERATEPROFORMAINVOICE_ERROR: "Error on genrating proforma invoice."
    },
    Maintenance: {
        MAIN_ADD_CUSTOMER_TO_DIVISION_ERROR: "De klant is niet beschikbaar voor deze divisie, voeg de klant eerst toe aan de divisie",
        MAIN_ADD_PRODUCT_TO_DIVISION_ERROR: "Het product is niet beschikbaar voor deze divisie, voeg het product eerst toe aan de divisie",
    },
    Supplier: {
        SUP_AddressInvaid: "Country and Type are required in Contact address",
        SUP_DeliveryCondition_REQUIRED: "Delivery condition is required",
        SUP_Name_REQUIRED: "Name is required",
        SUP_Code_REQUIRED: "Code is required",
        SUP_Contact_FirstName_REQUIRED: "First name is required"
    },
    Product: {
        PROD_Name_REQUIRED: "Name is required",
        PROD_Code_REQUIRED: "Code is required",
        PROD_Description_REQUIRED: "Description is required",
        PROD_ShortDescription_REQUIRED: "Short description is required",
        PROD_Division_REQUIRED: "Division is required"
    },
    Intake: {
        WAREHOUSE_REQUIRED: "Een magazijn is nodig voor intake orders.",
        LOCATION_REQUIRED: "Een locatie is vereist voor inname orders.",
        SUPPLIER_LOTCODE_REQUIRED: "Een leverancier lotcode is vereist voor inname orders.",
        INTAKE_AMOUNT_REQUIRED: "Een Intake bedrag is nodig voor intake orders.",
    },
    Warehouse:{
        SUM_AVAILABLE_AND_TOCORRECT_INCORRECT: "Indien de serie nummer ingevuld is, moet de som van beschikbaar en te corrigeren steeds gelijk zijn aan 0 of 1",
        NOT_SELECTED: "Geen magazijn geselecteerd",
        SAVE_CURRENT_CHANGES: "Gelieve de huidige wijzigingen eerst op te slaan",
        TRANSFER_TO_SAME_WAREHOUSE: "Geen transfer mogelijk naar zelfde magazijn",
        DESELECT_SUPPLIER: "Gelieve eerst de leverancier te deselecteren",
        DESELECT_CUSTOMER: "Gelieve eerst de klant te deselecteren",
        INVALID_DATE: "Ongeldige datum",
        INVALID_PRICE: "Ongeldige prijs"
    },
    Scan: {
        SCAN_NOTFOUND: "Geen match gevonden.",
        LOC_VALIDATE: "Geef een locatie eerste, alvorens verder te gaan.",
        INAMOUNT_VALIDATE: "Intaked bedrag moet lager dan of gelijk aan bedrag.",
        SCAN_PRODUCTFOUND: "Gescande product is succesvol getraceerd.",
        SCAN_LOTFOUND: "Scanned supplier lot is traced successfully.",
        SCAN_LOCATIONFOUND: "Scanned location is traced successfully.",
        CURRENTPRODUCTEMPTY: "Please provide product first before proceeding.",
        AMOUNTLESSTHAN0: "Amount is less than 0.",
        PRODUCTAMOUNTCHANGED: "Intake amount changed."
    },
    Customer: {
        CUS_TOOMANY_DEFAULT_ADDRESS: "Slechts één adres kan het standaard adres voor de levering of facturatie zijn",
    },
    Trace: {
        TRACE_VALIDATE: "Please provide ateast a lot code, supplier lot code or an article serial number, before proceeding.",
        TRACE_NO_DOCUMENT: "Geen document beschikbaar."
    },
    ServiceOrder: {
        SERVICEORDER_ADJUSTSTOCK: "Close this service order and adjust stock if necessary?"
    },
    LensFitter: {
        CLEAR: "Verwijderen"
    }
});