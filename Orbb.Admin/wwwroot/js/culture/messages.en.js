﻿$.extend($.validator.messages, {
    Username: {

        required: "Username is required"

    },
    Password: {

        required: "Password is required"

    },
    General: {
        welcome: "General",
        unsavedChanges: "You have unsaved changes. Are you sure you want to leave this page",
        changeDetected: "A change was registered, don't forget to save.",
        changeDetected_SaveAfterProcessAllRows: "A change was registered, don't forget to process all rows and save.",
        noDiscountSet: "There is no discount set collection available",
        unExpectedResult: "An unexpected error occured.",
        G_DELETE: "Are you sure you want to delete this object?",
        G_VAR_DELETED: "%NAME% has been deleted.",
        G_WARNING_DELETE_IN_EDIT: "Attention, you deleted an object while editing other rows. Please save your changes to confirm the delete",
        G_SAVED: "Successfuly saved!",
        G_DELETE_ERROR: "Error while removing!",
        G_SAVE_ERROR: "Error while saving!",
        G_UPDATE_CONFLICT: "Conflict while updating!",
        G_SAVING: "Saving, please wait...",
        G_Days: "day(s)",
        G_DESCRIPTION_REQIURED: "Description is required",
        G_D_LINKED_CATEGORY: "Unable to delete because the category is linked to a productcategory.",
        G_USERRIGHTUSED: "Unable to delete because the user right is linked to a usergroup.",
        G_CANCEL: "Are you sure you want to cancel your changes?",
        G_VAL_ERROR: "Could not load comment.",
        G_NO_SELECTION: "No lines selected",
        G_MISSING_ELEMENT: "A configuration element is missing in order to perform this action",
        G_EMPTY_RESULT: "Empty result for your search",
        G_NO_SEARCH_TERM: "Please enter a search term",
        G_GENERATENOTE_ERROR: "Error bij het genereren van het document.",
        G_IMAGE_ONLY_1_ALLOWED: "Maximum one image allowed",
        G_IMAGE_MIN_SIZE: "Minimal image resolution is %SIZE%",
        G_IMAGE_ONLY_PNG_ALLOWED: "Only png files allowed"
    },
    Category: {
        CAT_CODEVAL: "The field Code must be a string with a maximum length of 10",
        CAT_LANG_DUPLICATION: "Duplicate language selection exists",
        CAT_CODE_DUPLICATION: "Category code already exists. Code should be Unique."
    },
    Maintenance: {
        MAIN_ADD_CUSTOMER_TO_DIVISION_ERROR: "The customer is not available for this division, add the customer to this division first",
        MAIN_ADD_PRODUCT_TO_DIVISION_ERROR: "The product is not available for this division, add the product to this division first",
    },
    DiscountSets: {
        DS_ADDED: "A new discount set was added",
        DS_UNSAVED_DISCOUNTS: "Please save your discounts for  %DISCOUNTSET_NAME% first, before adding a new discount set"
    },
    EditUserCredentials: {
        INVALID_PASSWORD: "The entered password was invalid",
        ENTER_PASSWORD: "Enter your password",
        PASSWORDS_DONT_MATCH: "Passwords dont match",
        EMPTY_PASSWORD: "Password cannot be empty"
    },
    Pricelists: {
        pricelist_ADDED: "A new pricelist has been added"
    },
    ProductReturns: {
        incorrect_quantity: "Entered quantity is incorrect!"
    },
    ReturnIntakes: {
        INCORRECT_SUM_RETURNEDANDDECLINEDAMOUNTS: "The sum of Amount Accepted and Amount Declined has to be equal to returned product quantity!",
        REASONDECLINED_REQUIRED: "When the declined amount is added, the Reason Declined is mandatory!",
        LOCATION_REQUIRED: "When there is Amount Accepted and the Return To Stock is marked, the location is mandatory field!",
        SUPPLIER_REQUIRED: "Supplier required",
        PURCHASEPRICE_REQUIRED: "Purchaseprice required"
    },
    Retour: {
        STOCKARTICLE_NOT_FOUND: "Could not find the stock article, make sure either the serial number or lot code is entered correctly"
    },
    OrderHistory: {
        OH_VALIDATE: "Please provide %NAME% first, before proceeding search.",
        //NEW TASK MAIKEL
        OH_SUPPLIERID_PRODUCTGROUP_PRODUCT: "Supplier or ProductGroup or Product",
        OH_STARTDATE_ENDDATE: "Start Date or End Date",
        OH_INV_ENDDATE: "End Date should have dd/mm/yyyy format",
        OH_INV_STARTDATE: "Start Date should have dd/mm/yyyy format",
        OH_INV_DATES: "valid start and enddates",
        //OLD
        OH_STARTDATE: "Start Date",
        OH_ENDDATE: "End Date",
        OH_STARTENDDATE: "Start and End Dates",
        OH_NO_ROWS: "No articles found to order"
    },
    SalesOrder: {
        WAREHOUSE_REQUIRED: "A warehouse is required for consignation orders",
        SO_DELIVERYADDRESS_REQUIRED: "Delivery address is required.",
        SO_INVOICEADDRESS_REQUIRED: "Invoice address is required.",
        SO_CONFIRMBATCHPICKING: "Do you want to reserve the articles based on their default location?",
        CONSIGNATIONENDDATE_REQUIRED: "Consignation end date is required for consignation orders."
    },
    Delivery: {
        CURRENT_ORDER_CONFLICT: "The selected line is part of the selected order to deliver and is therefor a required line for this delivery",
        DEL_VALIDATE_LINES: "Please select the order lines for processing the delivery.",
        DELIVERY_COST_UPDATE: "The delivery cost was changed based on the dispensation settings",
        SO_GENERATEPROFORMAINVOICE_ERROR: "Error on genrating proforma invoice."
    },
    Supplier: {
        SUP_AddressInvaid: "Country and Type are required in Contact address",
        SUP_DeliveryCondition_REQUIRED: "Delivery condition is required",
        SUP_Name_REQUIRED: "Name is required",
        SUP_Code_REQUIRED: "Code is required",
        SUP_Contact_FirstName_REQUIRED:"First name is required"
    },
    Product: {
        PROD_Name_REQUIRED: "Name is required",
        PROD_Code_REQUIRED: "Code is required",
        PROD_Description_REQUIRED: "Description is required",
        PROD_ShortDescription_REQUIRED: "Short description is required",
        PROD_Division_REQUIRED: "Division is required"
    },
    Intake: {
        WAREHOUSE_REQUIRED: "A warehouse is required for intake orders.",
        LOCATION_REQUIRED: "A location is required for intake orders.",
        SUPPLIER_LOTCODE_REQUIRED: "A Supplier lotcode is required for intake orders.",
        INTAKE_AMOUNT_REQUIRED: "An Intake Amount is required for intake orders.",
    },
    Warehouse: {
        SUM_AVAILABLE_AND_TOCORRECT_INCORRECT: "If the serial number is entered, then the sum of available and to correct must be 0 or 1",
        NOT_SELECTED: "No warehouse selected",
        SAVE_CURRENT_CHANGES: "Save current changes first",
        TRANSFER_TO_SAME_WAREHOUSE: "No transfer to the same warehouse possible",
        DESELECT_SUPPLIER: "First deselect supplier",
        DESELECT_CUSTOMER: "First deselect customer",
        INVALID_DATE: "Invalid date",
        INVALID_PRICE: "Invalid price"
    },
    Scan:{
        SCAN_NOTFOUND: "No match found.",
        LOC_VALIDATE: "Please provide a location first, before proceeding.",
        INAMOUNT_VALIDATE: "Intaked amount should be less than or equal to amount.",
        SCAN_PRODUCTFOUND: "Scanned product is traced successfully.",
        SCAN_LOTFOUND: "Scanned supplier lot is traced successfully.",
        SCAN_LOCATIONFOUND: "Scanned location is traced successfully.",
        CURRENTPRODUCTEMPTY: "Please provide product first before proceeding.",
        AMOUNTLESSTHAN0: "Amount is less than 0.",
        PRODUCTAMOUNTCHANGED: "Intake amount changed."
    },
    Customer: {
        CUS_TOOMANY_DEFAULT_ADDRESS: "only one address can be the default address for delivery or invoicing",
        CUS_INVOICE2_CUSTOMER_REQUIRED:"Invoice to customer is requred"
    },
    Trace: {
        TRACE_VALIDATE: "Please provide ateast a lot code, supplier lot code or an article serial number, before proceeding.",
        TRACE_NO_DOCUMENT: "No document available."
    },
    ServiceOrder: {
        SERVICEORDER_ADJUSTSTOCK: "Close this service order and adjust stock if necessary?"
    },
    LensFitter: {
        CLEAR: "Clear"
    }
});