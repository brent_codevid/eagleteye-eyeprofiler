﻿$(document).ready(function () {

    //Change Division
    function onSelectDivision(e) {
        var item = this.dataItem(e.item.index());
        var divisionId = null;
        if (item.value != "empty")
            divisionId = item.value;

        var returnUrl = encodeURI(window.location.pathname);
        var url = "/Security/Login/SwitchDivision?divisionId=" + divisionId + "&returnUrl=" + returnUrl;

        document.location.href = url;
        /*
        var url = "/Home/ChangeDivision?divisionId=" + divisionId;
        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            success: function (message) {
                window.location.reload(true);
            },
            error: function () {
                showNotification($.validator.messages.General.unExpectedResult);
            }
        });*/
    }

    // create ComboBox from select HTML element
    $("#division").kendoComboBox({
        select: onSelectDivision
    });

    // Change Language
    function onSelectLanguage(e) {
        var item = this.dataItem(e.item.index());
        if (item.value == "empty")
            return;
        var url = "/Home/ChangeLanguage?languageId=" + item.value;

        $.ajax({
            type: "POST",
            url: url,
            contentType: false,
            processData: false,
            success: function (message) {
                window.location.reload(true);
            },
            error: function () {
                showNotification($.validator.messages.General.unExpectedResult);
            }
        });
    }

    // create ComboBox from select HTML element
    $("#language").kendoComboBox({
        select: onSelectLanguage
    });

});