﻿var OldFiles;
// Initialize the jQuery File Upload widget
$("#fileupload").fileupload({
    url: "/lensfitting/overview/UploadFileAsync",
    acceptFileTypes: new RegExp(/(\.|\/)(json||)$/i),
    success: function (response, status) {
        var rows = $();
        $.each(response.files, function (index, file) {

            const row = $('<tr class="template-download fade in">' +
                '<td><span class="preview"></span></td>' +
                '<td><div class="name"></div>' +
                '<td><div class="result"></div>' +
                '<td><div class="fittingAngle"></div>' +
                '<td><div class="SelectedAngle"></div>' +
                (file.Error ? '<div class="error"></div>' : "") +
                "</td>" +
                '<td><button class= "btn btn-warning cancel"><i class="glyphicon glyphicon-ban-circle"></i><span>' + $.validator.messages.LensFitter.CLEAR + "</span></button ></td >" +
                "</tr>");
            row.find(".size").text(file.size);
            if (file.Error) {
                row.find(".name").text(file.Name);
                row.find(".error").text(file.Error);
            } else {
                row.find(".name").text(file.Name);
                row.find(".result").text(file.FittedLens.Name);
                row.find(".fittingAngle").text("flat:" + file.FittedLens.FittingAngle);
                row.find(".SelectedAngle").text("selected:" + file.FittedLens.SelectedAngle);
                row.find("button.delete")
                    .attr("data-type", file.delete_type)
                    .attr("data-url", file.delete_url);
            }
            rows = rows.add(row);
        });
        $(".files").append(rows);
    },
    error: function (error) {
    },
    autoUpload: true,
    downloadTemplateId: null
});
