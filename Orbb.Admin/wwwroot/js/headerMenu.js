﻿function logout() {

    document.location.href = "/Security/Login/exit";

};

function openProfile() {

        if (!$("#profileMenu").is(':visible')) {

            $("#profileMenu").slideDown(300);

        }
        else {

            $("#profileMenu").slideUp(300);

        }

}

function openMainMenu() {

    document.location.href = '/home/';

}
    
function openLeftMenu() {

            if (!$("#leftMenu").is(':visible')) {

                $('#openMenuIcon').css({ 'display': 'none' });
                $('#openLeftMenuIcon').css({ 'background':'url("/images/leftmenuopen.png")', 'background-size': 'cover', 'background-position':'center' });
                $("#detailView").css({ 'float': 'right' });
                $("#subButtonMenu").css({ 'transform': 'translate(-42%,-50%)' });

                var width = $(document).width();

                switch (true) {
                    case (width > 1539 && width < 2400):

                        $("#detailView").animate({ 'width': '81.5%' }, 300);
                        $("#detailView").css({ 'margin-left': '2.6%', 'margin-right': '3%' });
                        $("#leftMenu").delay(300).show(300);

                        break;

                    case (width > 1239 && width < 1540):

                        $("#detailView").animate({ 'width': '77.5%' }, 300);
                        $("#detailView").css({ 'margin': '45px 3.7%' });
                        $("#leftMenu").delay(300).show(300);

                        break;

                    case (width > 959 && width < 1240):

                        $("#detailView").animate({ 'width': '74%' }, 300);
                        $("#detailView").css({ 'margin': '40px 4.2%' });
                        $("#leftMenu").delay(300).show(300);

                        break;

                    case (width > 639 && width < 960):

                        $("#detailView").animate({ 'width': '68%' }, 300);
                        $("#detailView").css({ 'margin': '35px 5.9%' });
                        $("#leftMenu").delay(300).show(300);

                        break;

                }

            }

            else {

                $('#openMenuIcon').css({ 'display': 'block' });
                $('#openLeftMenuIcon').css({ 'background': 'url("/images/leftmenuopenwhite.png")', 'background-size': 'cover', 'background-position': 'center' });
                $("#leftMenu").hide(250);
                $("#subButtonMenu").css({ 'transform': 'translate(-50%,-50%)' });
                $("#leftMenu").hide();

                var width = $(document).width();

                switch (true) {
                    case (width > 1539 && width < 2400):

                        $("#detailView").animate({ 'width': '94.7%' }, 300);
                        $("#detailView").css({ 'margin-left': '2.6%', 'margin-right': '3%' });

                        break;

                    case (width > 1239 && width < 1540):

                        $("#detailView").animate({ 'width': '93%' }, 300);
                        $("#detailView").css({ 'margin': '45px 3.7%' });

                        break;

                    case (width > 959 && width < 1240):

                        $("#detailView").animate({ 'width': '92%' }, 300);
                        $("#detailView").css({ 'margin': '40px 4.2%' });

                        break;

                    case (width > 639 && width < 960):

                        $("#detailView").animate({ 'width': '89%' }, 300);
                        $("#detailView").css({ 'margin': '35px 5.9%' });

                        break;

                }

            }
}

function openMenu() {

        var title = $("#detailView #detailHeaderTitle ul li a").text();
        var customers = ["Klanten", "Clients", "Customers"];

        switch (title) {
            case 'Klanten':

                $('#tma-customers').addClass('active');
                $('#mma-customers').addClass('active');

                break;

                //case (suppliers):

                //    $("#detailView").animate({ 'width': '77.5%' }, 300);
                //    $("#detailView").css({ 'margin': '45px 3.7%' });
                //    $("#leftMenu").delay(300).show(300);

                //    break;

                //case (width > 959):

                //    $("#detailView").animate({ 'width': '74%' }, 300);
                //    $("#detailView").css({ 'margin': '40px 4.2%' });
                //    $("#leftMenu").delay(300).show(300);

                //    break;

                //case (width > 639):

                //    $("#detailView").animate({ 'width': '68%' }, 300);
                //    $("#detailView").css({ 'margin': '35px 5.9%' });
                //    $("#leftMenu").delay(300).show(300);

                //    break;

        }

        if (!$("#topMenuNavigation").is(':visible')) {

            $('#openMenuIcon').css({ 'color': '#333' });
            $('#topMenuNavigation').slideDown(300);

        }

        else {

            $('#openMenuIcon').css({ 'color': 'white' });
            $('#topMenuNavigation').slideUp(300);
        }

}


$(window).click(function () {
    $('#topMenuNavigation').slideUp(300);
});

$('#topMenuNavigation').click(function (event) {
    event.stopPropagation();
});

$("#openMenuIcon").click(function (event) {
    event.stopPropagation();
});
