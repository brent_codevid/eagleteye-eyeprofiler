﻿function showMessage(message) {
    alertify.alert(message).set('labels', { ok: " ", cancel: " " });
}

// eg. showConfirmationDialog("message", function() {alert('ok');},function() {alert('cancel');})
function showConfirmationDialog(message, onOK, onCancel) {
    alertify.confirm("", message, function (e) {
        if (e) {
            onOK();
        } else {
            onCancel();
        }
    }, function () {
        if (typeof onCancel === 'function') {
            onCancel()
        }
    }).set('labels', { ok: " ", cancel: " " });
}

// eg. showPrompt("message", "1", function (str) { alert(str); }, function () { alert('cancel'); })
function showPrompt(message, defaultValue, onSubmit, onCancel, type) {
    if (!type) {
        type = "text";
    }

    alertify.prompt("", message, defaultValue, function (e, str) {
        // str is the input text
        if (e) {
            onSubmit(str);
        } else {
            onCancel();
        }
    }, function () {
        if (typeof onCancel === 'function') {
            onCancel()
        }
        }).set({ 'labels': { ok: " ", cancel: " " }, 'type': type });
}

function showNotification(message, type) {
    alertify.set('notifier', 'delay', 10);

    switch (type) {
        case 1:
            alertify.success(message);
            break;
        case 2:
            alertify.error(message, 0);
            break;
        default:
            alertify.message(message);
            break;
    }
}

// begin Commonly used notifications

function showSavedNotification() {
    showNotification($.validator.messages.General.G_SAVED, 1);
}

// end of Commonly used notifications