﻿
// main menu items
function securityOverview (isSideMenu) {
    document.location.href = '/security/menu';
}

function settingsOverview (isSideMenu) {

    document.location.href = '/settings/menu';
}

function lensdatasetOverview (isSideMenu) {
    document.location.href = '/lenssets/overview';
}

function lenssupplierOverview (isSideMenu) {
    document.location.href = '/LensSuppliers/overview';
}

function customersOverview(isSideMenu) {
    document.location.href = '/Customers/overview';
}

function ordersOverview(isSideMenu) {
    document.location.href = '/Orders/overview';
}

function lensfittingOverview(isSideMenu) {

    document.location.href = '/Lensfitting/menu';
}

// end main menu items

// security
function security_usersOverview(isSideMenu) {
    document.location.href = '/security/users';
}

function security_usergroupsOverview(isSideMenu) {
    document.location.href = '/security/usergroups';

}
// end security

// settings

function settings_lenstypesOverview (isSideMenu) {
    document.location.href = '/LensTypes/overview';
}

function settings_countriesOverview(isSideMenu) {
    document.location.href = '/Countries/overview';
}


function settings_controlpointcategoriesOverview (isSideMenu) {
    document.location.href = '/ControlPointCategories/overview';
}
// end settings

// lensfitting 
function lensfitting_algorithmtestingOverview(isSideMenu) {
    document.location.href = '/Lensfitting/overview';
}
// end lensfitting