﻿$(function () {
    var ts = $(".detailContainerContentTabBar").kendoTabStrip({
        animation: { open: { effects: "fadeIn" } },
        contentUrls: [
            null,
            null,
            null,
            null
        ]
    }).data('kendoTabStrip');

    // create ComboBox from select HTML element
    $("#userLanguageId").kendoComboBox({
        select: onUserLanguageSelection
    });
    var languageIDS = $("#userLanguageId").data("kendoComboBox").value();
    if (parseInt(languageIDS) > 0) {
        $("#hdnLanguageID").val(parseInt(languageIDS));
    }

    $('.changeRequest').hover(
        function () { $('.requestIcon').addClass('fas fa-unlock'); $('.usernameIcon').removeClass('fas fa-lock'); },
        function () { $('.requestIcon').removeClass('fas fa-unlock'); $('.usernameIcon').addClass('fas fa-lock'); }
    )

    $(".changeRequest").click(function () {
        showPrompt($.validator.messages.EditUserCredentials.ENTER_PASSWORD, "", checkPassword, null, "password");
    });

    $(".TabBarStyle").append('<li class="tabBarCommandButtons"> </li>');
    $(".tabBarCommandButtons").append('<button type="button" id="canDeleteButton" class="icon far fa-trash-alt iconOnlyButtonRed" onclick="deleteUser()"></button>');

    if (applicationContext.cantDelete) {
        var deleteButton = $("#canDeleteButton");
        deleteButton.attr("disabled", true);
        deleteButton.wrap("<div class='cantdelete'></div>");
        deleteButton.parent().kendoTooltip({ content: kendo.template($("#cantdelete-template").html()) });
    }
});

function onUserLanguageSelection(e) {
    var item = this.dataItem(e.item.index());
    $("#hdnLanguageID").val(parseInt(item.value));
}

function submitCredentials() {
    var username = $("#UserNamePopUp").val();
    var password = $("#PasswordPopUp").val();
    var confirmPassword = $("#ConfirmPasswordPopUp").val();

    if (password != confirmPassword) {
        showNotification($.validator.messages.EditUserCredentials.PASSWORDS_DONT_MATCH);
        return false;
    }

    if (!password) {
        showNotification($.validator.messages.EditUserCredentials.EMPTY_PASSWORD);
        return false;
    }

    $("#modalEditCredentials").modal('hide');
    $(".username").val(username);
    $(".password").val(password);
};

function checkPassword(password) {    
    var userIdToEdit = $("#Id").val();

    $.ajax({
        type: "POST",
        url: "/Security/ChangeAccount/CheckPassword", // the URL of the controller action method
        data: { password: password, userIdToEdit: userIdToEdit },
        success: function (result) {
            if (result.succes) {
                $("#modalEditCredentialsBody").html(result.data);
                $("#modalEditCredentials").modal();
            }
            else {
                showNotification($.validator.messages.EditUserCredentials.INVALID_PASSWORD);
            }
        },
        error: function (req, status, error) {
            // do something with error
        }
    });
};

function deleteUser() {
    showConfirmationDialog(
        $.validator.messages.General.G_DELETE,
        function () {
            var form = $('#canDeleteButton').closest('form')[0];
            if (form == null || form == undefined || form == 'undefined' || form == '') {
                return;
            }
            var action = $(form).attr('action').toLowerCase();

            action = action.replace('update', 'deleteUser');
            if (action.indexOf('delete') > -1) {
                $(form).attr('action', action);
            }
            $(form).submit();
        },
        function () {
        });
}