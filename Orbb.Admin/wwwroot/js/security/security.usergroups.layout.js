﻿$(document).ready(function () {
    $(".TabBarStyle").append('<li class="tabBarCommandButtons"> </li>');
    $(".tabBarCommandButtons").append('<button type="button" id="canDeleteButton" class="icon far fa-trash-alt iconOnlyButtonRed" onclick=""></button>');

    if (applicationContext.cantDelete) {
        var deleteButton = $("#canDeleteButton");
        deleteButton.attr("disabled", true);
        deleteButton.wrap("<div class='cantdelete'></div>");
        deleteButton.parent().kendoTooltip({ content: kendo.template($("#cantdelete-template").html()) });
    }

    var ts = $(".detailContainerContentTabBar").kendoTabStrip({
        animation: { open: { effects: "fadeIn" } },
        contentUrls: [
            null,
            null
        ]
    }).data('kendoTabStrip');
});

$(window).resize(function () {
    kendo.resize($("#permissionsGrid"));
});

function index(dataItem) {
    var data = $("#permissionsGrid").data("kendoGrid").dataSource.data();

    return data.indexOf(dataItem);
}

function gridRowChanged() { }
