﻿$(function () {
    $(".detailContainerContentTabBar").kendoTabStrip({
        animation: { open: { effects: "fadeIn" } },
        contentUrls: [
            null,
            null
        ]
    }).data('kendoTabStrip');

    const id = $("#Id").val();

    $(".TabBarStyle").append('<li class="tabBarCommandButtons"> </li>');

    if (id > 0) {
        $(".tabBarCommandButtons").append('<button type="button" id="canDeleteButton" class="icon far fa-trash-alt iconOnlyButtonRed" onclick="deleteOrder()"></button>');
    } else {
        $(".hideOnNew").hide();
    }

    // create Editor from textarea HTML element with default set of tools
    $(".kendo-editor").kendoEditor({
        resizable: {
            content: true,
            toolbar: true
        },
        encoded: false,
        change: function () {
            processChanges();
        }
    });
});

function deleteOrder() {
    showConfirmationDialog(
        $.validator.messages.General.G_DELETE,
        function () {
            const form = $("#canDeleteButton").closest("form")[0];
            if (form == null || form == "undefined" || form == "") {
                return;
            }
            var action = $(form).attr("action").toLowerCase();

            action = action.replace("update", "delete");
            if (action.indexOf("delete") > -1) {
                $(form).attr("action", action);
            }
            $(form).submit();
        },
        function () {
        });
}

function onChange() {
    if (!adding) {
        processChanges();
        const list = document.getElementById("UsedLenssets");
        while (list.firstChild) {
            list.removeChild(list.firstChild);
        }
        const ids = this.selectedKeyNames();
        for (let i = 0; i < ids.length; i++) {
            addLensset(ids[i]);
        }
    }
}
