﻿
$(document).ready(function () {
    resizeGrid();
});


$(window).resize(function () {
    resizeGrid();
});

function resizeGrid() {

    $(".overviewGrid").each(function () {
        var gridElement = $(this);
        var dataArea = gridElement.find(".k-grid-content"),
        gridHeight = gridElement.innerHeight(),
        otherElements = gridElement.children().not(".k-grid-content"),
        otherElementsHeight = 0;
        otherElements.each(function () {
            otherElementsHeight += $(this).outerHeight();
        });
        var heightCorrection = gridElement.data("height-correction");
        if (!heightCorrection) heightCorrection = 0;
        dataArea.height(gridHeight - otherElementsHeight - $(".TabBarStyle").outerHeight() - heightCorrection);
    });

    $(".resizeHeightGrid").each(function () {
        var gridElement = $(this);
        var parentcontainer = gridElement.data("parent-container");
        var parentcontainerheight = $("." + parentcontainer).outerHeight();
        var dataArea = gridElement.find(".k-grid-content"),
        gridHeight = gridElement.innerHeight(),
        otherElements = gridElement.children().not(".k-grid-content"),
        otherElementsHeight = 0;
        otherElements.each(function () {
            otherElementsHeight += $(this).outerHeight();
        });
        var heightCorrection = gridElement.data("height-correction");
        if (!heightCorrection) heightCorrection = 0;
        dataArea.height(parentcontainerheight - otherElementsHeight - heightCorrection);
    });
}
