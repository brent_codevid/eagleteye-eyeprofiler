﻿$(document).ready(function () {
    $(".calculateContainerSize").parent().height($(".detailContainerContentTabBar").height() - $(".TabBarStyle").height());
});

$(window).resize(function () {
    $(".calculateContainerSize").parent().height($(".detailContainerContentTabBar").height() - $(".TabBarStyle").height());
    //resizeSettingGrids; VJ : uncomment for applying auto height calculation of all grids.
   
});

function resizeSettingGrids(e)
{
    //debugger;
    $(".k-widget .k-grid").each(function (index) {
        //console.log(index + ": " + $(this).attr('id'));
       // debugger;
        var element = document.getElementById($(this).attr('id'));
        if (typeof (element) != undefined && typeof (element) != null && typeof (element) != 'undefined') {
            var gridElement = $(element),
            oldHeight = $(element).children(".k-grid-content").height(),
            newHeight = $(".detailContainerContentTabBar").height() - $(".TabBarStyle").height();
            //if (oldHeight > newHeight)
            //{
            $(element).children(".k-grid-content").height(newHeight);
            // }
            if (e != null)
            {
                $(".detailContainerContentTabBar").promise().done(toggleGridScrollbar(e));
            }
            else {
                gridElement.toggleClass("no-scrollbar", true);
            }
            
        }
    });
   
   
   
}
function toggleGridScrollbar(e)
{
   
    var gridWrapper = e.sender.wrapper;
    var gridDataTable = e.sender.table;
    var gridDataArea = gridDataTable.closest(".k-grid-content");

    gridWrapper.toggleClass("no-scrollbar", gridDataTable[0].offsetHeight < gridDataArea[0].offsetHeight);
    
}
