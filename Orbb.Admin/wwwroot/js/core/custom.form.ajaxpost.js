﻿jQuery(document).ready(function () {

    $("form").submit(function (event) {
        if (typeof submitForm == "function") {
            event.preventDefault();

            submitForm($(this));

            return false;
        }
    })
});

function submitForm(form) {

    var submitButton = $('#submitButton');

    $.ajax({
        type: 'POST',
        url: form.prop('action'),
        accept: { javascript: 'application/javascript' },
        data: form.serialize(),
        beforeSend: function () {
            submitButton.prop('disabled', 'disabled');
        },
        success: function (data) {
            showAlertMessages(data.alertMessages);
            submitDone(data);
        },
        error: function (e) {

        }
    }).done(function (data) {
        submitButton.prop('disabled', false);
    });
}

function showAlertMessages(alerts) {
    if ((alerts || null) == null) return;
    $.each(alerts, function (i, n) {
        showNotification(n.Message, n.AlertMessageType);
    });
}

function submitDone(data) {
    if (typeof savedChanges == "function") {
        savedChanges();
    }

    if (typeof afterSubmit == "function") {
        afterSubmit(data);
    }
}