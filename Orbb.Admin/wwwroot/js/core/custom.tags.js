﻿

var collectTags = function () {
   
    var elem = $('#my-tag-list');
    if (elem.length > 0) {
        var tags = elem.tags().getTags();
        var existingTags = $.parseJSON($("#Tags").val());

        var i = 0;
        var newTags = [];
        $.each(tags, function (index, item) {
         
            var existingObj = function () {
                var obj;
                $.each(existingTags, function (index1, item1) {

                    if (item1.Text == item) {
                        obj = item1;
                        return false;
                    }

                });
                return obj;
            }();
            if (existingObj) {
                newTags.push(existingObj);
            }
            else {
                var objTag = new Object();
                objTag.Id = (i - 1);
                objTag.TargetId = 0;
                //  objTag.TagTargetType = 2;
                objTag.Text = item;
                newTags.push(objTag);
            }
        });

        //$.each(existingTags, function (index, item) {

        //    var deletedObj = function () {
        //        var obj;
        //        $.each(tags, function (index1, item1) {

        //            if (item.Text == item1) {
        //                obj = item1;
        //                return false;
        //            }

        //        });
        //        return obj;
        //    }();
        //    if (deletedObj == undefined) {
        //        item.IsDeleted = true;
        //        newTags.push(item);
        //    }

        //});
        //debugger
      
        $("#Tags").val(JSON.stringify(newTags));

    }

}