﻿$(document).ready(function () {
    $('.fileupload').unbind('fileuploaddestroy').bind('fileuploaddestroy', fileuploaddestroy);
});

// Callback for file deletion:
function fileuploaddestroy(e, data) {
    e.preventDefault();

    var that = $(this).data('blueimp-fileupload') ||
        $(this).data('fileupload'),
        removeNode = function (fileData) {
            if (fileData && fileData.files && fileData.files.length > 0) {
                var file = fileData.files[0];

                if (file && file.error) {
                    $(this).find('.error').text(file.error);
                    data.error = file.error;
                    that._trigger('destroyfailed', e, data);

                    return false;
                }
            }

            that._transition(data.context).done(
                function () {
                    $(this).remove();
                    that._trigger('destroyed', e, data);
                }
            );
        };
    if (data.url) {
        data.dataType = data.dataType || that.options.dataType;
        $.ajax(data).done(removeNode).fail(function (jqXHR, textStatus, errorThrown) {
            that._trigger('destroyfailed', e, data);
        });
    } else {
        removeNode();
    }
}