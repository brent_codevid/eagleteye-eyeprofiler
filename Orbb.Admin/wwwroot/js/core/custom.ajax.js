﻿var ajaxDo = function(url, type, data, beforeSend, complete, success) {
    $.ajax({
        url: url,
        type: type,
        cache: false,
        data: data,
        beforeSend: function (e) {
            showLoading();
            if (beforeSend !== undefined && beforeSend !== null) beforeSend(e);
        },
        complete: function (e) {
            hideLoading();
            if (complete !== undefined && complete !== null) complete(e);
        },
        success: function (e) {
            if (success !== undefined && success !== null) success(e);
        }
    });
}
var ajaxPost = function (url, data, beforeSend, complete, success) {
    ajaxDo(url, 'POST', data, beforeSend, complete, success);
}

var ajaxGet = function(url, data, beforeSend, complete, success) {
    ajaxDo(url, 'GET', data, beforeSend, complete, success);
}

function showLoading() {
    $("#spinneroverlay").show();
    $('#spinneroverlay').spin();
}

function hideLoading() {
    $('#spinneroverlay').spin(false);
    $("#spinneroverlay").hide();
}