﻿var isChanged = false;
var reloadUrl = "";
//var cancelProcessChanges = false;

(function () {
    $.fn.serializeObject = function () {
        var o = {};
        var a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery)

window.addEventListener("beforeunload", function (event) {
    if (isChanged)
        event.returnValue = $.validator.messages.General.unsavedChanges;

    event.cancelBubble = true;
});

function showChangeNotification() {
    showNotification($.validator.messages.General.changeDetected);
}

function onChangeDetected() {
    if (isChanged)
        return;

    isChanged = true;
    showChangeNotification();
}

var rdiscountsSaved = new $.Deferred();

function submitAndSave() {
    $('.form-horizontal').submit(function (event) {
        showNotification($.validator.messages.General.G_SAVING);
    });

    var saveData = function () {
        if (typeof detailSave == 'function') { detailSave(); }
        isChanged = false;
        var result = $('.form-horizontal').submit();
        $("#submitButton").attr("disabled", false);
    };

    var beforeSubmit = function () {
        if (typeof discountSet_save == 'function') {
            discountSet_save(function () {
                rdiscountsSaved.resolve();
            });
        }
        else
            rdiscountsSaved.resolve();

        return $.Deferred(function (def) {
            $.when(rdiscountsSaved).done(function () {
                def.resolve();
            });
        });
    };

        

    beforeSubmit().done(saveData);
}

function bindCommandButtons() {
    $("#submitButton").click(function () {

        $("#submitButton").attr("disabled", true);

        if (typeof clientSideValidation == 'function') {
            if (!clientSideValidation()) {
                $("#submitButton").attr("disabled", false);
                return false;
            }
        }
        if (typeof onBeforeSubmit == 'function') {
            onBeforeSubmit(submitAndSave);
        } else {
            submitAndSave();
        }
    });

    $("#cancelButton").click(function () {
        cancelChanges();
        if (reloadUrl == "")
            window.location.reload();
        else
            window.location.href = reloadUrl;

    });


    $("#popupSubmitButton").click(function ()
    {
        if (typeof salesorderlinesdeliverygrid_saveGrids == 'function') { salesorderlinesdeliverygrid_saveGrids(); }
        if (typeof salesorderlinesinvoicegrid_saveGrids == 'function') { salesorderlinesinvoicegrid_saveGrids(); }
        onChangeDetected();
    });

    $(".popupCancelButton").click(function () {
        cancelChanges();

    });
}

function initChangeCommandButtons() {
    $(".tabBarCommandButtons").append('<div type="button" id="cancelButton"><button class="btn btn-default grow fas fa-times"></button></div><div type="button" id="submitButton"><button class="btn btn-default grow fas fa-check"></button></div>');
    $("#canDeleteButton").css("display", "none");
    $('.HideWhenHasChanges').css("display", "none");
}

function animateSaveButton() {
    $("#submitButton").css("transform", "scale(1.1)");
    $("#submitButton").css("color", "green");
    setTimeout(function () {
        $("#submitButton").css("transform", "scale(1)");
        $("#submitButton").css("color", "gray");
    }, 5000)
}

function initChangedState() {
    isChanged = true;
    initChangeCommandButtons();
    bindCommandButtons();
}

function cancelChanges() {
    isChanged = false;
}

function toggleCommandButtons() {
    if (isChanged) {
        $("#cancelButton").css("display", "block");
        $("#submitButton").css("display", "block");
        $("#canDeleteButton").css("display", "none");
        $('.HideWhenHasChanges').css("display", "none");
    }
    else {
        $("#cancelButton").css("display", "none");
        $("#submitButton").css("display", "none");
        $("#canDeleteButton").css("display", "block");
        $('.HideWhenHasChanges').css("display", "block");
    }
}

function processChanges() {
    if (typeof isCancelProcessChanges == "function") {
        if (isCancelProcessChanges()) {
            return;
        }
    }

    onChangeDetected();
    
    if ($(".tabBarCommandButtons").find("#cancelButton").length > 0) {
        toggleCommandButtons();
    }
    else {
        initChangeCommandButtons();
        bindCommandButtons();
    }
}

function savedChanges() {
    isChanged = false;
    toggleCommandButtons();
}

jQuery(document).ready(function () {
    bindCommandButtons();
    bindFormElements();
});

function gridRowChanged() {
    processChanges();
}

function bindFormElements()
{
    $(".updatePanel .k-grid").change(processChanges);
    
    $("form input:not(.nochangetracking, .k-filtercell)").change(function () {
        if ($(this).closest('.k-filtercell').length == 0) { //input is not in kendo grid header filter (probably some refactoring could be done)
            processChanges();
        }
    });

    $("form select:not(.nochangetracking)").change(processChanges);

    $("form textarea:not(.nochangetracking)").change(processChanges);

    $("form .k-grid .k-grid-add:not(.nochangetracking)").click(processChanges);


    $("form .tags .tag a").click(processChanges);

    if ($('.gridtrackactions').find('.k-grid').length > 0)
        $('.gridtrackactions').find('.k-grid').data("kendoGrid").dataSource.bind("change", function (e) { if (e.action !== undefined && e.action !== 'sync') processChanges(); });

}


function showDeleteGridRowDialog(e, softDelete, sessionDelete, detailCollectionName) {
    e.preventDefault();

    if (detailCollectionName == undefined)
        detailCollectionName = "DescriptionEntries";

    var row = $(e.currentTarget).closest("tr");
    var grid = $(row.closest("[data-role=grid]")).data("kendoGrid");
    var masterGrid = this;
    var dataItem = grid.dataItem(row);
    var isNewRecord = false;

    if (dataItem.Id == 0)
        isNewRecord = true;

    showConfirmationDialog($.validator.messages.General.G_DELETE, function () {

        grid.removeRow(row);

        processChanges();
        return;

        var hasChanges = grid.dataSource.hasChanges();


        if (isNewRecord) {
            grid.dataSource.cancelChanges(dataItem);
            grid.saveChanges();
            return;
        }

        if (hasChanges || softDelete) {
            showNotification($.validator.messages.General.G_WARNING_DELETE_IN_EDIT);
            animateSaveButton();
        }
        else {
            if (grid.dataSource.hasChanges() == false) {

                var parentRow = e.target.closest(".k-detail-row");
                var masterRow = $(parentRow).prev(".k-master-row");
                var parentGrid = $(parentRow.closest("[data-role=grid]")).data("kendoGrid");
                var parentModel = grid.dataItem(masterRow);
                var entries = parentModel.get(detailCollectionName);
                var index = row.index();
                entries.splice(index, 1);

                parentModel.set(detailCollectionName, entries);
            }
            
            if (sessionDelete === undefined || sessionDelete) {
                processChanges();
            }

            grid.saveChanges();
        }
    }, function () {

    })
}



function showDeleteTreeNodeDialog(e) {
    showConfirmationDialog($.validator.messages.General.G_DELETE, function () {
        var row = $(e.currentTarget).closest("tr");
        //$(row).remove();
        $("#categorytreelist").data("kendoTreeList").dataSource.remove(row);
        //this.saveChanges();
    }, function () {
        e.preventDefault();
    })

}

function detailRowSaved(e) {
    var parentRow = e.sender.element.closest(".k-detail-row");
    var grid = $(parentRow.closest("[data-role=grid]")).data("kendoGrid");
    var masterRow = $(parentRow).prev(".k-master-row");
    var parentGrid = $(parentRow.closest("[data-role=grid]")).data("kendoGrid");
    var parentModel = grid.dataItem(masterRow);
    var entries = parentModel.get("DescriptionEntries");
    parentModel.dirty = true;

    gridRowChanged();
}
// VJ : Adding dummy negative ids on creating new entities to handle mapping issue.
//function beforeCreate(data) {
//    debugger;
//    var counter = 0;
//    var entries = null;
//    $(data.models).each(function (index) {
//        if (data.models[index].DescriptionEntries != null) {
//            entries = data.models[index].DescriptionEntries;
           
//        }
//        else {
//            if (data.models[index].NameEntries != null) {
//                entries = data.models[index].NameEntries;
//            }

//        }
//        if (entries != null)
//        {
//            for (var i = 0; i < entries.length; i++) {
//                counter--
//                if (entries[i].Id == 0) {
//                    entries[i].Id = counter
//                }
//            }
//        }
//    });
//}
var entriesIdCounter = 0;
function gridTranslation_onChange(e) {
    
    if (e.action == "add") {
        var item = e.items[0];

        if (item.Id == 0) {
            item.Id = entriesIdCounter;
            entriesIdCounter--;
        }
    }
}