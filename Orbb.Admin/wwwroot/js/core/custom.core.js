﻿var decimalSeparator = ",";

function floatPrepareForServer(f) {
    var s = "" + f;
    if (decimalSeparator === ".") {
        return s.replace(",", ".");
    } else {
        return s.replace(".", ",");
    }
}

function htmlEncode(value) {
    //create a in-memory div, set it's inner text(which jQuery automatically encodes)
    //then grab the encoded contents back out.  The div never exists on the page.
    return $('<div/>').text(value).html();
}
function htmlEscape(str) {
    return str
        .replace(/&/g, '&amp;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#39;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/\//g, '&#x2F;');
}

// I needed the opposite function today, so adding here too:
function htmlUnescape(str) {
    return str
        .replace(/&quot;/g, '"')
        .replace(/&#39;/g, "'")
        .replace(/&lt;/g, '<')
        .replace(/&gt;/g, '>')
        .replace(/&amp;/g, '&');
}

function setTextAndIdElements(text, id, element, idElement) {
    element.empty();
    element.text(text);
    idElement.val(id);
}

function formatAddressLine(address) {
    return String.format("{0}, {1} {2}", address.AddressLine1, address.PostalCode, address.City);
}
