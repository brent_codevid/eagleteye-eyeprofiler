﻿var _targetElement = '';
var jcrop_api = null;
var imageCropWidth = 0;
var imageCropHeight = 0;
var cropPointX = 0;
var cropPointY = 0;


$(document).ready(function () {
    uploadImageHoverActions($(".headerLeftInformationBlock"), $("#customerAvatar"), $("#Id"), 1);
    uploadImageHoverActions($(".headerRightInformationBlock"), $("#representativeAvatar"), $("#InternalRepresentativeId"), 2);
    uploadImageHoverActions($(".headerLeftInformationBlock"), $("#productAvatar"), $("#Id"), 4);
});

function uploadImageHoverActions(hoverElement, targetElement, idElement, targetType) {
    hoverElement.hover(
           function () {
               var imageSpan = targetElement.find(".imageSpan");
               var imageButtons = targetElement.find(".imageButtons");

               var deleteButton = $('<button/>').attr({
                   type: "button",
                   id: "deleteButton",
                   class: "btn btn-default avatarDeleteButton"
               });

               var editButton = $('<button/>').attr({
                   type: "button",
                   id: "editButton",
                   class: "btn btn-default"
               });

               var fileInput = $('<input/>').attr({
                   type: "file",
                   id: "profileImageFile",
                   style: "display: none",
                   accept: "image/*"
               });

               var hiddenInput = $('<input/>').attr({
                   type: "hidden",
                   id: "profileImagePath"
               });

               var ProfileImageSpan = $('<span/>').attr({
                   type: "span",
                   id: "profileImageName"
               });

               imageSpan.css({ "opacity": "0.2", "border": "1px solid #3dae4a", "border-top-right-radius": "60%" });
               imageButtons.append(deleteButton, editButton, fileInput, hiddenInput, ProfileImageSpan);
               imageButtons.css({ "z-index": "99" });

               deleteButton.click(function () {
                   
                   showConfirmationDialog("Would you like to delete this profile image?",
                       function ()
                       {
                           //For now set back to static
                           imageSpan.attr('src', "/images/profile_default.jpg");
                           var logoHolder = $('#Logo');
                           if (logoHolder != null && logoHolder != '' && logoHolder != undefined) {
                               $(logoHolder).val('');
                               processChanges();
                           }
                       },
                       function () { }
                       );

               });

               editButton.click(function () {
                   fileInput.click();
               });

               fileInput.change(function () {
                   var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '')
                   ProfileImageSpan.html(file_name);
                   hiddenInput.val($(this).val());

                   var data = new FormData();
                   var files = fileInput.get(0).files;

                   if (files.length > 0) {
                       data.append("file", files[0]);

                       data.append("id", 1);
                       data.append("targetType", targetType);

                       $.ajax({
                           type: "POST",
                           url: "/File/UploadAvatar",
                           contentType: false,
                           processData: false,
                           cache: false,
                           data: data,
                           success: function (data) {
                               if (data.success) {
                                   if ($('#ModalCrop').length < 1) {
                                       imageSpan.attr('src', data.src);
                                       var logoHolder = $('#Logo');
                                       if (logoHolder != null && logoHolder != '' && logoHolder != undefined) {
                                           $(logoHolder).val(data.src);
                                           processChanges();
                                       }
                                   }
                                   else {
                                       if (jcrop_api != null) {
                                           jcrop_api.destroy();
                                       }
                                       $('#origin-image').attr('src', data.src);
                                       _targetElement = targetElement;
                                       
                                       var maxWidth = ($(window).width() * 80) / 100;
                                       var maxHeight = ($(window).height() * 80) / 100;
                                       var imageWidth = data.width;
                                       var imageHeight = data.height;
                                       var cutOff = 0;
                                       var cutOffPercentage = 0;

                                       if (imageWidth > maxWidth)
                                       {
                                           cutOff = imageWidth - maxWidth;
                                           cutOffPercentage = (cutOff * 100) / imageWidth;
                                           imageWidth = maxWidth;

                                           imageHeight = imageHeight - ((imageHeight * cutOffPercentage) / 100);
                                       }

                                       if (imageHeight > maxHeight) {
                                           cutOff = imageHeight - maxHeight;
                                           cutOffPercentage = (cutOff * 100) / imageHeight;
                                           imageHeight = maxHeight;

                                           imageWidth = imageWidth - ((imageWidth * cutOffPercentage) / 100);
                                       }

                                       $('#origin-image').css('width', imageWidth + 'px');
                                       $('#origin-image').css('height', imageHeight + 'px');
                                       $('#origin-image').closest('.modal-content').css('width', (imageWidth + 40) + 'px');

                                       cropPointX = 0;
                                       cropPointY = 0;
                                       imageCropWidth = 100;
                                       imageCropHeight = 100;

                                       if (imageWidth < 100) {
                                           imageCropWidth = imageWidth;
                                       }
                                       if (imageHeight < 100) {
                                           imageCropHeight = imageHeight;
                                       }

                                       $('#ModalCrop').modal();
                                       $('#origin-image').Jcrop({
                                           onChange: setCoordsAndImgSize,
                                           aspectRatio: 1,
                                           setSelect: [imageCropWidth, imageCropHeight, cropPointX, cropPointY]
                                       }, function () { jcrop_api = this; });
                                   }
                               }
                           }
                       });
                   }

               });

           },
             function () {
                 var imageSpan = targetElement.find(".imageSpan");
                 var imageButtons = targetElement.find(".imageButtons");

                 imageSpan.css({ "opacity": "1", "border": "none", "border-top-right-radius": "0%" });
                 imageButtons.children().remove();
             }
          );
}





$(document).on("click", "#hl-crop-image", function (e) {
    e.preventDefault();
    cropImage();
});


function setCoordsAndImgSize(e) {

    imageCropWidth = e.w;
    imageCropHeight = e.h;

    cropPointX = e.x;
    cropPointY = e.y;
}

function cropImage() {
    if (imageCropWidth == 0 && imageCropHeight == 0) {
        cropPointX = 0;
        cropPointY = 0;
        imageCropWidth = $("#origin-image").width();
        imageCropHeight = $("#origin-image").height();
    }

    $.ajax({
        url: '/File/CropAvatar',
        type: 'POST',
        data: {
            imagePath: $("#origin-image").attr("src"),
            x: cropPointX,
            y: cropPointY,
            width: imageCropWidth,
            height: imageCropHeight,
            frameWidth: $("#origin-image").width(),
            frameHeight: $("#origin-image").height()
        },
        success: function (data) {
            var imageSpan = _targetElement.find(".imageSpan");
            imageSpan.attr('src', data.src);
            var logoHolder = $('#Logo');
            if (logoHolder != null && logoHolder != '' && logoHolder != undefined) {
                $(logoHolder).val(data.src.split('?')[0]);
                processChanges();
            }
        },
        error: function (data) { }
    });
}