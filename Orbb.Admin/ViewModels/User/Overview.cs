﻿using Orbb.Data.Models.Models.UI.Base;
using System.Collections.Generic;


namespace Orbb.Admin.ViewModels.User
{
    public class OverviewModel
    {
        public IList<MenuItem> MenuItems;
        public IList<MenuItem> QuickMenuItems;
    }
}
