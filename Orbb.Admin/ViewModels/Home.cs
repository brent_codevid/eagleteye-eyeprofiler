﻿using Orbb.Data.ViewModels.Models.Security;
using System.Collections.Generic;

namespace Orbb.Admin.ViewModels
{
    public class HomeModel
    {
        public IList<MenuItemVM> MenuItems;
    }
}
