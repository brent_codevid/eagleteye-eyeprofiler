﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Threading.Tasks;

namespace Orbb.Admin.Controllers
{
    public class FileController : Controller
    {
        private readonly IHostingEnvironment hostingEnvironment;

        public FileController(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        public async Task<string> UploadFile(IFormFile file, string uploadFilePath)
        {
            if (file != null && file.Length > 0)
            {
                using (var fileStream = new FileStream(uploadFilePath, FileMode.Create))
                {
                    await file.CopyToAsync(fileStream);
                }

                return uploadFilePath;
            }

            return null;
        }

        private byte[] CropImage(string imagePath, int width, int height, float x, float y, float frameWidth = 0, float frameHeight = 0)
        {
            using (Image originalImage = Image.FromFile(imagePath))
            {
                float newX = x;
                float newY = y;
                int newWidth = originalImage.Width;
                int newHeight = originalImage.Height;

                if (frameWidth > 0 && frameHeight > 0)
                {
                    newX = (originalImage.Width * x) / frameWidth;
                    newY = (originalImage.Height * y) / frameHeight;
                    newWidth = (originalImage.Width * width) / Convert.ToInt32(frameWidth);
                    newHeight = (originalImage.Height * height) / Convert.ToInt32(frameHeight);
                }

                using (Bitmap bmp = new Bitmap(newWidth, newHeight))
                {
                    bmp.SetResolution(originalImage.HorizontalResolution, originalImage.VerticalResolution);
                    using (Graphics graphic = Graphics.FromImage(bmp))
                    {
                        graphic.SmoothingMode = SmoothingMode.AntiAlias;
                        graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                        graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                        graphic.CompositingQuality = CompositingQuality.HighQuality;
                        graphic.DrawImage(originalImage, new Rectangle(0, 0, newWidth, newHeight), newX, newY, newWidth, newHeight, GraphicsUnit.Pixel);
                    }


                    MemoryStream ms = new MemoryStream();
                    bmp.Save(ms, originalImage.RawFormat);
                    return ms.GetBuffer();
                }
            }
        }

        [HttpPost]
        public async Task<JsonResult> UploadAvatar(IFormFile file, int id, int targetType)
        {
            bool isSuccess = false;
            string src = string.Empty, fileName = string.Empty, groupPhysicalPath = string.Empty, filePyshicalPath = string.Empty, filePath = String.Empty;
            string groupPath = "avatars";
            IList<string> supportedTypes = new List<string>() { "image/jpg", "image/jpeg" };
            int width = 0, height = 0;

            groupPhysicalPath = Path.Combine(hostingEnvironment.WebRootPath, groupPath);

            if (file != null)
            {
                switch (targetType)
                {
                    case 1:
                        filePath = "customers";
                        break;
                    case 2:
                        filePath = "representatives";
                        break;
                    case 3:
                        filePath = "suppliers";
                        break;
                    case 4:
                        filePath = "product";
                        break;
                    default:
                        break;
                }

                if (!string.IsNullOrEmpty(filePath) && supportedTypes.Contains(file.ContentType))
                {
                    fileName = $"{Guid.NewGuid()}.{file.ContentType.Split('/')[1]}";
                    filePyshicalPath = Path.Combine(groupPhysicalPath, filePath, fileName);
                    src = await UploadFile(file, filePyshicalPath);

                    if (!string.IsNullOrEmpty(src))
                    {
                        isSuccess = true;
                        src = $"http://{HttpContext.Request.Host.Value}/{groupPath}/{filePath}/{fileName}";

                        using (Image img = Image.FromFile(src))
                        {
                            if (img != null)
                            {
                                width = img.Width;
                                height = img.Height;
                            }
                        }
                    }
                }
            }

            var data = new { success = isSuccess, src = src, width = width, height = height };
            return Json(data);
        }

        [HttpPost]
        public JsonResult CropAvatar(string imagePath, int width, int height, float x, float y, float frameWidth, float frameHeight)
        {
            bool isSuccess = false;

            byte[] cropedImage = CropImage(imagePath, width, height, x, y, frameWidth, frameHeight);

            if (cropedImage != null)
            {
                using (MemoryStream ms = new MemoryStream(cropedImage, 0, cropedImage.Length))
                {
                    ms.Write(cropedImage, 0, cropedImage.Length);
                    using (Image img = Image.FromStream(ms, true))
                    {
                        img.Save(imagePath, img.RawFormat);
                        isSuccess = true;
                    }
                }
            }

            var data = new { success = isSuccess, src = $"{imagePath}?v={DateTime.Now.GetHashCode()}" };
            return Json(data);
        }
    }
}
