﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.Mvc;
using Orbb.Admin.Utility.UI;
using Orbb.Admin.ViewModels;
using Orbb.BL.Interfaces.Security;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Security;


namespace Orbb.Admin.Controllers
{
    [Authorize]
    public class HomeController : BaseController
    {
        ICurrent<UserMenuVM> userMenu;
        StringEncryption encrypt;
        IUserRepository userRepository;

        public HomeController(StringEncryption encrypt,
            IUserRepository userRepository,
            ICurrent<UserMenuVM> userMenu)
        {
            this.encrypt = encrypt;
            this.userMenu = userMenu;
            this.userRepository = userRepository;
        }

        public IActionResult Index()
        {
            MenuHelper menuHelper = new MenuHelper(menuFactory, currentUser, userMenu);
            var currentUserMenu = menuHelper.GetMenuContainer("");
            ViewBag.CompanyName = currentDivision?.GetCurrent()?.Description;

            return View(currentUserMenu);
        }

        public IActionResult ChangeLanguage(string returnUrl, int languageId)
        {
            string shortCode = userRepository.UpdateUserLanguage(languageId);
            currentLanguageVM.SetCurrent(new LanguageModel { ShortCode = shortCode });
            userMenu.SetCurrent(null);

            return Json("ok");
        }
    }
}
