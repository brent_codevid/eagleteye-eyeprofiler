﻿using AutoMapper;
using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Orbb.Admin.Utility.Mvc;
using Orbb.Admin.ViewModels;
using Orbb.BL.Interfaces;
using Orbb.BL.Interfaces.Customers;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.Lenssets;
using Orbb.BL.Interfaces.LensSuppliers;
using Orbb.BL.Interfaces.Orders;
using Orbb.BL.Interfaces.Security;
using Orbb.BL.Interfaces.Settings;
using Orbb.Data.Common;
using Orbb.Data.Models.Models.Base;
using Orbb.Data.Models.Models.Customers;
using Orbb.Data.Models.Models.Globalization;
using Orbb.Data.Models.Models.Lenssets;
using Orbb.Data.Models.Models.LensSuppliers;
using Orbb.Data.Models.Models.Orders;
using Orbb.Data.Models.Models.Views;
using Orbb.Data.ViewModels.Models.Customers;
using Orbb.Data.ViewModels.Models.Lenssets;
using Orbb.Data.ViewModels.Models.Other;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using static Orbb.Common.Enumerations;

namespace Orbb.Admin.Controllers
{
    public class DataController : BaseController
    {
        [SetterProperty]
        public IUserRepository UserRepository { get; set; }

        [SetterProperty]
        public IControlpointCategoryRepository ControlpointCategoryRepository { get; set; }

        [SetterProperty]
        public ILensSupplierRepository SupplierRepository { get; set; }

        [SetterProperty]
        public ILensTypeRepository LensTypeRepository { get; set; }

        [SetterProperty]
        public IStringLocalizer Localizer { get; set; }

        [SetterProperty]
        public IRepository<Country> CountryRepository { get; set; }

        [SetterProperty]
        public IRepository<Translation> TranslationRepository { get; set; }

        [SetterProperty]
        public ICurrentStorage<LanguageModel> CurrentStorage { get; set; }

        [SetterProperty]
        public ILookupProvider LookupProvider { get; set; }

        [SetterProperty]
        public ILensRepository LensRepository { get; set; }

        [SetterProperty]
        public IOrderRepository OrderRepository { get; set; }

        [SetterProperty]
        public IOrderStatusRepository OrderStatusRepository { get; set; }

        [SetterProperty]
        public ICustomerRepository CustomerRepository { get; set; }

        [SetterProperty]
        public IMapper Mapper { get; set; }

        public JsonResult GetLandingZoneComboboxItems()
        {
            //for each LandingZoneBaseType defined in the enumeration, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = Enum.GetValues(typeof(LandingZoneBaseType))
                .Cast<LandingZoneBaseType>()
                .Select(landingZoneBase => new ComboBoxItemVm((int)landingZoneBase, landingZoneBase.ToString()))
                .ToList();

            return Json(items);
        }

        public JsonResult GetTypeComboboxItems()
        {
            //for each LensType defined in the enumeration, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = Enum.GetValues(typeof(LensTypes))
                .Cast<LensTypes>()
                .Select(result => new ComboBoxItemVm((int)result, stringLocalizer[result.ToString()]))
                .ToList();

            return Json(items);
        }

        public JsonResult GetMeridian1PositionComboboxItems()
        {
            //for each LensType defined in the enumeration, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = Enum.GetValues(typeof(MeridianPosition))
                .Cast<MeridianPosition>()
                .Select(result => new ComboBoxItemVm((int)result, stringLocalizer[result.ToString()]))
                .ToList();

            return Json(items);
        }
        public JsonResult GetSectionsComboboxItems()
        {
            //for each Section defined in the enumeration, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = Enum.GetValues(typeof(Sections))
                .Cast<Sections>()
                .Select(section => new ComboBoxItemVm((int)section, section.ToString()))
                .ToList();

            return Json(items);
        }

        public JsonResult GetAxisComboboxItems()
        {
            //for each ControlPointCategory defined in the enumeration, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = Enum.GetValues(typeof(ControlPointCategories))
                .Cast<ControlPointCategories>()
                .Select(result => new ComboBoxItemVm((int)result, stringLocalizer[result.ToString()]))
                .ToList();

            return Json(items);
        }

        public JsonResult GetSupplierComboboxItems()
        {
            //for each LensSupplier, add a new ComboBoxItemVM to return value
            IList<ComboBoxItemVm> items = new List<ComboBoxItemVm>();
            var results = SupplierRepository.GetAll<LensSupplier>(null);
            foreach (LensSupplier result in results)
            {
                items.Add(new ComboBoxItemVm(result.Id, result.Name));
            }

            return Json(items);
        }

        public JsonResult GetSag2Items(int lensId, [DataSourceRequest] DataSourceRequest request)
        {
            IList<CP2ToCP1ItemVM> items = new List<CP2ToCP1ItemVM>();

            try
            {
                var results = LensRepository.GetCP2ToCP1ItemsByLensId(lensId);
                foreach (CP2ToCP1Item result in results)
                {
                    items.Add(new CP2ToCP1ItemVM(result.Id, result.CP2Value, result.CP1Value));
                }
            }
            catch (Exception)
            {
                // ignored
            }

            return Json(items.ToDataSourceResult(request));
        }

        public JsonResult GetLensesForCountry(int countryId, [DataSourceRequest] DataSourceRequest request)
        {

            IList<LensDetail> results = LensRepository.GetAll(new[] { "Supplier" });
            List<LenssetVM> output = new List<LenssetVM>();

            foreach (LensDetail result in results)
            {
                output.Add(Mapper.Map<LenssetVM>(result));
            }
            return Json(output.ToDataSourceResult(request));
        }

        public JsonResult GetCustomerGridItems(int customerId, [DataSourceRequest] DataSourceRequest request, int newCountryId = 0)
        {
            List<int> availableLensIds;
            if (newCountryId == 0)
            {
                Customer customer = CustomerRepository.GetSingle(c => c.Id == customerId, new[] { "Country", "Country.AvailableLenssets" });
                if (customer.CountryId == null)
                {
                    availableLensIds = LensRepository.GetAll(new string[] { }).Select(l => l.Id).ToList();
                }
                else
                {
                    availableLensIds = customer.Country.AvailableLenssets.Select(l => l.Id).ToList();
                }

            }
            else
            {
                availableLensIds = CountryRepository.GetSingle(c => c.Id == newCountryId, new[] { "AvailableLenssets" }).AvailableLenssets.Select(l => l.Id).ToList();
            }


            IList<LensDetail> results = LensRepository.GetAllAvailableInCountry(availableLensIds, new[] { "Supplier" });
            List<LenssetVM> output = new List<LenssetVM>();

            foreach (LensDetail result in results)
            {
                output.Add(Mapper.Map<LenssetVM>(result));
            }
            return Json(output.ToDataSourceResult(request));
        }

        public JsonResult GetCustomerComboboxItems()
        {
            IList<ComboBoxItemVm> items = CustomerRepository.GetOverview()
                .Select(result => new ComboBoxItemVm(result.Id, result.Name))
                .ToList();

            return Json(items);
        }

        public JsonResult GetOrderStatusComboboxItems()
        {
            IList<ComboBoxItemVm> items = OrderStatusRepository.GetOrderStatusOverview()
                .Select(result => new ComboBoxItemVm(result.Id, Localizer[result.Name]))
                .ToList();

            return Json(items);
        }

        public JsonResult GetLensesComboboxItems()
        {
            try
            {
                IList<ComboBoxItemVm> items = new List<ComboBoxItemVm>();
                var results = LensRepository.GetAll<LensDetail, LenssetVM>(new[] { "Supplier" });
                foreach (LenssetVM result in results)
                {
                    items.Add(new ComboBoxItemVm(result.Id,
                        result.Supplier + " " + result.Name + " " + result.LensDiameter));
                }

                return Json(items);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public JsonResult GetOrders(int customerId, [DataSourceRequest] DataSourceRequest request)
        {
            IList<OrderView> items = new List<OrderView>();

            var results = OrderRepository.GetOrdersByCustomerId(customerId);
            foreach (Order result in results)
            {
                OrderView ov = new OrderView(result);
                ov.StatusName = Localizer[ov.StatusName];
                items.Add(ov);
            }

            ModifyDateFilters(request.Filters, "Date");
            return Json(items.ToDataSourceResult(request));
        }

        public JsonResult GetCountryComboboxItems()
        {
            IList<ComboBoxItemVm> items = new List<ComboBoxItemVm>();

            var results = CountryRepository.GetAll(new string[] { });
            foreach (Country result in results)
            {
                items.Add(new ComboBoxItemVm(result.Id, result.Description));
            }

            return Json(items);
        }
    }
}