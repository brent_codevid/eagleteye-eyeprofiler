﻿using AutoMapper;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json.Serialization;
using Orbb.Admin.DependencyResolution;
using Orbb.Admin.Middleware;
using Orbb.Admin.Utility.Localization;
using Orbb.Admin.ViewModels;
using Orbb.BL.Mappings;
using Orbb.Common.Environment;
using Orbb.Common.Localization;
using Orbb.Data.Common;
using StructureMap;
using System;

namespace Orbb.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.AddLocalization(o =>
            {
                o.ResourcesPath = "Resources";
            });

            // Maintain property names during serialization. See: https://github.com/aspnet/Announcements/issues/194
            //services.AddMvc(options => options.Filters.Add(new AutoValidateAntiforgeryTokenAttribute()))
            services.AddMvc(o =>
                {
                    var loc = services.BuildServiceProvider().GetService<IStringLocalizer<SharedResource>>();
                    o.ModelBindingMessageProvider.SetValueMustBeANumberAccessor(
                        x => string.Format(loc["val_must_be_a_number"], x));
                    o.ModelBindingMessageProvider.SetUnknownValueIsInvalidAccessor( //doesn't work
                        x => string.Format(loc["val_is_required"], x));
                })
                .AddDataAnnotationsLocalization()
                .AddJsonOptions(options => options.SerializerSettings.ContractResolver = new DefaultContractResolver())
                .AddControllersAsServices();

            // Adds default in-memory implementation of IDistributedCache.
            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                options.Cookie.HttpOnly = true;
            });

            // If you don't want the cookie to be automatically authenticated and assigned to HttpContext.User, 
            // remove the CookieAuthenticationDefaults.AuthenticationScheme parameter passed to AddAuthentication.
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                    .AddCookie(options =>
                    {
                        options.LoginPath = "/Security/Login/Index";
                        options.LogoutPath = "/Security/Login/Exit";
                    });

            services.AddAntiforgery();

            var env = services.BuildServiceProvider().GetService<IHostingEnvironment>();
            Config.BasePath = env.ContentRootPath;

            // Add Kendo UI services to the services container
            services.AddKendo();

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            // Automapper configuration
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfile>();
            });

            services.AddSingleton(sp => config.CreateMapper());

            // Add StructureMap dependency injection framework (replace standard DI with StructureMap DI librabry)
            var container = new Container(new DefaultRegistry());
            container.Populate(services);
            BL.Bootstrapper.RegisterWithContainer(container);
            Common.Bootstrapper.RegisterWithContainer(container);

            return container.GetInstance<IServiceProvider>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app,
            IHostingEnvironment env,
            ILoggerFactory loggerFactory,
            IStringLocalizerFactory stringLocalizerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseStatusCodePages();
                app.UseBrowserLink();
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseSession();
            app.UseStaticFiles();

            // Enable the application to use a cookie to store information for the signed in user
            app.UseAuthentication();

            app.UseLanguageMiddleware();

            var currentUser = app.ApplicationServices.GetRequiredService<ICurrentUser>();
            var currentLanguage = app.ApplicationServices.GetRequiredService<ICurrent<LanguageModel>>();
            var availableCultures = app.ApplicationServices.GetRequiredService<IAvailableCultures>().GetCultures();
            var locOptions = app.ApplicationServices.GetService<IOptions<RequestLocalizationOptions>>();

            locOptions.Value.RequestCultureProviders.Insert(0, new UserRequestCultureProvider(currentUser, currentLanguage));
            // locOptions.Value.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
            locOptions.Value.SupportedCultures = availableCultures;
            locOptions.Value.SupportedUICultures = availableCultures;

            app.UseRequestLocalization(locOptions.Value);

            //app.UseDivisionMiddleware();

            app.UseMvc(routes =>
            {
                routes.MapRoute(name: "areaRoute",
                    template: "{area:exists}/{controller=Home}/{action=Index}/{id?}");

                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            // Configure Kendo UI
            app.UseKendo(env);
        }
    }
}