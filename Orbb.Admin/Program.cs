﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using NLog.Web;
using System;

namespace Orbb.Admin
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // NLog: setup the logger first to catch all errors
            var logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            try
            {
                logger.Debug("init main");

                BuildWebHost(args).Run();
            }
            catch (Exception e)
            {
                logger.Error(e, "Exception");
                throw;
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseNLog() // setup NLog for Dependency injection
                           //.UseSetting("detailedErrors", "true")
                           //.CaptureStartupErrors(true)
                .Build();
    }
}