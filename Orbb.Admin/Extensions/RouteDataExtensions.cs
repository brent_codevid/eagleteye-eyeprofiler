﻿using Microsoft.AspNetCore.Routing;

namespace Orbb.Admin.Extensions
{
    public static class RouteDataExtensions
    {
        public static string GetRequiredString(this RouteData session, string key)
        {
            var requiredString = session.Values.TryGetValue("controller", out object keyObject) ? keyObject.ToString() : string.Empty;

            return requiredString;
        }
    }
}