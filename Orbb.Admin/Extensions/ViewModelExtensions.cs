﻿using Microsoft.AspNetCore.Http;
using Orbb.Common.Utility;
using Orbb.Data.ViewModels.Attributes;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.Globalization;
using System.Collections.Generic;
using System.Linq;

namespace Orbb.Admin.Extensions
{
    public static class ViewModelExtensions
    {
        public static void UpdateTranslations(this IViewModel model, IFormCollection form, IViewModel original)
        {
            if (model == null)
                return;

            int minId1 = -1000;
            int minId2 = -2000;

            var translationproperties = model.GetType()
                    .GetProperties()
                    .Where(p => p.PropertyType == typeof(TranslationVM)
                    && p.CustomAttributes.All(ca => ca.AttributeType != typeof(IgnoreSave)))
                    .ToList();

            foreach (var translationprop in translationproperties)
            {
                TranslationVM translation = translationprop.GetValue(model) as TranslationVM;
                if (translation == null)
                {
                    if (original != null)
                        translation = CloneObject.Clone((translationprop.GetValue(original))) as TranslationVM;

                    if (translation == null)
                    {
                        translation = new TranslationVM { DefaultValue = "", Id = --minId1 };
                    }
                    translationprop.SetValue(model, translation);
                }

                if (translation.Entries == null)
                    translation.Entries = new List<TranslationEntryVM>();

                string prefix = "Translation_" + translationprop.Name + "_";
                var editors =
                    form.Keys.Where(k => k.StartsWith(prefix)).ToList();
                if (editors.Count > 0)
                {
                    foreach (string editorKey in editors)
                    {
                        string sId = editorKey.Replace(prefix, "");
                        string value = form[editorKey];
                        if (int.TryParse(sId, out var id))
                        {
                            TranslationEntryVM entry = translation.Entries.FirstOrDefault(e => e.LanguageId == id);
                            if (entry == null && !string.IsNullOrEmpty(value))
                            {
                                entry = new TranslationEntryVM { LanguageId = id, Id = --minId2 };
                                translation.Entries.Add(entry);
                            }
                            if (entry != null)
                            {
                                string oldValue = entry.Value;
                                entry.Value = value;
                                if (translation.DefaultValue == oldValue ||
                                    string.IsNullOrEmpty(translation.DefaultValue))
                                {
                                    translation.DefaultValue = value;
                                }
                            }
                        }
                    }
                }
            }
        }

        public static void UpdateTranslations(this IViewModel model, IFormCollection form)
        {
            model?.UpdateTranslations(form, null);
        }
    }
}