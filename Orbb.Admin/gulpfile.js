/// <binding ProjectOpened='sass:watch' />
"use strict";

var gulp = require('gulp'),
 sass = require("gulp-sass");

var paths = {
    webroot: "./wwwroot/"
};

gulp.task("sass", function () {
    return gulp.src('./styles/**/theme*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('wwwroot/css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./styles/**/theme*.scss', ['sass']);
});

gulp.task('default', function () {
    console.log('default task!');
});