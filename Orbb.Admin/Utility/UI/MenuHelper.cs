﻿using Orbb.BL.Interfaces.UISetup;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Security;
using System.Threading;

namespace Orbb.Admin.Utility.UI
{
    public class MenuHelper
    {
        IMenuRepository menuFactory;
        ICurrent<UserMenuVM> userMenu;
        ICurrentUser currentUser;
        public MenuHelper(IMenuRepository menuFactory,
            ICurrentUser currentUser,
            ICurrent<UserMenuVM> userMenu)
        {
            this.currentUser = currentUser;
            this.userMenu = userMenu;
            this.menuFactory = menuFactory;
        }

        public UserMenuVM GetMenuContainer(string currentMenu)
        {
            UserMenuVM currentUserMenu = userMenu.GetCurrent();

            int currentUserId = currentUser.GetCurrentUser()?.Id ?? 1;
            string languageCode = currentUser.GetCurrentUser()?.Language?.Code ?? Thread.CurrentThread.CurrentUICulture.Name;

            if (currentUserMenu == null)
            {
                currentUserMenu = new UserMenuVM
                {
                    MainMenuItems = menuFactory.GetMenu(currentUserId, languageCode, null),
                    SubMenuItems = menuFactory.GetMenu(currentUserId, languageCode, currentMenu)
                };
                userMenu.SetCurrent(currentUserMenu);
            }
            else if (currentMenu != string.Empty && currentUserMenu.SubMenuItems.Count == 0)
                currentUserMenu.SubMenuItems = menuFactory.GetMenu(currentUserId, languageCode, currentMenu);

            return currentUserMenu;
        }
    }
}