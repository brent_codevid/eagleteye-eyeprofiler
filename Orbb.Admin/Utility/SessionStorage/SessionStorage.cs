﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;

namespace Orbb.Admin.Utility.SessionStorage
{
    public class SessionStorage : ISessionStorage
    {
        private readonly IHttpContextAccessor httpContextAccessor;
        private static readonly object lockObject = new object();
        private ISession Session
        {
            get
            {
                lock (lockObject)
                {
                    return httpContextAccessor.HttpContext.Session;
                }
            }
        }

        public SessionStorage(IHttpContextAccessor httpContextAccessor)
        {
            this.httpContextAccessor = httpContextAccessor;
        }

        public void Set<T>(string key, T value)
        {
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/app-state?view=aspnetcore-2.2
            // Session state is non-locking. If two requests simultaneously attempt to modify the contents of a session, the last request overrides the first. Session is implemented as a coherent session, which means that all the contents are stored together. When two requests seek to modify different session values, the last request may override session changes made by the first.
            lock (lockObject)
            {
                byte[] data = new byte[0];
                var binaryFormatter = new BinaryFormatter();

                if (value != null)
                {
                    using (var ms = new MemoryStream())
                    {
                        binaryFormatter.Serialize(ms, value);
                        data = ms.ToArray();
                    }
                }
                //var k = Thread.CurrentThread.ManagedThreadId;

                Session.Set(key, data);
                Session.CommitAsync();
            }

        }

        public async void SetAsync<T>(string key, T value)
        {
            // https://docs.microsoft.com/en-us/aspnet/core/fundamentals/app-state?view=aspnetcore-2.2
            // Session state is non-locking. If two requests simultaneously attempt to modify the contents of a session, the last request overrides the first. Session is implemented as a coherent session, which means that all the contents are stored together. When two requests seek to modify different session values, the last request may override session changes made by the first.

            byte[] data = new byte[0];
            var binaryFormatter = new BinaryFormatter();

            if (value != null)
            {
                using (var ms = new MemoryStream())
                {
                    binaryFormatter.Serialize(ms, value);
                    data = ms.ToArray();
                }
            }
            //var k = Thread.CurrentThread.ManagedThreadId;
            if (!Session.IsAvailable)
                await Session.LoadAsync();

            Session.Set(key, data);
            await Session.CommitAsync();
        }

        public T Get<T>(string key)
        {
            T returnValue = default(T);
            var binaryFormatter = new BinaryFormatter();

            byte[] data = Session.Get(key);

            if (data != null && data.Length > 0)
            {
                using (var ms = new MemoryStream(data))
                {
                    returnValue = (T)binaryFormatter.Deserialize(ms);
                }
            }

            return returnValue;
        }

        public async Task<T> GetAsync<T>(string key)
        {
            T returnValue = default(T);
            var binaryFormatter = new BinaryFormatter();

            if (!Session.IsAvailable)
                await Session.LoadAsync();
            byte[] data = Session.Get(key);

            if (data != null && data.Length > 0)
            {
                using (var ms = new MemoryStream(data))
                {
                    returnValue = (T)binaryFormatter.Deserialize(ms);
                }
            }

            return returnValue;
        }

        public object Get(string key)
        {

            object returnValue = null;

            var binaryFormatter = new BinaryFormatter();

            byte[] data = Session.Get(key);

            if (data != null && data.Length > 0)
            {
                using (var ms = new MemoryStream(data))
                {
                    returnValue = binaryFormatter.Deserialize(ms);
                }
            }

            return returnValue;
        }

        public void Remove(string key)
        {
            lock (lockObject)
            {
                Session.Remove(key);
                Session.CommitAsync();
            }
        }

        public IEnumerable<string> Keys()
        {
            lock (lockObject)
            {
                return Session.Keys;
            }
        }
    }
}