﻿
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Orbb.Admin.Utility.SessionStorage
{
    public interface IStorage
    {
        void Set<T>(string key, T value);
        T Get<T>(string key);
        void Remove(string key);
        IEnumerable<string> Keys();
    }
}
