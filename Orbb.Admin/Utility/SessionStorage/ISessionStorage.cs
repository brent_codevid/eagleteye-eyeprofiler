﻿using System.Threading.Tasks;

namespace Orbb.Admin.Utility.SessionStorage
{
    public interface ISessionStorage : IStorage
    {
        object Get(string key);
        void SetAsync<T>(string key, T value);
        Task<T> GetAsync<T>(string key);
    }
}
