﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Orbb.Admin.ViewModels;
using Orbb.Data.Common;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace Orbb.Admin.Utility.Localization
{
    public class UserRequestCultureProvider : IRequestCultureProvider
    {
        private readonly ICurrentUser currentUser;
        private readonly ICurrent<LanguageModel> currentLanguage;

        public UserRequestCultureProvider(ICurrentUser currentUser, ICurrent<LanguageModel> currentLanguage)
        {
            this.currentUser = currentUser;
            this.currentLanguage = currentLanguage;
        }

        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            var userLanguageCode = currentUser?.GetCurrentUser()?.Language?.Code;

            if (string.IsNullOrEmpty(userLanguageCode))
                userLanguageCode = currentLanguage?.GetCurrent()?.ShortCode;

            if (string.IsNullOrEmpty(userLanguageCode))
                return Task.FromResult((ProviderCultureResult)null);

            var requestCulture = new ProviderCultureResult(userLanguageCode);
            Debug.WriteLine(Thread.CurrentThread.CurrentCulture);
            return Task.FromResult(requestCulture);
        }
    }
}
