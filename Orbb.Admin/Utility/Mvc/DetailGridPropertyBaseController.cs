﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Extensions;
using Orbb.Admin.Utility.SessionStorage;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.UISetup;
using Orbb.Data.Common;
using Orbb.Data.Common.General;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace Orbb.Admin.Utility.Mvc
{
    public abstract class DetailGridPropertyBaseController<TEntity, TViewModel> : BaseController
         where TViewModel : class, IViewModel where TEntity : class, IEntity, new()
    {
        [SetterProperty]
        public ILookupProvider LookupProvider { get; set; }

        [SetterProperty]
        public IMenuRepository MenuFactory { get; set; }
        [SetterProperty]
        public IDataService DataService { get; set; }
        [SetterProperty]
        public ISessionStorage SessionStorage { get; set; }
        public string SessionStorageKey { get; set; }

        protected string[] modelIncludes;
        protected string redirectTo;
        protected string sessionObjectKey;
        protected string propertyName;

        // protected override Initialize
        public DetailGridPropertyBaseController(string sessionObjectKey = "MainProperty", string propertyName = "")
        {
            this.sessionObjectKey = sessionObjectKey;
            this.propertyName = propertyName;
        }
        protected void SetSessionModel(List<TViewModel> vm)
        {
            var model = SessionStorage.Get<object>(sessionObjectKey);

            PropertyInfo propertyInfo = model.GetType().GetProperty(propertyName);

            propertyInfo.SetValue(model, Convert.ChangeType(vm, propertyInfo.PropertyType), null);

            SessionStorage.Set(sessionObjectKey, model);
        }

        public IActionResult Read([DataSourceRequest] DataSourceRequest request, int foreignKeyId = 0, string foreignKeyName = "")
        {
            List<TViewModel> entities = new List<TViewModel>();

            if (foreignKeyId != 0)
            {
                var param = Expression.Parameter(typeof(TEntity), "e");
                Expression nameProperty = Expression.Property(param, foreignKeyName);
                var body = Expression.Equal(nameProperty, Expression.Constant(foreignKeyId));

                var predicate = Expression.Lambda<Func<TEntity, bool>>(body, param);

                entities = DataService.GetAll<TEntity, TViewModel>(predicate, modelIncludes);

                SetSessionModel(entities);
            }

            return Json(entities.ToDataSourceResult(request));
        }

        [HttpPost]
        public IActionResult Delete([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<TViewModel> entities)
        {
            var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

            if (entities.Any())
            {
                object original = SessionStorage.Get<object>(sessionObjectKey);
                PropertyInfo propertyInfo = original.GetType().GetProperty(propertyName);
                List<TViewModel> orgEntities = (List<TViewModel>)propertyInfo.GetValue(original);

                foreach (var entity in entities)
                {
                    TViewModel originalEntry = orgEntities.First(p => p.Id == entity.Id);

                    if (!CheckCanDelete(entity.Id, out var reasons))
                    {
                        StringBuilder sBuilder = new StringBuilder();
                        foreach (var item in reasons)
                        {
                            sBuilder.Append(stringLocalizer[item] + Environment.NewLine);
                        }

                        alertMessages.Add(new AlertMessage()
                        {
                            Message = sBuilder.ToString(),
                            AlertMessageType = Enumerations.AlertMessageTypes.Error,
                            Subject = "Delete"
                        });
                    }
                    else
                    {
                        orgEntities.Remove(originalEntry);
                    }
                }

                SetSessionModel(orgEntities);
            }

            foreach (var item in alertMessages)
            {
                ModelState.AddModelError("", item.Message);
            }

            return Json(entities.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public IActionResult Create([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<TViewModel> entities)
        {
            var results = new List<TViewModel>();
            var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

            if (entities != null && ModelState.IsValid)
            {
                object orginal = SessionStorage.Get<object>(sessionObjectKey);
                PropertyInfo propertyInfo = orginal.GetType().GetProperty(propertyName);
                List<TViewModel> orgEntities = (List<TViewModel>)propertyInfo.GetValue(orginal) ?? new List<TViewModel>();

                foreach (var entity in entities)
                {
                    orgEntities.Add(entity);
                    results.Add(entity);
                }

                SetSessionModel(orgEntities);
            }

            foreach (var item in alertMessages)
            {
                ModelState.AddModelError(item.Subject, item.Message);
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        [HttpPost]
        public IActionResult Update([DataSourceRequest] DataSourceRequest request, [Bind(Prefix = "models")]IEnumerable<TViewModel> entities)
        {
            var results = new List<TViewModel>();
            var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

            if (entities != null && ModelState.IsValid)
            {
                object orginal = SessionStorage.Get<object>(sessionObjectKey);
                PropertyInfo propertyInfo = orginal.GetType().GetProperty(propertyName);
                List<TViewModel> orgEntities = (List<TViewModel>)propertyInfo.GetValue(orginal);

                foreach (var entity in entities)
                {
                    TViewModel original = orgEntities.First(p => p.Id == entity.Id);

                    if (original == null)
                    {
                        alertMessages.Add(new AlertMessage() { AlertMessageType = Enumerations.AlertMessageTypes.Info, Message = stringLocalizer["G_UPDATE_CONFLICT"] });
                        TempData.Set(Constants.AlertMessages, alertMessages);
                    }
                    else
                    {
                        int index = orgEntities.IndexOf(original);
                        orgEntities[index] = entity;
                        results.Add(entity);
                    }
                }

                SetSessionModel(orgEntities);
            }

            foreach (var item in alertMessages)
            {
                ModelState.AddModelError("", item.Message);
            }

            return Json(results.ToDataSourceResult(request, ModelState));
        }

        private bool CheckCanDelete(int id, out string[] reasons)
        {
            bool canDelete = DataService.CanDelete<TEntity>(id, out reasons);

            return canDelete;
        }
    }
}
