﻿using Kendo.Mvc;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Orbb.Admin.ViewModels;
using Orbb.BL.Interfaces.Base;
using Orbb.BL.Interfaces.Security;
using Orbb.BL.Interfaces.UISetup;
using Orbb.Common.Security;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.System;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using static Orbb.Data.Common.Enumerations;
using FilterDescriptor = Kendo.Mvc.FilterDescriptor;

namespace Orbb.Admin.Utility.Mvc
{
    public class BaseController : Controller
    {
        [SetterProperty]
        public ICurrentUser currentUser { get; set; }
        [SetterProperty]
        public ICurrent<LanguageModel> currentLanguageVM { get; set; }
        [SetterProperty]
        public ICurrent<DivisionVM> currentDivision { get; set; }
        [SetterProperty]
        public IUserValidation userValidation { get; set; }
        [SetterProperty]
        public IStringLocalizer stringLocalizer { get; set; }
        [SetterProperty]
        public IMenuRepository menuFactory { get; set; }
        [SetterProperty]
        public IDivisionRepository divisionRepository { get; set; }
        [SetterProperty]
        public IHostingEnvironment hostingEnvironment { get; set; }
        [SetterProperty]
        public ILogger<BaseController> baseLogger { get; set; }

        protected const string SESSIONKEYFORMAT = "SESSION-{0}-{1}"; // SESSION-[OBJECTKEY]-[ID]

        //NOTE: Language has to be set before OnActionExecuting. Because the Model DataBinders happen before OnActionExecuting wich can cause problems for number conversions,
        //since it uses default culture. Perhaps this should be better placed in Global.asax Application_BeginRequest
        //protected override void Initialize(RequestContext requestContext)
        //{
        //    CheckLanguage();
        //    CheckDivision(requestContext.HttpContext);

        //    currentLanguage = currentUser?.GetCurrentUser()?.Language?.Code;
        //    if (String.IsNullOrEmpty(currentLanguage))
        //        currentLanguage = currentLanguageVM?.GetCurrent()?.ShortCode;
        //    if (String.IsNullOrEmpty(currentLanguage))
        //    {
        //        string headerLang = requestContext.HttpContext.Request.Headers["Accept-Language"];
        //        if (!string.IsNullOrEmpty(headerLang))
        //        {
        //            if (headerLang.Contains(","))
        //                currentLanguage = headerLang.Substring(0, headerLang.IndexOf(','));
        //            else
        //                currentLanguage = headerLang;
        //        }
        //        string customLang = requestContext.HttpContext.Request.Headers["my-culture"];
        //        if (!string.IsNullOrEmpty(customLang))
        //            currentLanguage = customLang;
        //    }
        //    if (currentLanguage != null)
        //    {
        //        Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture = new CultureInfo(currentLanguage);
        //    }
        //    base.Initialize(requestContext);
        //}

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            Debug.WriteLine(Thread.CurrentThread.CurrentCulture);
            if (!CheckSecurity())
            {
                context.Result = new RedirectResult("/");
                return;
            }

            base.OnActionExecuting(context);
        }

        private bool CheckSecurity()
        {
            var attr = GetType().GetCustomAttributes(typeof(SecuredObjectAttribute), true);
            var user = currentUser?.GetCurrentUser();
            if (attr != null && attr.Length > 0 && user != null)
            {
                return userValidation.UserHasAccess(user.Id, (attr[0] as SecuredObjectAttribute).Name);
            }
            return (attr == null) || (attr.Length == 0);
        }

        protected string GetSessionKey(string sessionObjectKey, int id)
        {
            return string.Format(SESSIONKEYFORMAT, sessionObjectKey, id);
        }

        protected IActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return Redirect("/");
            }
        }

        [HttpPost]
        public bool AddQuickMenuItem(int menuItemId)
        {
            return menuFactory.AddQuickMenuItem(menuItemId).Success;
        }

        [HttpPost]
        public bool RemoveQuickMenuItem(int menuItemId)
        {
            return menuFactory.RemoveQuickMenuItem(menuItemId).Success;
        }
        [HttpPost]
        public bool OrderQuickMenuItem(int menuItemId)
        {
            return menuFactory.OrderQuickMenuItem(menuItemId).Success;
        }

        public IActionResult Error()
        {
            // Get exception details
            var exceptionHandler = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionHandler != null)
            {
                // Get route the exception occurred at
                var routeWhereExceptionOccurred = exceptionHandler.Path;

                // Get the exception that occurred
                var ex = exceptionHandler.Error;

                // Log
                baseLogger?.LogError(ex, $"{routeWhereExceptionOccurred} - {ex.Message}");
            }

            return View("~/Views/Shared/Error.cshtml");
        }

        public FileStreamResult PrintReport(string fileName)
        {
            if (fileName == null || fileName == "null")
                return null;

            var fullPath = Path.Combine(hostingEnvironment.WebRootPath, "Documents");
            FileStream stream = System.IO.File.OpenRead(fullPath);
            byte[] fileBytes = new byte[stream.Length];

            stream.Read(fileBytes, 0, fileBytes.Length);
            stream.Close();

            MemoryStream ms = new MemoryStream();
            ms.Write(fileBytes, 0, fileBytes.Length);
            ms.Position = 0;

            HttpContext.Response.Headers.Add("content-disposition", "inline; filename=\"" + fileName + "\"");
            return new FileStreamResult(ms, "application/pdf");
        }

        public bool CheckSecurity(PermissionType permissionType)
        {
            var attr = GetType().GetCustomAttributes(typeof(SecuredObjectAttribute), true);
            var user = currentUser?.GetCurrentUser();
            if (attr != null && attr.Length > 0 && user != null)
            {
                return userValidation.UserHasAccess(user.Id, (attr[0] as SecuredObjectAttribute).Name, permissionType);
            }
            return (attr == null) || (attr.Length == 0);
        }

        public bool CheckReadOnly()
        {
            var attr = GetType().GetCustomAttributes(typeof(SecuredObjectAttribute), true);
            var user = currentUser?.GetCurrentUser();
            if (attr != null && attr.Length > 0 && user != null)
            {
                return userValidation.UserHasReadOnlyAcces(user.Id, (attr[0] as SecuredObjectAttribute).Name);
            }
            return (attr == null) || (attr.Length == 0);
        }

        //Modifies the filters so that we only filter on the Date part of our DateTime field
        protected void ModifyDateFilters(IEnumerable<IFilterDescriptor> filters, string member)
        {
            if (filters == null || !filters.Any())
                return;
            foreach (IFilterDescriptor filter in filters)
            {
                if (filter is FilterDescriptor descriptor && descriptor.Member == member)
                {
                    descriptor.Member = member + ".Date";
                }
                else if (filter is CompositeFilterDescriptor)
                {
                    ModifyDateFilters(((CompositeFilterDescriptor)filter).FilterDescriptors, member);
                }
            }
        }
    }
}