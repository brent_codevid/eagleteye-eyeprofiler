﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.SessionStorage;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.UISetup;
using Orbb.Data.Common;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using StructureMap.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Orbb.Admin.Utility.Mvc
{
    public abstract class DetailListPropertyBaseController<TEntity, TViewModel> : BaseController
         where TViewModel : class, IViewModel where TEntity : class, IEntity, new()
    {

        [SetterProperty]
        public ILookupProvider LookupProvider { get; set; }
        [SetterProperty]
        public IMenuRepository MenuFactory { get; set; }
        [SetterProperty]
        public IDataService DataService { get; set; }
        [SetterProperty]
        public ISessionStorage SessionStorage { get; set; }
        public string SessionStorageKey { get; set; }

        protected string[] modelIncludes;
        protected string redirectTo;
        protected string sessionObjectKey;
        protected string sessionCurrentObjectKey;
        protected List<KeyValuePair<string, string>> sessionManagedProperties;

        // protected override Initialize
        public DetailListPropertyBaseController(string sessionObjectKey = "MainProperty", string sessionCurrentObjectKey = "CurrentObject")
        {
            this.sessionObjectKey = sessionObjectKey;
            this.sessionCurrentObjectKey = sessionCurrentObjectKey;
            sessionManagedProperties = new List<KeyValuePair<string, string>>();
        }

        protected TViewModel Get(int id)
        {
            List<TViewModel> originals = SessionStorage.Get<List<TViewModel>>(sessionObjectKey);
            TViewModel original = originals.FirstOrDefault(p => p.Id == id);
            SessionStorage.Set(sessionCurrentObjectKey, original);

            return original;
        }

        [HttpPost]
        public JsonResult Add(TViewModel model)
        {
            var set = SessionStorage.Get<List<TViewModel>>(sessionObjectKey) ?? new List<TViewModel>();

            set.Add(model);
            SessionStorage.Set(sessionCurrentObjectKey, model);
            SessionStorage.Set(sessionObjectKey, set);

            return Json("ok");
        }

        [HttpPost]
        public JsonResult Update(TViewModel entity)
        {
            if (!ModelState.IsValid)
            {
                List<string> messages = new List<string>();
                foreach (var value in ModelState.Values)
                {
                    foreach (var error in value.Errors)
                    {
                        messages.Add(error.ErrorMessage);
                    }
                }
                return Json(messages.ToArray());
            }

            List<TViewModel> entities = SessionStorage.Get<List<TViewModel>>(sessionObjectKey);
            if (entities == null || !entities.Any())
                return Json("ok");

            TViewModel original = entities.First(p => p.Id == entity.Id);

            foreach (var item in sessionManagedProperties)
            {
                var model = SessionStorage.Get<object>(item.Value);

                PropertyInfo propertyInfoMainObject = model.GetType().GetProperty(item.Key);

                PropertyInfo propertyInfo = entity.GetType().GetProperty(item.Key);

                propertyInfo.SetValue(entity, propertyInfoMainObject.GetValue(model), null);
            }

            int index = entities.IndexOf(original);
            entities[index] = entity;

            SessionStorage.Set(sessionCurrentObjectKey, entity);
            SessionStorage.Set(sessionObjectKey, entities);

            return Json("ok");
        }

        public JsonResult Delete(TViewModel model)
        {
            List<TViewModel> originals = SessionStorage.Get<List<TViewModel>>(sessionObjectKey);
            TViewModel original = originals.First(p => p.Id == model.Id);

            originals.Remove(original);

            SessionStorage.Set(sessionObjectKey, originals);

            return Json("ok");
        }

        private JsonResult CheckCanDelete(int id)
        {
            bool canDelete = DataService.CanDelete<TEntity>(id, out var reasons);

            return Json(new CanDeleteResult(!canDelete, reasons));
        }
    }
}
