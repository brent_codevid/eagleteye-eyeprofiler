﻿using Kendo.Mvc.Extensions;
using Kendo.Mvc.UI;
using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Extensions;
using Orbb.BL.Interfaces.General;
using Orbb.Data.Common;
using Orbb.Data.Common.General;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Models;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Orbb.Admin.Utility.Mvc
{
    public class BaseOverviewController<TEntity, TViewModel> : BaseController
         where TViewModel : class, IViewModel where TEntity : class, IEntity, new()
    {
        public virtual TViewModel BeforeDelete(TViewModel model) { return model; }

        [SetterProperty]
        public IDataService DataService { get; set; }

        [HttpPost]
        public IActionResult DeleteItem([DataSourceRequest] DataSourceRequest request, TViewModel entity)
        {
            entity = BeforeDelete(entity);
            var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

            if (entity != null)
            {
                if (!DataService.Delete<TEntity>(entity.Id, out string[] reasons))

                {
                    StringBuilder sBuilder = new StringBuilder();
                    foreach (var item in reasons)
                    {
                        sBuilder.Append(stringLocalizer[item] + Environment.NewLine);
                    }
                    alertMessages.Add(new AlertMessage()
                    {
                        Message = sBuilder.ToString(),
                        AlertMessageType = Enumerations.AlertMessageTypes.Error,
                        Subject = "Delete"
                    });
                }

            }

            foreach (var item in alertMessages)
            {
                ModelState.AddModelError(item.Subject, item.Message);

            }
            return Json(new[] { entity }.ToDataSourceResult(request, ModelState));
        }

        [HttpGet]
        public IActionResult Index()
        {
            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }
            return View();
        }
    }
}