﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Orbb.Admin.Extensions;
using Orbb.Admin.Utility.SessionStorage;
using Orbb.BL.Interfaces.General;
using Orbb.BL.Interfaces.UISetup;
using Orbb.Common.Utility;
using Orbb.Data.Common.General;
using Orbb.Data.Models.Interfaces;
using Orbb.Data.ViewModels.Attributes;
using Orbb.Data.ViewModels.Models;
using Orbb.Data.ViewModels.Models.Globalization;
using StructureMap.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using static Orbb.Data.Common.Enumerations;

namespace Orbb.Admin.Utility.Mvc
{
    public abstract class DetailBaseController<TEntity, TViewModel> : BaseController
         where TViewModel : class, IViewModel where TEntity : class, IEntity, new()
    {
        [SetterProperty]
        public ILookupProvider LookupProvider { get; set; }
        [SetterProperty]
        public IMenuRepository MenuFactory { get; set; }
        [SetterProperty]
        public IDataService DataService { get; set; }
        [SetterProperty]
        public ISessionStorage SessionStorage { get; set; }

        protected string[] modelIncludes;
        protected string redirectTo;
        protected string OverviewURL;
        protected string sessionObjectKey;

        protected List<KeyValuePair<string, string>> sessionManagedProperties;
        protected string defaultView = "Index";

        // protected override Initialize
        public DetailBaseController(string sessionObjectKey = "MainObject")
        {
            this.sessionObjectKey = sessionObjectKey;
            sessionManagedProperties = new List<KeyValuePair<string, string>>();
        }

        public virtual void LoadViewBagData(int id = 0)
        {
            ViewBag.Languages = LookupProvider.GetLanguages();
            ViewBag.CompanyName = currentDivision.GetCurrent().Description;
        }

        public virtual void BeforeLoad(int id = 0) { }
        public virtual TViewModel AfterLoad(TViewModel model, string extraData = null, bool isCopy = false) { return model; }
        public virtual TViewModel AfterUpdate(TViewModel model, bool success) { return model; }
        public virtual void AfterUpdate(int id = 0) { }
        public virtual TViewModel BeforeUpdate(TViewModel model) { return model; }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Update(TViewModel model)
        {
            var sessionKey = GetSessionKey(sessionObjectKey, model.Id);
            var original = SessionStorage.Get<TViewModel>(sessionKey);

            UpdateTranslations(ref model, Request.Form, original);

            model = BeforeUpdate(model);

            ViewBag.ReloadUrl = "/" + ControllerContext.RouteData.DataTokens["area"] + "/" + ControllerContext.RouteData.GetRequiredString("controller") + "/index/" + model.Id.ToString();

            if (string.IsNullOrEmpty(redirectTo) || !ModelState.IsValid)
            {
                LoadViewBagData();
                CheckCanDelete(model.Id);
                if (!string.IsNullOrEmpty(Request.Form["Ids"]))
                {
                    ViewBag.List = Request.Form["Ids"];
                    ViewBag.IdsCount = Request.Form["IdsCount"];
                    ViewBag.CurrentItem = Convert.ToInt32(Request.Form["CurrentItem"]);
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["Ids"]))
                {
                    TempData.Add("List", Request.Form["Ids"]);
                    TempData.Add("IdsCount", Request.Form["IdsCount"]);
                    TempData.Add("CurrentItem", Convert.ToInt32(Request.Form["CurrentItem"]));
                }
            }

            if (!ModelState.IsValid)
            {
                var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

                foreach (var state in ModelState)
                {
                    var item = state.Value;
                    foreach (var error in item.Errors)
                    {
                        alertMessages.Add(
                            new AlertMessage() { AlertMessageType = AlertMessageTypes.Error, Message = error.ErrorMessage }
                            );
                    }
                }

                ViewBag.Errors = "Errors";

                TempData.Set(Constants.AlertMessages, alertMessages);

                return View(defaultView, model);
            }

            var currentDbVersion = DataService.Get<TEntity, TViewModel>(model.Id, modelIncludes);

            foreach (var item in sessionManagedProperties)
            {
                PropertyInfo propertyInfo = model.GetType().GetProperty(item.Key);

                propertyInfo.SetValue(model, Convert.ChangeType(SessionStorage.Get(item.Value), propertyInfo.PropertyType), null);
            }

            var result = DataService.Update<TViewModel, TEntity>(original, model, currentDbVersion, modelIncludes);

            if (!result.Success)
            {
                LoadViewBagData(model.Id);
                CheckCanDelete(model.Id);
                ViewBag.Errors = "Errors";

                var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

                if (result.SaveException != null)
                    alertMessages.Add(new AlertMessage() { AlertMessageType = AlertMessageTypes.Error, Message = result.SaveException.Message });

                if (result.EntityValidationErrors != null)
                {
                    foreach (var error in new HashSet<string>(result.EntityValidationErrors))
                        alertMessages.Add(new AlertMessage() { AlertMessageType = AlertMessageTypes.Error, Message = stringLocalizer[error] });
                }

                TempData.Set(Constants.AlertMessages, alertMessages);

                return View(defaultView, model);
            }
            else
            {
                model.Id = result.EntityId;

                if (redirectTo.Contains("{id}"))
                    redirectTo = redirectTo.Replace("{id}", model.Id.ToString());

                AfterUpdate(model.Id);
                AfterUpdate(model, true);

                var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();

                alertMessages.Add(new AlertMessage() { AlertMessageType = AlertMessageTypes.Info, Message = stringLocalizer["G_SAVED"] });

                TempData.Set(Constants.AlertMessages, alertMessages);

                if (!string.IsNullOrEmpty(redirectTo))
                    return RedirectToLocal(redirectTo);
                else
                {
                    LoadViewBagData(model.Id);

                    return View(defaultView, model);
                }
            }
        }

        private void UpdateTranslations<T>(ref TViewModel model, IFormCollection form, T original)
        {
            int minId1 = -1000;
            int minId2 = -2000;
            var translationproperties = model.GetType()
                    .GetProperties()
                    .Where(p => p.PropertyType == typeof(TranslationVM)
                    && p.CustomAttributes.All(ca => ca.AttributeType != typeof(IgnoreSave)))
                    .ToList();
            foreach (var translationprop in translationproperties)
            {
                TranslationVM translation = translationprop.GetValue(model) as TranslationVM;
                if (translation == null)
                {
                    translation = CloneObject.Clone((translationprop.GetValue(original))) as TranslationVM;
                    if (translation == null)
                    {
                        translation = new TranslationVM { DefaultValue = "", Id = --minId1 };
                    }
                    translationprop.SetValue(model, translation);
                }

                if (translation.Entries == null)
                    translation.Entries = new List<TranslationEntryVM>();

                string prefix = "Translation_" + translationprop.Name + "_";
                var editors =
                    form.Keys.Where(k => k.StartsWith(prefix)).ToList();
                if (editors.Count > 0)
                {
                    foreach (string editorKey in editors)
                    {
                        string sId = editorKey.Replace(prefix, "");
                        string value = form[editorKey];
                        if (int.TryParse(sId, out var id))
                        {
                            TranslationEntryVM entry = translation.Entries.FirstOrDefault(e => e.LanguageId == id);
                            if (entry == null && !String.IsNullOrEmpty(value))
                            {
                                entry = new TranslationEntryVM { LanguageId = id, Id = --minId2 };
                                translation.Entries.Add(entry);
                            }
                            if (entry != null)
                            {
                                string oldValue = entry.Value;
                                entry.Value = value;
                                if (translation.DefaultValue == oldValue ||
                                    String.IsNullOrEmpty(translation.DefaultValue))
                                {
                                    translation.DefaultValue = value;
                                }
                            }
                        }
                    }
                }

            }
        }

        [HttpPost]
        public IActionResult Cancel(TViewModel model)
        {
            return RedirectToLocal(redirectTo);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Delete(TViewModel model)
        {
            var alertMessages = TempData.Get<IList<AlertMessage>>(Constants.AlertMessages) ?? new List<AlertMessage>();
            if (!CheckSecurity(PermissionType.Delete))
            {
                alertMessages.Add(new AlertMessage() { AlertMessageType = AlertMessageTypes.Error, Message = stringLocalizer["G_NOTALLOWED"] });
                TempData.Set(Constants.AlertMessages, alertMessages);

                return Redirect("/");
            }
            if (DataService.Delete<TEntity>(model.Id, out var reasons))
            {
                alertMessages.Add(
                    new AlertMessage() { AlertMessageType = AlertMessageTypes.Info, Message = stringLocalizer["G_DELETE_SUCCES"] }
                );
                TempData.Set(Constants.AlertMessages, alertMessages);
                return RedirectToLocal(OverviewURL);
            }

            else
            {
                foreach (var error in reasons)
                {
                    alertMessages.Add(
                        new AlertMessage() { AlertMessageType = AlertMessageTypes.Error, Message = stringLocalizer[error] }
                        );
                }
                TempData.Set(Constants.AlertMessages, alertMessages);

                LoadViewBagData();
                CheckCanDelete(model.Id);
                return View(defaultView, model);
            }
        }

        [HttpGet]
        public IActionResult Index(int id, string data = null, bool isCopy = false)
        {
            var sessionKey = GetSessionKey(sessionObjectKey, id);

            if (TempData["List"] != null)
            {
                ViewBag.IsList = true;
                ViewBag.List = TempData["List"];
                ViewBag.IdsCount = TempData["IdsCount"];
                ViewBag.CurrentItem = TempData["CurrentItem"];
            }

            if (CheckReadOnly())
            {
                ViewBag.Readonly = "true";
            }

            BeforeLoad(id);

            TViewModel vm = null;

            vm = id == 0 ? DataService.GetNew<TEntity, TViewModel>() : DataService.Get<TEntity, TViewModel>(id, modelIncludes);

            vm = AfterLoad(vm, data, isCopy);
            vm.Id = isCopy ? 0 : vm.Id;

            SessionStorage.Set(sessionKey, vm);

            LoadViewBagData(id);
            CheckCanDelete(id);

            return View(defaultView, vm);
        }

        [HttpPost]
        public IActionResult List(string ids, int currentListItem)
        {
            List<string> idsList = ids.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList();
            ViewBag.IsList = true;
            ViewBag.List = ids;
            ViewBag.IdsCount = idsList.Count;
            ViewBag.CurrentItem = currentListItem;

            return Index(Convert.ToInt32(idsList[currentListItem]));
        }

        private void CheckCanDelete(int id)
        {
            bool canDelete = DataService.CanDelete<TEntity>(id, out var reasons);
            var translatedReasons = reasons.Where(r => !string.IsNullOrEmpty(r)).Select(r => stringLocalizer[r]);

            //string reasonsString = string.Empty;
            //Array.ForEach(reasons, (str) => reasonsString += stringLocalizer[str] + "&#013;&#010;");     
            ViewBag.CantDelete = !canDelete;
            ViewBag.CantDeleteReasons = translatedReasons;
        }

        protected T ConvertData<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }
    }
}