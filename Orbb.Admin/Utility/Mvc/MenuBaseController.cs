﻿using Microsoft.AspNetCore.Mvc;
using Orbb.Admin.Utility.UI;
using Orbb.Data.Common;
using Orbb.Data.ViewModels.Models.Security;

namespace Orbb.Admin.Utility.Mvc
{
    public class MenuBaseController : BaseController
    {
        ICurrent<UserMenuVM> userMenu;
        private readonly string currentMenu;

        public MenuBaseController(ICurrent<UserMenuVM> userMenu, string currentMenu)
        {
            this.userMenu = userMenu;
            this.currentMenu = currentMenu;
        }

        public IActionResult Index()
        {
            MenuHelper menuHelper = new MenuHelper(menuFactory, currentUser, userMenu);
            ViewBag.CompanyName = currentDivision.GetCurrent().Description;
            var currentUserMenu = menuHelper.GetMenuContainer(currentMenu);

            return View("Index", currentUserMenu);
        }
    }
}