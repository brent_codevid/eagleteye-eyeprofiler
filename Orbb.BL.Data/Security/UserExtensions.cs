﻿using Orbb.Data.EF;
using Orbb.Data.Models.Models.Security;
using System;
using System.Data.Entity;
using System.Linq;

namespace Orbb.BL.Data.Security
{
    public static class UserExtensions
    {
        public static User CheckUser(this User usr, IDatabaseContext dbContext, string username, string password)
        {
            var user =
                dbContext.Query<User>().Where(u => u.UserName == username && u.Password == password).Include(u => u.Language).FirstOrDefault();
            return user;
        }

        public static void TouchUser(this User user, IDatabaseContext dbContext)
        {
            user.LastLoggedInAt = DateTime.Now;
            dbContext.Update(user);
            dbContext.SaveChanges(null);
        }
    }
}
