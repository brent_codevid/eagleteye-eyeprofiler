﻿using Orbb.Data.Models.Models.Globalization;
using System.Linq;

namespace Orbb.BL.Data.Globalization
{
    public static class TranslationExtention
    {
        public static string Value(this Translation translation, int languageId)
        {
            TranslationEntry entry = translation.Entries.SingleOrDefault(e => e.LanguageId == languageId);
            return entry == null ? translation.DefaultValue : entry.Value;
        }
    }
}
