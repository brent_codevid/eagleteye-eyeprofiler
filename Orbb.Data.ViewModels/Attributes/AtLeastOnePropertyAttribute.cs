﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Reflection;

namespace Orbb.Data.ViewModels.Attributes
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class AtLeastOnePropertyAttribute : ValidationAttribute
    {
        private string[] PropertyList { get; }

        public AtLeastOnePropertyAttribute(params string[] propertyList)
        {
            PropertyList = propertyList;
        }

        public override object TypeId => this;

        public override bool IsValid(object value)
        {
            foreach (string propertyName in PropertyList)
            {
                PropertyInfo propertyInfo = value.GetType().GetProperty(propertyName);

                if (propertyInfo != null && propertyInfo.GetValue(value, null) != null)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
