﻿using System;

namespace Orbb.Data.ViewModels.Attributes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class DeleteWhenRemovedFromCollection : Attribute
    {
    }
}
