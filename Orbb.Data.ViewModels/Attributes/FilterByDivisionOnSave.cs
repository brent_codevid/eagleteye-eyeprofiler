﻿using System;

namespace Orbb.Data.ViewModels.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FilterByDivisionOnSave : Attribute
    {
    }
}
