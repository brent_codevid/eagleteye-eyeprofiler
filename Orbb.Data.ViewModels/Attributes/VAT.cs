﻿using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Attributes
{
    public class ValidateVAT : ValidationAttribute
    {
        public string CountryField { get; }

        public ValidateVAT(string countryField)
        {
            CountryField = countryField;
        }

        //protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        //{
        //    string country = validationContext.ObjectType.GetProperty(this.CountryField).GetValue(validationContext.ObjectInstance, (object[])null).ToString();

        //    if (value != null && !string.IsNullOrEmpty((string)value))
        //    {
        //        EuropeVAT.checkVatService vatService = new EuropeVAT.checkVatService();
        //        bool isValid;
        //        string name, address;
        //        string vatNr = value.ToString().ToUpper().Replace(country.ToUpper(), "").Replace(" ", "").Replace(".", "").Replace("-", "");

        //        try
        //        {
        //            vatService.checkVat(ref country, ref vatNr, out isValid, out name, out address);
        //        }
        //        catch
        //        {
        //            try
        //            {
        //                System.Threading.Thread.Sleep(200);
        //                vatService.checkVat(ref country, ref vatNr, out isValid, out name, out address);
        //            }
        //            catch (Exception ex)
        //            {
        //                //LogService.LogDB(ex.Message, "ProductController.Index", LogTypes.ERROR, "nr: " + vatNr + ", country: " + country, Bco.Ecommerce.ServiceContext.CurrentContext.GetContext());
        //                return (ValidationResult)null;
        //            }

        //            return (ValidationResult)null;
        //        }

        //        if (isValid)
        //            return (ValidationResult)null;
        //        else
        //            return new ValidationResult(Resources.BcoResources.INVALID_VAT);
        //    }
        //    else
        //        return (ValidationResult)null;
        //}
    }
}
