﻿using System;

namespace Orbb.Data.ViewModels.Attributes
{
    [AttributeUsage(AttributeTargets.Class)]
    public class MapTo : Attribute
    {
        public MapTo(string type)
        {
            Type = type;
        }

        public string Type { get; set; }
    }
}
