﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Settings
{
    [Serializable]

    public class LensTypeVM : IViewModel
    {
        [Required]
        public string Name { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
