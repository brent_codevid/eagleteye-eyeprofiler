﻿using System;

namespace Orbb.Data.ViewModels.Models.Settings
{
    [Serializable]
    public class ControlPointCategoryVM : IViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int NumberOfAxes { get; set; }

        public ControlPointCategoryVM(int id, string name)
        {
            Id = id;
            Name = name;
            NumberOfAxes = id;
        }

        public ControlPointCategoryVM() { }
    }
}
