﻿namespace Orbb.Data.ViewModels.Models.Base
{
    public class DisplayOrderVM
    {
        public int Id { get; set; }
        public int DisplayOrder { get; set; }
    }
}
