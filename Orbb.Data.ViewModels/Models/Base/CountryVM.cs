﻿using Orbb.Data.ViewModels.Attributes;
using Orbb.Data.ViewModels.Models.Customers;
using System;
using System.Collections.Generic;

namespace Orbb.Data.ViewModels.Models.Base
{
    [Serializable]
    public class CountryVM : IViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public List<LenssetVM> AvailableLenssets { get; set; }
    }

    [Serializable]
    public class CountryListItem : IViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        [IgnoreSave]
        public string Description { get; set; }
    }
}