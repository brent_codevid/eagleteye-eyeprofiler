﻿using System;

namespace Orbb.Data.ViewModels.Models.Base
{
    [Serializable]

    public class MediaVM : IViewModel
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public int DisplayOrder { get; set; }
        public int Type { get; set; }
        public string MimeType { get; set; }
    }
}
