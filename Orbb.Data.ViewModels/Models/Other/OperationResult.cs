﻿namespace Orbb.Data.ViewModels.Models.Other
{
    public class OperationResult
    {
        public bool Success { get; set; }
        public string ErrorMessage { get; set; }
        public string Data { get; set; }
    }
}
