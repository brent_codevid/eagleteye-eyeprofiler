﻿namespace Orbb.Data.ViewModels.Models.Other
{
    public class MediaFileResult
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public string Type { get; set; }
        public string Url { get; set; }
        public string ThumbnailUrl { get; set; }
        public string DeleteUrl { get; set; }
        public string DeleteType { get; set; }
        public bool Video { get; set; }
        public int DisplayOrder { get; set; }
        public string Error { get; set; }
    }
}