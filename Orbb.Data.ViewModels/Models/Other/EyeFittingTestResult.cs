﻿using Orbb.Data.ViewModels.Models.LensFitter;

namespace Orbb.Data.ViewModels.Models.Other
{
    public class EyeFittingTestResult
    {
        public string Name { get; set; }
        public int Size { get; set; }
        public LensFittingResultVm FittedLens { get; set; }
        public string Error { get; set; }
    }
}