﻿namespace Orbb.Data.ViewModels.Models.Other
{
    public class ComboBoxItemVm
    {
        public int Id { get; set; }

        public string Text { get; set; }

        public string Reference { get; set; }

        public ComboBoxItemVm()
        { }

        public ComboBoxItemVm(int id, string text)
        {
            Id = id;
            Text = text;
        }
    }
}
