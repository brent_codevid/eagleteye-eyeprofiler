﻿namespace Orbb.Data.ViewModels.Models
{
    public interface IHasDivision
    {
        int DivisionId { get; set; }
    }
}
