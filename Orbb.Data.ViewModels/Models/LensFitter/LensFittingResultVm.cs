﻿using System.Collections.Generic;
using Orbb.Common.LensFitting;

namespace Orbb.Data.ViewModels.Models.LensFitter
{
    public class LensFittingResultVm
    {
        public string Name { get; set; }
        public int Id { get; set; }
        public double FittingAngle { get; set; }
        public string SupplierName { get; set; }

        public double Diameter { get; set; }
        public IList<NameValueTuple> Bcrs { get; set; }

        public IList<NameValueTuple> Vaults { get; set; }

        public IList<NameValueTuple> LandingZones { get; set; }

        public IList<NameValueTuple> Toricities { get; set; }
        public int SelectedAngle { get; set; }
    }
}
