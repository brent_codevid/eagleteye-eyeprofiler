﻿using System;

namespace Orbb.Data.ViewModels.Models.Document
{
    [Serializable]
    public class DocumentVm : IViewModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public string Extension { get; set; }
        public string Description { get; set; }
    }
}