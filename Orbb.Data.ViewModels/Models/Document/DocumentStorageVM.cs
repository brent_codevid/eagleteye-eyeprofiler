﻿using System.IO;

namespace Orbb.Data.ViewModels.Models.Document
{
    public class DocumentStorageVM
    {
        public MemoryStream FileStream { get; set; }
        public int EntityId { get; set; }
        public int TypeId { get; set; }
        public int StoreTypeId { get; set; }
        public string FileName { get; set; }
        public string FileExtension { get; set; }
    }
}
