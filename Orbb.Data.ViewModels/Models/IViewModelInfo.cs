﻿using System;

namespace Orbb.Data.ViewModels.Models
{
    public interface IViewModelInfo
    {
        string CreatedBy { get; set; }

        DateTime? CreatedOn { get; set; }

        string LastModifiedBy { get; set; }

        DateTime? LastModifiedOn { get; set; }
    }
}