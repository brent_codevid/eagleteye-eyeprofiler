﻿using System;

namespace Orbb.Data.ViewModels.Models.Orders
{
    [Serializable]
    public class OrderVM : IViewModel
    {
        public DateTime Date { get; set; }
        public int CustomerId { get; set; }
        public int SupplierId { get; set; }
        public string FittedLens { get; set; }
        public int Status { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}
