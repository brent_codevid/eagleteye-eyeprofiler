﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.LensSuppliers
{
    [Serializable]
    public class LensSupplierVM : IViewModel
    {
        [Required]
        public string Name { get; set; }
        public string EmailAdress { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}
