﻿using Orbb.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]

    public class TangentListVM : List<CPTangentItemVM>, IViewModel
    {
        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}
