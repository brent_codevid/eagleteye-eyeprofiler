﻿using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class CP2ToCP1ItemVM : IViewModel
    {
        public double CP2Value { get; set; }
        public double CP1Value { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion

        public CP2ToCP1ItemVM() { }

        public CP2ToCP1ItemVM(int id, double cp2Value, double cp1Value)
        {
            Id = id;
            CP2Value = cp2Value;
            CP1Value = cp1Value;
        }
    }
}