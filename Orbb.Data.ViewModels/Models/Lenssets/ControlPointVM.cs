﻿using Orbb.Common;
using Orbb.Common.Utility;
using Orbb.Data.ViewModels.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]

    public class ControlPointVM : IViewModel
    {
        [Required]
        public int? Category { get; set; }

        public bool RelativeChordLength { get; set; }
        public double? DiameterMultiplier { get; set; }
        public double? SemiChordLength { get; set; }

        [Required]
        public string SupplierSpecificName { get; set; }
        public string DisplayCpFormat { get; set; }
        public bool IncludeInApiResult { get; set; }

        public double? MinValue { get; set; }
        public double? MaxValue { get; set; }
        public double? StepSize { get; set; }
        public double Correction { get; set; }
        public double Lift { get; set; }
        public double? StandardVal1 { get; set; }
        public double? StandardVal2 { get; set; }
        public double? StandardVal3 { get; set; }
        public double? StandardVal4 { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion

        public ControlPointVM SynchroniseStandardVal()
        {
            switch (Category)
            {
                case (int)Enumerations.ControlPointCategories.sferic:
                    StandardVal2 = StandardVal1;
                    StandardVal3 = StandardVal1;
                    StandardVal4 = StandardVal1;
                    break;
                case (int)Enumerations.ControlPointCategories.toric:
                    StandardVal3 = StandardVal1;
                    StandardVal4 = StandardVal2;
                    break;
            }

            return this;
        }

        public ControlPointVM SynchroniseChordLength()
        {
            if (RelativeChordLength)
            {
                SemiChordLength = null;
            }
            else
            {
                DiameterMultiplier = null;
            }
            return this;
        }

        public void Cleanup()
        {
            if (RelativeChordLength)
            {
                SemiChordLength = null;
            }
            else
            {
                DiameterMultiplier = null;
            }

            if (DisplayCpFormat == null)
            {
                DisplayCpFormat = "";
            }
        }

        public Enumerations.CpRoundingStrategy RoundingStrategy { get; set; } = Enumerations.CpRoundingStrategy.Nearest;

        public Enumerations.CpCalculationMethod CalculationMethod { get; set; } = Enumerations.CpCalculationMethod.Formula;

        [DeleteWhenRemovedFromCollection]
        public List<CPListItemVM> CpList { get; set; } = new List<CPListItemVM>();

        [DeleteWhenRemovedFromCollection]
        public List<CPTangentItemVM> CpTangentList { get; set; } = new List<CPTangentItemVM>();

        public Enumerations.TangentListPositioning TangentPosition { get; set; } = Enumerations.TangentListPositioning.Horizontal;

        [DeleteWhenRemovedFromCollection]
        public List<CPCurvatureItemVM> CpCurvatureList { get; set; } = new List<CPCurvatureItemVM>();
        [IgnoreSave]
        public List<double> CurvatureDiameters
        {
            get
            {
                if (CurvatureDiametersString != null && CurvatureDiametersString != "")
                {
                    return DataParser.ParseStringToList(CurvatureDiametersString, ';');
                }
                else
                {
                    return new List<double>() { 0 };
                }
            }
            set
            {
                if (value != null)
                {
                    CurvatureDiametersString = DataParser.ParseListToString(value, ';');
                }
                else
                {
                    CurvatureDiametersString = "";
                }
            }
        }
        public string CurvatureDiametersString { get; set; }
        public int CurvatureSectionCount { get; set; }
        public string DiscriminatorValue
        {
            get
            {
                return this.GetType().Name;
            }
        }
    }
}
