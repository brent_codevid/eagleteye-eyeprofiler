﻿using Orbb.Common;
using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class ControlPoint1VM : ControlPointVM
    {
        public bool? RelativeSag { get; set; }
        public double? CP1SagCP2Percentage { get; set; }
        public double? CP1SagCP2Correction { get; set; }
        public double? CP1SagCP2MinValue { get; set; }
        public double? CP1SagCP2MaxValue { get; set; }
        public double? CP1SagCP2StepSize { get; set; }
        public bool? AddCalculatedCP1SagCP2PercentageToOtherCPs { get; set; }
        public Enumerations.MeasurementUnit MeasurementUnit { get; set; }
        public string DisplayValueFormat { get; set; }

        //Used to trick the system in thinking we changed something so that it validates the content 
        public DateTime? LastModifiedOn { get; set; }

        public void Cleanup(LensDetailVM ld)
        {
            base.Cleanup();
            if (DisplayValueFormat == null)
            {
                DisplayValueFormat = "";
            }
            if (ld.UseCP2ToCP1List)
            {
                MinValue = null;
                MaxValue = null;
                StepSize = null;
                SemiChordLength = null;
                DiameterMultiplier = null;
                CP1SagCP2Percentage = null;
                StandardVal1 = null;
                StandardVal2 = null;
                StandardVal3 = null;
                StandardVal4 = null;
            }
            else
            {
                if (RelativeChordLength)
                {
                    SemiChordLength = null;
                }
                else
                {
                    DiameterMultiplier = null;
                }

                if (MeasurementUnit.Equals(Enumerations.MeasurementUnit.BaseCurve))
                {
                    CP1SagCP2Percentage = null;
                    StandardVal1 = null;
                    StandardVal2 = null;
                    StandardVal3 = null;
                    StandardVal4 = null;
                }
                else if (MeasurementUnit.Equals(Enumerations.MeasurementUnit.SAG))
                {
                    if (RelativeSag ?? false)
                    {
                        //nothing yet
                    }
                    else
                    {
                        StandardVal1 = null;
                        StandardVal2 = null;
                        StandardVal3 = null;
                        StandardVal4 = null;
                    }
                }
            }

            SynchroniseStandardVal().SynchroniseChordLength();
        }
    }
}
