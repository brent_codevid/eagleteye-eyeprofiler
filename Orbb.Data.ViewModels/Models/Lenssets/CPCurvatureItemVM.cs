﻿using Orbb.Common.Utility;
using Orbb.Data.ViewModels.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class CPCurvatureItemVM : IViewModel
    {
        public string SupplierSpecificName { get; set; }
        public List<double> Radii
        {
            get
            {
                if (RadiiString != null && RadiiString != "")
                {
                    return DataParser.ParseStringToList(RadiiString, ';');
                }
                else
                {
                    return new List<double>() { 0 };
                }
            }
            set
            {
                if (value != null)
                {
                    RadiiString = DataParser.ParseListToString(value, ';');
                }
                else
                {
                    RadiiString = "";
                }
            }
        }
        public string RadiiString { get; set; }

        public List<double> Excentricities
        {
            get
            {
                if (ExcentricitiesString != null && ExcentricitiesString != "")
                {
                    return DataParser.ParseStringToList(ExcentricitiesString, ';');
                }
                else
                {
                    return new List<double>() { 0 };
                }
            }
            set
            {
                if (value != null)
                {
                    ExcentricitiesString = DataParser.ParseListToString(value, ';');
                }
                else
                {
                    ExcentricitiesString = "";
                }
            }
        }
        public string ExcentricitiesString { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion

        [IgnoreSave]
        public string Sagitta { get; set; }
        public void CalculateSagitta(List<double> diameterList)
        {

            int SectionCount = Excentricities.Count();

            double result = 0;
            try
            {
                for (int j = 0; j < SectionCount; j++)
                {

                    var curvatureList = this;

                    double sectionResult =
                   (curvatureList.Radii[j] - (Math.Sqrt(Math.Pow(curvatureList.Radii[j], 2) - (1 - Math.Pow(curvatureList.Excentricities[j], 2)) * Math.Pow(diameterList[j + 1], 2) / (1 - Math.Pow(curvatureList.Excentricities[j], 2)))))
                   -
                   (curvatureList.Radii[j] - (Math.Sqrt(Math.Pow(curvatureList.Radii[j], 2) - (1 - Math.Pow(curvatureList.Excentricities[j], 2)) * Math.Pow(diameterList[j], 2) / (1 - Math.Pow(curvatureList.Excentricities[j], 2)))));

                    result += sectionResult;
                }

                Sagitta = String.Format("{0:0.0}", result * 1000);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                Sagitta = "ERROR";
            }
            Sagitta = String.Format("{0:0.0}", result * 1000);
        }

    }
}