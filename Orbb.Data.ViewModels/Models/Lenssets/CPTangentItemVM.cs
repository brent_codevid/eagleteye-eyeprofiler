﻿using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class CPTangentItemVM : IViewModel
    {

        public double CPTangentValue { get; set; }
        public string SupplierSpecificName { get; set; }
        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}