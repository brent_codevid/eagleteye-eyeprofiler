﻿using Orbb.Common.Utility;
using Orbb.Data.ViewModels.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class CPCurvatureItemVMForKendo : IViewModel
    {
        public string SupplierSpecificName { get; set; }

        public List<double> Radii { get; set; }

        public List<double> Excentricities {get;set;}

        #region IEntity Members
        public int Id { get; set; }
        #endregion

    }
}