﻿using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class CPListItemVM : IViewModel
    {
        public double CPValue { get; set; }
        public string SupplierSpecificName { get; set; }
        #region IEntity Members
        public int Id { get; set; }
        #endregion

    }
}