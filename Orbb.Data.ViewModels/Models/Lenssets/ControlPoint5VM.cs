﻿using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class ControlPoint5VM : ControlPointVM
    {
        public double Threshold { get; set; }
        public string DisplayValueFormat { get; set; }
        public bool RelativeSag { get; set; }

        public bool IncreaseClearance { get; set; }
        public double? MinimalClearance { get; set; }
        public double? SagCorrectionClearance { get; set; }
        public double? EdgeLift { get; set; }


        public new void Cleanup()
        {
            base.Cleanup();
            if (DisplayValueFormat == null)
            {
                DisplayValueFormat = "";
            }
            SynchroniseStandardVal().SynchroniseChordLength();
        }
    }
}
