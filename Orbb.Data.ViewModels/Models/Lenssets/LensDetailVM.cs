﻿using Orbb.Common;
using Orbb.Data.ViewModels.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class LensDetailVM : IViewModel
    {
        public bool IsDraft { get; set; }
        public int? SupplierId { get; set; }

        public bool RotateEyeDataBeforeFitting { get; set; }
        public double? LensDiameter { get; set; }
        public double? LensDiameterMax { get; set; }
        public double? LensDiameterMin { get; set; }
        public double? LensDiameterStepSize { get; set; }
        public double? LensDiameterPerc { get; set; }
        public double? LensDiameterCp { get; set; }
        public double? HvidBuffer { get; set; }

        public bool IncludeLzInLensName { get; set; }
        public double? LzInLensName1Correction { get; set; }
        public double? LzInLensName2Correction { get; set; }
        public double? LzInLensName3Correction { get; set; }
        public double? LzInLensName4Correction { get; set; }
        public double? LzInLensName5Correction { get; set; }

        public int Type { get; set; }

        [Required]
        public string Name { get; set; }

        public int LandingZoneBase { get; set; }

        public int SuggestedApicalClearance { get; set; }
        public int SuggestedMidPeriphClearance { get; set; }
        public int SuggestedSettling { get; set; }

        public bool UseNewMer1Selector { get; set; } //Select if the old system has to be used (flattest angle) or the new system (left,right,top, bottom, flat)
        public int? Meridian1Position { get; set; }


        public bool UseCP2ToCP1List { get; set; }
        [DeleteWhenRemovedFromCollection]
        public List<CP2ToCP1ItemVM> CP2ToCP1List { get; set; }

        public bool UseCP1 { get; set; }
        public int? CP1Id { get; set; }
        public ControlPoint1VM CP1 { get; set; }

        public bool UseCP2 { get; set; }
        public int? CP2Id { get; set; }
        public ControlPoint2VM CP2 { get; set; }

        public bool UseCP3 { get; set; }
        public int? CP3Id { get; set; }
        public ControlPoint3VM CP3 { get; set; }

        public bool UseCP4 { get; set; }
        public int? CP4Id { get; set; }
        public ControlPoint4VM CP4 { get; set; }

        public bool UseCP5 { get; set; }
        public int? CP5Id { get; set; }
        public ControlPoint5VM CP5 { get; set; }

        [IgnoreSave]
        public bool DiameterInterval { get; set; }

        //bad but fast replace with list + grid in Gui if changes needed in future
        public bool UseHvidForDiameters { get; set; }
        public double HvidVal10 { get; set; }
        public double HvidVal105 { get; set; }
        public double HvidVal11 { get; set; }
        public double HvidVal115 { get; set; }
        #region IEntity Members
        public int Id { get; set; }
        #endregion

        public LensDetailVM()
        {
            CP1 = new ControlPoint1VM { Id = 0 };
            CP2 = new ControlPoint2VM { Id = 0 };
            CP3 = new ControlPoint3VM { Id = 0 };
            CP4 = new ControlPoint4VM { Id = 0 };

            CP1.Category = 0;
            CP2.Category = 0;
            CP3.Category = 0;
            CP4.Category = 0;

            CP2ToCP1List = new List<CP2ToCP1ItemVM>();
        }

        public void Cleanup()
        {
            if (DiameterInterval)
            {
                LensDiameter = null;
            }
            else
            {
                LensDiameterMax = null;
                LensDiameterMin = null;
                LensDiameterStepSize = null;
                LensDiameterPerc = null;
                LensDiameterCp = null;
            }

            CP1.Cleanup(this);
            if (!UseCP2ToCP1List)
            {
                CP2ToCP1List.Clear();
            }
            else
            {
                //done in CP1.Cleanup();
            }
            CP2.Cleanup();
            CP3.Cleanup();
            CP4.Cleanup();
        }
    }
}

