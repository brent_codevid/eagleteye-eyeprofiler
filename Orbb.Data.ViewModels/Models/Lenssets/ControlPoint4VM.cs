﻿using Orbb.Common;
using System;

namespace Orbb.Data.ViewModels.Models.Lenssets
{
    [Serializable]
    public class ControlPoint4VM : ControlPointVM
    {
        public double Threshold { get; set; }

        public string PositiveName { get; set; }
        public string NegativeName { get; set; }

        public bool CalculateLz { get; set; }
        public double? LzPercentage { get; set; }

        public Enumerations.DisplayLandingZoneStepType DisplayLandingZoneStepType { get; set; }
        public int DisplayLandingZoneCenter { get; set; }
        public int? DisplayLandingZonePlusExtrema { get; set; }
        public int? DisplayLandingZoneMinExtrema { get; set; }

        public new void Cleanup()
        {
            base.Cleanup();

            if (CalculateLz)
            {
                StandardVal1 = null;
                StandardVal2 = null;
                StandardVal3 = null;
                StandardVal4 = null;
            }
            else
            {
                LzPercentage = null;
            }

            if (PositiveName == null)
            {
                PositiveName = "";
            }
            if (NegativeName == null)
            {
                NegativeName = "";
            }

            if (DisplayLandingZoneStepType.Equals(Enumerations.DisplayLandingZoneStepType.Microns))
            {
                DisplayLandingZoneCenter = 0;
                DisplayLandingZoneMinExtrema = null;
                DisplayLandingZonePlusExtrema = null;
            }
            else if (DisplayLandingZoneStepType.Equals(Enumerations.DisplayLandingZoneStepType.Steps))
            {
                //nothing yet
            }
            SynchroniseStandardVal().SynchroniseChordLength();
        }
    }
}
