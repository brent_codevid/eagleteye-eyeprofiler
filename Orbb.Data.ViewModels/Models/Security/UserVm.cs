﻿using Orbb.Data.ViewModels.Models.Globalization;
using System;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class UserVm : IViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? LanguageId { get; set; }
        public LanguageVM Language { get; set; }
        public int? DefaultDivisionId { get; set; }
        public string UserName { get; set; }
        //public string Password { get; set; }
    }
}
