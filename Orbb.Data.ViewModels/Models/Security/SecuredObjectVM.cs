﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class SecuredObjectVM : IViewModel
    {
        public int Id { get; set; }
        [StringLength(60)]
        public string Object { get; set; }
        public string Type { get; set; }
    }
}
