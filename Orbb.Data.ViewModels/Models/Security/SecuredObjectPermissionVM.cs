﻿using Orbb.Data.ViewModels.Attributes;
using System;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class SecuredObjectPermissionVm : IViewModel
    {
        public int Id { get; set; }
        //[StringLength(60)]
        //public SecuredObjectVM SecuredObject { get; set; }
        public int SecuredObjectId { get; set; }
        [IgnoreSave]
        public string SecuredObjectObject { get; set; }
        public int UserGroupId { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Add { get; set; }
        public bool Delete { get; set; }
    }
}
