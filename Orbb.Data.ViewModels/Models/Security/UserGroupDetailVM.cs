﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class UserGroupDetailVM : IViewModel
    {
        public int Id { get; set; }
        [StringLength(60)]
        public string Description { get; set; }
        public List<SecuredObjectPermissionVm> Permissions { get; set; }
    }
}
