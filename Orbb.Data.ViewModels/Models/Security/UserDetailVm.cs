﻿using Orbb.Data.ViewModels.Attributes;
using System;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class UserDetailVm : IViewModel
    {
        public int Id { get; set; }
        [StringLength(30)]
        public string FirstName { get; set; }
        [StringLength(50)]
        public string LastName { get; set; }
        public string FullName => string.Concat(FirstName, " ", LastName);
        [StringLength(80)]
        public string EmailAddress { get; set; }
        [StringLength(40)]
        public string UserName { get; set; }
        [StringLength(64)]
        public string Password { get; set; }
        public string Salt { get; set; }
        [IgnoreSave]
        public string OldPassword { get; set; }
        public int UserGroupId { get; set; }
        public int LanguageId { get; set; }
        public int? DefaultDivisionId { get; set; }
        public string PhoneNumber { get; set; }
        public int? CurrencyId { get; set; }
        [IgnoreSave]
        public string CurrencyCode { get; set; }
        public int Gender { get; set; }
        public int TypeId { get; set; }
    }
}
