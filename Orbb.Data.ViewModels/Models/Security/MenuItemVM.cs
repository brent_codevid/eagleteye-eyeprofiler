﻿using System;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class MenuItemVM : IViewModel
    {
        public int Id { get; set; }
        public string Destination { get; set; }
        public string Description { get; set; }
    }
}
