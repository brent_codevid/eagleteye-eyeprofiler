﻿using System;
using System.Collections.Generic;

namespace Orbb.Data.ViewModels.Models.Security
{
    [Serializable]
    public class UserMenuVM
    {
        public IList<MenuItemVM> MainMenuItems { get; set; }
        public IList<MenuItemVM> SubMenuItems { get; set; }
    }
}
