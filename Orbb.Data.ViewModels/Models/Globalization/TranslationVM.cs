﻿using System;
using System.Collections.Generic;

namespace Orbb.Data.ViewModels.Models.Globalization
{
    [Serializable]
    public class TranslationVM : IViewModel
    {
        public string DefaultValue { get; set; }

        public List<TranslationEntryVM> Entries { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
