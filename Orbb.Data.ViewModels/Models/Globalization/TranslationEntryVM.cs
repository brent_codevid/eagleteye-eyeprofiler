﻿using System;

namespace Orbb.Data.ViewModels.Models.Globalization
{
    [Serializable]
    public class TranslationEntryVM : IViewModel
    {
        public int LanguageId { get; set; }
        public string Value { get; set; }
        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
