﻿using System;

namespace Orbb.Data.ViewModels.Models.Globalization
{
    [Serializable]
    public class LanguageVM : IViewModel
    {
        public string Code { get; set; }
        public string ShortCode { get; set; }
        public string EnglishDescription { get; set; }

        #region IEntity Members
        public int Id { get; set; }

        public string CreatedBy { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string LastModifiedBy { get; set; }

        public DateTime? LastModifiedOn { get; set; }
        #endregion
    }
}
