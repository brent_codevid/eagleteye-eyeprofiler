﻿using Orbb.Data.ViewModels.Attributes;
using System;
using static Orbb.Common.Enumerations;

namespace Orbb.Data.ViewModels.Models.Customers
{
    [Serializable]
    public class LenssetVM : IViewModel
    {
        [IgnoreSave]
        public string Name { get; set; }
        [IgnoreSave]
        public int Type { get; set; }

        private string _typeName;
        [IgnoreSave]
        public string TypeName
        {
            get => _typeName ?? ((LensTypes)Type).ToString();
            set => _typeName = value;
        }
        [IgnoreSave]
        public double? LensDiameter { get; set; }
        [IgnoreSave]
        public string Supplier { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}

