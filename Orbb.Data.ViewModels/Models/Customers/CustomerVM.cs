﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Orbb.Data.ViewModels.Models.Customers
{
    [Serializable]
    public class CustomerVM : IViewModel
    {
        [Required]
        public string Name { get; set; }
        public string Email { get; set; }
        public List<LenssetVM> UsedLenssets { get; set; } = new List<LenssetVM>();

        public int CountryId { get; set; }

        #region IEntity Members
        public int Id { get; set; }
        #endregion
    }
}

