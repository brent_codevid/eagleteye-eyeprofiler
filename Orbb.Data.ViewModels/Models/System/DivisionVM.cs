﻿using System;

namespace Orbb.Data.ViewModels.Models.System
{
    [Serializable]
    public class DivisionVM : IViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string TelephoneNumber { get; set; }
        public string Email { get; set; }
        public int FaxNumber { get; set; }
        public string ShortCode { get; set; }
        public string Logo { get; set; }
        public bool IsDefault { get; set; }
        public string ImagePath { get; set; }
    }
}
