﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Orbb.Common.Utility
{
    public static class DataParser
    {
        public static string ParseListToString(List<double> list, char delimitor)
        {
            List<string> sList = new List<string>();
            foreach (double item in list)
            {
                sList.Add(item.ToString());
            }
            return String.Join(delimitor.ToString(), sList);
        }

        public static List<double> ParseStringToList(string list, char delimitor)
        {
            List<double> result = new List<double>();

            string[] stringlist = list.Split(delimitor);
            foreach (string item in stringlist)
            {
                result.Add(Double.Parse(item));
            }


            return result;
        }
    }
}
