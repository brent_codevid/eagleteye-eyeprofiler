﻿using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace Orbb.Common.Utility
{
    public static class SerializeObject
    {
        public static Task<string> SerializeAsync(object obj, bool omitXmlDeclaration = false)
        {
            return Task.Run(() => Serialize(obj, omitXmlDeclaration));
        }

        public static string Serialize(object obj, bool omitXmlDeclaration = false)
        {
            StringBuilder sb = new StringBuilder();

            var serializer = new XmlSerializer(obj.GetType());
            serializer.Serialize(XmlWriter.Create(sb, new XmlWriterSettings { OmitXmlDeclaration = omitXmlDeclaration, Indent = true }), obj);

            return sb.ToString();
        }
    }
}
