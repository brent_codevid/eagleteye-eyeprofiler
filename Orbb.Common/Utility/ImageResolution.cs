﻿namespace Orbb.Common.Utility
{
    public class ImageResolution
    {
        public int Width { get; set; }

        public int Height { get; set; }
    }
}
