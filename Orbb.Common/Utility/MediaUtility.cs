﻿using Orbb.Common.Environment;
using System;

namespace Orbb.Common.Utility
{
    public static class MediaUtility
    {
        public const string NoImage = "/images/no-image.jpg";

        public static string ConvertToUrl(string relativeFilePath)
        {
            var url = NoImage;

            if (string.IsNullOrEmpty(relativeFilePath))
                return url;

            var mediaBaseUrl = Config.MediaBaseUrl;

            if (!mediaBaseUrl.EndsWith("/"))
                mediaBaseUrl = mediaBaseUrl + "/";

            var mediaUrl = $"{mediaBaseUrl}{relativeFilePath}";

            try
            {
                // creating Uri also makes any required escape encodings
                var uri = new Uri(mediaUrl);
                url = uri.AbsolutePath;
            }
            catch
            {
                url = string.Empty;
            }

            return url;
        }
    }
}