﻿using Orbb.Common.Security.Models;
using System;
using System.Security.Cryptography;
using System.Text;

namespace Orbb.Common.Security
{
    public class StringHash
    {
        private string CalculateSHA1(string text, Encoding enc)
        {
            return BitConverter.ToString(new SHA256Managed().ComputeHash(enc.GetBytes(text))).Replace("-", "");
        }

        private string GenerateSalt(int bytes)
        {
            byte[] numArray = new byte[bytes];
            new RNGCryptoServiceProvider().GetBytes(numArray);
            return Convert.ToBase64String(numArray);
        }

        public HashSalt Hash(string key)
        {
            string salt = GenerateSalt(16);
            string hash = CalculateSHA1(salt + key, Encoding.UTF8);

            return new HashSalt()
            {
                Hash = hash,
                Salt = salt
            };
        }

        public string HashWithSalt(string key, string salt)
        {
            return CalculateSHA1(salt + key, Encoding.UTF8);
        }
    }
}