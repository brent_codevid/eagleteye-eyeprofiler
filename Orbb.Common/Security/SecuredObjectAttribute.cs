﻿namespace Orbb.Common.Security
{
    [System.AttributeUsage(System.AttributeTargets.Class)]
    public class SecuredObjectAttribute : System.Attribute
    {
        public string Name;
        public SecuredObjectAttribute(string name)
        {
            Name = name;
        }
    }
}
