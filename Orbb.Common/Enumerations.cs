﻿namespace Orbb.Common
{
    public static class Enumerations
    {
        public enum ControlPointEnum
        {
            Cp1 = 1,
            Cp2Old = 2,
            Cp2 = 3,
            Cp4 = 4,
            Cp3 = 5,
            Cp5 = 6,
        }
        public enum LandingZoneBaseType
        {
            Limbus = 1,
            HVID = 2
        }

        public enum Sections
        {
            s1 = 1,
            s2 = 2,
            s3 = 3,
            s4 = 4,
            s5 = 5,
            s6 = 6
        }

        public enum MeasurementUnit
        {
            BaseCurve = 1,
            SAG = 2,
            Munnerlyn = 3,
            Jessen = 4
        }

        public enum DisplayLandingZoneStepType
        {
            Microns = 1,
            Steps = 2
        }

        public enum CpRoundingStrategy
        {
            Nearest = 1,
            NearestLarger = 2
        }

        public enum CpCalculationMethod
        {
            Formula = 1,
            List = 2,
            TangentList = 3,
            CurvatureDesign = 4,
            AlignmentCurve = 5,
            Curvature = 6,
        }

        public enum TangentListPositioning
        {
            Horizontal = 1,
            Vertical = 2
        }

        public enum LensTypes
        {
            scleral = 1,
            soft = 2,
            orthok = 3,
            rgp = 4,
            hybrid = 5
        }

        public enum ControlPointCategories
        {
            sferic = 1,
            toric = 2,
            quad = 4
        }

        public enum OrderStatus
        {
            cancelled = 1,
            ordered = 2,
            sent = 3,
            received = 4
        }

        public enum MeridianPosition
        {
            Flat = 1,
            Top = 2,
            Right = 3,
            Bottom = 4,
            Left = 5
        }

    }
}