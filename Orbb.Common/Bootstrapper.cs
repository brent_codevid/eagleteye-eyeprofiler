﻿using Microsoft.Extensions.Localization;
using Orbb.Common.Localization;
using Orbb.Common.Security;
using StructureMap;

namespace Orbb.Common
{
    public static class Bootstrapper
    {
        public static void RegisterWithContainer(IContainer container, bool useStringLocalizer = true)
        {
            container.Configure(c =>
            {
                if (useStringLocalizer)
                    c.For<IStringLocalizerFactory>().Use<StringLocalizerFactory>();
                c.For<IAvailableCultures>().Use<StringLocalizerFactory>();
                var enc = container.GetInstance<StringEncryption>();
                c.For<StringEncryption>().Use(enc);
            });
            if (useStringLocalizer)
                container.Configure(c =>
                {
                    IStringLocalizerFactory locfactory = container.GetInstance<IStringLocalizerFactory>();
                    c.For<IStringLocalizer>().Use(s => locfactory.Create(null));
                });
        }
    }
}
