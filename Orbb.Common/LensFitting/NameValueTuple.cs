﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Orbb.Common.LensFitting
{
    public class NameValueTuple
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
