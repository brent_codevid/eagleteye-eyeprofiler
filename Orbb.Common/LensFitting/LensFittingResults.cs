﻿using System;
using System.Collections.Generic;

namespace Orbb.Common.LensFitting
{
    public class LensFittingResults
    {
        public int Angle;
        public double Diameter;
        public List<int> Cp5Ids;
        public List<double> CP5Result;
        public List<double> CP5FinalValueToAdd;
        public List<int> Cp4Ids;
        public List<double> CP4Result;
        public List<double> CP4FinalValueToAdd;
        public List<int> Cp3Ids;
        public List<double> CP3Result;
        public List<double> CP3FinalValueToAdd;
        public List<int> Cp2Ids;
        public List<double> CP2Result;
        public List<double> CP2FinalValueToAdd;
        public List<int> Cp1Ids;
        public List<double> Cp1Diffs;
        public List<double> CP1Result;
        public List<double> CP1FinalValueToAdd;

        public int SelectedAngle { get; set; }

        public LensFittingResults()
        {
            CP5Result = new List<double>();
            CP4Result = new List<double>();
            CP3Result = new List<double>();
            CP2Result = new List<double>();
            CP1Result = new List<double>();
            CP5FinalValueToAdd = new List<double>() { 0, 0, 0, 0 };
            CP4FinalValueToAdd = new List<double>() { 0, 0, 0, 0 };
            CP3FinalValueToAdd = new List<double>() { 0, 0, 0, 0 };
            CP2FinalValueToAdd = new List<double>() { 0, 0, 0, 0 };
            CP1FinalValueToAdd = new List<double>() { 0, 0, 0, 0 };
            Cp1Diffs = new List<double>();
        }

        public void PlaceMerOneOnTopForQuad()
        {
            if (CP1Result.Count == 4 || CP2Result.Count == 4 || CP3Result.Count == 4 || CP4Result.Count == 4 || CP5Result.Count == 4)
            {
                int rotateCount = +3;

                if (Angle > 45 && Angle <= 135)
                {
                    rotateCount = 0;
                }
                else if (Angle > 135 && Angle <= 225)
                {
                    rotateCount = 1;
                }
                else if (Angle > 225 && Angle <= 315)
                {
                    rotateCount = 2;
                }

                List<double> oldOrder = new List<double>(CP5Result);
                for (int i = 0; i < CP5Result.Count; i++)
                {
                    if (CP5Result.Count == 4)
                    {
                        CP5Result[i] = oldOrder[(i + rotateCount) % 4];
                    }
                    else if (CP4Result.Count == 2)
                    {
                        CP5Result[i] = oldOrder[(i + rotateCount) % 2];
                    }
                }

                oldOrder = new List<double>(CP4Result);
                for (int i = 0; i < CP4Result.Count; i++)
                {
                    if (CP4Result.Count == 4)
                    {
                        CP4Result[i] = oldOrder[(i + rotateCount) % 4];
                    }
                    else if (CP4Result.Count == 2)
                    {
                        CP4Result[i] = oldOrder[(i + rotateCount) % 2];
                    }
                }

                oldOrder = new List<double>(CP3Result);
                for (int i = 0; i < CP3Result.Count; i++)
                {
                    if (CP3Result.Count == 4)
                    {
                        CP3Result[i] = oldOrder[(i + rotateCount) % 4];
                    }
                    else if (CP3Result.Count == 2)
                    {
                        CP3Result[i] = oldOrder[(i + rotateCount) % 2];
                    }
                }

                oldOrder = new List<double>(CP2Result);
                for (int i = 0; i < CP2Result.Count; i++)
                {
                    if (CP2Result.Count == 4)
                    {
                        CP2Result[i] = oldOrder[(i + rotateCount) % 4];
                    }
                    else if (CP2Result.Count == 2)
                    {
                        CP2Result[i] = oldOrder[(i + rotateCount) % 2];
                    }
                }

                oldOrder = new List<double>(CP1Result);
                for (int i = 0; i < CP1Result.Count; i++)
                {
                    if (CP1Result.Count == 4)
                    {
                        CP1Result[i] = oldOrder[(i + rotateCount) % 4];
                    }
                    else if (CP1Result.Count == 2)
                    {
                        CP1Result[i] = oldOrder[(i + rotateCount) % 2];
                    }
                }
            }
        }

        public void ApplyFinalValuesToAdd()
        {
            for (int i = 0; i < CP5Result.Count; i++)
            {
                CP5Result[i] = CP5Result[i] + CP5FinalValueToAdd[i];
            }
            for (int i = 0; i < CP4Result.Count; i++)
            {
                CP4Result[i] = CP4Result[i] + CP4FinalValueToAdd[i];
            }
            for (int i = 0; i < CP3Result.Count; i++)
            {
                CP3Result[i] = CP3Result[i] + CP3FinalValueToAdd[i];
            }
            for (int i = 0; i < CP2Result.Count; i++)
            {
                CP2Result[i] = CP2Result[i] + CP2FinalValueToAdd[i];
            }
            for (int i = 0; i < CP1Result.Count; i++)
            {
                CP1Result[i] = CP1Result[i] + CP1FinalValueToAdd[i];
            }
        }
    }
}
