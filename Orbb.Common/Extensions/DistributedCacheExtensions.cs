﻿using Microsoft.Extensions.Caching.Distributed;
using Newtonsoft.Json;

namespace Orbb.Common.Extensions
{
    public static class DistributedCacheExtensions
    {
        public static T Get<T>(this IDistributedCache cache, string key)
        {
            var value = cache.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        public static void Set<T>(this IDistributedCache cache, string key, T value)
        {
            cache.SetString(key, JsonConvert.SerializeObject(value));
        }
    }
}
