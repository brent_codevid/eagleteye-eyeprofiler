﻿using IniParser;
using IniParser.Model;
using System;
using System.IO;
using System.Reflection;
using System.Threading;

namespace Orbb.Common.Environment
{
    public static class Config
    {
        public static string BasePath { get; set; }
        private const string StrIni = "config.ini";
        //   public Environments Environment { get; set; }

        private static string localizationFilesPath = "";
        private static string importMappingFilesPath = "";
        private static bool? useDivisions;
        private static string connectionString = "";
        private static string tempExportDir = "";
        private static string protocol = "";
        private static string baseUrl = "";
        private static string securityCurrentSalt = "";
        private static string securityCurrentKey = "";
        private static string cookiePrefix = "";
        private static string mediaPath = "";
        private static string mediaBaseUrl = "";
        private static string displayName = "";
        private static string decimalSeparator = "";
        private static string winnerGeneralRateInfoUrl = "";
        private static string winnerCalendarUrl = "";
        private static string winnerUserName = "";
        private static string winnerPassWord = "";
        private static string winnerCustomer = "";
        private static string winnerProperties = "";

        private static string GetSettingString(string section, string item)
        {
            string value = "";
            var directory = new Uri(Assembly.GetAssembly(typeof(Config)).CodeBase).LocalPath;
            Console.WriteLine(directory);
            directory = Path.GetDirectoryName(directory);
            directory = Path.Combine(directory, StrIni);
            Console.WriteLine("using " + directory);
            if (!File.Exists(directory))
            {
                throw new Exception(directory + " not found");
            }
            try
            {
                IniData data = null;
                do
                {
                    try
                    {
                        data = ReadSettingsFile(directory);
                    }
                    catch
                    {
                        // File in use
                        Thread.Sleep(100);
                    }
                }
                while (data == null);
                string s = data[section][item];
                if (!String.IsNullOrEmpty(s))
                    value = s;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return value;
        }

        private static IniData ReadSettingsFile(string directory)
        {
            var parser = new FileIniDataParser();

            IniData data = parser.ReadFile(directory);
            return data;
        }

        private static int GetSettingInt(string section, string item)
        {
            int value = 0;
            string s = GetSettingString(section, item);
            if (!String.IsNullOrEmpty(s))
                int.TryParse(s, out value);

            return value;
        }

        private static bool GetSettingBool(string section, string item)
        {
            bool value = false;
            string s = GetSettingString(section, item);
            if (!String.IsNullOrEmpty(s))
                value = s == "true";

            return value;
        }

        public static string LocalizationFilesPath
        {
            get
            {
                if (String.IsNullOrEmpty(localizationFilesPath))
                {
                    localizationFilesPath = GetSettingString("Directories", "LocalizationFiles");
                    localizationFilesPath = localizationFilesPath.Replace("[BASE]", BasePath);
                }
                return localizationFilesPath;
            }
        }

        public static string ImportMappingFilesPath
        {
            get
            {
                if (String.IsNullOrEmpty(importMappingFilesPath))
                {
                    importMappingFilesPath = GetSettingString("Directories", "MappingFiles");
                    importMappingFilesPath = importMappingFilesPath.Replace("[BASE]", BasePath);
                }
                return importMappingFilesPath;
            }
        }

        public static string TempExportDir
        {
            get
            {
                if (String.IsNullOrEmpty(tempExportDir))
                {
                    tempExportDir = GetSettingString("Directories", "TempExport");
                }
                return tempExportDir;
            }
        }

        public static string ConnectionString
        {
            get
            {
                if (String.IsNullOrEmpty(connectionString))
                {
                    connectionString = GetSettingString("Data", "ConnectionString");
                }
                return connectionString;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (String.IsNullOrEmpty(displayName))
                {
                    displayName = GetSettingString("Customer", "DisplayName");
                }
                return displayName;
            }
        }

        public static bool UseDivisions
        {
            get
            {
                if (!useDivisions.HasValue)
                {
                    useDivisions = GetSettingBool("Customer", "UseDivisions");
                }
                return useDivisions.Value;
            }
        }

        public static string Protocol
        {
            get
            {
                if (String.IsNullOrEmpty(protocol))
                {
                    protocol = GetSettingString("Installation", "Protocol");
                }
                return protocol;
            }
        }

        public static string BaseUrl
        {
            get
            {
                if (String.IsNullOrEmpty(baseUrl))
                {
                    baseUrl = GetSettingString("Installation", "BaseUrl");
                }
                return baseUrl;
            }
        }

        public static string SecurityCurrentSalt
        {
            get
            {
                if (String.IsNullOrEmpty(securityCurrentSalt))
                {
                    securityCurrentSalt = GetSettingString("System", "SecurityCurrentSalt");
                }
                return securityCurrentKey;
            }
        }

        public static string SecurityCurrentKey
        {
            get
            {
                if (String.IsNullOrEmpty(securityCurrentKey))
                {
                    //securityCurrentKey = GetSettingString("System", "SecurityCurrentKey");
                    securityCurrentKey = Guid.NewGuid().ToString();
                }
                return securityCurrentKey;
            }
        }

        public static string CookiePrefix
        {
            get
            {
                if (string.IsNullOrEmpty(cookiePrefix))
                {
                    cookiePrefix = GetSettingString("System", "CookiePrefix");
                }
                return cookiePrefix;
            }
        }

        public static string MediaPath
        {
            get
            {
                if (String.IsNullOrEmpty(mediaPath))
                {
                    mediaPath = GetSettingString("System", "MediaPath");
                }
                return mediaPath;
            }
        }

        public static string MediaBaseUrl
        {
            get
            {
                if (String.IsNullOrEmpty(mediaBaseUrl))
                {
                    mediaBaseUrl = GetSettingString("System", "MediaBaseUrl");
                }
                return mediaBaseUrl;
            }
        }

        public static string WinnerGeneralRateInfoUrl
        {
            get
            {
                if (String.IsNullOrEmpty(winnerGeneralRateInfoUrl))
                {
                    winnerGeneralRateInfoUrl = GetSettingString("Winner", "GeneralRateInfoUrl");
                }
                return winnerGeneralRateInfoUrl;
            }
        }

        public static string WinnerCalendarUrl
        {
            get
            {
                if (String.IsNullOrEmpty(winnerCalendarUrl))
                {
                    winnerCalendarUrl = GetSettingString("Winner", "CalendarUrl");
                }
                return winnerCalendarUrl;
            }
        }

        public static string WinnerUserName
        {
            get
            {
                if (String.IsNullOrEmpty(winnerUserName))
                {
                    winnerUserName = GetSettingString("Winner", "UserName");
                }
                return winnerUserName;
            }
        }

        public static string WinnerPassWord
        {
            get
            {
                if (String.IsNullOrEmpty(winnerPassWord))
                {
                    winnerPassWord = GetSettingString("Winner", "PassWord");
                }
                return winnerPassWord;
            }
        }

        public static string WinnerCustomer
        {
            get
            {
                if (String.IsNullOrEmpty(winnerCustomer))
                {
                    winnerCustomer = GetSettingString("Winner", "Customer");
                }
                return winnerCustomer;
            }
        }

        public static string WinnerProperties
        {
            get
            {
                if (String.IsNullOrEmpty(winnerProperties))
                {
                    winnerProperties = GetSettingString("Winner", "Properties");
                }
                return winnerProperties;
            }
        }

        private static string ReturnSettingString(string section, string item, Func<string> checkFunc, Func<string, string> setFunc)
        {
            if (String.IsNullOrEmpty(checkFunc()))
            {
                setFunc(GetSettingString(section, item));
            }
            return checkFunc();
        }

        private static int ReturnSettingInt(string section, string item, Func<int> checkFunc, Func<int, int> setFunc)
        {
            if (checkFunc() == 0)
            {
                setFunc(GetSettingInt(section, item));
            }
            return checkFunc();
        }

        public static string DecimalSeparator => ReturnSettingString("System", "DecimalSeparator", () => decimalSeparator, (val) => decimalSeparator = val);
    }
}
