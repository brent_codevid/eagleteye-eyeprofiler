﻿using Microsoft.Extensions.Localization;
using Orbb.Common.Localization.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Orbb.Common.Localization
{
    public class StringLocalizer<T> : IStringLocalizer<T>
    {
        private readonly List<Culture> cultures;

        public StringLocalizer(List<Culture> cultures)
        {
            Console.WriteLine("Init StringLocalizer.");
            this.cultures = cultures;
        }

        private string GetString(string name)
        {
            var resource = cultures.SingleOrDefault(x => x.Name == CultureInfo.CurrentCulture.Name) ?? cultures.SingleOrDefault(x => x.Default);
            if (resource == null)
                return name;
            return resource.Resources[name];
        }

        public LocalizedString this[string name]
        {
            get
            {
                var value = GetString(name);
                return new LocalizedString(name, value ?? name, resourceNotFound: value == null);
            }
        }

        public LocalizedString this[string name, params object[] arguments]
        {
            get
            {
                var format = GetString(name);
                var value = string.Format(format ?? name, arguments);
                return new LocalizedString(name, value, resourceNotFound: format == null);
            }
        }

        public IEnumerable<LocalizedString> GetAllStrings(bool includeAncestorCultures)
        {
            return
                cultures.SingleOrDefault(x => x.Name == CultureInfo.CurrentCulture.Name)
                    .Resources.Select(r => new LocalizedString(r.Key, r.Value, true));
        }

        public IStringLocalizer WithCulture(CultureInfo culture)
        {
            CultureInfo.DefaultThreadCurrentCulture = culture;
            return new StringLocalizer(cultures);
        }
    }
}
