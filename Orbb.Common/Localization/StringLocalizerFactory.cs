﻿using CsvHelper;
using Microsoft.Extensions.Localization;
using Orbb.Common.Environment;
using Orbb.Common.Localization.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;

namespace Orbb.Common.Localization
{
    public class StringLocalizerFactory : IStringLocalizerFactory, IAvailableCultures
    {
        class CultureModel
        {
            public string CultureName { get; set; }
            public bool Default { get; set; }
        }

        public class ResourceModel
        {
            public string Id { get; set; }
            public string Translation { get; set; }
        }
        private static List<Culture> cultures;
        private readonly string path;
        public StringLocalizerFactory()
        {
            path = Config.LocalizationFilesPath;
            if (cultures == null)
                LoadCultures();
        }

        private void LoadCultures()
        {
            Console.WriteLine("Init culture resources. ");
            cultures = new List<Culture>();
            using (TextReader textReader = File.OpenText(Path.Combine(path, "cultures.csv")))
            {
                var csv = new CsvReader(textReader, new CsvHelper.Configuration.Configuration { Delimiter = ";", HasHeaderRecord = true });
                var records = csv.GetRecords<CultureModel>().ToList();
                foreach (var m in records)
                {
                    cultures.Add(new Culture { Default = m.Default, Name = m.CultureName });
                }
            }

            foreach (Culture c in cultures)
            {
                Console.WriteLine("Loading culture data for " + c.Name);
                var filter = "*." + c.Name + ".csv";
                var files = Directory.GetFiles(path, filter);
                foreach (string f in files)
                {

                    using (TextReader textReader = File.OpenText(f))
                    {
                        var csv = new CsvReader(textReader,
                            new CsvHelper.Configuration.Configuration
                            {
                                Delimiter = ";",
                                HasHeaderRecord = true,
                                Encoding = Encoding.Unicode,
                                MissingFieldFound = null
                            });
                        var records = csv.GetRecords<ResourceModel>().ToList();
                        if (c.Resources == null)
                            c.Resources = new Dictionary<string, string>();
                        records.ForEach(r =>
                        {
                            if (!c.Resources.ContainsKey(r.Id))
                                c.Resources.Add(r.Id, r.Translation);
                            else { throw new Exception($"Localizer Error: Entry {r.Id} was found more than once."); }
                        });
                    }
                }
            }
        }

        public IStringLocalizer Create(Type resourceSource)
        {
            return new StringLocalizer(cultures);
        }

        public IStringLocalizer Create(string baseName, string location)
        {
            return new StringLocalizer(cultures);
        }

        public List<CultureInfo> GetCultures()
        {
            return cultures.Select(c => new CultureInfo(c.Name)).ToList();
        }
    }
}
