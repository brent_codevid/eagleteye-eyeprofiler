﻿using System.Collections.Generic;
using System.Globalization;

namespace Orbb.Common.Localization
{
    public interface IAvailableCultures
    {
        List<CultureInfo> GetCultures();
    }
}
