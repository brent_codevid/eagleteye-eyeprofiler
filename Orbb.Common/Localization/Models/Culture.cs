﻿using System.Collections.Generic;

namespace Orbb.Common.Localization.Models
{
    public class Culture
    {
        public string Name { get; set; } // eg. 'en-US'
        public bool Default { get; set; }
        public Dictionary<string, string> Resources;
    }
}
